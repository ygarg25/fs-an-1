let cameradata = [
  {
    id: "C1",
    category: "Cameras",
    brand: "Nikon",
    name:
      "Nikon D3500 DSLR Camera AF-P DX NIKKOR 18-55mm (With Starboy Headphone) DSLR Camera AF-P DX NIKKOR 18-55mm f/3.5-5.6G VR  (Black)",
    img: "https://i.ibb.co/3v45bhw/n3.jpg",
    rating: "3.5",
    ratingDesc: "10,108 Ratings & 1,649 Reviews",
    details: [
      "Effective Pixels: 20 MP",
      "Sensor Type CMOS ",
      "1080p recording at 60p",
      "2 Year Warrenty"
    ],
    price: 29900,
    prevPrice: 36250,
    discount: 17,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 20,
    popularity: 7360
  },
  {
    id: "C2",
    category: "Cameras",
    brand: "Nikon",
    name:
      "Nikon D5600 DSLR Camera Body with Dual Lens: AF-P DX Nikkor 18 - 55 MM F/3.5-5.6G VR and 70-300 MM F/4.5-6.3G ED VR (16 GB SD Card)  (Black)",
    img: "https://i.ibb.co/GdwvjYw/2.jpg",
    rating: "4.6",
    ratingDesc: "7,108 Ratings & 1,249 Reviews",
    details: [
      "Effective Pixels: 24.2 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "2 Year Warrenty"
    ],
    price: 49990,
    prevPrice: 66450,
    discount: 24,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 24.2,
    popularity: 5365
  },
  {
    id: "C3",
    category: "Cameras",
    brand: "Nikon",
    name:
      "Nikon D3500 DSLR Camera Body with Dual lens: 18-55 mm f/3.5-5.6 G VR and AF-P DX Nikkor 70-300 mm f/4.5-6.3G ED VR  (Black)",
    img: "https://i.ibb.co/p2vcrtj/3.jpg",
    rating: "3.2",
    ratingDesc: "10,108 Ratings & 1,649 Reviews",
    details: [
      "Effective Pixels: 22.2 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "2 Year Warrenty"
    ],
    price: 38990,
    prevPrice: 49950,
    discount: 21,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 22.2,
    popularity: 23767
  },
  {
    id: "C4",
    category: "Cameras",
    brand: "Nikon",
    name: "Nikon Z 6 Mirrorless Camera Body Only  (Black)",
    img: "https://i.ibb.co/4j4H0yd/4.jpg",
    rating: "4.8",
    ratingDesc: "108 Ratings & 1 Reviews",
    details: [
      "Effective Pixels: 36 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "Full HD",
      "2 Year Warrenty"
    ],
    price: 12499,
    prevPrice: 156450,
    discount: 20,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 36,
    popularity: 2
  },
  {
    id: "C5",
    category: "Cameras",
    brand: "Nikon",
    name:
      "Nikon D5600 DSLR Camera Body with Single Lens: AF-S DX Nikkor 18 - 140 MM F/3.5-5.6G ED VR (16 GB SD Card)  (Black)",
    img: "https://i.ibb.co/zQ9t7Mq/5.jpg",
    rating: "2.7",
    ratingDesc: "7,159 Ratings & 1,269 Reviews",
    details: [
      "Effective Pixels: 28 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "2 Year Warrenty"
    ],
    price: 51299,
    prevPrice: 61250,
    discount: 16,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 28,
    popularity: 2545
  },
  {
    id: "C6",
    category: "Cameras",
    brand: "Nikon",
    name: "Nikon D 500 DSLR Camera AF-S DX 16 - 80 f/2.8 - 4E ED VR  (Black)",
    img: "https://i.ibb.co/f8Mhd8F/6.jpg",
    rating: "4.9",
    ratingDesc: "59 Ratings & 19 Reviews",
    details: [
      "Effective Pixels: 36.3 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "2 Year Warrenty"
    ],
    price: 153999,
    prevPrice: 86450,
    discount: 17,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 36.3,
    popularity: 4549
  },
  {
    id: "C7",
    category: "Cameras",
    brand: "Nikon",
    name:
      "Nikon D3500 DSLR Camera Body with Dual lens: 18-55 mm f/3.5-5.6 G VR and AF-P DX Nikkor 70-300 mm f/4.5-6.3G ED VR - (With Starboy Headphone) DSLR Camera Body with Dual lens: 18-55 mm f/3.5-5.6 G VR and AF-P DX Nikkor 70-300 mm f/4.5-6.3G ED VR  (Black)",
    img: "https://i.ibb.co/nRHyykk/7.jpg",
    rating: "4.5",
    ratingDesc: "9996 Ratings & 1934 Reviews",
    details: [
      "Effective Pixels: 24.2 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "2 Year Warrenty"
    ],
    price: 2546,
    prevPrice: 2700,
    discount: 1,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 24.2,
    popularity: 9876
  },
  {
    id: "C8",
    category: "Cameras",
    brand: "Nikon",
    name:
      "Nikon D5600 (With Basic Accessory Kit) DSLR Camera Body with Single Lens: AF-P DX Nikkor 18-55 MM F/3.5-5.6G VR (16 GB SD Card)  (Black)",
    img: "https://i.ibb.co/CQq8ZF0/8.jpg",
    rating: "4.6",
    ratingDesc: "7596 Ratings & 1534 Reviews",
    details: [
      "Effective Pixels: 24.2 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "WiFi Available",
      "2 Year Warrenty"
    ],
    price: 40990,
    prevPrice: 47403,
    discount: 13,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 24.2,
    popularity: 98768
  },
  {
    id: "C9",
    category: "Cameras",
    brand: "Sony",
    name: "Sony ILCA-68K Mirrorless Camera with 18-55 mm Lens  (Black)",
    img: "https://i.ibb.co/sHGx3dm/9.jpg",
    rating: "3.9",
    ratingDesc: "96 Ratings & 34 Reviews",
    details: [
      "Effective Pixels: 24.2 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "WiFi Available",
      "2 Year Warrenty"
    ],
    price: 49999,
    prevPrice: 54290,
    discount: 7,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 24.2,
    popularity: 968
  },
  {
    id: "C10",
    category: "Cameras",
    brand: "Sony",
    name: "Sony Alpha.ILCE5100 APSC DSLR Camera  (Black)",
    img: "https://i.ibb.co/ZhLhj8M/10.jpg",
    rating: "1.2",
    ratingDesc: "9 Ratings & 3 Reviews",
    details: [
      "Effective Pixels: 18 MP",
      "Sensor Type CMOS ",
      "2 Year Warrenty"
    ],
    price: 49989,
    prevPrice: 54290,
    discount: 7,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 18,
    popularity: 968
  },
  {
    id: "C11",
    category: "Cameras",
    brand: "Sony",
    name:
      "Sony ILCE-6000L/B IN5 Mirrorless Camera Body with Single Lens: 16-50mm",
    img: "https://i.ibb.co/2SVHRV4/11.jpg",
    rating: "4.4",
    ratingDesc: "576 Ratings & 121 Reviews",
    details: [
      "Effective Pixels: 24.2 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "2 Year Warrenty"
    ],
    price: 29990,
    prevPrice: 31990,
    discount: 4,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 24.2,
    popularity: 6344
  },
  {
    id: "C12",
    category: "Cameras",
    brand: "Sony",
    name:
      "Sony Alpha ILCE-6400L Mirrorless Camera Vlogger Starter kit  (Black)",
    img: "https://i.ibb.co/60WrF3R/12.jpg",
    rating: "2.4",
    ratingDesc: "56 Ratings & 21 Reviews",
    details: [
      "Effective Pixels: 36 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "NTSC/PAL",
      "2 Year Warrenty"
    ],
    price: 99999,
    prevPrice: 103990,
    discount: 3,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 36,
    popularity: 643444
  },
  {
    id: "C13",
    category: "Cameras",
    brand: "Sony",
    name:
      "Sony Alpha ILCE-6400L Mirrorless Camera Vlogger Starter kit  (Black)",
    img: "https://i.ibb.co/QYZLXwZ/13.jpg",
    rating: "4.8",
    ratingDesc: "42 Ratings & 8 Reviews",
    details: [
      "Effective Pixels: 22 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "WiFi Available",
      "XAVC S AVCHD",
      "2 Year Warrenty"
    ],
    price: 150990,
    prevPrice: 171990,
    discount: 12,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 22,
    popularity: 33323
  },
  {
    id: "C14",
    category: "Cameras",
    brand: "Sony",
    name:
      "Sony Alpha Full Frame ILCE-7M2K/BQ IN5 Mirrorless Camera Body with 28 - 70 mm Lens  (Black))",
    img: "https://i.ibb.co/1R8CW7f/14.jpg",
    rating: "4.5",
    ratingDesc: "82 Ratings & 16 Reviews",
    details: [
      "Effective Pixels: 24.3 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "2 Year Warrenty"
    ],
    price: 85999,
    prevPrice: 128990,
    discount: 33,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 24.3,
    popularity: 64765
  },
  {
    id: "C15",
    category: "Cameras",
    brand: "Sony",
    name:
      "Sony ILCA-77M2Q Mirrorless Camera Body + 16 - 50 mm Zoom Lens  (Black)",
    img: "https://i.ibb.co/mvwzTxw/15.jpg",
    rating: "3.9",
    ratingDesc: "61 Ratings & 26 Reviews",
    details: [
      "Effective Pixels: 12.4 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "WiFi Available",
      "2 Year Warrenty"
    ],
    price: 113000,
    prevPrice: 128990,
    discount: 12,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 12.4,
    popularity: 64
  },
  {
    id: "C16",
    category: "Cameras",
    brand: "Sony",
    name: "Sony Alpha ILCE-6500/BQ IN5 Mirrorless Camera Body Only  (Black)",
    img: "https://i.ibb.co/2gpNydB/16.jpg",
    rating: "4.9",
    ratingDesc: "23 Ratings & 6 Reviews",
    details: [
      "Effective Pixels: 42.2 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "WiFi Available",
      "2 Year Warrenty"
    ],
    price: 76990,
    prevPrice: 87490,
    discount: 12,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 42.2,
    popularity: 5454
  },
  {
    id: "C17",
    category: "Cameras",
    brand: "Canon",
    name:
      "Canon 3000D (With Basic Accessory Kit) DSLR Camera With 18-55 lens  (Black)",
    img: "https://i.ibb.co/3rmnQg4/17.jpg",
    rating: "4.4",
    ratingDesc: "12704 Ratings & 2109 Reviews",
    details: [
      "Effective Pixels: 20.5 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "WiFi Available",
      "2 Year Warrenty"
    ],
    price: 24990,
    prevPrice: 33694,
    discount: 25,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 20.5,
    popularity: 34238
  },
  {
    id: "C18",
    category: "Cameras",
    brand: "Canon",
    name:
      "Canon M50 Mirrorless Camera Body with Single Lens EF-M 15-45 mm IS STM  (Black)",
    img: "https://i.ibb.co/p4HmZjT/18.jpg",
    rating: "4.6",
    ratingDesc: "513 Ratings & 74 Reviews",
    details: [
      "Effective Pixels: 23.1 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "WiFi Available",
      "1 Year Warrenty"
    ],
    price: 46990,
    prevPrice: 51995,
    discount: 9,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 23.1,
    popularity: 1275
  },
  {
    id: "C19",
    category: "Cameras",
    brand: "Canon",
    name:
      "Canon EOS 3000D DSLR Camera Single Kit with 18-55 lens (16 GB Memory Card & Carry Case)  (Black)",
    img: "https://i.ibb.co/8rK931z/19.jpg",
    rating: "4.4",
    ratingDesc: "12704 Ratings & 2109 Reviews",
    details: [
      "Effective Pixels: 18 MP",
      "Sensor Type CMOS ",
      "1080p recording at 30p",
      "WiFi Available",
      "1 Year Warrenty"
    ],
    price: 22990,
    prevPrice: 29495,
    discount: 22,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 18,
    popularity: 23828
  },
  {
    id: "C20",
    category: "Cameras",
    brand: "Canon",
    name:
      "Canon EOS 77D DSLR Camera Body with Single Lens: EF-S18-135 IS USM (16 GB SD Card + Camera Bag)  (Black)",
    img: "https://i.ibb.co/c2qt5H9/20.jpg",
    rating: "4.6",
    ratingDesc: "581 Ratings & 104 Reviews",
    details: [
      "Effective Pixels: 26 MP",
      "Sensor Type CMOS ",
      "1080p recording at 60p",
      "WiFi Available",
      "1 Year Warrenty"
    ],
    price: 72990,
    prevPrice: 86495,
    discount: 15,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 26,
    popularity: 34375
  },
  {
    id: "C21",
    category: "Cameras",
    brand: "Canon",
    name:
      "Canon EOS 90D DSLR Camera Body with Single Lens 18 - 55 mm IS STM  (Black)",
    img: "https://i.ibb.co/cx2cfpj/21.jpg",
    rating: "4.6",
    ratingDesc: "11 Ratings & 3 Reviews",
    details: [
      "Effective Pixels: 32.5 MP",
      "Sensor Type CMOS ",
      "1080p recording at 60p",
      "WiFi Available",
      "1 Year Warrenty"
    ],
    price: 90999,
    prevPrice: 103495,
    discount: 12,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 32.5,
    popularity: 4648
  },
  {
    id: "C22",
    category: "Cameras",
    brand: "Canon",
    name:
      "Canon RP Mirrorless Camera Body with single Lens: RF 24 - 105 mm f/4L IS USM  (Black)",
    img: "https://i.ibb.co/DLHN4wY/22.jpg",
    rating: "3.4",
    ratingDesc: "5 Ratings & 1 Reviews",
    details: [
      "Effective Pixels: 28 MP",
      "Sensor Type CMOS ",
      "1080p recording at 60p",
      "WiFi Available",
      "2 Year Warrenty"
    ],
    price: 184999,
    prevPrice: 199490,
    discount: 7,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 28,
    popularity: 4648
  },
  {
    id: "C23",
    category: "Cameras",
    brand: "Canon",
    name:
      "Canon EOS 800D DSLR Camera Body with Single Lens: EF S18-55 IS STM (16 GB SD Card + Camera Bag)  (Black)",
    img: "https://i.ibb.co/dcFSLTx/23.jpg",
    rating: "3.4",
    ratingDesc: "5 Ratings & 1 Reviews",
    details: [
      "Effective Pixels: 22 MP",
      "Sensor Type CMOS ",
      "1080p recording at 60p",
      "WiFi Available",
      "2 Year Warrenty"
    ],
    price: 65995,
    prevPrice: 70000,
    discount: 8,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 22,
    popularity: 6424
  },
  {
    id: "C24",
    category: "Cameras",
    brand: "Canon",
    name:
      "Canon EOS 5D Mark IV DSLR Camera Body with EF-24-70mm f/4L IS USM  (Black)",
    img: "https://i.ibb.co/0DydVFX/24.jpg",
    rating: "3.6",
    ratingDesc: "27 Ratings & 8 Reviews",
    details: [
      "Effective Pixels: 32 MP",
      "Sensor Type CMOS ",
      "1080p recording at 60p",
      "WiFi Available",
      "2 Year Warrenty"
    ],
    price: 239999,
    prevPrice: 243521,
    discount: 1,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    megapixel: 32,
    popularity: 45368
  }
];

module.exports.cameradata = cameradata;
