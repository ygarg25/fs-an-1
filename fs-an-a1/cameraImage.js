let cameraImg = [
  {
    brand: "Nikon",
    imgList: [
      "https://i.ibb.co/sqPXGdQ/n2.jpg",
      "https://i.ibb.co/3v45bhw/n3.jpg",
      "https://i.ibb.co/kqwJR5g/n4.jpg",
      "https://i.ibb.co/31strDS/n5.jpg"
    ],
    brandImg: "https://i.ibb.co/QdPDDyV/n1.jpg"
  },
  {
    brand: "Canon",
    imgList: [
      "https://i.ibb.co/R36cKbC/c2.jpg",
      "https://i.ibb.co/vcpwxyS/c3.jpg",
      "https://i.ibb.co/K0Gq0yp/c4.jpg"
    ],
    brandImg: "https://i.ibb.co/QJSZSsY/c1.png"
  },
  {
    brand: "Sony",
    imgList: [
      "https://i.ibb.co/QcdKhgF/s2.jpg",
      "https://i.ibb.co/RNjtYv7/s3.jpg",
      "https://i.ibb.co/hRrhfx6/s4.jpg"
    ],
    brandImg: "https://i.ibb.co/Sn9J4TZ/s1.png"
  }
];

module.exports.cameraImg = cameraImg;
