const functions = require("firebase-functions");
var express = require("express");
var app = express();
app.use(express.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin,X-Requested-With,Content-Type,Accept"
  );
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
  next();
});

const port = 2410;
exports.app = functions.https.onRequest(app);

let laptopData = require("./laptopData.js").laptopData;
let lappyImg = require("./laptopImageData.js").lappyImg;
let cameraData = require("./cameraData.js").cameradata;
let cameraImg = require("./cameraImage.js").cameraImg;
//console.log(laptopData, lappyImg);

app.get("/products/Laptops/:brand", function(req, res) {
  let brand = req.params.brand;
  let page = req.query.page;
  let assured = req.query.assured;
  let sort = req.query.sort;
  let ram = req.query.ram;
  let rating = req.query.rating;
  let price = req.query.price;

  page = page ? page : 1;

  console.log(
    "in get request for all laptops ",
    brand,
    page,
    assured,
    sort,
    ram,
    rating,
    price
  );

  let brandData = laptopData.filter(d => d.brand === brand);

  let outarr = brandData;

  if (assured) {
    outarr = outarr.filter(d => d.assured);
  }

  if (sort === "popularity")
    outarr = outarr.sort((a, b) => b.popularity - a.popularity);

  if (sort === "asc") outarr = outarr.sort((a, b) => a.price - b.price);
  if (sort === "desc") outarr = outarr.sort((a, b) => b.price - a.price);

  if (ram) {
    let RAMArray = ram.split(",");
    outarr = outarr.filter(d => RAMArray.find(c => +c === +d.ram));
  }
  if (rating) {
    let RatingArray = rating.split(",");
    outarr = outarr.filter(d => RatingArray.find(c => +c <= +d.rating));
  }
  if (price) {
    let PriceArray = price.split(",");
    let outarrfinal = [];
    for (let i = 0; i < PriceArray.length; i++) {
      if (PriceArray[i] === "0-30000") {
        outarrfinal = outarrfinal.concat(outarr.filter(p => +p.price <= 30000));
      }
      if (PriceArray[i] === "30000-50000") {
        outarrfinal = outarrfinal.concat(
          outarr.filter(p => +p.price > 30000 && +p.price <= 50000)
        );
      }
      if (PriceArray[i] === "50000-100000") {
        outarrfinal = outarrfinal.concat(
          outarr.filter(p => +p.price > 50000 && +p.price <= 100000)
        );
      }
      if (PriceArray[i] === ">100000") {
        outarrfinal = outarrfinal.concat(outarr.filter(p => +p.price > 100000));
      }
    }
    outarr = outarrfinal;
  }
  let pageNumber = +page;
  let pageSize = 5;
  let startIndex = (pageNumber - 1) * pageSize;

  let TempArr = [...outarr];
  let mobilePage = TempArr.splice(startIndex, pageSize);

  let pageInfo = {
    pageNumber: pageNumber,
    numberOfPages: Math.ceil(outarr.length / pageSize),
    numOfItems: pageSize,
    totalItemCount: outarr.length
  };
  let obj = {};
  obj.data = mobilePage;
  obj.pageInfo = pageInfo;
  //console.log("obj", obj);
  res.send(obj);
});

app.get("/products/Laptops", function(req, res) {
  let page = req.query.page;
  let assured = req.query.assured;
  let sort = req.query.sort;
  let ram = req.query.ram;
  let rating = req.query.rating;
  let price = req.query.price;

  page = page ? page : 1;

  console.log(
    "in get request for all laptops ",
    page,
    assured,
    sort,
    ram,
    rating,
    price
  );

  let outarr = laptopData;

  if (assured) {
    outarr = outarr.filter(d => d.assured);
  }

  if (sort === "popularity")
    outarr = outarr.sort((a, b) => b.popularity - a.popularity);

  if (sort === "asc") outarr = outarr.sort((a, b) => a.price - b.price);
  if (sort === "desc") outarr = outarr.sort((a, b) => b.price - a.price);

  if (ram) {
    let RAMArray = ram.split(",");
    outarr = outarr.filter(d => RAMArray.find(c => +c === +d.ram));
  }
  if (rating) {
    let RatingArray = rating.split(",");
    outarr = outarr.filter(d => RatingArray.find(c => +c <= +d.rating));
  }
  if (price) {
    let PriceArray = price.split(",");
    let outarrfinal = [];
    for (let i = 0; i < PriceArray.length; i++) {
      if (PriceArray[i] === "0-30000") {
        outarrfinal = outarrfinal.concat(outarr.filter(p => +p.price <= 30000));
      }
      if (PriceArray[i] === "30000-50000") {
        outarrfinal = outarrfinal.concat(
          outarr.filter(p => +p.price > 30000 && +p.price <= 50000)
        );
      }
      if (PriceArray[i] === "50000-100000") {
        outarrfinal = outarrfinal.concat(
          outarr.filter(p => +p.price > 50000 && +p.price <= 100000)
        );
      }
      if (PriceArray[i] === ">100000") {
        outarrfinal = outarrfinal.concat(outarr.filter(p => +p.price > 100000));
      }
    }
    outarr = outarrfinal;
  }
  let pageNumber = +page;
  let pageSize = 5;
  let startIndex = (pageNumber - 1) * pageSize;

  let TempArr = [...outarr];
  let mobilePage = TempArr.splice(startIndex, pageSize);

  let pageInfo = {
    pageNumber: pageNumber,
    numberOfPages: Math.ceil(outarr.length / pageSize),
    numOfItems: pageSize,
    totalItemCount: outarr.length
  };
  let obj = {};
  obj.data = mobilePage;
  obj.pageInfo = pageInfo;
  //console.log("obj", obj);
  res.send(obj);
});

app.get("/product/Laptop/:id", function(req, res) {
  let id = req.params.id;
  console.log("in get request for specific laptop ", id);

  let prod = laptopData.find(l => l.id === id);
  let pics = lappyImg.find(l => l.brand === prod.brand);

  let obj = {};
  obj.prod = prod;
  obj.pics = pics;
  console.log("obj", obj);
  res.send(obj);
});

app.get("/products/Cameras/:brand", function(req, res) {
  let brand = req.params.brand;
  let page = req.query.page;
  let assured = req.query.assured;
  let sort = req.query.sort;
  let mp = req.query.mp;
  let rating = req.query.rating;
  let price = req.query.price;

  page = page ? page : 1;

  console.log(
    "in get request for all Cameras ",
    brand,
    page,
    assured,
    sort,
    mp,
    rating,
    price
  );

  let brandData = cameraData.filter(d => d.brand === brand);

  let outarr = brandData;

  if (assured) {
    outarr = outarr.filter(d => d.assured);
  }

  if (sort === "popularity")
    outarr = outarr.sort((a, b) => b.popularity - a.popularity);

  if (sort === "asc") outarr = outarr.sort((a, b) => a.price - b.price);
  if (sort === "desc") outarr = outarr.sort((a, b) => b.price - a.price);

  if (mp) {
    let MPArray = mp.split(",");
    outarr = outarr.filter(d => +d.megapixel >= 24);
  }
  if (rating) {
    let RatingArray = rating.split(",");
    outarr = outarr.filter(d => RatingArray.find(c => +c <= +d.rating));
  }
  if (price) {
    let PriceArray = price.split(",");
    let outarrfinal = [];
    for (let i = 0; i < PriceArray.length; i++) {
      if (PriceArray[i] === "0-30000") {
        outarrfinal = outarrfinal.concat(outarr.filter(p => +p.price <= 30000));
      }
      if (PriceArray[i] === "30000-50000") {
        outarrfinal = outarrfinal.concat(
          outarr.filter(p => +p.price > 30000 && +p.price <= 50000)
        );
      }
      if (PriceArray[i] === "50000-100000") {
        outarrfinal = outarrfinal.concat(
          outarr.filter(p => +p.price > 50000 && +p.price <= 100000)
        );
      }
      if (PriceArray[i] === ">100000") {
        outarrfinal = outarrfinal.concat(outarr.filter(p => +p.price > 100000));
      }
    }
    outarr = outarrfinal;
  }
  let pageNumber = +page;
  let pageSize = 5;
  let startIndex = (pageNumber - 1) * pageSize;

  let TempArr = [...outarr];
  let mobilePage = TempArr.splice(startIndex, pageSize);

  let pageInfo = {
    pageNumber: pageNumber,
    numberOfPages: Math.ceil(outarr.length / pageSize),
    numOfItems: pageSize,
    totalItemCount: outarr.length
  };
  let obj = {};
  obj.data = mobilePage;
  obj.pageInfo = pageInfo;
  //console.log("obj", obj);
  res.send(obj);
});

app.get("/products/Cameras", function(req, res) {
  let page = req.query.page;
  let assured = req.query.assured;
  let sort = req.query.sort;
  let mp = req.query.mp;
  let rating = req.query.rating;
  let price = req.query.price;

  page = page ? page : 1;

  console.log(
    "in get request for all Cameras ",
    page,
    assured,
    sort,
    mp,
    rating,
    price
  );

  let outarr = cameraData;

  if (assured) {
    outarr = outarr.filter(d => d.assured);
  }

  if (sort === "popularity")
    outarr = outarr.sort((a, b) => b.popularity - a.popularity);

  if (sort === "asc") outarr = outarr.sort((a, b) => a.price - b.price);
  if (sort === "desc") outarr = outarr.sort((a, b) => b.price - a.price);

  if (mp) {
    let MPArray = mp.split(",");
    outarr = outarr.filter(d => +d.megapixel >= 24);
  }
  if (rating) {
    let RatingArray = rating.split(",");
    outarr = outarr.filter(d => RatingArray.find(c => +c <= +d.rating));
  }
  if (price) {
    let PriceArray = price.split(",");
    let outarrfinal = [];
    for (let i = 0; i < PriceArray.length; i++) {
      if (PriceArray[i] === "0-30000") {
        outarrfinal = outarrfinal.concat(outarr.filter(p => +p.price <= 30000));
      }
      if (PriceArray[i] === "30000-50000") {
        outarrfinal = outarrfinal.concat(
          outarr.filter(p => +p.price > 30000 && +p.price <= 50000)
        );
      }
      if (PriceArray[i] === "50000-100000") {
        outarrfinal = outarrfinal.concat(
          outarr.filter(p => +p.price > 50000 && +p.price <= 100000)
        );
      }
      if (PriceArray[i] === ">100000") {
        outarrfinal = outarrfinal.concat(outarr.filter(p => +p.price > 100000));
      }
    }
    outarr = outarrfinal;
  }
  let pageNumber = +page;
  let pageSize = 5;
  let startIndex = (pageNumber - 1) * pageSize;

  let TempArr = [...outarr];
  let mobilePage = TempArr.splice(startIndex, pageSize);

  let pageInfo = {
    pageNumber: pageNumber,
    numberOfPages: Math.ceil(outarr.length / pageSize),
    numOfItems: pageSize,
    totalItemCount: outarr.length
  };
  let obj = {};
  obj.data = mobilePage;
  obj.pageInfo = pageInfo;
  //console.log("obj", obj);
  res.send(obj);
});

app.get("/product/Camera/:id", function(req, res) {
  let id = req.params.id;
  console.log("in get request for specific Camera ", id);

  let prod = cameraData.find(l => l.id === id);
  let pics = cameraImg.find(l => l.brand === prod.brand);

  let obj = {};
  obj.prod = prod;
  obj.pics = pics;
  console.log("obj", obj);
  res.send(obj);
});
