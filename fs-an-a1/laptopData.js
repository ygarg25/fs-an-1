let laptopData = [
  {
    id: "L1",
    category: "Laptops",
    brand: "Apple",
    name:
      "Apple MacBook Air Core i5 8th Gen - (8 GB/256 GB SSD/Mac OS Mojave) A1932  (13.3 inch, Space Grey, 1.25 kg)",
    img: "https://i.ibb.co/3TqvvRb/apple2.jpg",
    rating: "4.7",
    ratingDesc: "191 Ratings & 20 Reviews",
    details: [
      "Intel Core i5 Processor (8th Gen)",
      "8 GB DDR3 RAM",
      "64 bit Mac Os Operating System",
      "128 GB SSD",
      "33.78 cm(13.3 inch)Dispaly",
      "1 Year Carry in Warrenty"
    ],
    price: 109900,
    prevPrice: 119900,
    discount: 8,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 7360
  },
  {
    id: "L2",
    category: "Laptops",
    brand: "Apple",
    name:
      "Apple MacBook Pro Core i5 8th Gen - (16 GB/256 GB SSD/Mac OS Mojave) MV962HN  (13.3 inch, Space Grey, 1.37 kg)",
    img: "https://i.ibb.co/cNdDqPf/1.jpg",
    rating: "4.5",
    ratingDesc: "125 Ratings & 30 Reviews",
    details: [
      "Intel Core i5 Processor (8th Gen)",
      "16 GB DDR3 RAM",
      "64 bit Mac Os Operating System",
      "256 GB SSD",
      "33.78 cm(13.3 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 144900,
    prevPrice: 159900,
    discount: 9,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 16,
    popularity: 5360
  },
  {
    id: "L3",
    category: "Laptops",
    brand: "Apple",
    name:
      "Apple MacBook Air Core i5 5th Gen - (4 GB/128 GB SSD/Mac OS Sierra) MQD32HN/A A1466  (13.3 inch, Silver, 1.35 kg)",
    img: "https://i.ibb.co/cNdDqPf/1.jpg",
    rating: "4.7",
    ratingDesc: "23,555 Ratings & 2664 Reviews",
    details: [
      "Intel Core i5 Processor (8th Gen)",
      "4 GB DDR3 RAM",
      "64 bit Mac Os Operating System",
      "128 GB SSD",
      "33.78 cm(13.3 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 64990,
    prevPrice: 84900,
    discount: 23,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 4,
    popularity: 1360
  },
  {
    id: "L4",
    category: "Laptops",
    brand: "Apple",
    name:
      "Apple MacBook Air Core i5 8th Gen - (8 GB/128 GB SSD/Mac OS Mojave) MVFM2HN/A  (13.3 inch, Gold, 1.25 kg)",
    img:
      "https://i.ibb.co/q1PLFBW/apple-na-thin-and-light-laptop-original-imafgwew9puxqp3k.jpg",
    rating: "4.7",
    ratingDesc: "444 Ratings & 59 Reviews",
    details: [
      "Intel Core i5 Processor (8th Gen)",
      "8 GB DDR3 RAM",
      "64 bit Mac Os Operating System",
      "128 GB SSD",
      "33.78 cm(13.3 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 94900,
    prevPrice: 99900,
    discount: 4,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 9360
  },
  {
    id: "L5",
    category: "Laptops",
    brand: "Apple",
    name:
      "Apple MacBook Pro Core i9 8th Gen - (32 GB/512 GB SSD/Mac OS Mojave/4 GB Graphics) MV932HN  (15.4 inch, Silver, 1.83 kg)",
    img:
      "https://i.ibb.co/W6P6msy/apple-na-thin-and-light-laptop-original-imafgwevgryzyn4h.jpg",
    rating: "4.8",
    ratingDesc: "6 Ratings & 2 Reviews",
    details: [
      "Intel Core i9 Processor (8th Gen)",
      "32 GB DDR3 RAM",
      "64 bit Mac Os Operating System",
      "512 GB SSD",
      "39.12 cm(15.4 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 224990,
    prevPrice: 239900,
    discount: 6,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 32,
    popularity: 10360
  },
  {
    id: "L6",
    category: "Laptops",
    brand: "Apple",
    name:
      "Apple MacBook Pro Core i5 8th Gen - (8 GB/256 GB SSD/Mac OS Mojave) MUHP2HN/A  (13.3 inch, Space Grey, 1.37 kg)",
    img: "https://i.ibb.co/cNdDqPf/1.jpg",
    rating: "4.9",
    ratingDesc: "12 Ratings & 0 Reviews",
    details: [
      "Intel Core i9 Processor (8th Gen)",
      "8 GB DDR3 RAM",
      "64 bit Mac Os Operating System",
      "256 GB SSD",
      "39.12 cm(15.4 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 136204,
    prevPrice: 139900,
    discount: 2,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 2360
  },
  {
    id: "L7",
    category: "Laptops",
    brand: "Dell",
    name:
      "Dell Inspiron 3000 APU Dual Core A6 - (4 GB/1 TB HDD/Windows 10 Home) 3595 Laptop  (15.6 inch, Silver, 2.2 kg)",
    img:
      "https://i.ibb.co/W3V6YgM/dell-na-laptop-original-imafzf89nfqngbzt.jpg",
    rating: "1.9",
    ratingDesc: "4 Ratings & 1 Reviews",
    details: [
      "AMD APU Dual Core A6 Processor",
      "4 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "1 TB HDD",
      "39.62 cm(15.6 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 21990,
    prevPrice: 24882,
    discount: 11,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 4,
    popularity: 1000
  },
  {
    id: "L8",
    category: "Laptops",
    brand: "Dell",
    name:
      "Dell 14 3000 Core i3 7th Gen - (4 GB/1 TB HDD/Linux) inspiron 3481 Laptop  (14 inch, Platinum Silver, 1.79 kg)",
    img:
      "https://i.ibb.co/W3V6YgM/dell-na-laptop-original-imafzf89nfqngbzt.jpg",
    rating: "4.1",
    ratingDesc: "3564 Ratings & 351 Reviews",
    details: [
      "AMD APU Dual Core A6 Processor",
      "4 GB DDR4 RAM",
      "Linux/Ubumtu Operating System",
      "1 TB HDD",
      "35.56 cm(14 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 25490,
    prevPrice: 29452,
    discount: 13,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 4,
    popularity: 4678
  },
  {
    id: "L9",
    category: "Laptops",
    brand: "Dell",
    name:
      "Dell Vostro 3000 Core i5 8th Gen - (8 GB/1 TB HDD/Windows 10 Home) 3480 Laptop  (14 inch, Black, 1.79 kg)",
    img: "https://i.ibb.co/bRWhLVh/dell2.jpg",
    rating: "4.1",
    ratingDesc: "357 Ratings & 37 Reviews",
    details: [
      "intel core i5 Processor(8th Gen)",
      "8 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "1 TB HDD",
      "35.56 cm(14 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 45174,
    prevPrice: 45174,
    discount: 0,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 8678
  },
  {
    id: "L10",
    category: "Laptops",
    brand: "Dell",
    name:
      "Dell XPS 15 Core i9 9th Gen - (32 GB/1 TB SSD/Windows 10 Home/4 GB Graphics) 7590 Laptop  (15.6 inch, Silver, 2 kg, With MS Office)",
    img: "https://i.ibb.co/CwY1zHn/dsdsad.jpg",
    rating: "4",
    ratingDesc: "0 Ratings & 0 Reviews",
    details: [
      "intel core i9 Processor(9th Gen)",
      "32 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "1 TB HDD",
      "39.62 cm(15.6 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 248990,
    prevPrice: 250222,
    discount: 1,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 32,
    popularity: 118678
  },
  {
    id: "L11",
    category: "Laptops",
    brand: "Dell",
    name:
      "Dell Inspiron 7000 Core i7 9th Gen - (16 GB/512 GB SSD/Windows 10 Home/6 GB Graphics/NVIDIA Geforce RTX 2060) INS 7590 Gaming Laptop  (15.6 inch, Abyss Grey, 2.5 kg, With MS Office)",
    img: "https://i.ibb.co/dM7r577/121.jpg",
    rating: "4",
    ratingDesc: "2 Ratings & 2 Reviews",
    details: [
      "intel core i7 Processor(9th Gen)",
      "16 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "512 GB HDD",
      "39.62 cm(15.6 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 139990,
    prevPrice: 151575,
    discount: 7,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 16,
    popularity: 11678
  },
  {
    id: "L12",
    category: "Laptops",
    brand: "Dell",
    name:
      "Dell G7 Series Core i7 8th Gen - (16 GB/1 TB HDD/128 GB SSD/Windows 10 Home/6 GB Graphics/NVIDIA Geforce GTX 1060) 7588 Gaming Laptop  (15.6 inch, Licorice Black, 2.63 kg, With MS Office)",
    img: "https://i.ibb.co/YZhkp31/3231.jpg",
    rating: "4.5",
    ratingDesc: "11 Ratings & 0 Reviews",
    details: [
      "intel core i7 Processor(9th Gen)",
      "16 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "512 GB HDD",
      "39.62 cm(15.6 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 129990,
    prevPrice: 131575,
    discount: 1,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 16,
    popularity: 11678
  },
  {
    id: "L13",
    category: "Laptops",
    brand: "Dell",
    name:
      "Dell Inspiron 5000 Core i5 8th Gen - (8 GB/1 TB HDD/512 GB SSD/Windows 10 Home/2 GB Graphics) 5584 Laptop  (15.6 inch, Silver, 1.95 kg, With MS Office)",
    img: "https://i.ibb.co/bvDhJP0/eqwqed.jpg",
    rating: "4.4",
    ratingDesc: "18 Ratings & 3 Reviews",
    details: [
      "intel core i5 Processor(8th Gen)",
      "8 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "1 TB HDD | 512 GB HDD",
      "39.62 cm(15.6 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 67990,
    prevPrice: 70990,
    discount: 4,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 9086
  },
  {
    id: "L14",
    category: "Laptops",
    brand: "HP",
    name:
      "HP 14q Core i3 7th Gen - (8 GB/256 GB SSD/Windows 10 Home) 14q-cs0023TU Thin and Light Laptop  (14 inch, Jet Black, 1.47 kg, With MS Office)",
    img:
      "https://i.ibb.co/xMfThR7/hp-na-thin-and-light-laptop-original-imafk9p4wpuxk3hy.jpg",
    rating: "4.3",
    ratingDesc: "1734 Ratings & 214 Reviews",
    details: [
      "intel core i5 Processor(8th Gen)",
      "8 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "256 GB SSD",
      "35.56 cm(14 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 32490,
    prevPrice: 34547,
    discount: 5,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 6777
  },
  {
    id: "L15",
    category: "Laptops",
    brand: "HP",
    name:
      "HP Pavilion 15-EC Ryzen 5 Quad Core - (8 GB/1 TB HDD/128 GB SSD/Windows 10 Home/3 GB Graphics/NVIDIA Geforce GTX 1050) 15-ec0062AX Gaming Laptop  (15.6 inch, Shadow Black, 2.19 kg)",
    img:
      "https://i.ibb.co/Nm4r4DM/hp-na-gaming-laptop-original-imafmcwzbga7kqaj.jpg",
    rating: "4.5",
    ratingDesc: "537 Ratings & 88 Reviews",
    details: [
      "AMD Ryzen 5 Quad Core Processor",
      "8 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "1 Tb HDD | 128 GB SSD",
      "39.62 cm(15.6 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 49990,
    prevPrice: 58240,
    discount: 14,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 3434
  },
  {
    id: "L16",
    category: "Laptops",
    brand: "HP",
    name:
      "HP 15q APU Dual Core A9 - (4 GB/1 TB HDD/Windows 10 Home) 15q-dy0007AU Laptop  (15.6 inch, Jet Black, 2.18 kg)",
    img: "https://i.ibb.co/pJPsvdb/hp-na-laptop-original-imafg9tsqupgg2qa.jpg",
    rating: "4.1",
    ratingDesc: "9539 Ratings & 1197 Reviews",
    details: [
      "AMD APU DualCore Processor",
      "4 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "1 Tb HDD ",
      "39.62 cm(15.6 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 22990,
    prevPrice: 27451,
    discount: 16,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 2432
  },
  {
    id: "L17",
    category: "Laptops",
    brand: "HP",
    name:
      "HP Pavilion Ryzen 7 Quad Core - (8 GB/1 TB HDD/128 GB SSD/Windows 10 Home/6 GB Graphics/NVIDIA Geforce GTX 1660 Ti) 15-ec0073AX Gaming Laptop  (15.6 inch, Shadow Black, 2.19 kg)",
    img:
      "https://i.ibb.co/Nm4r4DM/hp-na-gaming-laptop-original-imafmcwzbga7kqaj.jpg",
    rating: "4.5",
    ratingDesc: "9539 Ratings & 1197 Reviews",
    details: [
      "AMD Ryzen 7 Quad Core Processor",
      "8 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "1 Tb HDD |128 GB SSD",
      "39.62 cm(15.6 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 74990,
    prevPrice: 78365,
    discount: 4,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 5434
  },
  {
    id: "L18",
    category: "Laptops",
    brand: "HP",
    name:
      "HP Pavilion 14-ce Core i5 10th Gen - (8 GB/512 GB SSD/Windows 10 Home) 14-ce3006TU Thin and Light Laptop  (14 inch, Mineral Silver, 1.59 kg, With MS Office)",
    img: "https://i.ibb.co/1RXqHDc/hp-na-original-imafmratjt82ushj.jpg",
    rating: "4.6",
    ratingDesc: "25 Ratings & 6 Reviews",
    details: [
      "Intel Core i5 processor",
      "8 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "512 GB HDD",
      "35.56 cm(14 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 49671,
    prevPrice: 55814,
    discount: 11,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 3533
  },
  {
    id: "L19",
    category: "Laptops",
    brand: "HP",
    name:
      "HP Spectre x360 Core i5 8th Gen - (8 GB/256 GB SSD/Windows 10 Pro) 13-ap0121TU 2 in 1 Laptop  (13.3 inch, Poseidon Blue, 1.32 kg, With MS Office)",
    img:
      "https://i.ibb.co/z22GCr1/hp-na-2-in-1-laptop-original-imafdzjsg9c7csh5.jpg",
    rating: "4.6",
    ratingDesc: "25 Ratings & 6 Reviews",
    details: [
      "AMD Core i5 processor",
      "8 GB DDR4 RAM",
      "Windows 10 Pro",
      "1 TB HDD | 256 GB SSD",
      "39.62 cm(15.6 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 151133,
    prevPrice: 153300,
    discount: 1,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 7
  },
  {
    id: "L20",
    category: "Laptops",
    brand: "Acer",
    name:
      "Acer Swift 3 Core i5 8th Gen - (8 GB/512 GB SSD/Windows 10 Home/2 GB Graphics) SF314-55G Thin and Light Laptop  (14 inch, Sparkly Silver, 1.35 kg)",
    img:
      "https://i.ibb.co/LhHC2ST/acer-na-thin-and-light-laptop-original-imafhnvjheyngmvq.jpg",
    rating: "4.5",
    ratingDesc: "4865 Ratings & 799 Reviews",
    details: [
      "Intel Core i5 processor",
      "8 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "512 GB SSD",
      "35.56 cm(14 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 48990,
    prevPrice: 62589,
    discount: 21,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 78
  },
  {
    id: "L21",
    category: "Laptops",
    brand: "Acer",
    name:
      "Acer Predator Helios 300 Core i7 9th Gen - (16 GB/2 TB HDD/256 GB SSD/Windows 10 Home/6 GB Graphics/NVIDIA Geforce GTX 1660 Ti) ph317-53-77ux Gaming Laptop  (17.3 inch, Abyssal Black, 2.93 kg, With MS Office)",
    img:
      "https://i.ibb.co/VYDPLMs/acer-na-gaming-laptop-original-imafmy4wdhs4gnyg.jpg",
    rating: "4.8",
    ratingDesc: "6 Ratings & 1 Reviews",
    details: [
      "Intel Core i7 processor",
      "16 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "2 TB HDD | 256 GB SSD",
      "43.94 cm(17.3 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 124990,
    prevPrice: 128809,
    discount: 2,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 23678
  },
  {
    id: "L22",
    category: "Laptops",
    brand: "Acer",
    name:
      "Acer Aspire 3 Pentium Quad Core - (4 GB/500 GB HDD/Windows 10 Home) A315-34-P7EG Laptop  (15.6 inch, Charcoal Black, 1.9 kg)",
    img: "https://i.ibb.co/S7VKzNq/acer-3.jpg",
    rating: "4.1",
    ratingDesc: "1900 Ratings & 317 Reviews",
    details: [
      "Intel Pentium Quad Core processor",
      "4 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "500 GB HDD ",
      "39.62 cm(15.6 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 20990,
    prevPrice: 29999,
    discount: 30,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 6678
  },
  {
    id: "L23",
    category: "Laptops",
    brand: "Acer",
    name:
      "Acer Aspire 3 Ryzen 5 Quad Core - (4 GB/1 TB HDD/Windows 10 Home) A315-41 / A315-41G / A315-41-R45R Laptop  (15.6 inch, Obsidian Black, 2.2 kg)",
    img: "https://i.ibb.co/mXBTpB6/acer-4.jpg",
    rating: "3.8",
    ratingDesc: "461 Ratings & 113 Reviews",
    details: [
      "AMD Ryzen 5 Quad Core processor",
      "4 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "1 TB HDD ",
      "39.62 cm(15.6 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 28299,
    prevPrice: 42720,
    discount: 33,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 7878
  },
  {
    id: "L24",
    category: "Laptops",
    brand: "Acer",
    name:
      "Acer Aspire 5 Core i3 8th Gen - (4 GB/1 TB HDD/Windows 10 Home) A515-52 Thin and Light Laptop  (15.6 inch, Sparkly Silver, 1.8 kg)",
    img: "https://i.ibb.co/kSHMRnD/acer-5.jpg",
    rating: "3.8",
    ratingDesc: "55 Ratings & 10 Reviews",
    details: [
      "Intel Core i3 processor",
      "4 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "1 TB HDD ",
      "39.62 cm(15.6 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 33990,
    prevPrice: 39598,
    discount: 14,
    emi: "No Cost Emi",
    assured: true,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 6665
  },
  {
    id: "L25",
    category: "Laptops",
    brand: "Acer",
    name:
      "Acer Aspire 5 Core i5 7th Gen - (8 GB/1 TB HDD/Windows 10 Home/2 GB Graphics) A515-51G -5673 Laptop  (15.6 inch, Obsidian Black, 2 kg)",
    img:
      "https://i.ibb.co/rvCdKfN/acer-na-laptop-original-imaf8zjm5hqkamyp.jpg",
    rating: "4.1",
    ratingDesc: "2774 Ratings & 462 Reviews",
    details: [
      "Intel Core i5 processor",
      "8 GB DDR4 RAM",
      "64 bit Windows 10 Operating System",
      "1 TB HDD ",
      "39.62 cm(15.6 inch)Dispaly",
      "1 Year onsite Warrenty"
    ],
    price: 40990,
    prevPrice: 54074,
    discount: 24,
    emi: "No Cost Emi",
    assured: false,
    exchange: "Upto ₹10,800 Off on Exchange",
    ram: 8,
    popularity: 678
  }
];

module.exports.laptopData = laptopData;
