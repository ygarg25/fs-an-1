let lappyImg = [
  {
    brand: "Apple",
    imgList: [
      "https://i.ibb.co/7v54Cx9/apple1.jpg",
      "https://i.ibb.co/3TqvvRb/apple2.jpg",
      "https://i.ibb.co/k2x2X4W/apple3.jpg",
      "https://i.ibb.co/Dr4nL74/apple5.jpg"
    ],
    brandImg:
      "https://i.ibb.co/mqSbvwc/4bd73efd1ffb0f36c470652a52ff83651b696c11438e092d83ed373401a88f41.jpg"
  },
  {
    brand: "Dell",
    imgList: [
      "https://i.ibb.co/Hdqc901/dell1.jpg",
      "https://i.ibb.co/bRWhLVh/dell2.jpg",
      "https://i.ibb.co/PQZ5Bkv/dell3.jpg",
      "https://i.ibb.co/L5hd3h0/dell4.jpg"
    ],
    brandImg: "https://i.ibb.co/fG0xc9b/dellLogo.jpg"
  },
  {
    brand: "HP",
    imgList: [
      "https://i.ibb.co/KmcXhc7/hp1.jpg",
      "https://i.ibb.co/tsSj02N/hp2.jpg",
      "https://i.ibb.co/BVTjgWj/hp3.jpg",
      "https://i.ibb.co/FwpLdX3/hp4.jpg"
    ],
    brandImg: "https://i.ibb.co/4NchPG0/hplogo.jpg"
  },
  {
    brand: "Acer",
    imgList: [
      "https://i.ibb.co/XFvxQvX/acer1.jpg",
      "https://i.ibb.co/Dkw7cX6/acer2.jpg",
      "https://i.ibb.co/5c3X4Qb/acer3.jpg",
      "https://i.ibb.co/JzZs5RX/acer4.jpg"
    ],
    brandImg: "https://i.ibb.co/Bz5JB9t/acerlogo.png"
  }
];

module.exports.lappyImg = lappyImg;
