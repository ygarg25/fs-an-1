var express = require("express");

const fetch = require("node-fetch");
var app = express();
app.use(express.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin,X-Requested-With,Content-Type,Accept"
  );
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
  next();
});

const port = 2410;
app.listen(port, () => console.log("listening on port ", port));

let laptopData = require("./laptopData.js").laptopData;
let lappyImg = require("./laptopImageData.js").lappyImg;
let cameraData = require("./cameraData.js").cameradata;
let cameraImg = require("./cameraImage.js").cameraImg;
//console.log(laptopData, lappyImg);

app.get("/products/Laptops/:brand?", function (req, res) {
  let brand = req.params.brand;
  let q = req.query.q;
  let page = req.query.page;
  let assured = req.query.assured;
  let sort = req.query.sort;
  let ram = req.query.ram;
  let rating = req.query.rating;
  let price = req.query.price;

  page = page ? page : 1;

  console.log(
    "in get request for all laptops one function",
    brand,
    q,
    page,
    assured,
    sort,
    ram,
    rating,
    price
  );

  // let brandData = laptopData.filter((d) => d.brand === brand);
  // console.log("sdd", brandData);

  let outarr = laptopData;
  if (q) {
    outarr = outarr.filter((c) =>
      c.name.toUpperCase().includes(q.toUpperCase())
    );
  }

  if (brand) {
    outarr = outarr.filter((d) => d.brand === brand);
  }

  if (assured) {
    outarr = outarr.filter((d) => d.assured);
  }

  if (sort === "popularity")
    outarr = outarr.sort((a, b) => b.popularity - a.popularity);

  if (sort === "asc") outarr = outarr.sort((a, b) => a.price - b.price);
  if (sort === "desc") outarr = outarr.sort((a, b) => b.price - a.price);

  if (ram) {
    let RAMArray = ram.split(",");
    outarr = outarr.filter((d) => RAMArray.find((c) => +c === +d.ram));
  }
  if (rating) {
    let RatingArray = rating.split(",");
    outarr = outarr.filter((d) => RatingArray.find((c) => +c <= +d.rating));
  }
  if (price) {
    let PriceArray = price.split(",");

    console.log("price array", PriceArray);

    let outarrfinal = [];

    function filPrice(x) {
      x = x.split("-");
      if (x.length < 2) {
        outarrfinal = outarrfinal.concat(
          outarr.filter((p) => +p.price > 100000)
        );
      } else {
        outarrfinal = outarrfinal.concat(
          outarr.filter((p) => +p.price > x[0] && +p.price <= x[1])
        );
      }
    }
    PriceArray.map(filPrice);
    outarr = outarrfinal;
  }
  let pageNumber = +page;
  let pageSize = 5;
  let startIndex = (pageNumber - 1) * pageSize;

  let TempArr = [...outarr];
  let mobilePage = TempArr.splice(startIndex, pageSize);

  let pageInfo = {
    pageNumber: pageNumber,
    numberOfPages: Math.ceil(outarr.length / pageSize),
    numOfItems: pageSize,
    totalItemCount: outarr.length,
  };
  let obj = {};
  obj.data = mobilePage;
  obj.pageInfo = pageInfo;
  //console.log("obj", obj);
  res.send(obj);
});

app.get("/product/Laptop/:id", function (req, res) {
  let id = req.params.id;
  console.log("in get request for specific laptop ", id);

  let prod = laptopData.find((l) => l.id === id);
  let pics = lappyImg.find((l) => l.brand === prod.brand);

  let obj = {};
  obj.prod = prod;
  obj.pics = pics;
  console.log("obj", obj);
  res.send(obj);
});

app.get("/products/Cameras/:brand?", function (req, res) {
  let brand = req.params.brand;
  let q = req.query.q;
  let page = req.query.page;
  let assured = req.query.assured;
  let sort = req.query.sort;
  let mp = req.query.mp;
  let rating = req.query.rating;
  let price = req.query.price;

  page = page ? page : 1;

  console.log(
    "in get request for all Cameras one function ",
    brand,
    q,
    page,
    assured,
    sort,
    mp,
    rating,
    price
  );

  //let brandData = cameraData.filter((d) => d.brand === brand);

  let outarr = cameraData;

  if (q) {
    outarr = outarr.filter((c) =>
      c.name.toUpperCase().includes(q.toUpperCase())
    );
  }

  if (brand) {
    outarr = outarr.filter((d) => d.brand === brand);
  }

  if (assured) {
    outarr = outarr.filter((d) => d.assured);
  }

  if (sort === "popularity")
    outarr = outarr.sort((a, b) => b.popularity - a.popularity);

  if (sort === "asc") outarr = outarr.sort((a, b) => a.price - b.price);
  if (sort === "desc") outarr = outarr.sort((a, b) => b.price - a.price);

  if (mp) {
    let MPArray = mp.split(",");
    outarr = outarr.filter((d) => +d.megapixel >= 24);
  }
  if (rating) {
    let RatingArray = rating.split(",");
    outarr = outarr.filter((d) => RatingArray.find((c) => +c <= +d.rating));
  }
  if (price) {
    let PriceArray = price.split(",");

    console.log("price array", PriceArray);

    let outarrfinal = [];

    function filPrice(x) {
      x = x.split("-");
      if (x.length < 2) {
        outarrfinal = outarrfinal.concat(
          outarr.filter((p) => +p.price > 100000)
        );
      } else {
        outarrfinal = outarrfinal.concat(
          outarr.filter((p) => +p.price > x[0] && +p.price <= x[1])
        );
      }
    }
    PriceArray.map(filPrice);
    outarr = outarrfinal;
  }
  let pageNumber = +page;
  let pageSize = 5;
  let startIndex = (pageNumber - 1) * pageSize;

  let TempArr = [...outarr];
  let mobilePage = TempArr.splice(startIndex, pageSize);

  let pageInfo = {
    pageNumber: pageNumber,
    numberOfPages: Math.ceil(outarr.length / pageSize),
    numOfItems: pageSize,
    totalItemCount: outarr.length,
  };
  let obj = {};
  obj.data = mobilePage;
  obj.pageInfo = pageInfo;
  //console.log("obj", obj);
  res.send(obj);
});

app.get("/product/Camera/:id", function (req, res) {
  let id = req.params.id;
  console.log("in get request for specific Camera ", id);

  let prod = cameraData.find((l) => l.id === id);
  let pics = cameraImg.find((l) => l.brand === prod.brand);

  let obj = {};
  obj.prod = prod;
  obj.pics = pics;
  console.log("obj", obj);
  res.send(obj);
});

app.get("/search/:q", function (req, res) {
  let q = req.params.q;
  let mob;
  let lappy;
  let cam;
  console.log("search request", q);

  fetch(
    `https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/products/Mobiles?q=${q}&page=1`
  )
    .then((res) => res.json())
    .then((json) => {
      mob = json;
      console.log(json);
      res.send({ path: `/home/Mobiles?q=${q}` }).end();
    });

  console.log("mob", mob);

  lappy = laptopData.filter((c) =>
    c.name.toUpperCase().includes(q.toUpperCase())
  );
  cam = cameraData.filter((c) =>
    c.name.toUpperCase().includes(q.toUpperCase())
  );
  console.log("lappy", lappy);
  console.log("cam", cam);
  if (lappy !== undefined && lappy.length > 0) {
    res.send({ path: `/home/Laptops?q=${q}` }).end();
  } else if (cam !== undefined && cam.length > 0) {
    res.send({ path: `/home/Cameras?q=${q}` }).end();
  }
});
