import React, { Component } from "react";
import axios from "axios";
import queryString from "query-string";
import { Link } from "react-router-dom";
import "./link.css";
//import { url } from "./../config.json";

let url = "http://localhost:2410";
//let url = "https://us-central1-fpk01-48921.cloudfunctions.net/app";

let mpCheckBox = [];
let ratingCheckBox = [];
let priceCheckBox = [];

class AllCamera extends Component {
  state = {
    mpData: [{ name: "24 MP and Above", value: ">=24" }],
    ratingData: [
      { name: "4", value: "4" },
      { name: "3", value: "3" },
      { name: "2", value: "2" },
      { name: "1", value: "1" }
    ],
    priceData: [
      { name: "0-30,000", value: "0-30000" },
      { name: "30,000-50,000", value: "30000-50000" },
      { name: "50,000-1,00,000", value: "50000-100000" },
      { name: "More than 1,00,000", value: ">100000" }
    ],
    mpTrue: false,
    ratingTrue: false,
    priceTrue: false
  };

  // WARNING! To be deprecated in React v17. Use componentDidMount instead.
  async componentWillMount() {
    let { page, q, assured, sort, mp, rating, price } = queryString.parse(
      this.props.location.search
    );
    const brand = this.props.match.params.brand;
    let params = "";
    page = page ? page : 1;
    params = this.addToParams(params, "page", page);
    params = this.addToParams(params, "q", q);
    params = this.addToParams(params, "assured", assured);
    params = this.addToParams(params, "sort", sort);
    params = this.addToParams(params, "mp", mp);
    params = this.addToParams(params, "rating", rating);
    params = this.addToParams(params, "price", price);
    let apiEndPoint = url + "/" + "products" + "/Cameras";
    if (brand) apiEndPoint = apiEndPoint + "/" + brand;
    if (params) apiEndPoint += params;
    let { data: allCamera } = await axios.get(apiEndPoint);
    this.setState({ allCamera: allCamera });
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    let { page, q, assured, sort, mp, rating, price } = queryString.parse(
      this.props.location.search
    );
    const catg = this.props.match.params.catg;
    const brand = this.props.match.params.brand;
    let params = "";
    page = page ? page : 1;
    params = this.addToParams(params, "page", page);
    params = this.addToParams(params, "q", q);
    params = this.addToParams(params, "assured", assured);
    params = this.addToParams(params, "sort", sort);
    params = this.addToParams(params, "mp", mp);
    params = this.addToParams(params, "rating", rating);
    params = this.addToParams(params, "price", price);
    let apiEndPoint = url + "/" + "products" + "/Cameras";
    if (brand) apiEndPoint = apiEndPoint + "/" + brand;
    if (params) apiEndPoint += params;
    if (currProps !== prevProps) {
      let { data: allCamera } = await axios.get(apiEndPoint);
      this.setState({ allCamera: allCamera });
    }
  }
  
  makeCbStructure(Data, sel) {
    let temp = Data.map(n1 => ({
      name: n1.name,
      value: n1.value,
      isSelected: false
    }));
    let cbData = sel.split(",");
    for (let i = 0; i < cbData.length; i++) {
      let obj = temp.find(n1 => n1.value === cbData[i]);
      if (obj) obj.isSelected = true;
    }
    return temp;
  }

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  callUrl = (params, page, q, assured, sort, mp, rating, price) => {
    const brand = this.props.match.params.brand;
    let path = "/home/Cameras";
    if (brand) path = path + "/" + brand;
    params = this.addToParams(params, "page", page);
    params = this.addToParams(params, "q", q);
    params = this.addToParams(params, "assured", assured);
    params = this.addToParams(params, "sort", sort);
    params = this.addToParams(params, "mp", mp);
    params = this.addToParams(params, "rating", rating);
    params = this.addToParams(params, "price", price);
    console.log("params", params);
    this.props.history.push({ pathname: path, search: params });
  };

  gotoPage = x => {
    let { page, q, assured, sort, mp, rating, price } = queryString.parse(
      this.props.location.search
    );
    let currentPage = page ? +page : 1;
    currentPage = currentPage + x;
    let params = "";
    this.callUrl(params, currentPage, q, assured, sort, mp, rating, price);
  };

  handleAssured = () => {
    let { page, q, assured, sort, mp, rating, price } = queryString.parse(
      this.props.location.search
    );
    assured = !assured;
    page = 1;
    this.callUrl("", page, q, assured, sort, mp, rating, price);
  };

  handledropDown = key => {
    if (key === "mpTrue") {
      this.setState({ mpTrue: !this.state.mpTrue });
    }
    if (key === "ratingTrue") {
      this.setState({ ratingTrue: !this.state.ratingTrue });
    }
    if (key === "priceTrue") {
      this.setState({ priceTrue: !this.state.priceTrue });
    }
  };

  handleChange = e => {
    let { page, q, assured, sort, mp, rating, price } = queryString.parse(
      this.props.location.search
    );
    const { currentTarget: input } = e;
    if (input.type === "checkbox") {
      if (input.id === "mp") {
        let cb = mpCheckBox.find(n1 => n1.name === input.name);
        if (cb) cb.isSelected = input.checked;
        let filteredRAM = mpCheckBox.filter(n1 => n1.isSelected);
        let arrayRAM = filteredRAM.map(n1 => n1.value);
        let selRAM = arrayRAM.join(",");
        mp = selRAM;
      }
      if (input.id === "rating") {
        let cb = ratingCheckBox.find(n1 => n1.name === input.name);
        if (cb) cb.isSelected = input.checked;
        let filteredRating = ratingCheckBox.filter(n1 => n1.isSelected);
        let arrayRating = filteredRating.map(n1 => n1.value);
        let selRating = arrayRating.join(",");
        rating = selRating;
      }
      if (input.id === "price") {
        let cb = priceCheckBox.find(n1 => n1.name === input.name);
        if (cb) cb.isSelected = input.checked;
        let filteredPrice = priceCheckBox.filter(n1 => n1.isSelected);
        let arrayPrice = filteredPrice.map(n1 => n1.value);
        let selPrice = arrayPrice.join(",");
        price = selPrice;
      }
    }

    page = 1;
    this.callUrl("", page, q, assured, sort, mp, rating, price);
  };

  handleSort = s => {
    let { page, q, assured, sort, mp, rating, price } = queryString.parse(
      this.props.location.search
    );
    sort = s;
    page = 1;
    this.callUrl("", page, q, assured, sort, mp, rating, price);
  };

  handleDirectPage = p => {
    let { page, q, assured, sort, mp, rating, price } = queryString.parse(
      this.props.location.search
    );
    page = p;
    this.callUrl("", page, q, assured, sort, mp, rating, price);
  };

  handleLike = index => {
    const allCamera = { ...this.state.allCamera };
    allCamera.data[index].liked = !allCamera.data[index].liked;
    this.setState({ allCamera: allCamera });
  };

  render() {
    const {
      mpData,
      priceData,
      ratingData,
      mpTrue,
      ratingTrue,
      priceTrue
    } = this.state;
    let { page, q, assured, sort, mp, rating, price } = queryString.parse(
      this.props.location.search
    );
    const brand = this.props.match.params.brand;
    let TotalPage = [];
    if (this.state.allCamera) {
      for (let i = 1; i <= this.state.allCamera.pageInfo.numberOfPages; i++) {
        TotalPage.push(i);
      }
    }
    page = page ? page : 1;
    mp = mp ? mp : "";
    rating = rating ? rating : "";
    price = price ? price : "";
    mpCheckBox = this.makeCbStructure(mpData, mp);
    ratingCheckBox = this.makeCbStructure(ratingData, rating);
    priceCheckBox = this.makeCbStructure(priceData, price);
    // console.log("allCamera", brand, this.state.allCamera);
    return (
      <div style={{ backgroundColor: "lightgrey" }}>
        <div className="row mt-4 border ">
          <div className="col-2 ml-2 d-none d-lg-block">
            <div className="row bg-white border-bottom pt-2 pb-2">
              <div
                className="col"
                style={{
                  fontSize: "18px",
                  textTransform: "capitalize",
                  width: "67%",
                  fontWeight: "500"
                }}
              >
                Filters
              </div>
            </div>
            <div className=" row bg-white pt-2 pb-2">
              <div className="col-9">
                <div className="chekbox">
                  <label className="ml-4">
                    <input
                      value={assured === true}
                      type="checkbox"
                      className="form-check-input "
                      onChange={this.handleAssured}
                      checked={assured}
                    />
                    <img
                      src={"https://i.ibb.co/t8bPSBN/fa-8b4b59.png"}
                      style={{ width: "80px", height: "21px" }}
                    />
                  </label>
                </div>
              </div>
              <div className="col-3">
                <span>
                  <i className="fa fa-question-circle-o" aria-hidden="true" />
                </span>
              </div>
            </div>
            {mpTrue ? (
              <div className="row bg-white border-top">
                <div className="col-10">
                  <div
                    className="row ml-1 pb-2"
                    style={{
                      fontSize: "13px",
                      fontWeight: "500",
                      textTransform: "uppercase",
                      letterSpacing: ".3px",
                      display: "inline-block"
                    }}
                  >
                    MEGA PIXEL
                  </div>
                </div>
                <div className="col-2 text-right">
                  <span>
                    <i
                      className="fa fa-chevron-down"
                      onClick={() => this.handledropDown("mpTrue")}
                      style={{
                        fontSize: "10px",
                        color: "lightgrey",
                        cursor: "pointer"
                      }}
                    />
                  </span>
                </div>
              </div>
            ) : (
              <div className="row bg-white border-top">
                <div className="col-10">
                  <div
                    className="row ml-1 pb-2"
                    style={{
                      fontSize: "13px",
                      fontWeight: "500",
                      textTransform: "uppercase",
                      letterSpacing: ".3px",
                      display: "inline-block"
                    }}
                  >
                    MEGA PIXEL
                  </div>
                  <div>
                    {mpCheckBox.map((item, i) => (
                      <div className="row form-check ml-1 pb-1" key={i}>
                        <div
                          className="checkbox form-check"
                          style={{
                            verticalAlign: "middle",
                            fontSize: "14px",
                            paddingLeft: "11px",
                            color: "#212121",
                            display: "inline-block",
                            width: "calc(100% - 25px",
                            whiteSpace: "nowrap",
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            lineHeight: "1",
                            cursor: "pointer"
                          }}
                        >
                          <label htmlFor={item.name}>
                            <input
                              value={item.isSelected}
                              onChange={this.handleChange}
                              id="mp"
                              type="checkbox"
                              name={item.name}
                              checked={item.isSelected}
                              className=""
                            />
                            {item.name}
                          </label>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
                <div className="col-2 text-right">
                  <span>
                    <i
                      className="fa fa-chevron-up"
                      onClick={() => this.handledropDown("mpTrue")}
                      style={{
                        fontSize: "10px",
                        color: "lightgrey",
                        cursor: "pointer"
                      }}
                    />
                  </span>
                </div>
              </div>
            )}
            {ratingTrue ? (
              <div className="row bg-white border-top">
                <div className="col-10">
                  <div
                    className="row ml-1 pb-2"
                    style={{
                      fontSize: "13px",
                      fontWeight: "500",
                      textTransform: "uppercase",
                      letterSpacing: ".3px",
                      display: "inline-block"
                    }}
                  >
                    CUSTOMER RATING
                  </div>
                </div>
                <div className="col-2 text-right">
                  <span>
                    <i
                      className="fa fa-chevron-down"
                      onClick={() => this.handledropDown("ratingTrue")}
                      style={{
                        fontSize: "10px",
                        color: "lightgrey",
                        cursor: "pointer"
                      }}
                    />
                  </span>
                </div>
              </div>
            ) : (
              <div className="row bg-white border-top">
                <div className="col-10">
                  <div
                    className="row ml-1 pb-2"
                    style={{
                      fontSize: "13px",
                      fontWeight: "500",
                      textTransform: "uppercase",
                      letterSpacing: ".3px",
                      display: "inline-block"
                    }}
                  >
                    CUSTOMER RATING
                  </div>
                  <div>
                    {ratingCheckBox.map((item, i) => (
                      <div className="row form-check ml-1 pb-1" key={i}>
                        <div
                          className="checkbox form-check"
                          style={{
                            verticalAlign: "middle",
                            fontSize: "14px",
                            paddingLeft: "11px",
                            color: "#212121",
                            display: "inline-block",
                            width: "calc(100% - 25px",
                            whiteSpace: "nowrap",
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            lineHeight: "1",
                            cursor: "pointer"
                          }}
                        >
                          <label htmlFor={item.name}>
                            <input
                              value={item.isSelected}
                              onChange={this.handleChange}
                              id="rating"
                              type="checkbox"
                              name={item.name}
                              checked={item.isSelected}
                              className=""
                            />
                            {item.name}{" "}
                            <i className="fa fa-star" aria-hidden="true" />&
                            above
                          </label>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
                <div className="col-2 text-right">
                  <span>
                    <i
                      className="fa fa-chevron-up"
                      onClick={() => this.handledropDown("ratingTrue")}
                      style={{
                        fontSize: "10px",
                        color: "lightgrey",
                        cursor: "pointer"
                      }}
                    />
                  </span>
                </div>
              </div>
            )}
            {priceTrue ? (
              <div className="row bg-white border-top">
                <div className="col-10">
                  <div
                    className="row ml-1 pb-2"
                    style={{
                      fontSize: "13px",
                      fontWeight: "500",
                      textTransform: "uppercase",
                      letterSpacing: ".3px",
                      display: "inline-block"
                    }}
                  >
                    PRICE
                  </div>
                </div>
                <div className="col-2 text-right">
                  <span>
                    <i
                      className="fa fa-chevron-down"
                      onClick={() => this.handledropDown("priceTrue")}
                      style={{
                        fontSize: "10px",
                        color: "lightgrey",
                        cursor: "pointer"
                      }}
                    />
                  </span>
                </div>
              </div>
            ) : (
              <div className="row bg-white border-top">
                <div className="col-10">
                  <div
                    className="row ml-1 pb-2"
                    style={{
                      fontSize: "13px",
                      fontWeight: "500",
                      textTransform: "uppercase",
                      letterSpacing: ".3px",
                      display: "inline-block"
                    }}
                  >
                    PRICE
                  </div>
                  <div>
                    {priceCheckBox.map((item, i) => (
                      <div className="row form-check ml-1 pb-1" key={i}>
                        <div
                          className="checkbox form-check"
                          style={{
                            verticalAlign: "middle",
                            fontSize: "14px",
                            paddingLeft: "11px",
                            color: "#212121",
                            display: "inline-block",
                            width: "calc(100% - 25px",
                            whiteSpace: "nowrap",
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            lineHeight: "1",
                            cursor: "pointer"
                          }}
                        >
                          <label htmlFor={item.name}>
                            <input
                              value={item.isSelected}
                              onChange={this.handleChange}
                              id="price"
                              type="checkbox"
                              name={item.name}
                              checked={item.isSelected}
                              className=""
                            />
                            {item.name}
                          </label>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
                <div className="col-2 text-right">
                  <span>
                    <i
                      className="fa fa-chevron-up"
                      onClick={() => this.handledropDown("priceTrue")}
                      style={{
                        fontSize: "10px",
                        color: "lightgrey",
                        cursor: "pointer"
                      }}
                    />
                  </span>
                </div>
              </div>
            )}
          </div>
          <div className="col-lg-9 col-12 bg-white ml-1">
            <div className="row">
              <div className="col">
                <nav style={{ fontSize: "10px", backgroundColor: "white" }}>
                  <ol className="breadcrumb bg-white">
                    <li className="breadcrumb-item">
                      <Link to="/home">Home</Link>
                    </li>
                    <li className="breadcrumb-item">
                      <Link to="/home/Cameras">Camera</Link>
                    </li>
                    <li className="breadcrumb-item">{brand}</li>
                  </ol>
                </nav>
              </div>
            </div>
            <div className="row">
              <div className="col">{brand} Camera</div>
            </div>
            <div
              className="row pb-1 border-bottom"
              style={{ fontSize: "14px" }}
            >
              <div className="col-2 d-none d-lg-block">
                <strong>Sort By</strong>
              </div>
              <div
                style={{ cursor: "pointer", borderBottom: "5px" }}
                className={
                  "col-2 d-none d-lg-block" +
                  (sort === "popularity" ? " text-primary" : "")
                }
                onClick={() => this.handleSort("popularity")}
              >
                Popularity
              </div>
              <div
                className={
                  "col-2 d-none d-lg-block" +
                  (sort === "desc" ? " text-primary" : "")
                }
                onClick={() => this.handleSort("desc")}
                style={{ cursor: "pointer" }}
              >
                Price High to Low
              </div>
              <div
                className={
                  "col-2 d-none d-lg-block" +
                  (sort === "asc" ? " text-primary" : "")
                }
                onClick={() => this.handleSort("asc")}
                style={{ cursor: "pointer" }}
              >
                Price Low to High
              </div>
            </div>
            {this.state.allCamera && (
              <div>
                {this.state.allCamera.data.map((a, i) => (
                  <div key={i}>
                    <div className="row">
                      <div className="col-lg-2 col-9 text-center">
                        <Link to={`/home/Camera/${a.brand}/${a.id}`}>
                          <img
                            src={a.img}
                            style={{
                              height: "200px",
                              width: "100px",
                              cursor: "pointer"
                            }}
                            tabIndex="0"
                          />
                        </Link>
                      </div>
                      <div className="col-lg-1 col-2 text-secondary">
                        <i
                          style={{ cursor: "pointer" }}
                          className={
                            "fa fa-heart" +
                            (a.liked === true ? " text-danger" : "")
                          }
                          onClick={() => this.handleLike(i)}
                        />
                      </div>
                      <div className="col-lg-5 col-12 text-left">
                        <div className="row">
                          <div
                            className="col "
                            style={{ fontSize: "16px", cursor: "pointer" }}
                          >
                            <Link
                              to={`/home/Mobile/${a.brand}/${a.id}`}
                              className="text-dark prodName"
                              style={{ textDecoration: "none" }}
                            >
                              <span className tabIndex="0">
                                {a.name}
                              </span>
                            </Link>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col">
                            <span
                              style={{
                                lineHeight: "normal",
                                display: "inline-block",
                                color: "#fff",
                                padding: "2px 4px 2px 6px",
                                borderRadius: "3px",
                                fontWeight: "500",
                                fontSize: "12px",
                                verticalAlign: "middle",
                                backgroundColor: "#388e3c"
                              }}
                            >
                              <strong>
                                {a.rating}{" "}
                                <img
                                  src={
                                    "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg=="
                                  }
                                />
                              </strong>
                            </span>
                            {"  "}
                            <span className="text-muted">{a.ratingDesc}</span>
                          </div>
                        </div>
                        {a.details.map(d => (
                          <ul
                            key={d}
                            style={{
                              color: "#c2c2c2",
                              fontSize: "12px",
                              display: "inline",
                              padding: "0%"
                            }}
                          >
                            <li>{d}</li>
                          </ul>
                        ))}
                      </div>
                      <div className="col-lg-3 col-12">
                        <div className="row">
                          <div className="col" style={{ fontSize: "25px" }}>
                            <strong>
                              <i className="fa fa-inr" aria-hidden="true" />
                              {a.price}
                            </strong>
                            {"  "}
                            <span>
                              {"  "}
                              {a.assured && (
                                <img
                                  className="img-fluid"
                                  style={{ width: "70px" }}
                                  src={"https://i.ibb.co/t8bPSBN/fa-8b4b59.png"}
                                />
                              )}
                            </span>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col">
                            <span
                              style={{
                                textDecoration: "line-through",
                                fontSize: "14px",
                                color: "#878787"
                              }}
                            >
                              {a.prevPrice}
                            </span>
                            {"  "}
                            <span
                              style={{
                                color: "#388e3c",
                                fontSize: "13px",
                                fontWeight: "500"
                              }}
                            >
                              {a.discount}%
                            </span>
                          </div>
                        </div>
                        <div className="row" style={{ fontSize: "14px" }}>
                          <div className="col">{a.exchange}</div>
                        </div>
                      </div>
                    </div>
                    <hr />
                  </div>
                ))}
                <div className="row">
                  <div className="col-lg-2 col-4">
                    Page {this.state.allCamera.pageInfo.pageNumber} of{" "}
                    {this.state.allCamera.pageInfo.numberOfPages}
                  </div>
                  <div
                    className="col-8 p-0 text-center"
                    style={{
                      cursor: "pointer",
                      lineHeight: "32px",
                      height: "32px",
                      width: "32px",
                      borderRadius: "50%"
                    }}
                  >
                    {TotalPage.length > 0 &&
                      TotalPage.map(i => (
                        <span key={i}>
                          {this.state.allCamera.pageInfo.pageNumber === i ? (
                            <span
                              key={i}
                              className="bg-primary border text-white m-1"
                              style={{
                                borderRadius: "100%",
                                padding: "6px"
                              }}
                              onClick={() => this.handleDirectPage(i)}
                            >
                              {i}
                            </span>
                          ) : (
                            <span
                              key={i}
                              className="m-1"
                              style={{
                                borderRadius: "100%"
                              }}
                              onClick={() => this.handleDirectPage(i)}
                            >
                              {i}
                            </span>
                          )}
                        </span>
                      ))}
                    {this.state.allCamera.pageInfo.pageNumber <
                      this.state.allCamera.pageInfo.numberOfPages && (
                      <a
                        className="text-primary m-1"
                        onClick={() => this.gotoPage(1)}
                      >
                        Next
                      </a>
                    )}
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default AllCamera;
