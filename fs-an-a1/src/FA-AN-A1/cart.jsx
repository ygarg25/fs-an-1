import React, { Component } from "react";

class Cart extends Component {
  state = {};

  handleCart = (s, prod, qty, props) => {
    console.log("cart Mobile", s, prod, props);
    this.props.onCart(s, prod, qty, props);
  };
  render() {
    const { cartItem, totalCart } = this.props;
    let totalPrice = cartItem.reduce(
      (acc, curr) => acc + +curr.qty * +curr.price,
      0
    );
    return (
      <div style={{ backgroundColor: "lightgrey" }}>
        <div className="row mt-5 pt-1">
          <div className="col-lg-8 col-12 bg-white ml-1 mr-1">
            <div className="row border bottom">
              <div
                className="col-6"
                style={{
                  fontSize: "18px",
                  lineHeight: "56px",
                  padding: "0 24px",
                  fontWeight: "500"
                }}
              >
                My Cart ({totalCart})
              </div>
            </div>
            {cartItem.length > 0 &&
              cartItem.map((c, i) => (
                <div key={i}>
                  <div className="row mt-1 bg-white">
                    <div className="col-lg-2 col-4">
                      <div className="row">
                        <div className="col text-center">
                          <img
                            src={c.img}
                            style={{ width: "50px", height: "92px" }}
                          />
                        </div>
                      </div>
                      <div className="row mt-1">
                        <div className="col-12 text-center">
                          <button
                            className="btn btn-sm "
                            style={{
                              backgroundColor: "white",
                              borderColor: "#e0e0e0",
                              cursor: "auto",
                              borderRadius: "50%",
                              cursor: "pointer",
                              fontSize: "12px"
                            }}
                            disabled={c.qty <= 1}
                            onClick={() => this.handleCart(0, c, -1, "")}
                          >
                            -
                          </button>
                          <span className="border pl-1 pr-2 ">{c.qty}</span>
                          <button
                            className="btn btn-sm "
                            style={{
                              backgroundColor: "white",
                              borderColor: "#e0e0e0",
                              cursor: "auto",
                              borderRadius: "50%",
                              cursor: "pointer",
                              fontSize: "12px"
                            }}
                            onClick={() => this.handleCart(0, c, 1, "")}
                          >
                            +
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-5 col-4">
                      <div className="row">
                        <div
                          className="col"
                          style={{ fontSize: "16px", color: "#212121" }}
                        >
                          {c.name}
                        </div>
                      </div>
                      <div className="row">
                        <div
                          className="col"
                          style={{ color: "#878787", fontSize: "14px" }}
                        >
                          {c.brand}
                          {"   "}
                          {"   "}
                          <span>
                            {" "}
                            {"   "}
                            {c.assured && (
                              <img
                                className="img-fluid"
                                style={{ width: "70px" }}
                                src={"https://i.ibb.co/t8bPSBN/fa-8b4b59.png"}
                              />
                            )}
                          </span>
                        </div>
                      </div>
                      <div className="row mt-2">
                        <div className="col" style={{ fontWeight: "500" }}>
                          <i className="fa fa-inr" /> {c.price}
                          {"  "}
                          <span
                            style={{
                              textDecoration: "line-through",
                              fontSize: "16px",
                              color: "#878787"
                            }}
                          >
                            {"  "}
                            <i className="fa fa-inr"></i>
                            {c.prevPrice}
                          </span>
                          {"  "}
                          <span
                            style={{
                              color: "#388e3c",
                              fontSize: "16px",
                              fontWeight: "500"
                            }}
                          >
                            {"  "}
                            {c.discount}%
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-3 col-4">
                      <div className="row">
                        <div className="col" style={{ fontSize: "14px" }}>
                          Delivery in 2 days | Free{"   "}
                          <span style={{ textDecoration: "line-through" }}>
                            <i className="fa fa-inr" /> 40
                          </span>
                        </div>
                      </div>
                      <div className="row">
                        <div
                          className="col"
                          style={{ fontSize: "12px", color: "grey" }}
                        >
                          10 Days Replacemnet Policy
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr />
                </div>
              ))}
          </div>
          <div className="col-lg-3 col-12 ml-1">
            <div className="row bg-white">
              <div className="col">
                <div
                  className="row border-bottom "
                  style={{
                    fontSize: "14px",
                    fontWeight: "500",
                    color: "#878787",
                    minHeight: "47px"
                  }}
                >
                  <div className="col pt-2">Price Details</div>
                </div>
                <div className="row pt-2 pb-2">
                  <div className="col-6 text-left">
                    Price ({totalCart} items)
                  </div>
                  <div className="col-6 text-right">
                    <i className="fa fa-inr" />
                    {totalPrice}
                  </div>
                </div>
                <div className="row pt-2 pb-2">
                  <div className="col-6 text-left">Delivery</div>
                  <div className="col-6 text-right text-success">Free</div>
                </div>
                <div
                  className="row pt-2 pb-2"
                  style={{
                    fontWeight: "500",
                    borderTop: "1px dashed #e0e0e0",
                    fontSize: "18px"
                  }}
                >
                  <div className="col-6 text-left">Total Payable</div>
                  <div className="col-6 text-right ">{totalPrice}</div>
                </div>
              </div>
            </div>
            <div className="row mt-2">
              <div className="col-1">
                <img
                  style={{ width: "25px" }}
                  src={
                    "https://img1a.flixcart.com/www/linchpin/fk-cp-zion/img/shield_435391.svg"
                  }
                />
              </div>
              <div
                className="col-9"
                style={{
                  fontSize: "14px",
                  textAlign: "left",
                  fontWeight: "500",
                  display: "inline-block",
                  color: "#878787"
                }}
              >
                Safe and Secure Payments. Easy returns. 100% Authentic Products.
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Cart;
