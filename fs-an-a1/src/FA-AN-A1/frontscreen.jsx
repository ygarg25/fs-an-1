import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { url } from "./../config.json";

class FrontScreen extends Component {
  state = {
    currentPage: 1,
    pageSize: 5
  };

  async componentWillMount() {
    let apiEndPint = url + "/" + "deals";

    const { data: dealsData } = await axios.get(apiEndPint);
    this.setState({ dealsData: dealsData });
  }

  async componentDidUpdate(prevProps, prevState, snapshot) {
    let currProps = this.props;

    let apiEndPint = url + "/" + "deals";
    if (currProps !== prevProps) {
      const { data: dealsData } = await axios.get(apiEndPint);
      this.setState({ dealsData: dealsData });
    }
  }

  handlePage = p => {
    this.setState({ currentPage: this.state.currentPage + p });
  };

  render() {
    let { currentPage, pageSize } = this.state;
    // console.log("deals", this.state.dealsData);
    let dealPage = [];
    let maxPage;
    if (this.state.dealsData) {
      maxPage = Math.ceil(this.state.dealsData.length / pageSize);
      let startIndex = (currentPage - 1) * pageSize;
      let copyDealData = [...this.state.dealsData];
      dealPage = copyDealData.splice(startIndex, pageSize);
    }
    // console.log("FrontScreen", dealPage, maxPage, currentPage);
    return (
      <div style={{ backgroundColor: "lightgrey" }}>
        <div style={{ backgroundColor: "lightgrey" }}>
          <div className="container d-block col-12 ">
            <div id="carouselExampleIndicators" className="carousel slide">
              <ol className="carousel-indicators">
                <li
                  data-target="#carouselExampleIndicators"
                  data-slide-to="0"
                ></li>
                <li
                  data-target="#carouselExampleIndicators"
                  data-slide-to="1"
                  className="active"
                ></li>
                <li
                  data-target="#carouselExampleIndicators"
                  data-slide-to="2"
                ></li>
              </ol>
              <div className="carousel-inner">
                <div className="carousel-item active">
                  <Link to={`/home/Mobile/RealMe/M17`}>
                    <img
                      className="d-block w-100"
                      src={"https://i.ibb.co/qxHzVsp/30d7dffe1a1eae09.jpg"}
                      alt="First slide"
                    />
                  </Link>
                </div>
                <div className="carousel-item">
                  <Link to="/home/Laptop/HP/L14">
                    {" "}
                    <img
                      className="d-block w-100"
                      src={"https://i.ibb.co/tq9j6V7/4dfdf0c59f26c4a1.jpg"}
                      alt="Second slide"
                    />
                  </Link>
                </div>
                <div className="carousel-item">
                  <img
                    className="d-block w-100"
                    src={"https://i.ibb.co/vQZhcvT/68af1ae7331acd1c.jpg"}
                    alt="Third slide"
                  />
                </div>
              </div>
              <a
                className="carousel-control-prev"
                href="#carouselExampleIndicators"
                role="button"
                data-slide="prev"
              >
                <span
                  className="carousel-control-prev-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Previous</span>
              </a>
              <a
                className="carousel-control-next"
                href="#carouselExampleIndicators"
                role="button"
                data-slide="next"
              >
                <span
                  className="carousel-control-next-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Next</span>
              </a>
            </div>
          </div>
        </div>
        <div style={{ backgroundColor: "lightgrey" }}>
          <div className="row mt-1">
            <div
              className="col-lg-9 col-12 border border-bottom ml-0 bg-white"
              style={{ fontSize: "22px", lineHeight: "32px" }}
            >
              <b>Deals Of the Day</b>
              <hr />
              {this.state.dealsData && (
                <div className="row ml-1">
                  {currentPage > 1 && (
                    <div
                      className="col-1 text-right"
                      onClick={() => this.handlePage(-1)}
                    >
                      <button
                        style={{
                          alignSelf: "center",
                          padding: "60px 5px ",
                          color: "black",
                          boxShadow: "1px 2px 10px -1px rgba(0,0,0,.3)",
                          backgroundColor: "hsla(0,0%,100%,.98)",
                          cursor: "pointer",
                          display: "flex",
                          position: "absolute",
                          zIndex: "1"
                        }}
                      >
                        <i className="fa fa-angle-left" />
                      </button>
                    </div>
                  )}
                  {dealPage.map((d, i) => (
                    <div className="col-lg-2 col-3 text-center" key={i}>
                      <div className="row ml-lg-3">
                        <Link to={`/home/Mobile/${d.brand}/${d.id}`}>
                          <img
                            src={d.img}
                            style={{ height: "150px", cursor: "pointer" }}
                          />
                        </Link>
                      </div>
                      <div
                        className="row ml-1"
                        style={{
                          fontSize: "12px",
                          fontWeight: "500",
                          marginTop: "15px"
                        }}
                      >
                        {d.name.substring(0, 21)}...
                      </div>
                      <div className="row">
                        <div
                          className="col"
                          style={{ color: "#388e3c", fontSize: "10px" }}
                        >
                          {d.discount}%
                        </div>
                      </div>
                    </div>
                  ))}
                  {currentPage < maxPage && (
                    <div
                      className="col-1 text-right"
                      onClick={() => this.handlePage(1)}
                    >
                      <button
                        style={{
                          alignSelf: "center",
                          padding: "60px 5px ",
                          color: "black",
                          boxShadow: "1px 2px 10px -1px rgba(0,0,0,.3)",
                          backgroundColor: "hsla(0,0%,100%,.98)",
                          cursor: "pointer",
                          display: "flex",
                          position: "absolute",
                          zIndex: "1"
                        }}
                      >
                        <i className="fa fa-angle-right" />
                      </button>
                    </div>
                  )}
                </div>
              )}
            </div>
            <div className="col-2 border-light  bg-white text-right d-none d-lg-block">
              <img
                src={"https://i.ibb.co/1GBrRnn/fa04c5362949d9f1.jpg"}
                className="img-fluid"
                style={{ width: "auto" }}
              />
            </div>
          </div>
        </div>
        <div className="row ml-1 mt-1">
          <div className="col-4 text-center">
            <Link to={`/home/Mobile/RealMe/M20`}>
              <img
                className="img-fluid"
                src={"https://i.ibb.co/dPVHZGW/d5db30a716f82657.jpg"}
                style={{ width: "auto", height: "auto" }}
                tabIndex="0"
              />
            </Link>
          </div>
          <div className="col-4 text-center">
            <img
              className="img-fluid"
              src={"https://i.ibb.co/Lzz36zB/31efaad41a3e4208.jpg"}
              style={{ width: "auto", height: "auto" }}
            />
          </div>
          <div className="col-4 text-center">
            <img
              className="img-fluid"
              src={"https://i.ibb.co/fGX7sFh/4e219998fadcbc70.jpg"}
              style={{ width: "auto", height: "auto" }}
            />
          </div>
        </div>
        <div>
          {" "}
          <div
            className=" row border-top mt-2"
            style={{ backgroundColor: "#fff", color: "#848484" }}
          >
            <div className="col mt-3 ml-2" style={{ fontSize: "10px" }}>
              <h6>Flipkart: The One-stop Shopping Destination</h6>
              <p>
                E-commerce is revolutionizing the way we all shop in India. Why
                do you want to hop from one store to another in search of the
                latest phone when you can find it on the Internet in a single
                click? Not only mobiles. Flipkart houses everything you can
                possibly imagine, from trending electronics like laptops,
                tablets, smartphones, and mobile accessories to in-vogue fashion
                staples like shoes, clothing and lifestyle accessories; from
                modern furniture like sofa sets, dining tables, and wardrobes to
                appliances that make your life easy like washing machines, TVs,
                ACs, mixer grinder juicers and other time-saving kitchen and
                small appliances; from home furnishings like cushion covers,
                mattresses and bedsheets to toys and musical instruments, we got
                them all covered. You name it, and you can stay assured about
                finding them all here. For those of you with erratic working
                hours, Flipkart is your best bet. Shop in your PJs, at night or
                in the wee hours of the morning. This e-commerce never shuts
                down.
              </p>
            </div>
          </div>
          <div
            className=" row"
            style={{ backgroundColor: "#fff", color: "#848484" }}
          >
            <div className="col ml-2" style={{ fontSize: "10px" }}>
              <h6>Flipkart Plus</h6>
              <p>
                A world of limitless possibilities awaits you - Flipkart Plus
                was kickstarted as a loyalty reward programme for all its
                regular customers at zero subscription fee. All you need is 500
                supercoins to be a part of this service. For every 100 rupees
                spent on Flipkart order, Plus members earns 4 supercoins &
                non-plus members earn 2 supercoins. Free delivery, early access
                during sales and shopping festivals, exchange offers and
                priority customer service are the top benefits to a Flipkart
                Plus member. In short, earn more when you shop more! What's
                more, you can even use the Flipkart supercoins for a number of
                exciting services, like: An annual Zomato Gold membership An
                annual Hotstar Premium membership 6 months Gaana plus
                subscription Rupees 550 instant discount on flights on ixigo
                Check out https://www.flipkart.com/plus/all-offers for the
                entire list. Terms and conditions apply.
              </p>
            </div>
          </div>
          <div
            className=" row"
            style={{ backgroundColor: "#fff", color: "#848484" }}
          >
            <div className="col ml-2" style={{ fontSize: "10px" }}>
              <h6>No Cost EMI</h6>
              <p>
                In an attempt to make high-end products accessible to all, our
                No Cost EMI plan enables you to shop with us under EMI, without
                shelling out any processing fee. Applicable on select mobiles,
                laptops, large and small appliances, furniture, electronics and
                watches, you can now shop without burning a hole in your pocket.
                If you've been eyeing a product for a long time, chances are it
                may be up for a no cost EMI. Take a look ASAP! Terms and
                conditions apply.
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FrontScreen;
