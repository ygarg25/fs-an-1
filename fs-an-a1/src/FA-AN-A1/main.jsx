import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Navbar from "./navbar";
import FrontScreen from "./frontscreen";
import AllMobile from "./allMobile";
import SpecificMobile from "./specificMobile";
import Cart from "./cart";
import AllLaptop from "./allLaptop";
import SpecificLaptop from "./specificLaptop";
import AllCamera from "./allCamera";
import SpecificCamera from "./specificCamera";

class Main extends Component {
  state = {
    cartItem: [],
    totolInCart: 0
  };

  addInCart = (s, prod, q, props) => {
    let cartItem = [...this.state.cartItem];
    let totolInCart = this.state.totolInCart;
    if (s === 0) {
      let i = cartItem.findIndex(c => c.id === prod.id);
      if (i < 0) {
        prod.qty = q;
        cartItem.push(prod);
        totolInCart += q;
      } else {
        cartItem[i].qty += q;
        totolInCart += q;
      }
      this.setState({
        cartItem: cartItem,
        totolInCart: totolInCart
      });
    }
    if (s === 1) {
      let i = cartItem.findIndex(c => c.id === prod.id);
      if (i < 0) {
        prod.qty = q;
        cartItem.push(prod);
        totolInCart += q;
      } else {
        cartItem[i].qty += q;
        totolInCart += q;
      }
      this.setState({
        cartItem: cartItem,
        totolInCart: totolInCart
      });
      console.log("dd", props);
      props.history.push("/checkout");
    }
  };
  render() {
    // console.log("cart item", this.state.cartItem);
    // console.log("cart item length", this.state.cartItem.length);
    //console.log("total item in cart", this.state.totolInCart);
    return (
      <div>
        <Route
          path="/"
          render={props => <Navbar total={this.state.totolInCart} {...props} />}
        />
        <div>
          <Switch>
            <Route
              path="/home/Mobile/:brand/:id"
              render={props => (
                <SpecificMobile onCart={this.addInCart} {...props} />
              )}
            />
            <Route
              path="/checkout"
              render={props => (
                <Cart
                  cartItem={this.state.cartItem}
                  totalCart={this.state.totolInCart}
                  onCart={this.addInCart}
                  {...props}
                />
              )}
            />

            <Route path="/home/Mobiles/:brand" component={AllMobile} />
            <Route path="/home/Mobiles" component={AllMobile} />
            <Route
              path="/home/Laptop/:brand/:id"
              render={props => (
                <SpecificLaptop onCart={this.addInCart} {...props} />
              )}
            />
            <Route path="/home/Laptops/:brand" component={AllLaptop} />
            <Route path="/home/Laptops" component={AllLaptop} />

            <Route
              path="/home/Camera/:brand/:id"
              render={props => (
                <SpecificCamera onCart={this.addInCart} {...props} />
              )}
            />
            <Route path="/home/Cameras/:brand" component={AllCamera} />
            <Route path="/home/Cameras" component={AllCamera} />

            <Route path="/home" component={FrontScreen} />

            <Redirect to="/" exact to="/home" />
          </Switch>
        </div>
      </div>
    );
  }
}

export default Main;
