import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import "./navbar.css";

let urlMobile =
  "https://us-central1-fytpo-f6ed3.cloudfunctions.net/app/products";
let urlForLppyCamera = "http://localhost:2410/products";

class Navbar extends Component {
  state = {
    accountInfo: ["My Profile", "Orders", "WishList"],
    moreInf0: [
      "Notifications",
      "Sell on FlipKart",
      "24X7 Customer Care",
      "Adevertise",
    ],
    mobileData: ["Mi", "RealMe", "Samsung", "OPPO", "Apple"],
    laptopData: ["Apple", "HP", "Acer", "Dell"],
    cameraData: ["Nikon", "Canon", "Sony"],
    formData: { q: "" },
  };

  handleSelect = (m) => {
    this.setState({ selecteDcity: m });
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    console.log("search");
    let { data: response } = await axios.get(
      "http://localhost:2410/search/" + this.state.formData.q
    );
    this.props.history.push(response.path);
  };

  handleChange = (e) => {
    const { currentTarget: input } = e;
    let formData = { ...this.state.formData };
    console.log("change", input.value);
    formData[input.name] = input.value;
    this.setState({ formData: formData });
  };
  render() {
    //console.log("dssx", this.props.total);
    return (
      <div className="">
        <div className="row  bg-primary">
          <div className=" d-xs-block col-4  col-lg-2  mt-1 ">
            <Link className="m-1" to="/home">
              <img
                src={"https://i.ibb.co/qs8BK6Y/flipkart-plus-4ee2f9.png"}
                style={{ width: "60%", height: "auto" }}
              />
            </Link>
            <br />
            <Link className="m-1 text-white" to="/#">
              <span
                className="text-white font-italic"
                style={{ fontSize: "11px" }}
              >
                Explore{" "}
                <span
                  className="text-warning font-italic"
                  style={{ fontSize: "11px" }}
                >
                  {" "}
                  Plus{" "}
                </span>
              </span>

              <img
                src={"https://i.ibb.co/t2WXyzj/plus-b13a8b.png"}
                style={{ width: "5%", height: "auto" }}
              />
            </Link>
          </div>
          <div
            className=" d-xs-block col-5  col-lg-4 mt-2  text-right"
            style={{ fontSize: "13px" }}
          >
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <input
                  value={this.state.formData.q}
                  className="form-control"
                  type="search"
                  id="q"
                  name="q"
                  placeholder="Search for products, brand and more"
                  onChange={this.handleChange}
                />
              </div>
            </form>
          </div>
          <div
            className="d-none d-lg-block col-2 text-white mt-1"
            style={{ fontSize: "13px", minHeight: "20px" }}
          >
            <div className=" dropdown ml-2">
              <div className="dropbtn row">
                My Account
                <i
                  className="fa fa-chevron-up mt-1 ml-1"
                  style={{ fontSize: "10px", color: "white" }}
                  id="onhover"
                />
              </div>
              <div className="dropdown-content">
                {this.state.accountInfo.map((m, index) => (
                  <div key={index} style={{ margin: "10px" }}>
                    <Link className="text-dark" to="#">
                      {m}
                    </Link>
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div
            className="d-none d-lg-block col-2 text-white mt-1"
            style={{ fontSize: "13px", minHeight: "20px" }}
          >
            <div className=" dropdown ml-2">
              <div className="dropbtn row">
                More
                <i
                  className="fa fa-chevron-up mt-1 ml-1"
                  style={{ fontSize: "10px", color: "white" }}
                  id="onhover"
                />
              </div>
              <div className="dropdown-content">
                {this.state.moreInf0.map((m, index) => (
                  <div key={index} style={{ margin: "10px" }}>
                    <Link className="text-dark" to="#">
                      {m}
                    </Link>
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div
            className="d-xs-block col-3 col-lg-2 text-white mt-3 "
            style={{ cursor: "pointer" }}
          >
            <Link to="/checkout">
              <span className="text-white">
                <i className="fa fa-shopping-cart fa-lg">
                  {this.props.total > 0 && (
                    <span
                      className="badge badge-pill badge-xs badge-danger"
                      style={{ fontSize: "11px" }}
                    >
                      {this.props.total}
                    </span>
                  )}
                </i>
                {"   "}Cart
              </span>
            </Link>
          </div>
        </div>
        <div className="row ">
          <div className="col-4 text-center ">
            <div className=" dropdown ml-2">
              <div className="dropbtn row">
                Mobiles
                <i
                  className="fa fa-chevron-down mt-1 ml-1"
                  style={{ fontSize: "10px", color: "lightgrey" }}
                  id="onhover"
                />
              </div>
              <div className="dropdown-content text-left ">
                {this.state.mobileData.map((m, index) => (
                  <Link
                    className="text-dark hov"
                    to={`/home/Mobiles/${m}?page=1`}
                    key={index}
                    style={{ textDecoration: "none" }}
                  >
                    <div
                      onClick={() => this.handleSelect(m)}
                      style={{ padding: "10px" }}
                    >
                      {m}
                    </div>
                  </Link>
                ))}
              </div>
            </div>
          </div>
          <div className="col-4 text-center " style={{ fontSize: "13px" }}>
            <div className=" dropdown ml-2">
              <div className="dropbtn row">
                Laptops
                <i
                  className="fa fa-chevron-down mt-1 ml-1"
                  style={{ fontSize: "10px", color: "lightgrey" }}
                  id="onhover"
                />
              </div>
              <div className="dropdown-content text-left">
                {this.state.laptopData.map((m, index) => (
                  <Link
                    to={`/home/Laptops/${m}?page=1`}
                    key={index}
                    className="text-dark hov"
                    style={{ textDecoration: "none" }}
                  >
                    <div
                      className="text-dark hov"
                      onClick={() => this.handleSelect(m)}
                      style={{ padding: "10px" }}
                    >
                      {m}
                    </div>
                  </Link>
                ))}
              </div>
            </div>
          </div>
          <div className="col-4 text-center" style={{ fontSize: "13px" }}>
            <div className=" dropdown ml-2">
              <div className="dropbtn row">
                Camera
                <i
                  className="fa fa-chevron-down mt-1 ml-1"
                  style={{ fontSize: "10px", color: "lightgrey" }}
                  id="onhover"
                />
              </div>
              <div className="dropdown-content text-left">
                {this.state.cameraData.map((m, index) => (
                  <Link
                    to={`/home/Cameras/${m}`}
                    key={index}
                    className="text-dark hov"
                    style={{ textDecoration: "none" }}
                  >
                    <div
                      onClick={() => this.handleSelect(m)}
                      style={{ padding: "10px" }}
                    >
                      {m}
                    </div>
                  </Link>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Navbar;
