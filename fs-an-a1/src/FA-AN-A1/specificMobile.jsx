import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { url } from "./../config.json";

class SpecificMobile extends Component {
  state = {
    imgId: 0
  };
  async componentWillMount() {
    const id = this.props.match.params.id;
    let apiEndPoint = url + "/" + "product";
    let apiForBankOffer = url + "/" + "bankOffers";
    if (id) apiEndPoint = apiEndPoint + "/" + id;
    let { data: allMobile } = await axios.get(apiEndPoint);
    let { data: allBankOffer } = await axios.get(apiForBankOffer);
    this.setState({ allMobile: allMobile, allBankOffer: allBankOffer });
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    const id = this.props.match.params.id;
    let apiEndPoint = url + "/" + "product";
    let apiForBankOffer = url + "/" + "bankOffers";
    if (id) apiEndPoint = apiEndPoint + "/" + id;
    if (currProps !== prevProps) {
      let { data: allMobile } = await axios.get(apiEndPoint);
      let { data: allBankOffer } = await axios.get(apiForBankOffer);
      this.setState({ allMobile: allMobile, allBankOffer: allBankOffer });
    }
  }

  handleImageId = i => {
    this.setState({ imgId: i });
  };

  handleCart = (s, prod, qty, props) => {
    console.log("cart Mobile", s, prod, props);
    this.props.onCart(s, prod, qty, props);
  };

  render() {
    const { allMobile: mob, imgId, allBankOffer: bo } = this.state;
    // console.log("Specific Mobile", mob, bo);
    return (
      <div>
        {this.state.allMobile && (
          <div className="row">
            <div className="col-lg-5 col-12">
              <div className="row p-0">
                <div className="col-lg-2 col-3 text-center">
                  {mob.pics.imgList.map((a, i) => (
                    <div
                      key={i}
                      style={{
                        height: "64px",
                        width: "64px",
                        textAlign: "center",
                        borderWidth: "2px solid"
                      }}
                      className={
                        "row border ml-lg-2 ml-0 mt-1 " +
                        (imgId === i ? " border-primary" : "")
                      }
                    >
                      <div className="col text-center">
                        <img
                          src={a}
                          style={{
                            width: "40px",
                            height: "50px",
                            cursor: "pointer"
                          }}
                          onClick={() => this.handleImageId(i)}
                        />
                      </div>
                    </div>
                  ))}
                </div>
                <div className="col-lg-8 col-8 ml-4 border text-center">
                  <img className="img-fluid" src={mob.pics.imgList[imgId]} />
                </div>
              </div>
              <div className="row">
                <div className="col-lg-2 col-4"></div>
                <div className="col-lg-4 col-4 text-sm-center">
                  <button
                    className="btn btn-sm btn-warning text-white"
                    onClick={() => this.handleCart(0, mob.prod, 1, "")}
                  >
                    <svg
                      className="_3oJBMI"
                      width="16"
                      height="16"
                      viewBox="0	0	16	15"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        className=""
                        d="M15.32 2.405H4.887C3 2.405 2.46.805 2.46.805L2.257.21C2.208.085 2.083 0 1.946 0H.336C.1 0-.064.24.024.46l.644 1.945L3.11 9.767c.047.137.175.23.32.23h8.418l-.493	1.958H3.768l.002.003c-.017 0-.033-.003-.05-.003-1.06 0-1.92.86-1.92	1.92s.86 1.92 1.92 1.92c.99	0 1.805-.75	1.91-1.712l5.55.076c.12.922.91 1.636 1.867 1.636 1.04 0 1.885-.844 1.885-1.885 0-.866-.584-1.593-1.38-1.814l2.423-8.832c.12-.433-.206-.86-.655-.86"
                        fill="#fff"
                      ></path>
                    </svg>
                    {"  "} Add to Cart
                  </button>
                </div>
                <div className="col-lg-4 col-4 text-sm-center">
                  <button
                    className="btn btn-sm  text-white"
                    style={{ backgroundColor: "#fb641b" }}
                    onClick={() => this.handleCart(1, mob.prod, 1, this.props)}
                  >
                    <i className="fa fa-bolt" />
                    {"  "} Buy Now
                  </button>
                </div>
              </div>
            </div>
            <div className="col-lg-6 ml-4 col-12 ">
              <div className="row"></div>
              <div
                className="row"
                style={{
                  color: "#212121",
                  fontSize: "18px",
                  fontWeight: "400",
                  padding: "0",
                  lineHeight: "1.4",
                  fontSize: "inherit",
                  fontWeight: "inherit",
                  display: "inline-block"
                }}
              >
                <div className="col">{mob.prod.name}</div>
              </div>
              <div className="row">
                <div className="col">
                  <span
                    style={{
                      lineHeight: "normal",
                      display: "inline-block",
                      color: "#fff",
                      padding: "2px 4px 2px 6px",
                      borderRadius: "3px",
                      fontWeight: "500",
                      fontSize: "12px",
                      verticalAlign: "middle",
                      backgroundColor: " #388e3c"
                    }}
                  >
                    <strong>
                      {mob.prod.rating}{" "}
                      <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg==" />
                    </strong>{" "}
                  </span>
                  {"  "}
                  <span
                    className="text-muted"
                    style={{ fontSize: "14px", fontWeight: "500" }}
                  >
                    {mob.prod.ratingDesc}
                  </span>
                  {"   "}
                  {mob.prod.assured && (
                    <img
                      className="img-fluid"
                      style={{ width: "70px" }}
                      src={"https://i.ibb.co/t8bPSBN/fa-8b4b59.png"}
                    />
                  )}
                </div>
              </div>
              <div className="row mt-2">
                <div
                  className="col"
                  style={{
                    fontSize: "28px",
                    verticalAlign: "sub",
                    fontWeight: "500"
                  }}
                >
                  <i className="fa fa-inr" />
                  {mob.prod.price}
                  {"  "}
                  <span
                    style={{
                      textDecoration: "line-through",
                      fontSize: "16px",
                      color: "#878787"
                    }}
                  >
                    <i className="fa fa-inr" />
                    {mob.prod.prevPrice}
                  </span>{" "}
                  <span
                    style={{
                      color: "#388e3c",
                      fontSize: "16px",
                      fontWeight: "500"
                    }}
                  >
                    {"  "} {mob.prod.discount}%
                  </span>
                </div>
              </div>
              <div className="row">
                <div
                  className="col"
                  style={{
                    fontSize: "16px",
                    marginLeft: "12px",
                    verticalAlign: "middle",
                    fontWeight: "500",
                    color: "black"
                  }}
                >
                  Available Offers
                </div>
              </div>
              <div className="row">
                {bo.map((b, i) => (
                  <div className="col-12" key={i}>
                    <img
                      style={{ height: "18px", width: "18px" }}
                      src={b.img}
                    />
                    <span
                      style={{
                        color: "#212121",
                        fontWeight: "500",
                        paddingLeft: "4px",
                        fontSize: "14px"
                      }}
                    >
                      {b.type}
                    </span>
                    <span style={{ fontSize: "14px", marginLeft: "10px" }}>
                      {b.detail}
                    </span>
                  </div>
                ))}
              </div>
              <div className="row mt-2">
                <div className="col-lg-1 col-3 border text-center ml-2">
                  <img
                    style={{ width: "38px", height: "30px" }}
                    src={mob.pics.brandImg}
                  />
                </div>
                <div
                  className="col-8 ml-3 d-none d-lg-block"
                  style={{ fontSize: "14px" }}
                >
                  Brand Warranty of 1 Year Available for Mobile and 6 Months for
                  Accessories
                </div>
              </div>
              <div className="row mt-2">
                <div
                  className="col-1 d-none d-lg-block"
                  style={{
                    color: "#878787",
                    fontWeight: "500",
                    width: "110px",
                    paddingRight: "10px",
                    float: "left",
                    fontSize: "14px"
                  }}
                >
                  Highights
                </div>
                <div className="col-5 d-none d-lg-block">
                  <ul>
                    {mob.prod.details.map((a, i) => (
                      <li
                        key={i}
                        style={{
                          fontSize: "14px",
                          color: "black",
                          lineHeight: "1.4"
                        }}
                      >
                        {a}
                      </li>
                    ))}
                  </ul>
                </div>
                <div
                  className="col-2 d-none d-lg-block"
                  style={{
                    color: "#878787",
                    fontWeight: "500",
                    width: "110px",
                    paddingRight: "10px",
                    float: "left",
                    fontSize: "14px"
                  }}
                >
                  Easy Payment Options
                </div>
                <div className="col-4 d-none d-lg-block">
                  <ul
                    style={{
                      fontSize: "14px",
                      color: "black",
                      lineHeight: "1.4"
                    }}
                  >
                    <li>
                      <span>No cost EMI NaN/month</span>
                    </li>
                    <li>
                      <span>Debit/Flipkart EMI available</span>
                    </li>
                    <li>
                      <span>Cash on Delivery</span>
                    </li>
                    <li>
                      <span>Net Banking {"&"} Credit/Debit/ATM Card</span>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="row">
                <div
                  className="col-lg-1 col-3"
                  style={{
                    fontWeight: "500",
                    color: "#878787",
                    width: "110px",
                    paddingRight: "10px",
                    float: "left",
                    fontSize: "14px"
                  }}
                >
                  Seller
                </div>
                <div className="col-lg-9 col-9">
                  <span
                    style={{
                      color: "#2874f0",
                      fontSize: "14px",
                      fontWeight: "500"
                    }}
                  >
                    SuperComNet
                  </span>
                  <span
                    style={{
                      lineHeight: "normal",
                      display: "inline-block",
                      color: "#fff",
                      padding: "2px 4px 2px 6px",
                      fontWeight: "500",
                      fontSize: "12px",
                      borderRadius: "4px",
                      backgroundColor: "#2874f0"
                    }}
                  >
                    4.2 {"    "}
                    <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg==" />
                  </span>
                  <ul
                    className="d-none d-lg-block "
                    style={{
                      fontSize: "14px",
                      color: "Grey",
                      lineHeight: "1.4"
                    }}
                  >
                    <li>
                      <span>10 Day Replacement</span>
                    </li>
                  </ul>
                </div>
                <div className="row">
                  <div className="col">
                    <img
                      className="img-fluid"
                      src={"https://i.ibb.co/j8CMRbn/CCO-PP-2019-07-14.png"}
                      style={{ width: "300px", height: "85px" }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default SpecificMobile;
