let users = [
  {
    Id: "1",
    Title: "Mr",
    Fname: "Jack",
    Lname: "Smith",
    Mob: "9876543210",
    Email: "jack@mail.com",
    EnterKey: "jack123",
  },
  {
    Id: "2",
    Title: "Ms",
    Fname: "Maria",
    Lname: "Ercel",
    Mob: "9876534210",
    Email: "maria@mail.com",
    EnterKey: "maria123",
  },
  {
    Id: "3",
    Title: "Mr",
    Fname: "Joe",
    Lname: "Kr",
    Mob: "9873452345",
    Email: "joe@mail.com",
    EnterKey: "joe123",
  },
  {
    Id: "4",
    Title: "Mr",
    Fname: "Abc",
    Lname: "Def",
    Mob: "9876543217",
    Email: "abc@test.com",
    EnterKey: "abc123",
  },
];

module.exports.users = users;
