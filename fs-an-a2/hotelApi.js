var express = require("express");
var app = express();
app.use(express.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin,X-Requested-With,Content-Type,Accept"
  );
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");

  next();
});

const port = 2410;

app.listen(port, () => console.log("listening on port", port));

let hotelData = require("./hotelData.js").hotelData;

let bookingData = {};

app.get("/hotel/:city", function (req, res) {
  let city = req.params.city;
  let rating = req.query.rating;
  let sort = req.query.sort;
  let price = req.query.price;
  console.log("in get request of hotel ", city, sort, rating, price);
  let outarr = [...hotelData];
  outarr = hotelData.filter((h) => h.city === city);
  if (rating) outarr = outarr.filter((h) => +h.rating === +rating);
  if (price === "<=2000") outarr = outarr.filter((h) => h.priceR1 <= 2000);
  if (price === "2001-7000") {
    console.log("dsxs");
    outarr = outarr.filter((h) => h.priceR1 >= 2001 && h.priceR1 <= 7000);
  }
  if (price === "7001-10000") {
    console.log("ddd");
    outarr = outarr.filter((h) => h.priceR1 >= 7001 && h.priceR1 <= 10000);
  }
  if (price === ">=10001") outarr = outarr.filter((h) => h.priceR1 >= 10001);
  if (sort === "rating") outarr = outarr.sort((h, i) => +i.rating - +h.rating);
  if (sort === "price") outarr = outarr.sort((h, i) => +i.priceR1 - +h.priceR2);

  res.send(outarr);
});

app.post("/booking", function (req, res) {
  let body = req.body;
  console.log("in post request for booking", body);
  bookingData = { ...body };
  res.send(bookingData);
});

app.get("/booking", function (req, res) {
  console.log("in get request for booking", bookingData);
  res.send(bookingData);
});
