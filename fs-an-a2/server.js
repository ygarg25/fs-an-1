var express = require("express");
var app = express();
app.use(express.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin,X-Requested-With,Content-Type,Accept"
  );
  res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
  //res.header("Access-Control-Allow-Credentials", true);
  res.setHeader("Access-Control-Expose-Headers", "X-Auth-Token");
  next();
});

const port = 2410;

app.listen(port, () => console.log("listening on port", port));

//for import all data file
let users = require("./YatraUser.js").users;
let eCash = require("./YatraUserEcash.js").ecash;
let yatraHotel = require("./YatraUserHotel.js").yatraHotel;
let yatraFlight = require("./YatraUserFlight.js").yatraFlight;

//console.log(users, eCash, yatraHotel, YatraFlight);

// for login and logout

const jwt = require("jsonwebtoken");
var bodyParser = require("body-parser");
var passport = require("passport");
var passportJWT = require("passport-jwt");
app.use(bodyParser.json());

passport.initialize();

var ExtractJWT = passportJWT.ExtractJwt;
var Strategy = passportJWT.Strategy;

var cfg = require("./config.js");

var params = {
  secretOrKey: cfg.jwtSecret,
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
};

var strategy = new Strategy(params, function (payload, done) {
  console.log("strategy", payload, payload.id);
  let id = users.findIndex((obj) => payload.id === obj.Id);
  var user = users[+id] || null;
  if (user) {
    return done(null, {
      id: user.Id,
    });
  } else {
    return done(new Error("user not found"), null);
  }
});

passport.use(strategy);

const jwtExpirySeconds = 300000;

app.post("/login", function (req, res, payload) {
  console.log("start", req.body);

  console.log("start", req.body);
  if (req.body.username && req.body.password) {
    var email = req.body.username;
    var password = req.body.password;
    var user = users.find(function (u) {
      return u.Email === email && u.EnterKey === password;
    });
    if (user) {
      console.log("users", user);
      var payload = {
        id: user.Id,
      };
      var token = jwt.sign(payload, cfg.jwtSecret, {
        algorithm: "HS256",
        expiresIn: jwtExpirySeconds,
      });
      res.setHeader("X-Auth-Token", token);
      res.json({ success: true, token: "bearer " + token });
    } else {
      res.sendStatus(401);
    }
  } else {
    res.sendStatus(401);
  }
});

app.get("/user", passport.authenticate("jwt", { session: false }), function (
  req,
  res
) {
  console.log("User", req.user);
  res.json(users[users.findIndex((obj) => obj.Id === req.user.id)]);
});

var logout = require("express-passport-logout");

app.delete(
  "/logout",
  passport.authenticate("jwt", { session: false }),
  logout()
);

app.post("/register", function (req, res) {
  console.log(
    "in post request of new user",
    req.body,
    JSON.stringify(req.body)
  );

  let data = {
    Id: +users[users.length - 1].Id + 1,
    Title: req.body.title,
    Fname: req.body.firstName,
    Lname: req.body.lastName,
    Mob: req.body.mob,
    Email: req.body.email,
    EnterKey: req.body.password,
  };

  let i = users.findIndex((obj) => obj.Email === req.body.email);
  console.log("index", i);
  if (i > -1) {
    console.log("if");
    let Err = { error: 1062 };
    res.send(Err).end();
  } else {
    console.log("else");
    users.push(data);
    console.log("users", users);
    res.end();
  }
});

// for getting and posting user booking and cash detail

//for getting ecash acc to user
app.get("/ecash/:id", function (req, res) {
  const id = req.params.id;
  console.log("myecash get");
  let results = eCash.filter((obj) => obj.UserId === id);
  res.send(results);
});

//for posting ecash acc to user
app.post("/ecash/:id", function (req, res) {
  const id = req.params.id;
  console.log("in post request of ecash if user", id, req.body);
  let data = {
    Id: +eCash[eCash.length - 1].Id + 1,
    UserId: id,
    Cdate: req.body.date,
    Credit: req.body.credit,
    Debit: "0",
  };

  eCash.push(data);
  console.log("eCash", eCash);
  res.send({ status: "Success" });
  res.end();
});

// for posting hotel book acc to user
app.post("/hotelbook/:id", function (req, res) {
  let id = req.params.id;
  console.log("in post request of hotel if user", id, req.body);

  let data = {
    Id: +yatraHotel[yatraHotel.length - 1].Id + 1,
    UserId: id,
    HName: req.body.hName,
    City: req.body.city,
    TRoom: req.body.tRoom,
    TDay: req.body.tDay,
    CheckIn: req.body.checkIn,
    CheckOut: req.body.CheckOut,
    Address: req.body.address,
    leadguest: req.body.guest,
    RoomType: req.body.roomType,
    TGuest: req.body.tGuest,
    BookingStatus: "Confirmed",
  };

  yatraHotel.push(data);
  console.log("yatraHotel", yatraHotel);
  res.send({ status: "Success" });
  res.end();
});

// for getting hotel book acc to user
app.get("/hotelbook/:id", function (req, res) {
  let id = req.params.id;
  let page = req.query.page;
  page = page ? page : 1;
  console.log("in get request of hotel if user", id, page);

  let results = yatraHotel.filter((obj) => obj.UserId === id);
  if (results.length > 0) {
    let pageNumber = +page;
    let pageSize = 1;
    let startIndex = (pageNumber - 1) * pageSize;

    let TempArr = [...results];
    let hotelPage = TempArr.splice(startIndex, pageSize);

    let pageInfo = {
      pageNumber: pageNumber,
      currPage: page,
      pageSize: pageSize,
      startIndex: startIndex,
      totalIteminPage: hotelPage.length,
      totalItem: results.length,
      totalPages: Math.ceil(results.length / pageSize),
    };
    let obj = {};
    obj.hotel = hotelPage;
    obj.pageInfo = pageInfo;
    console.log("obj", obj);
    res.send(obj);
  } else {
    res.sendStatus(401).end();
  }
});

// for getting specific hotel book acc to user
app.get("/specifichotelbook/:id/:UserId", function (req, res) {
  let id = req.params.id;
  let UserId = req.params.UserId;
  console.log("in get request of specific hotel if user", id, UserId);

  let results = yatraHotel.filter(
    (obj) => +obj.Id === +id && +obj.UserId === +UserId
  );
  console.log("specific hotel", results);
  if (results.length > 0) {
    console.log("if", results);
    res.send(results);
  } else {
    console.log("if", results);
    res.sendStatus(401).end();
  }
});

// for posting Flight book acc to user
app.post("/flightbook/:id", function (req, res) {
  const id = req.params.id;
  console.log("in post request of Flight if user", id, req.body);
  let data = {
    Id: +yatraFlight[yatraFlight.length - 1].Id + 1,
    UserId: id,
    GDepartCity: req.body.gDCity,
    GArrivalCity: req.body.gACity,
    GDuration: req.body.gDuration,
    GDepartTime: req.body.gDTime,
    GArrivalTime: req.body.gATime,
    GFlightName: req.body.gFName,
    GFlightCode: req.body.gFCode,
    GDate: req.body.gDate,
    TTravellers: req.body.tGuest,
    flightClass: req.body.flightClass,
    leadGuest: req.body.guest,
    bookingStatus: "Confirmed",
    returnBook: req.body.returnBook,
    RDepartCity: req.body.rDCity,
    RArrivalCity: req.body.rACity,
    RDuration: req.body.rDuration,
    RDepartTime: req.body.rDTime,
    RArrivalTime: req.body.rATime,
    RFlightName: req.body.rFName,
    RFlightCode: req.body.rFCode,
    RDate: req.body.rDate,
  };

  yatraFlight.push(data);
  console.log("yatraHotel", yatraFlight);
  res.send({ status: "Success" });
  res.end();
});

// for getting flight book acc to user
app.get("/flightbook/:id", function (req, res) {
  const id = req.params.id;
  let page = req.query.page;
  page = page ? page : 1;
  console.log("in get request of flight if user", id, page);

  let results = yatraFlight.filter((obj) => obj.UserId === id);
  if (results.length > 0) {
    let pageNumber = +page;
    let pageSize = 1;
    let startIndex = (pageNumber - 1) * pageSize;

    let TempArr = [...results];
    let flightPage = TempArr.splice(startIndex, pageSize);

    let pageInfo = {
      pageNumber: pageNumber,
      currPage: page,
      pageSize: pageSize,
      startIndex: startIndex,
      totalIteminPage: flightPage.length,
      totalItem: results.length,
      totalPages: Math.ceil(results.length / pageSize),
    };
    let obj = {};
    obj.flight = flightPage;
    obj.pageInfo = pageInfo;
    console.log("obj", obj);
    res.send(obj);
  } else {
    res.sendStatus(401).end();
  }
});

// for getting specific flight book acc to user
app.get("/specificflightbook/:id/:UserId", function (req, res) {
  let id = req.params.id;
  let UserId = req.params.UserId;
  console.log("in get request of specific flight if user", id, UserId);
  let results = yatraFlight.filter(
    (obj) => +obj.Id === +id && +obj.UserId === +UserId
  );
  if (results.length > 0) {
    res.send(results);
  } else {
    res.sendStatus(401).end();
  }
});
