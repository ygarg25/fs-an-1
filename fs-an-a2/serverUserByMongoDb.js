var express = require("express");
var app = express();
app.use(express.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin,X-Requested-With,Content-Type,Accept"
  );
  res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
  //res.header("Access-Control-Allow-Credentials", true);
  res.setHeader("Access-Control-Expose-Headers", "X-Auth-Token");
  next();
});

const port = 2410;

app.listen(port, () => console.log("listening on port", port));

//for import all data file
let users = require("./YatraUser.js").users;
let eCash = require("./YatraUserEcash.js").ecash;
let yatraHotel = require("./YatraUserHotel.js").yatraHotel;
let yatraFlight = require("./YatraUserFlight.js").yatraFlight;

// console.log("Ds", eCash);

//for mongoDB  connection
const MongoClient = require("mongodb").MongoClient;
const ObjectId = require("mongodb").ObjectID;
let url =
  "mongodb+srv://ygarg25:yashgarg@mongo-online-cluster-rdozs.gcp.mongodb.net/test?retryWrites=true&w=majority";
const client = new MongoClient(url);
const dbName = "yatraDB";
client.connect(function (err, client) {
  console.log("DB Connected");
  const db = client.db(dbName);
  // db.collection("yatraUser").createIndex({ Email: 1 }, { unique: true });
});

// client.connect(function (err, client) {
//   console.log("Ecash Connected");
//   const db = client.db(dbName);
//   db.collection("yatraUser").insertMany(users, function (err, docs) {
//     if (err) console.log(err);
//     console.log("Number of documents inserted: " + docs.insertedCount);
//   });
// });

// for login and logout

const jwt = require("jsonwebtoken");
var bodyParser = require("body-parser");
var passport = require("passport");
var passportJWT = require("passport-jwt");
app.use(bodyParser.json());

passport.initialize();

var ExtractJWT = passportJWT.ExtractJwt;
var Strategy = passportJWT.Strategy;

var cfg = require("./config.js");

var params = {
  secretOrKey: cfg.jwtSecret,
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
};

var strategy = new Strategy(params, function (payload, done) {
  console.log("strategy", payload);
  client.connect(function (err, client) {
    console.log("in Db of get user  detail strategy");
    if (err) return done(new Error("user not found"), null);
    else {
      const db = client.db(dbName);
      db.collection("yatraUser").findOne(
        { _id: ObjectId(payload.id) },
        function (err, results) {
          if (err) return done(new Error("user not found"), null);
          else {
            if (results) {
              console.log(
                "DB in get request of user after login strategy",
                results
              );
              return done(null, { id: results._id });
            } else return done(new Error("user not found"), null);
          }
        }
      );
    }
  });
});

passport.use(strategy);

const jwtExpirySeconds = 300000;

app.post("/login", function (req, res, payload) {
  console.log("login request", req.body);
  client.connect(function (err, client) {
    console.log("in Db for login reques");
    if (err) res.sendStatus(401).end();
    else {
      const db = client.db(dbName);
      db.collection("yatraUser").findOne(
        { Email: req.body.username, EnterKey: req.body.password },
        function (err, results) {
          if (err) res.sendStatus(401).end();
          else {
            console.log("DB in post request of login", results, results._id);
            if (results) {
              var payload = { id: results._id };
              var token = jwt.sign(payload, cfg.jwtSecret, {
                algorithm: "HS256",
                expiresIn: jwtExpirySeconds,
              });
              console.log("token", token);
              res.setHeader("X-Auth-Token", token);
              res.json({ success: true, token: "bearer " + token });
            } else {
              res.sendStatus(401).end();
            }
          }
        }
      );
    }
  });
});

app.get("/user", passport.authenticate("jwt", { session: false }), function (
  req,
  res
) {
  console.log("ssdddddddddddddddddddddddddddddddd", req.user);

  client.connect(function (err, client) {
    console.log("in Db of get user  detail user");
    if (err) res.sendStatus(401);
    else {
      const db = client.db(dbName);
      db.collection("yatraUser").findOne(
        { _id: ObjectId(req.user.id) },
        function (err, results) {
          if (err) res.sendStatus(401);
          else {
            if (results) {
              console.log("DB in get request of user after login to get user");
              res.json(results);
            } else res.sendStatus(401);
          }
        }
      );
    }
  });
});

var logout = require("express-passport-logout");

app.delete(
  "/logout",
  passport.authenticate("jwt", { session: false }),
  logout()
);

app.post("/register", function (req, res) {
  console.log("in post request of new user", req.body);
  let postData = {
    Title: req.body.title,
    Fname: req.body.firstName,
    Lname: req.body.lastName,
    Mob: req.body.mob,
    Email: req.body.email,
    EnterKey: req.body.password,
  };
  client.connect(function (err, client) {
    console.log("in Db for post new user", postData);
    if (err) res.sendStatus(401).end();
    else {
      const db = client.db(dbName);
      db.collection("yatraUser").insertOne(postData, function (err, results) {
        if (err) {
          console.log(err.code);
          if (err.code === 11000) {
            console.log("error");
            let Err = { error: 1062 };
            res.send(Err).end();
          } else res.sendStatus(401).end();
        } else {
          console.log("DB in post request of new Registeration", results);
          res.end();
        }
      });
    }
  });
});

// for getting and posting user booking and cash detail

//for getting ecash acc to user
app.get("/ecash/:id", function (req, res) {
  const id = req.params.id;
  console.log("in get request of ecash if user 1 ", id);
  client.connect(function (err, client) {
    console.log("DB in get request of ecasg 2 ");
    if (err) res.sendStatus(401);
    else {
      const db = client.db(dbName);
      db.collection("yatraUserEcash")
        .find({ UserId: id })
        .toArray(function (err, results) {
          if (err) res.sendStatus(401);
          else {
            if (results) {
              console.log("DB in get request of ecasg 3 ", results);
              let arr = results.map((b) => ({
                Id: b._id,
                UserId: b.UserId,
                Cdate: b.Cdate,
                Credit: b.Credit,
                Debit: b.Debit,
              }));
              res.send(arr);
            } else res.sendStatus(401);
          }
        });
    }
  });
});

//for posting ecash acc to user
app.post("/ecash/:id", function (req, res) {
  const id = req.params.id;
  console.log("in post request of ecash if user", id, req.body);
  let postData = {
    UserId: id,
    Cdate: req.body.date,
    Credit: req.body.credit,
    Debit: "0",
  };

  client.connect(function (err, client) {
    console.log("in Db for post ecash", postData);
    if (err) res.sendStatus(401).end();
    else {
      const db = client.db(dbName);
      db.collection("yatraUserEcash").insertOne(postData, function (
        err,
        results
      ) {
        if (err) res.sendStatus(401).end();
        else {
          console.log("DB in post request of ecash", results.ops);
          res.send({ status: "Success" });
        }
      });
    }
  });
});

// for posting hotel book acc to user
app.post("/hotelbook/:id", function (req, res) {
  const id = req.params.id;
  console.log("in post request of hotel if user", id, req.body);
  let postData = {
    UserId: id,
    HName: req.body.hName,
    City: req.body.city,
    TRoom: req.body.tRoom,
    TDay: req.body.tDay,
    CheckIn: req.body.checkIn,
    CheckOut: req.body.checkOut,
    Address: req.body.address,
    leadguest: req.body.guest,
    RoomType: req.body.roomType,
    TGuest: req.body.tGuest,
    BookingStatus: "Confirmed",
  };
  console.log(postData);
  client.connect(function (err, client) {
    console.log("in Db for post hotel", postData);
    if (err) res.sendStatus(401).end();
    else {
      const db = client.db(dbName);
      db.collection("yatraUserHotel").insertOne(postData, function (
        err,
        results
      ) {
        if (err) res.sendStatus(401).end();
        else {
          console.log("DB in post request of hotel", results.ops);
          res.send({ status: "Success" });
        }
      });
    }
  });
});

// for getting hotel book acc to user
app.get("/hotelbook/:id", function (req, res) {
  const id = req.params.id;
  let page = req.query.page;
  page = page ? page : 1;
  console.log("in get request of hotel if user", id, page);

  client.connect(function (err, client) {
    console.log("DB in get request of hotel 2 ");
    if (err) res.sendStatus(401);
    else {
      const db = client.db(dbName);
      db.collection("yatraUserHotel")
        .find({ UserId: id })
        .toArray(function (err, results) {
          if (err) res.sendStatus(401);
          else {
            if (results.length > 0 && results) {
              console.log("DB in get request of hotel 3 ", results);
              let arr = results.map((b) => ({
                Id: b._id,
                UserId: b.UserId,
                HName: b.HName,
                City: b.City,
                TRoom: b.TRoom,
                TDay: b.TDay,
                CheckIn: b.CheckIn,
                CheckOut: b.CheckOut,
                Address: b.Address,
                leadguest: b.leadguest,
                RoomType: b.RoomType,
                TGuest: b.TGuest,
                BookingStatus: b.BookingStatus,
              }));

              let pageNumber = +page;
              let pageSize = 1;
              let startIndex = (pageNumber - 1) * pageSize;

              let TempArr = [...arr];
              let hotelPage = TempArr.splice(startIndex, pageSize);

              let pageInfo = {
                pageNumber: pageNumber,
                currPage: page,
                pageSize: pageSize,
                startIndex: startIndex,
                totalIteminPage: hotelPage.length,
                totalItem: arr.length,
                totalPages: Math.ceil(arr.length / pageSize),
              };
              let obj = {};
              obj.hotel = hotelPage;
              obj.pageInfo = pageInfo;
              console.log("obj", obj);
              res.send(obj);
            } else res.sendStatus(401);
          }
        });
    }
  });
});

// for getting specific hotel book acc to user
app.get("/specifichotelbook/:id/:UserId", function (req, res) {
  const id = req.params.id;
  const UserId = req.params.UserId;
  console.log("in get request of specific hotel if user", id, UserId);
  client.connect(function (err, client) {
    console.log("DB in get request of ecasg 2 ");
    if (err) res.sendStatus(401);
    else {
      const db = client.db(dbName);
      db.collection("yatraUserHotel")
        .find({ _id: ObjectId(id), UserId: UserId })
        .toArray(function (err, results) {
          if (err) res.sendStatus(401);
          else {
            if (results.length > 0 && results) {
              console.log("DB in get request of ecasg 3 ", results);
              let arr = results.map((b) => ({
                Id: b._id,
                UserId: b.UserId,
                HName: b.HName,
                City: b.City,
                TRoom: b.TRoom,
                TDay: b.TDay,
                CheckIn: b.CheckIn,
                CheckOut: b.CheckOut,
                Address: b.Address,
                leadguest: b.leadguest,
                RoomType: b.RoomType,
                TGuest: b.TGuest,
                BookingStatus: b.BookingStatus,
              }));
              res.send(arr);
            } else res.sendStatus(401);
          }
        });
    }
  });
});

// for posting Flight book acc to user
app.post("/flightbook/:id", function (req, res) {
  const id = req.params.id;
  console.log("in post request of Flight if user", id, req.body);
  let postData = {
    UserId: id,
    GDepartCity: req.body.gDCity,
    GArrivalCity: req.body.gACity,
    GDuration: req.body.gDuration,
    GDepartTime: req.body.gDTime,
    GArrivalTime: req.body.gATime,
    GFlightName: req.body.gFName,
    GFlightCode: req.body.gFCode,
    GDate: req.body.gDate,
    TTravellers: req.body.tGuest,
    flightClass: req.body.flightClass,
    leadGuest: req.body.guest,
    bookingStatus: "Confirmed",
    returnBook: req.body.returnBook,
    RDepartCity: req.body.rDCity,
    RArrivalCity: req.body.rACity,
    RDuration: req.body.rDuration,
    RDepartTime: req.body.rDTime,
    RArrivalTime: req.body.rATime,
    RFlightName: req.body.rFName,
    RFlightCode: req.body.rFCode,
    RDate: req.body.rDate,
  };

  console.log(postData);
  client.connect(function (err, client) {
    console.log("in Db for post hotel", postData);
    if (err) res.sendStatus(401).end();
    else {
      const db = client.db(dbName);
      db.collection("yatraUserFlight").insertOne(postData, function (
        err,
        results
      ) {
        if (err) res.sendStatus(401).end();
        else {
          console.log("DB in post request of flight", results.ops);
          res.send({ status: "Success" });
        }
      });
    }
  });
});

// for getting flight book acc to user
app.get("/flightbook/:id", function (req, res) {
  const id = req.params.id;
  let page = req.query.page;
  page = page ? page : 1;
  console.log("in get request of flight if user", id, page);
  client.connect(function (err, client) {
    console.log("DB in get request of hotel 2 ");
    if (err) res.sendStatus(401);
    else {
      const db = client.db(dbName);
      db.collection("yatraUserFlight")
        .find({ UserId: id })
        .toArray(function (err, results) {
          if (err) res.sendStatus(401);
          else {
            if (results.length > 0 && results) {
              console.log("DB in get request of hotel 3 ", results);
              let arr = results.map((b) => ({
                Id: b._id,
                UserId: id,
                GDepartCity: b.GDepartCity,
                GArrivalCity: b.GArrivalCity,
                GDuration: b.GDuaration,
                GDepartTime: b.GDepartTime,
                GArrivalTime: b.GArrivalTime,
                GFlightName: b.GFlightName,
                GFlightCode: b.GFlightCode,
                GDate: b.GDate,
                TTravellers: b.TTravellers,
                flightClass: b.flightClass,
                leadGuest: b.leadGuest,
                bookingStatus: b.bookingStatus,
                returnBook: b.returnBook,
                RDepartCity: b.RDepartCity,
                RArrivalCity: b.RArrivalCity,
                RDuration: b.RDuration,
                RDepartTime: b.RDepartTime,
                RArrivalTime: b.RArrivalTime,
                RFlightName: b.RFlightName,
                RFlightCode: b.RFlightCode,
                RDate: b.RDate,
              }));

              let pageNumber = +page;
              let pageSize = 1;
              let startIndex = (pageNumber - 1) * pageSize;

              let TempArr = [...arr];
              let flightPage = TempArr.splice(startIndex, pageSize);

              let pageInfo = {
                pageNumber: pageNumber,
                currPage: page,
                pageSize: pageSize,
                startIndex: startIndex,
                totalIteminPage: flightPage.length,
                totalItem: arr.length,
                totalPages: Math.ceil(arr.length / pageSize),
              };
              let obj = {};
              obj.flight = flightPage;
              obj.pageInfo = pageInfo;
              console.log("obj", obj);
              res.send(obj);
            } else res.sendStatus(401);
          }
        });
    }
  });
});

// for getting specific flight book acc to user
app.get("/specificflightbook/:id/:UserId", function (req, res) {
  const id = req.params.id;
  const UserId = req.params.UserId;
  console.log("in get request of specific flight if user", id, UserId);
  client.connect(function (err, client) {
    console.log("DB in get request of ecasg 2 ");
    if (err) res.sendStatus(401);
    else {
      const db = client.db(dbName);
      db.collection("yatraUserFlight")
        .find({ _id: ObjectId(id), UserId: UserId })
        .toArray(function (err, results) {
          if (err) res.sendStatus(401);
          else {
            if (results.length > 0 && results) {
              console.log("DB in get request of ecasg 3 ", results);
              let arr = results.map((b) => ({
                Id: b._id,
                UserId: id,
                GDepartCity: b.GDepartCity,
                GArrivalCity: b.GArrivalCity,
                GDuration: b.GDuaration,
                GDepartTime: b.GDepartTime,
                GArrivalTime: b.GArrivalTime,
                GFlightName: b.GFlightName,
                GFlightCode: b.GFlightCode,
                GDate: b.GDate,
                TTravellers: b.TTravellers,
                flightClass: b.flightClass,
                leadGuest: b.leadGuest,
                bookingStatus: b.bookingStatus,
                returnBook: b.returnBook,
                RDepartCity: b.RDepartCity,
                RArrivalCity: b.RArrivalCity,
                RDuration: b.RDuration,
                RDepartTime: b.RDepartTime,
                RArrivalTime: b.RArrivalTime,
                RFlightName: b.RFlightName,
                RFlightCode: b.RFlightCode,
                RDate: b.RDate,
              }));
              res.send(arr);
            } else res.sendStatus(401);
          }
        });
    }
  });
});
