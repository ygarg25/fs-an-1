var express = require("express");
var app = express();
app.use(express.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin,X-Requested-With,Content-Type,Accept"
  );
  res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
  //res.header("Access-Control-Allow-Credentials", true);
  res.setHeader("Access-Control-Expose-Headers", "X-Auth-Token");
  next();
});

const port = 2410;

app.listen(port, () => console.log("listening on port", port));

//for sql connection

var mysql = require("mysql");
var connection = mysql.createConnection({
  host: "remotemysql.com",
  user: "vrgR1vCXfg",
  password: "KEGJz0CcSa",
  database: "vrgR1vCXfg",
  multipleStatements: true,
});

connection.query("SELECT * FROM YatraUser", function (error, results, fields) {
  if (error) console.log("errore", error);
  console.log("DB connected");
  // results.forEach((result) => {
  //   // console.log(result);
  // })
});

// for login and logout

const jwt = require("jsonwebtoken");
var bodyParser = require("body-parser");
var passport = require("passport");
var passportJWT = require("passport-jwt");
app.use(bodyParser.json());

passport.initialize();

var ExtractJWT = passportJWT.ExtractJwt;
var Strategy = passportJWT.Strategy;

var cfg = require("./config.js");

var params = {
  secretOrKey: cfg.jwtSecret,
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
};

var strategy = new Strategy(params, function (payload, done) {
  console.log("strategy", payload);
  connection.query(`SELECT * FROM YatraUser WHERE Id=${payload.id}`, function (
    error,
    results,
    fields
  ) {
    console.log("in query");
    if (error) {
      return done(new Error("user not found"), null);
    } else {
      console.log("DB in get request of user after login");
      return done(null, { id: results[0].Id });
    }
  });
});

passport.use(strategy);

const jwtExpirySeconds = 300000;

app.post("/login", function (req, res, payload) {
  console.log("start", req.body);
  connection.query(
    `SELECT * FROM YatraUser WHERE Email='${req.body.username}' AND EnterKey='${req.body.password}'`,
    function (error, results, fields) {
      console.log("in query");
      if (error) {
        console.log("error", error);
        res.sendStatus(401).end();
      } else {
        console.log("DB in post request of login", results);
        if (results.length > 0) {
          var payload = { id: results[0].Id };
          var token = jwt.sign(payload, cfg.jwtSecret, {
            algorithm: "HS256",
            expiresIn: jwtExpirySeconds,
          });
          console.log("token", token);
          res.setHeader("X-Auth-Token", token);
          res.json({ success: true, token: "bearer " + token });
        } else {
          res.sendStatus(401).end();
        }
      }
    }
  );
});

app.get("/user", passport.authenticate("jwt", { session: false }), function (
  req,
  res
) {
  console.log("ssdddddddddddddddddddddddddddddddd", req.user);

  connection.query(`SELECT * FROM YatraUser WHERE Id=${req.user.id}`, function (
    error,
    results,
    fields
  ) {
    console.log("in query");
    if (error) {
      console.log("in query error", error);
      res.sendStatus(401);
    } else {
      console.log("DB in get request of user after login to get user");
      res.json(results[0]);
    }
  });
});

var logout = require("express-passport-logout");

app.delete(
  "/logout",
  passport.authenticate("jwt", { session: false }),
  logout()
);

app.post("/register", function (req, res) {
  console.log(
    "in post request of new user",
    req.body,
    JSON.stringify(req.body)
  );
  connection.query(
    `INSERT INTO YatraUser(Title,Fname,Lname,Mob,Email,EnterKey) VALUES('${req.body.title}','${req.body.firstName}','${req.body.lastName}','${req.body.mob}','${req.body.email}','${req.body.password}')`,
    function (error, results, fields) {
      console.log("in query");
      if (error) {
        console.log("error", error.errno);
        let Err = { error: error.errno };
        res.send(Err).end();
      } else {
        console.log("DB in post request of new Registeration", results);
        res.end();
      }
    }
  );
});

// for getting and posting user booking and cash detail

//for getting ecash acc to user
app.get("/ecash/:id", function (req, res) {
  const id = req.params.id;
  console.log("in get request of ecash if user", id);
  connection.query(`SELECT * FROM YatraUserEcash WHERE UserId=${id}`, function (
    error,
    results,
    fields
  ) {
    console.log("in query");
    if (error) {
      console.log("error", error.errno);
      res.send(401).end();
    } else {
      console.log("DB in get request of ecasg", results);
      res.send(results);
    }
  });
});

//for posting ecash acc to user
app.post("/ecash/:id", function (req, res) {
  const id = req.params.id;
  console.log("in post request of ecash if user", id, req.body);
  connection.query(
    `INSERT INTO YatraUserEcash(UserId ,Cdate,Credit,Debit) VALUES(${id},"${req.body.date}",${req.body.credit},0)`,
    function (error, results, fields) {
      console.log("in query");
      if (error) {
        console.log("error", error.errno);
        res.sendStatus(401).end();
      } else {
        console.log("DB in post request of ecash", results);
        res.send({ status: "Success" });
      }
    }
  );
});

// for posting hotel book acc to user
app.post("/hotelbook/:id", function (req, res) {
  const id = req.params.id;
  console.log("in post request of hotel if user", id, req.body);
  connection.query(
    `INSERT INTO YatraUserHotel(UserId ,HName ,City ,TRoom  , TDay  , CheckIn , CheckOut ,Address  , leadguest ,RoomType ,TGuest,BookingStatus) VALUES(${id},"${req.body.hName}","${req.body.city}",${req.body.tRoom},${req.body.tDay},"${req.body.checkIn}","${req.body.checkOut}","${req.body.address}","${req.body.guest}","${req.body.roomType}",${req.body.tGuest},"Confirmed")`,
    function (error, results, fields) {
      console.log("in query");
      if (error) {
        console.log("error", error, error.errno);
        res.sendStatus(401).end();
      } else {
        console.log("DB in post request of Hotel", results);
        res.send({ status: "Success" });
      }
    }
  );
});

// for getting hotel book acc to user
app.get("/hotelbook/:id", function (req, res) {
  const id = req.params.id;
  let page = req.query.page;
  page = page ? page : 1;
  console.log("in get request of hotel if user", id, page);
  connection.query(`SELECT * FROM YatraUserHotel WHERE UserId=${id}`, function (
    error,
    results,
    fields
  ) {
    console.log("in query");
    if (error) {
      console.log("error", error, error.errno);
      res.sendStatus(401).end();
    } else {
      console.log("DB in get request of Hotel", results);
      if (results.length > 0) {
        let pageNumber = +page;
        let pageSize = 1;
        let startIndex = (pageNumber - 1) * pageSize;

        let TempArr = [...results];
        let hotelPage = TempArr.splice(startIndex, pageSize);

        let pageInfo = {
          pageNumber: pageNumber,
          currPage: page,
          pageSize: pageSize,
          startIndex: startIndex,
          totalIteminPage: hotelPage.length,
          totalItem: results.length,
          totalPages: Math.ceil(results.length / pageSize),
        };
        let obj = {};
        obj.hotel = hotelPage;
        obj.pageInfo = pageInfo;
        console.log("obj", obj);
        res.send(obj);
      } else {
        res.sendStatus(401).end();
      }
    }
  });
});

// for getting specific hotel book acc to user
app.get("/specifichotelbook/:id/:UserId", function (req, res) {
  const id = req.params.id;
  const UserId = req.params.UserId;
  console.log("in get request of specific hotel if user", id, UserId);
  connection.query(
    `SELECT * FROM YatraUserHotel WHERE Id=${id} AND UserId=${UserId}`,
    function (error, results, fields) {
      console.log("in query");
      if (error) {
        console.log("error", error, error.errno);
        res.sendStatus(401).end();
      } else {
        console.log("DB in get request of specific Hotel", results);
        // res.send(results);
        if (results.length > 0) {
          res.send(results);
        } else {
          res.sendStatus(401).end();
        }
      }
    }
  );
});

// for posting Flight book acc to user
app.post("/flightbook/:id", function (req, res) {
  const id = req.params.id;
  console.log("in post request of Flight if user", id, req.body);
  connection.query(
    `INSERT INTO YatraUserFlight(UserId,GDepartCity,GArrivalCity ,GDuration,GDepartTime ,GArrivalTime ,GFlightName ,GFlightCode ,GDate,TTravellers ,flightClass ,leadGuest ,bookingStatus ,returnBook ,RDepartCity ,RArrivalCity,RDuration,RDepartTime ,RArrivalTime ,RFlightName ,RFlightCode,RDate) VALUES(${id},"${req.body.gDCity}","${req.body.gACity}","${req.body.gDuration}","${req.body.gDTime}","${req.body.gATime}","${req.body.gFName}","${req.body.gFCode}","${req.body.gDate}",${req.body.tGuest},"${req.body.flightClass}","${req.body.guest}","Confirmed","${req.body.returnBook}","${req.body.rDCity}","${req.body.rACity}","${req.body.rDuration}","${req.body.rDTime}","${req.body.rATime}","${req.body.rFName}","${req.body.rFCode}","${req.body.rDate}")`,
    function (error, results, fields) {
      console.log("in query");
      if (error) {
        console.log("error", error, error.errno);
        res.sendStatus(401).end();
      } else {
        console.log("DB in post request of Flight", results);
        res.send({ status: "Success" });
      }
    }
  );
});

// for getting flight book acc to user
app.get("/flightbook/:id", function (req, res) {
  const id = req.params.id;
  let page = req.query.page;
  page = page ? page : 1;
  console.log("in get request of flight if user", id, page);
  connection.query(
    `SELECT * FROM YatraUserFlight WHERE UserId=${id}`,
    function (error, results, fields) {
      console.log("in query");
      if (error) {
        console.log("error", error, error.errno);
        res.sendStatus(401).end();
      } else {
        console.log("DB in get request of flight", results);
        if (results.length > 0) {
          let pageNumber = +page;
          let pageSize = 1;
          let startIndex = (pageNumber - 1) * pageSize;

          let TempArr = [...results];
          let flightPage = TempArr.splice(startIndex, pageSize);

          let pageInfo = {
            pageNumber: pageNumber,
            currPage: page,
            pageSize: pageSize,
            startIndex: startIndex,
            totalIteminPage: flightPage.length,
            totalItem: results.length,
            totalPages: Math.ceil(results.length / pageSize),
          };
          let obj = {};
          obj.flight = flightPage;
          obj.pageInfo = pageInfo;
          console.log("obj", obj);
          res.send(obj);
        } else {
          res.sendStatus(401).end();
        }
      }
    }
  );
});

// for getting specific flight book acc to user
app.get("/specificflightbook/:id/:UserId", function (req, res) {
  const id = req.params.id;
  const UserId = req.params.UserId;
  console.log("in get request of specific flight if user", id, UserId);
  connection.query(
    `SELECT * FROM YatraUserFlight WHERE Id=${id} AND UserId=${UserId}`,
    function (error, results, fields) {
      console.log("in query");
      if (error) {
        console.log("error", error, error.errno);
        res.sendStatus(401).end();
      } else {
        console.log("DB in get request of specific flight", results);
        // res.send(results);
        if (results.length > 0) {
          res.send(results);
        } else {
          res.sendStatus(401).end();
        }
      }
    }
  );
});
