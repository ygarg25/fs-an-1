import React, { Component } from "react";
import { Link } from "react-router-dom";
import UserHotel from "./userHotel";
import UserFlight from "./userFlight";
import HotelItinerary from "./hotelItenary";
import FlightItinerary from "./flightItinerary";

class Booking extends Component {
  state = {
    showDetail: "Flights",
  };

  handleShow = (c) => {
    this.setState({ showDetail: c });
  };

  render() {
    const detail = this.props.detail;
    //const { showDetail } = this.state;
    let showDetail = detail;
    let id = this.props.id;
    // console.log("details", showDetail, id);
    // console.log("booking", this.props);
    return (
      <div>
        <div className="row">
          <div
            className="col-12 p-3 pl-5 pb-0"
            style={{ backgroundColor: "#F4F6F7" }}
          >
            {showDetail === "Hotel" ? (
              <Link
                to="/manage-booking/My Booking/Hotel"
                style={{ textDecoration: "none" }}
              >
                <span
                  style={{
                    fontWeight: "450",
                    fontFamily: "Arial",
                    color: "#333",
                    fontSize: "18px",
                    marginRight: "10px",
                    cursor: "pointer",
                    borderBottom: "1px solid red",
                  }}
                  onClick={() => this.handleShow("Hotel")}
                >
                  Hotel
                </span>
              </Link>
            ) : (
              <Link
                to="/manage-booking/My Booking/Hotel"
                style={{ textDecoration: "none" }}
              >
                <span
                  style={{
                    fontWeight: "450",
                    fontFamily: "Arial",
                    color: "#666",
                    fontSize: "18px",
                    marginRight: "10px",
                    cursor: "pointer",
                  }}
                  onClick={() => this.handleShow("Hotel")}
                >
                  Hotel
                </span>
              </Link>
            )}
            {showDetail === "Flights" ? (
              <Link
                to="/manage-booking/My Booking/Flights"
                style={{ textDecoration: "none" }}
              >
                <span
                  style={{
                    fontWeight: "450",
                    fontFamily: "Arial",
                    color: "#333",
                    fontSize: "18px",
                    marginLeft: "10px",
                    cursor: "pointer",
                    borderBottom: "1px solid red",
                  }}
                  onClick={() => this.handleShow("Flights")}
                >
                  Flights
                </span>
              </Link>
            ) : (
              <Link
                to="/manage-booking/My Booking/Flights"
                style={{ textDecoration: "none" }}
              >
                <span
                  style={{
                    fontWeight: "450",
                    fontFamily: "Arial",
                    color: "#666",
                    fontSize: "18px",
                    marginLeft: "10px",
                    cursor: "pointer",
                  }}
                  onClick={() => this.handleShow("Flights")}
                >
                  Flights
                </span>
              </Link>
            )}
          </div>
          <div className="col-12 pt-2">
            <div>
              {!id && showDetail === "Hotel" && <UserHotel {...this.props} />}
            </div>
            <div>
              {!id && showDetail === "Flights" && (
                <UserFlight {...this.props} />
              )}
            </div>
            <div>
              {id && showDetail === "Hotel" && <HotelItinerary id={id} />}
            </div>
            <div>
              {id && showDetail === "Flights" && <FlightItinerary id={id} />}
            </div>
          </div>
        </div>
        <br />
      </div>
    );
  }
}

export default Booking;
