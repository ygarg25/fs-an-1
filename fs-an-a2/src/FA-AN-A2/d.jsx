import React, { Component } from "react";
import { Link } from "react-router-dom";
import UserHotel from "./userHotel";
import UserFlight from "./userFlight";
import HotelItinerary from "./hotelItenary";
import FlightItinerary from "./flightItinerary";

class Booking extends Component {
  state = {
    showDetail: "Flights",
  };

  handleShow = (c) => {
    this.setState({ showDetail: c });
  };

  render() {
    const { showDetail } = this.state;
    let id = this.props.id;
    console.log("details", showDetail, id);
    return (
      <div>
        <div className="row">
          <div
            className="col-12 p-3 pl-5 pb-0"
            style={{ backgroundColor: "#F4F6F7" }}
          >
            {showDetail === "Hotel" ? (
              <Link
                to="/manage-booking/My Booking/Hotel"
                style={{ textDecoration: "none" }}
              >
                <span
                  style={{
                    fontWeight: "450",
                    fontFamily: "Arial",
                    color: "#333",
                    fontSize: "18px",
                    marginRight: "10px",
                    cursor: "pointer",
                    borderBottom: "1px solid red",
                  }}
                  onClick={() => this.handleShow("Hotel")}
                >
                  Hotel
                </span>
              </Link>
            ) : (
              <span
                style={{
                  fontWeight: "450",
                  fontFamily: "Arial",
                  color: "#666",
                  fontSize: "18px",
                  marginRight: "10px",
                  cursor: "pointer",
                }}
                onClick={() => this.handleShow("Hotel")}
              >
                Hotel
              </span>
            )}
            {showDetail === "Flights" ? (
              <span
                style={{
                  fontWeight: "450",
                  fontFamily: "Arial",
                  color: "#333",
                  fontSize: "18px",
                  marginLeft: "10px",
                  cursor: "pointer",
                  borderBottom: "1px solid red",
                }}
                onClick={() => this.handleShow("Flights")}
              >
                Flights
              </span>
            ) : (
              <span
                style={{
                  fontWeight: "450",
                  fontFamily: "Arial",
                  color: "#666",
                  fontSize: "18px",
                  marginLeft: "10px",
                  cursor: "pointer",
                }}
                onClick={() => this.handleShow("Flights")}
              >
                Flights
              </span>
            )}
          </div>
          <div className="col-12 pt-2">
            <div>{!id && showDetail === "Hotel" && <UserHotel />}</div>
            <div>{!id && showDetail === "Flights" && <UserFlight />}</div>
            <div>
              {id && id.substring(0, 6) !== "flight" && (
                <HotelItinerary id={id} />
              )}
            </div>
            <div>
              {id && id.substring(0, 6) === "flight" && (
                <FlightItinerary id={+id[6]} />
              )}
            </div>
          </div>
        </div>
        <br />
      </div>
    );
  }
}

export default Booking;
