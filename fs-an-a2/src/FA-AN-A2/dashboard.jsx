import React, { Component } from "react";
import Navbar from "./navbar";
import UserInfo from "./userinfo";
import LeftPanel from "./leftpanel";
import Profile from "./profile";
import Ecash from "./ecash";
import Booking from "./booking";
import { Link, Redirect } from "react-router-dom";

class Dashboard extends Component {
  state = {};

  render() {
    if (!localStorage.getItem("token")) return <Redirect to="/login" />;
    const opt = this.props.match.params.opt;
    const detail = this.props.match.params.detail;
    const id = this.props.match.params.id;
    // console.log("opt", opt, id);
    // console.log("Dashboard", this.props);
    return (
      <div style={{ backgroundColor: "lightgrey" }}>
        {/* <Navbar /> */}
        <div className="ml-lg-5 ml-2 mr-lg-5 mr-2  mt-lg-0 mt-0  mb-2 row">
          <div className="col-lg-12 d-none d-lg-block">
            <nav
              aria-label="breadcrumb "
              style={{ backgroundColor: "lightgrey" }}
            >
              <ol
                className="breadcrumb"
                style={{ backgroundColor: "lightgrey" }}
              >
                <li className="breadcrumb-item">
                  <a
                    href="#"
                    style={{ color: "black", textDecoration: "none" }}
                  >
                    Dashboard
                  </a>
                </li>

                <li
                  className="breadcrumb-item text-muted"
                  aria-current="page"
                  style={{ backgroundColor: "lightgrey" }}
                >
                  <Link
                    to={`/manage-booking/${opt}`}
                    style={{ textDecoration: "none", color: "black" }}
                  >
                    {opt}
                  </Link>
                </li>
                {detail !== undefined ? (
                  <li
                    className="breadcrumb-item text-muted"
                    aria-current="page"
                    style={{ backgroundColor: "lightgrey" }}
                  >
                    <Link
                      to={`/manage-booking/${opt}/${detail}`}
                      style={{ textDecoration: "none", color: "black" }}
                    >
                      {detail}
                    </Link>
                  </li>
                ) : (
                  ""
                )}
                {id !== undefined ? (
                  <li
                    className="breadcrumb-item text-muted"
                    aria-current="page"
                    style={{ backgroundColor: "lightgrey" }}
                  >
                    {" "}
                    Itinerary Details
                  </li>
                ) : (
                  ""
                )}
              </ol>
            </nav>
          </div>
          <div className="col-lg-2 d-none d-lg-block p-0">
            <LeftPanel opt={opt} />
          </div>
          <div className="col-lg-7 col-12 bg-white">
            {opt === "My Profile" && <Profile />}
            {opt === "My eCash" && <Ecash />}
            {opt === "My Booking" && (
              <Booking id={id} detail={detail} {...this.props} />
            )}
          </div>
          <div className="col-lg-3 d-none d-lg-block">
            <UserInfo />
          </div>
        </div>
        <br />
      </div>
    );
  }
}

export default Dashboard;
