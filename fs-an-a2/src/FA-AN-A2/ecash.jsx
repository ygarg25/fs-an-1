import React, { Component } from "react";
import axios from "axios";
import jwtDecode from "jwt-decode";

const jwt = localStorage.getItem("token");
let userid;
if (jwt) {
  let token = jwt.split(" ")[1];

  userid = jwtDecode(token).id;

  // console.log("user ecash", token, userid);
}

let url = "http://localhost:2410";
// let url = "https://us-central1-yat12-77281.cloudfunctions.net/app";

class Ecash extends Component {
  state = {};
  async componentWillMount() {
    const jwt = localStorage.getItem("token");
    // console.log("get current ecash", jwt);
    try {
      const { data: response } = await axios.get(url + "/ecash/" + userid);
      // console.log("get current ecash", response);
      this.setState({ ecash: response });
    } catch (ex) {
      console.log("catch");
      return null;
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    const jwt = localStorage.getItem("token");
    // console.log("get current ecash", jwt);
    if (prevProps !== currProps) {
      try {
        const { data: response } = await axios.get(url + "/ecash/" + userid);
        // console.log("get current ecash", response);
        this.setState({ ecash: response });
      } catch (ex) {
        console.log("catch");
        return null;
      }
    }
  }
  render() {
    const { ecash } = this.state;
    console.log("ecash", ecash);
    return (
      <div>
        <div className="row">
          <div
            className="col-12 p-3 pl-5"
            style={{ backgroundColor: "#F4F6F7" }}
          >
            <div className="row">
              <div className="col-lg-6 col-12  text-center text-lg-left">
                <span
                  style={{
                    fontWeight: "450",
                    fontFamily: "Arial",
                    color: "#666",
                    fontSize: "14px",
                  }}
                >
                  VIEW ECASH SUMMARY
                </span>
              </div>
              <div className="col-lg-6 col-12 pr-5  text-center text-lg-right">
                <span
                  style={{
                    fontWeight: "450",
                    fontFamily: "Arial",
                    color: "#666",
                    fontSize: "14px",
                  }}
                >
                  CURRENT BALANCE{" "}
                  {this.state.ecash ? (
                    <span>
                      ₹
                      {this.state.ecash.reduce(
                        (acc, curr) => acc + +curr.Credit,
                        0
                      )}
                    </span>
                  ) : (
                    <span>₹0</span>
                  )}
                </span>
              </div>
            </div>
          </div>
          <div
            className="col-11  m-3 ml-4 p-0 border  mt-0 "
            style={{ borderRadius: "10px" }}
          >
            <div className="row ">
              <div
                className="col-lg-3 col-11  ml-3  p-3 pr-2 "
                style={{
                  marginTop: ".5px",
                  backgroundColor: "#F4F6F7",
                  border: "",
                  borderRadius: "10px 0 0 10px",
                }}
              >
                <div
                  className="col-12 pt-4 p-0 bg-white text-center"
                  style={{
                    marginRight: "10px",
                    borderRadius: "10px",
                    border: "1px solid lightgrey",
                  }}
                >
                  <p
                    className="text-danger"
                    style={{
                      fontFamily: "Rubik-Medium ,Arial",
                      fontSize: "20px",
                      fontWeight: "500",
                    }}
                  >
                    REDEEMABLE
                  </p>
                  <strong
                    style={{
                      fontFamily: "Rubik-Medium ,Arial",
                      fontSize: "20px",
                      fontWeight: "500",
                    }}
                  >
                    ₹{0}
                  </strong>
                  <br />
                  <div>
                    <hr />
                  </div>
                </div>

                <br />
                <div
                  className="col-12 pt-4 p-0 bg-white text-center"
                  style={{
                    borderRadius: "10px",
                    border: "1px solid lightgrey",
                  }}
                >
                  <p
                    className="text-danger"
                    style={{
                      fontFamily: "Rubik-Medium ,Arial",
                      fontSize: "20px",
                      fontWeight: "500",
                    }}
                  >
                    PENDING
                  </p>
                  <strong
                    style={{
                      fontFamily: "Rubik-Medium ,Arial",
                      fontSize: "20px",
                      fontWeight: "500",
                    }}
                  >
                    ₹{0}
                  </strong>
                  <br />
                  <div>
                    <hr />
                  </div>
                </div>
              </div>
              <div
                className="col mt-2"
                style={{
                  borderRadius: "10px 0 0 10px",
                }}
              >
                <div className="row mt-2">
                  <div className="col-5">
                    <div className="row p-2">
                      <div className="col-lg-8 col-12 ">
                        <span style={{ fontSize: "18px" }}>
                          Transferable &nbsp;
                          <i className="fa fa-exclamation-circle" />
                        </span>{" "}
                        <br />
                        <span style={{ fontSize: "18px" }}>₹{0}</span>
                      </div>
                      <div className="col-lg-4 col-12 ">
                        <button
                          className="btn btn-danger btn-sm"
                          disabled={true}
                          style={{ cursor: "no-drop" }}
                        >
                          Transfer
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="col-1 text-center p-0">
                    <div className="d-none d-lg-block">|</div>
                    <div className="mt-lg-0 mt-5">OR</div>
                    <div className="d-none d-lg-block">|</div>
                  </div>
                  <div className="col-5">
                    <div className="row p-2">
                      <div className="col-lg-8 col-12 ">
                        <span style={{ fontSize: "18px" }}>
                          Convertible &nbsp;
                          <i className="fa fa-exclamation-circle" />
                        </span>{" "}
                        <br />
                        <span style={{ fontSize: "18px" }}>₹{0}</span>
                      </div>
                      <div className="col-lg-4 col-12 ">
                        <button
                          className="btn btn-outline-danger btn-sm"
                          disabled={true}
                          style={{ cursor: "no-drop" }}
                        >
                          Convert
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <br />
                <div className="table-responsive">
                  <table className="table">
                    <thead>
                      <tr>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Reference Number</th>
                        <th>Credit</th>
                        <th>Debit</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.ecash &&
                        this.state.ecash.map((e, i) => (
                          <tr key={i}>
                            <td>{e.Cdate}</td>
                            <td>Earned Using Promocode</td>
                            <td>
                              JHF395444fedgy{" "}
                              {isNaN(e.Id) ? (
                                <span>{e.Id.substring(0, 4)}</span>
                              ) : (
                                <span>{e.Id}</span>
                              )}
                              {e.UserId.substring(0, 4)}
                            </td>
                            <td>₹{e.Credit}</td>
                            <td>₹{e.Debit}</td>
                          </tr>
                        ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br />
      </div>
    );
  }
}

export default Ecash;
