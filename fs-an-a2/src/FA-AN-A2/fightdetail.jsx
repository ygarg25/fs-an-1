import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import queryString from "query-string";
import Navbar from "./navbar";

let flightCheckBox = [];
let airbusCheckBox = [];
let priceRadio = [];
let timeRadio = [];

let url = "https://us-central1-yvbty-ab7f2.cloudfunctions.net/app/flights";

class FlightDetail extends Component {
  state = {
    ticketDetail: this.props.ticketDetail,
    // ticketDetail: {
    //   dept: "New Delhi (DEL)",
    //   going: "Mumbai (BOM)",
    //   deptdate: "Wed Apr 15 2020 06:52:57 GMT+0530 (India Standard Time)",
    //   returndate: "Wed Apr 15 2020 06:52:57 GMT+0530 (India Standard Time)",
    //   tclass: "Economy",
    //   totalpasanger: "2",
    //   returnstatus: true,
    //   adult: 1,
    //   child: 0,
    //   infants: 0,
    // },
    filter: false,
    flightData: ["Go Air", "Indigo", "SpiceJet", "Air India"],
    airbusData: [
      "Airbus A320 Neo",
      "AirbusA320",
      "Boeing737",
      "Airbus A320-100",
    ],
    priceData: ["0-5000", "5000-10000", "10000-15000", "15000-20000"],
    timeData: ["0-6", "6-12", "12-18", "18-00"],
    returnData: { deptData: "", deptValue: 0, rData: "", rValue: 0 },
  };

  async componentWillMount() {
    let { time, flight, airbus, sort, price } = queryString.parse(
      this.props.location.search
    );
    const { ticketDetail: td } = this.state;
    let params = "";
    params = this.addToParams(params, "time", time);
    params = this.addToParams(params, "flight", flight);
    params = this.addToParams(params, "airbus", airbus);
    params = this.addToParams(params, "sort", sort);
    params = this.addToParams(params, "price", price);
    let apiForGoing = url + "/" + td.dept + "/" + td.going;
    let apiForReturn = url + "/" + td.going + "/" + td.dept;
    if (params) apiForGoing += params;
    if (params) apiForReturn += params;
    let { data: deptFlight } = await axios.get(apiForGoing);
    let { data: returnFlight } = await axios.get(apiForReturn);
    this.setState({ deptFlight: deptFlight, returnFlight: returnFlight });
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    let { time, flight, airbus, sort, price } = queryString.parse(
      this.props.location.search
    );
    const { ticketDetail: td } = this.state;
    let params = "";
    params = this.addToParams(params, "time", time);
    params = this.addToParams(params, "flight", flight);
    params = this.addToParams(params, "airbus", airbus);
    params = this.addToParams(params, "sort", sort);
    params = this.addToParams(params, "price", price);
    let apiForGoing = url + "/" + td.dept + "/" + td.going;
    let apiForReturn = url + "/" + td.going + "/" + td.dept;
    if (params) apiForGoing += params;
    if (params) apiForReturn += params;
    if (currProps !== prevProps) {
      let { data: deptFlight } = await axios.get(apiForGoing);
      let { data: returnFlight } = await axios.get(apiForReturn);
      this.setState({ deptFlight: deptFlight, returnFlight: returnFlight });
    }
  }

  makeCbStructure(Data, sel) {
    let temp = Data.map((n1) => ({
      name: n1,
      isSelected: false,
    }));
    let cbData = sel.split(",");
    for (let i = 0; i < cbData.length; i++) {
      let obj = temp.find((n1) => n1.name === cbData[i]);
      if (obj) obj.isSelected = true;
    }
    return temp;
  }

  makeRadioStructure(data, sel) {
    let radio = {
      sectionData: data,
      selectedData: sel,
    };
    return radio;
  }

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  callUrl = (params, time, flight, airbus, sort, price) => {
    let path = "/yatra/airSearch";
    params = this.addToParams(params, "time", time);
    params = this.addToParams(params, "flight", flight);
    params = this.addToParams(params, "airbus", airbus);
    params = this.addToParams(params, "sort", sort);
    params = this.addToParams(params, "price", price);
    this.state.filter = false;
    this.props.history.push({ pathname: path, search: params });
  };

  handleChange = (e) => {
    let { time, flight, airbus, sort, price } = queryString.parse(
      this.props.location.search
    );
    const { currentTarget: input } = e;
    if (input.type === "checkbox") {
      if (input.id === "flight") {
        let cb = flightCheckBox.find((n1) => n1.name === input.name);
        if (cb) cb.isSelected = input.checked;
        let filteredFlight = flightCheckBox.filter((n1) => n1.isSelected);
        let arrayFlight = filteredFlight.map((n1) => n1.name);
        let selFlight = arrayFlight.join(",");
        flight = selFlight;
      }
      if (input.id === "airbus") {
        let cb = airbusCheckBox.find((n1) => n1.name === input.name);
        if (cb) cb.isSelected = input.checked;
        let filteredAirbus = airbusCheckBox.filter((n1) => n1.isSelected);
        let arrayAirbus = filteredAirbus.map((n1) => n1.name);
        let selAirbus = arrayAirbus.join(",");
        airbus = selAirbus;
      }
    }
    if (input.name === "price") {
      priceRadio.selectedData = input.value;
      price = input.value;
    }
    if (input.name === "time") {
      timeRadio.selectedData = input.value;
      time = input.value;
    }
    this.callUrl("", time, flight, airbus, sort, price);
  };

  handleFilter = (t) => {
    this.setState({ filter: t });
  };

  handleSort = (s) => {
    let { time, flight, airbus, sort, price } = queryString.parse(
      this.props.location.search
    );
    sort = s;
    this.callUrl("", time, flight, airbus, sort, price);
  };

  handleShow = (a, i) => {
    let deptFlight = [...this.state.deptFlight];
    let returnFlight = [...this.state.returnFlight];
    if (a === 0) {
      deptFlight[i].show = !this.state.deptFlight[i].show;
    }
    if (a === 1) {
      returnFlight[i].show = !this.state.returnFlight[i].show;
    }
    this.setState({ deptFlight: deptFlight, returnFlight: returnFlight });
  };

  clearFilter = () => {
    let { time, flight, airbus, sort, price } = queryString.parse(
      this.props.location.search
    );

    this.callUrl("", "", "", "", sort, "");
  };

  handleTicketBook = (d, a) => {
    const { ticketDetail: t } = this.state;
    let data = {};
    let date = "";
    let retData = "";
    let dateData = t.deptdate.split(" ");
    let rdata = t.returndate.split(" ");
    if (rdata) retData += rdata[2] + " " + rdata[1] + " " + rdata[3];
    if (dateData) date += dateData[2] + " " + dateData[1] + " " + dateData[3];
    data.departure = d;
    data.departure.dept = t.dept;
    data.departure.arr = t.going;
    data.departure.pickedDate = date;
    data.departure.returnDate = retData;
    data.departure.count = t.totalpasanger;
    data.departure.type = t.tclass;
    data.departure.travellers = [
      { name: "Adult", count: t.adult },
      { name: "Child", count: t.child },
      { name: "Infant", count: t.infants },
    ];
    data.arrival = a;
    this.props.onBook(data);
    this.props.history.push("/booking");
  };

  handleChange1 = (e) => {
    const returnData = { ...this.state.returnData };
    const { currentTarget: input } = e;
    returnData[input.name] = input.value;
    this.setState({ returnData: returnData });
  };

  render() {
    if (this.state.deptFlight) {
      this.state.returnData.deptData = this.state.deptFlight[
        this.state.returnData.deptValue
      ];
      this.state.returnData.rData = this.state.returnFlight[
        this.state.returnData.rValue
      ];
    }
    const {
      ticketDetail: td,
      filter,
      deptFlight: df,
      returnFlight: rf,
      timeData,
      flightData,
      airbusData,
      priceData,
      returnData: rd,
    } = this.state;
    let { time, flight, airbus, sort, price } = queryString.parse(
      this.props.location.search
    );

    time = time ? time : "";
    flight = flight ? flight : "";
    airbus = airbus ? airbus : "";
    sort = sort ? sort : "";
    price = price ? price : "";

    let date = "";
    if (df) {
      let dateData = td.deptdate.split(" ");
      date += dateData[2] + " " + dateData[1] + " " + dateData[3];
    }

    if (this.state.deptFlight) {
      if (this.state.deptFlight.length <= 0) alert("No  Flight Found");
    }
    timeRadio = this.makeRadioStructure(timeData, time);
    flightCheckBox = this.makeCbStructure(flightData, flight);
    airbusCheckBox = this.makeCbStructure(airbusData, airbus);
    priceRadio = this.makeRadioStructure(priceData, price);

    //console.log("flight detail", this.state.ticketDetail, date);
    //console.log(" detail", df, rf);
    //console.log("ddd", this.state.returnData);
    return (
      <div className="container-fluid">
        {/* <Navbar /> */}
        <br />
        <div className=" mt-2">
          <div
            className="row pt-2 pb-2"
            style={{
              background: "linear-gradient(to right, #b7243a, #132522)",
              color: "white",
            }}
          >
            <div className="col-1 text-center mt-1 d-none d-lg-block">
              <i className="fa fa-fighter-jet" style={{ fontSize: "26px" }} />
            </div>
            <div className="col-lg-2 col-5">
              <div className="row">
                <div className="col">From</div>
              </div>
              <div className="row">
                <div className="col">
                  <b>{td.dept}</b>
                </div>
              </div>
            </div>
            <div className="col-1 mt-1">
              <i className="fa fa-exchange" />
            </div>
            <div className="col-lg-2 col-5">
              <div className="row">
                <div className="col">To</div>
              </div>
              <div className="row">
                <div className="col">
                  <b>{td.going}</b>
                </div>
              </div>
            </div>
            <div className="col-2 d-none d-lg-block">
              <div
                className="row"
                style={{ fontSize: ".71429rem", fontFamily: "Rubik-Medium" }}
              >
                Date
              </div>
              <div
                className="row mt-1"
                style={{ fontSize: "1.14286rem", fontFamily: "Rubik-Medium" }}
              >
                {date}
              </div>
            </div>
            <div className="col-2 d-none d-lg-block">
              <div
                className="row"
                style={{ fontSize: ".71429rem", fontFamily: "Rubik-Medium" }}
              >
                Traveller(s)
              </div>
              <div
                className="row mt-1"
                style={{ fontSize: "1.14286rem", fontFamily: "Rubik-Medium" }}
              >
                <b>
                  {td.totalpasanger}, {td.tclass}
                </b>
              </div>
            </div>
            <div className="col-2 mt-1 d-none d-lg-block">
              <Link to="/">
                <button
                  className="btn btn-danger text-white router-link-active"
                  tabIndex="0"
                >
                  Search Again
                </button>
              </Link>
            </div>
          </div>
          {filter === false ? (
            <div>
              <div
                className="row pt-2 pb-2 bg-light"
                style={{ boxShadow: "0 2px 4px 0 #ffa2a2" }}
              >
                <div
                  className="col-1 text-center d-none d-lg-block"
                  style={{ fontSize: "14px" }}
                >
                  <i className="fa fa-filter" /> Filter
                </div>
                <div
                  className="col-lg-2 col-3 text-center p-0"
                  style={{ cursor: "pointer", fontSize: "3em+1vw" }}
                  onClick={() => this.handleFilter(true)}
                >
                  Price <i className="fa fa-angle-down" />
                </div>
                <div
                  className="col-lg-2 col-3 text-center p-0"
                  style={{ cursor: "pointer", fontSize: "3em+1vw" }}
                  onClick={() => this.handleFilter(true)}
                >
                  Depart <i className="fa fa-angle-down" />
                </div>
                <div
                  className="col-lg-2 col-3 text-center p-0"
                  style={{ cursor: "pointer", fontSize: "3em+1vw" }}
                  onClick={() => this.handleFilter(true)}
                >
                  Airline <i className="fa fa-angle-down" />
                </div>
                <div
                  className="col-lg-2 col-3 text-center p-0"
                  style={{ cursor: "pointer", fontSize: "3em+1em" }}
                  onClick={() => this.handleFilter(true)}
                >
                  Aircraft <i className="fa fa-angle-down" />
                </div>
                {price || airbus || flight || time ? (
                  <div className="col-2">
                    <button
                      className="btn btn-danger text-white "
                      onClick={() => this.clearFilter()}
                    >
                      Clear Filters
                    </button>
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
          ) : (
            <div>
              <div
                className="row pt-2 pb-2 bg-light"
                style={{ boxShadow: "0 2px 4px 0 #ffa2a2" }}
              >
                <div
                  className="col-1 text-center d-none d-lg-block"
                  style={{ fontSize: "14px" }}
                >
                  <i className="fa fa-filter" /> Filter
                </div>
                <div className="col-lg-8 col-3 text-right">
                  <button
                    className="btn btn-outline-dark text-secondary"
                    onClick={() => this.handleFilter(false)}
                  >
                    <strong>Cancel</strong>
                  </button>
                </div>
                {price || airbus || flight || time ? (
                  <div className="col-2">
                    <button
                      className="btn btn-danger text-white "
                      onClick={() => this.clearFilter()}
                    >
                      Clear Filters
                    </button>
                  </div>
                ) : (
                  ""
                )}
              </div>
              <div className="row">
                <div className="col-lg-3 col-3 ">
                  <div className="row ml-1">
                    <strong>Price</strong>
                  </div>
                  {priceRadio.sectionData.map((item, index) => (
                    <div className="form-check pl-3" key={index}>
                      <input
                        value={item}
                        onChange={this.handleChange}
                        id={item}
                        type="Radio"
                        name="price"
                        checked={item === priceRadio.selectedData}
                        className="form-check-input"
                      />
                      <label className="form-check-label" htmlFor={item}>
                        {item}
                      </label>
                    </div>
                  ))}
                </div>
                <div className="col-lg-3 col-3 ">
                  <div className="row ml-1">
                    <strong>Time</strong>
                  </div>
                  {timeRadio.sectionData.map((item, index) => (
                    <div className="form-check" key={index}>
                      <input
                        value={item}
                        onChange={this.handleChange}
                        id={item}
                        type="Radio"
                        name="time"
                        checked={item === timeRadio.selectedData}
                        className="form-check-input"
                      />
                      <label className="form-check-label" htmlFor={item}>
                        {item}
                      </label>
                    </div>
                  ))}
                </div>
                <div className="col-lg-3 col-3 ">
                  <div className="row ml-1">
                    <strong>AirLine</strong>
                  </div>
                  {flightCheckBox.map((item, index) => (
                    <div className="form-check" key={index}>
                      <input
                        value={item.isSelected}
                        onChange={this.handleChange}
                        id="flight"
                        type="checkbox"
                        name={item.name}
                        checked={item.isSelected}
                        className="form-check-input"
                      />
                      <label className="form-check-label" htmlFor={item.name}>
                        {item.name}
                      </label>
                    </div>
                  ))}
                </div>
                <div className="col-lg-3 col-3 ">
                  <div className="row ml-1">
                    <strong>AirCraft</strong>
                  </div>
                  {airbusCheckBox.map((item, index) => (
                    <div className="form-check" key={index}>
                      <input
                        value={item.isSelected}
                        onChange={this.handleChange}
                        id="airbus"
                        type="checkbox"
                        name={item.name}
                        checked={item.isSelected}
                        className="form-check-input"
                      />
                      <label className="form-check-label" htmlFor={item.name}>
                        {item.name}
                      </label>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          )}
          <br />
          {td.returnstatus === false && this.state.deptFlight && (
            <div className="row">
              <div className="col-lg-9 col-12">
                <div
                  className="row pl-4 pt-1 pb-1 mb-4"
                  style={{
                    backgroundColor: "#E5E7EB",
                    borderRadius: "3px",
                    marginLeft: "7px",
                  }}
                >
                  <div className="col-2">
                    <strong>Sort By:</strong>
                  </div>
                  {sort === "arrival" ? (
                    <div
                      className="col-lg-2 col-3 text-right text-primary"
                      style={{ fontSize: ".78571rem", cursor: "pointer" }}
                      onClick={() => this.handleSort("")}
                    >
                      ARRIVAL <i className="fa fa-arrow-up" />
                    </div>
                  ) : (
                    <div
                      className="col-lg-2 col-3 text-right "
                      style={{ fontSize: ".78571rem", cursor: "pointer" }}
                      onClick={() => this.handleSort("arrival")}
                    >
                      ARRIVAL
                    </div>
                  )}
                  {sort === "departure" ? (
                    <div
                      className="col-lg-2 col-3 text-right text-primary"
                      style={{ fontSize: ".78571rem", cursor: "pointer" }}
                      onClick={() => this.handleSort("")}
                    >
                      DEPART <i className="fa fa-arrow-up" />
                    </div>
                  ) : (
                    <div
                      className="col-lg-2 col-3 text-right "
                      style={{ fontSize: ".78571rem", cursor: "pointer" }}
                      onClick={() => this.handleSort("departure")}
                    >
                      DEPART
                    </div>
                  )}
                  {sort === "price" ? (
                    <div
                      className="col-lg-2 col-3 text-right text-primary"
                      style={{ fontSize: ".78571rem", cursor: "pointer" }}
                      onClick={() => this.handleSort("")}
                    >
                      PRICE <i className="fa fa-arrow-up" />
                    </div>
                  ) : (
                    <div
                      className="col-lg-2 col-3 text-right "
                      style={{ fontSize: ".78571rem", cursor: "pointer" }}
                      onClick={() => this.handleSort("price")}
                    >
                      PRICE
                    </div>
                  )}
                </div>
                <br />
                {this.state.deptFlight && (
                  <div>
                    {df.map((d, i) => (
                      <div key={i}>
                        <div className="row mt-1 ml-3 pt-2">
                          <div
                            className="col border p-2"
                            style={{
                              borderRadius: "5px",
                              boxShadow: "2px 2px 4px lightgrey",
                            }}
                          >
                            <div className="row mb-1">
                              <div className="col-lg-1 col-2">
                                <img
                                  src={d.logo}
                                  style={{
                                    width: "28px",
                                    height: "28px",
                                    borderRadius: "5px",
                                  }}
                                />
                              </div>
                              <div className="col-lg-1 col-3">
                                <div
                                  className="row"
                                  style={{
                                    fontSize: ".92857rem",
                                    fontFamily: "Rubik-Regular",
                                  }}
                                >
                                  <div className="col d-none d-lg-block">
                                    {d.name}
                                  </div>
                                </div>
                                <div
                                  className="row text-muted"
                                  style={{
                                    fontSize: ".78751rem",
                                    fontFamily: "Rubik-Regular",
                                    color: "#999",
                                  }}
                                >
                                  {d.code}
                                </div>
                              </div>
                              <div
                                className="col-lg-1 col-3 text-right"
                                style={{
                                  fontSize: "1.07413rem",
                                  fontFamily: "Rubik-Regular",
                                  fontWeight: "400",
                                }}
                              >
                                <div className="row">
                                  <strong>{d.timeDept}</strong>
                                </div>
                                <div
                                  className="row"
                                  style={{
                                    fontSize: "x-small",
                                  }}
                                >
                                  {d.desDept}
                                </div>
                              </div>
                              <div className="col-1 d-none d-lg-block">
                                <hr />
                              </div>
                              <div
                                className="col-lg-1 col-3 text-left"
                                style={{
                                  fontSize: "1.07143rem",
                                  fontFamily: "Rubik-Regualr",
                                  fontWeight: "400",
                                }}
                              >
                                <div className="row">
                                  <strong>{d.timeArr}</strong>
                                </div>
                                <div
                                  className="row"
                                  style={{
                                    fontSize: "x-small",
                                  }}
                                >
                                  {d.desArr}
                                </div>
                              </div>
                              <div className="col-1 pt-1 d-none d-lg-block">
                                <span
                                  style={{
                                    borderLeft: "3px solid lightgrey",
                                    height: "60px",
                                  }}
                                ></span>
                              </div>
                              <div
                                className="col-1 d-none d-lg-block text-left"
                                style={{
                                  fontSize: ".78571rem",
                                  fontFamily: "Rubik-Regular",
                                }}
                              >
                                <div className="row">
                                  <strong>{d.total}</strong>
                                </div>
                                <div className="row">Non Stop</div>
                              </div>
                              <div className="col-2 d-none d-lg-block"></div>
                              <div
                                className="col-lg-2 col-5 text-right"
                                style={{ fontSize: "1.28571rem" }}
                              >
                                <strong>₹ {d.Price}</strong>
                              </div>
                              <div className="col-lg-1 col-3 text-right">
                                <button
                                  className="btn btn-sm btn-outline-danger "
                                  onClick={() => this.handleTicketBook(d, {})}
                                >
                                  <strong>Book</strong>
                                </button>
                              </div>
                            </div>
                            <hr style={{ padding: "0", margin: "0" }} />
                            {!d.show ? (
                              <div className="row pb-1">
                                <div className="col-7 d-none d-lg-block">
                                  <a
                                    style={{
                                      fontSize: "12px",
                                      color: "blue",
                                      cursor: "pointer",
                                    }}
                                    onClick={() => this.handleShow(0, i)}
                                  >
                                    Flight Details{" "}
                                    <i className="fa fa-angle-down" />
                                  </a>
                                </div>
                                <div className="col-5 text-right d-none d-lg-block">
                                  <span
                                    className="border "
                                    style={{ borderRadius: "5px" }}
                                  >
                                    eCash
                                  </span>
                                  <span
                                    className="bg-warning border"
                                    style={{ borderRadius: "5px" }}
                                  >
                                    <i className="fa fa-inr" /> {"  "}250
                                  </span>
                                </div>
                              </div>
                            ) : (
                              <div>
                                <div className="row pb-1">
                                  <div className="col-7 d-none d-lg-block">
                                    <a
                                      style={{
                                        fontSize: "12px",
                                        color: "blue",
                                        cursor: "pointer",
                                      }}
                                      onClick={() => this.handleShow(0, i)}
                                    >
                                      Flight Details{" "}
                                      <i className="fa fa-angle-up" />
                                    </a>
                                  </div>
                                  <div className="col-5 text-right d-none d-lg-block">
                                    <span
                                      className="border "
                                      style={{ borderRadius: "5px" }}
                                    >
                                      eCash
                                    </span>
                                    <span
                                      className="bg-warning border"
                                      style={{ borderRadius: "5px" }}
                                    >
                                      <i className="fa fa-inr" /> {"  "}250
                                    </span>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-7">
                                    <div
                                      className="row"
                                      style={{
                                        backgroundColor: "#301b41",
                                        fontSize: "20px",
                                        paddingTop: "6px",
                                        paddingBottom: "6px",
                                        paddingLeft: "8px",
                                        color: "white",
                                        fontWeight: "500",
                                      }}
                                    >
                                      Flight Details
                                    </div>
                                    <div className="row">
                                      <div className="col-2">
                                        <img
                                          style={{ width: "40px" }}
                                          src={d.logo}
                                        />
                                      </div>
                                      <div className="col-7 text-left">
                                        <div
                                          className="row"
                                          style={{ fontSize: "12px" }}
                                        >
                                          {d.name}
                                        </div>
                                        <div
                                          className="row"
                                          style={{
                                            fontSize: "10px",
                                            color: "grey",
                                          }}
                                        >
                                          {d.code}
                                        </div>
                                      </div>
                                      <div className="col-3 text-right">
                                        <img
                                          style={{ width: "45px" }}
                                          src={
                                            "https://i.ibb.co/31BTG9K/icons8-food-100.png"
                                          }
                                        />
                                      </div>
                                    </div>
                                    <hr />
                                    <div className="row">
                                      <div
                                        className="col-3 ml-1"
                                        style={{ fontSize: "14px" }}
                                      >
                                        <div className="row ml-1">
                                          {d.desDept}
                                        </div>
                                        <div className="row ml-1">
                                          <strong>{d.timeDept}</strong>
                                        </div>
                                        <div className="row ml-1">{d.T1}</div>
                                      </div>
                                      <div className="col-5">
                                        <div
                                          className="row"
                                          style={{ fontSize: "12px" }}
                                        >
                                          <div className="col text-center">
                                            Time Taken
                                          </div>
                                        </div>
                                        <div className="row">
                                          <div className="col-9">
                                            <hr />
                                          </div>
                                          <div className="col-2 pt-2">
                                            <i className="fa fa-plane" />
                                          </div>
                                        </div>
                                      </div>
                                      <div
                                        className="col-3"
                                        style={{ fontSize: "14px" }}
                                      >
                                        <div className="row">
                                          <strong>{d.desDept}</strong>
                                        </div>
                                        <div className="row">
                                          <strong> {d.timeArr}</strong>
                                        </div>
                                        <div
                                          className="row "
                                          style={{ fontSize: "12px" }}
                                        >
                                          {d.T2}
                                        </div>
                                      </div>
                                    </div>
                                    <br />
                                    <div
                                      className="row ml-2 mr-2"
                                      style={{
                                        fontSize: "12px",
                                        borderRadius: "5px",
                                        backgroundColor: "#F9F9F9",
                                      }}
                                    >
                                      <div className="col text-center">
                                        Checkin Baggage{"  "}
                                        <i className="fa fa-briefcase" /> {"  "}
                                        {d.checkin}Kg
                                      </div>
                                    </div>
                                  </div>
                                  <div
                                    className="col-5"
                                    style={{
                                      backgroundColor: "#5a426d",
                                      color: "white",
                                    }}
                                  >
                                    <div className="row pl-1 pt-3 pb-3">
                                      <div className="col-6 border-bottom">
                                        Fare Summary
                                      </div>
                                      <div className="col-6">Fare Rules</div>
                                    </div>
                                    <div>
                                      <div
                                        className="row"
                                        style={{ fontSize: "14px" }}
                                      >
                                        <div className="col-4">
                                          Fare Summary
                                        </div>
                                        <div className="col-4">
                                          Base and Fare
                                        </div>
                                        <div className="col-4">
                                          Fees and Taxes
                                        </div>
                                      </div>
                                      <div
                                        className="row"
                                        style={{
                                          fontSize: "12px",
                                          color: "#fff",
                                        }}
                                      >
                                        <div className="col-4">Adult X 1</div>
                                        <div className="col-4">₹ {d.Price}</div>
                                        <div className="col-4">₹ 1000</div>
                                      </div>
                                      <hr />
                                      <div
                                        className="row"
                                        style={{ fontSize: "16px" }}
                                      >
                                        <div className="col-6">You Pay:</div>
                                        <div className="col-6 text-right">
                                          ₹ {d.Price}
                                        </div>
                                      </div>
                                      <div
                                        className="row"
                                        style={{ fontSize: ".85714rem" }}
                                      >
                                        <div className="col-12">
                                          Note: Total fare displayed above has
                                          been rounded off and may show a slight
                                          difference from actual fare.{" "}
                                        </div>
                                      </div>
                                      <div className="row pt-2 pb-2">
                                        <div className="col-12 text-center">
                                          <button
                                            className="btn btn-danger text-white btn-block"
                                            tabIndex="0"
                                            onClick={() =>
                                              this.handleTicketBook(d, {})
                                            }
                                          >
                                            Book
                                          </button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            )}
                          </div>
                        </div>
                        <br />
                      </div>
                    ))}
                  </div>
                )}
              </div>
              <div className="col-2 ml-1 d-none d-lg-block">
                <img
                  src={
                    "https://i.ibb.co/18cngjz/banner-1575268481164-ICICI-Dom-Flight-New-Homepage-SRP-182-X300.png"
                  }
                  style={{ width: "auto" }}
                />
              </div>
            </div>
          )}
          {td.returnstatus === true && this.state.deptFlight && (
            <div className="row">
              <div className="col-lg-9 col-12">
                <div
                  className="row pl-1 pt-1 pb-1 mb-4"
                  style={{
                    backgroundColor: "#E5E7EB",
                    borderRadius: "3px",
                    marginLeft: "7px",
                  }}
                >
                  <div className="col-3 pl-1 p-0">
                    <strong>Sort By:</strong>
                  </div>
                  {sort === "arrival" ? (
                    <div
                      className="col-lg-2 col-3 p-0 pt-1 text-right text-primary"
                      style={{ fontSize: ".785rem", cursor: "pointer" }}
                      onClick={() => this.handleSort("")}
                    >
                      ARRIVAL <i className="fa fa-arrow-up" />
                    </div>
                  ) : (
                    <div
                      className="col-lg-2 col-3 p-0 pt-1 text-right "
                      style={{ fontSize: ".78571rem", cursor: "pointer" }}
                      onClick={() => this.handleSort("arrival")}
                    >
                      ARRIVAL
                    </div>
                  )}
                  {sort === "departure" ? (
                    <div
                      className="col-lg-2 col-3 p-0 pt-1 text-right text-primary"
                      style={{ fontSize: ".78571rem", cursor: "pointer" }}
                      onClick={() => this.handleSort("")}
                    >
                      DEPART <i className="fa fa-arrow-up" />
                    </div>
                  ) : (
                    <div
                      className="col-lg-2 col-3 p-0 pt-1 text-right "
                      style={{ fontSize: ".78571rem", cursor: "pointer" }}
                      onClick={() => this.handleSort("departure")}
                    >
                      DEPART
                    </div>
                  )}
                  {sort === "price" ? (
                    <div
                      className="col-lg-2 col-3 pt-1 text-right text-primary"
                      style={{ fontSize: ".78571rem", cursor: "pointer" }}
                      onClick={() => this.handleSort("")}
                    >
                      PRICE <i className="fa fa-arrow-up" />
                    </div>
                  ) : (
                    <div
                      className="col-lg-2 col-3 pt-1 text-right "
                      style={{ fontSize: ".78571rem", cursor: "pointer" }}
                      onClick={() => this.handleSort("price")}
                    >
                      PRICE
                    </div>
                  )}
                </div>
                <br />
                {this.state.deptFlight && (
                  <div className="row">
                    <div className="col-6">
                      <div
                        className="row pl-4 "
                        style={{
                          backgroundColor: "#E5E7EB",
                          borderRadius: "3px",
                          marginLeft: "7px",
                          fontSize: ".78571rem",
                        }}
                      >
                        <div className="col-12">
                          {td.dept}
                          {"  "}
                          <i className="fa fa-arrow-right" />
                          {"  "}
                          {td.going}
                        </div>
                      </div>
                      <br />
                      {df.map((d, i) => (
                        <div key={i}>
                          <div
                            className="row border mt-2 ml-3 pt-2"
                            style={{
                              borderRadius: "5px",
                              boxShadow: "2px 2px 4px lightgrey",
                            }}
                          >
                            <div className="col p-2">
                              <div className="row">
                                <div className="col-lg-2 col-3">
                                  <img
                                    src={d.logo}
                                    style={{
                                      width: "28px",
                                      height: "28px",
                                    }}
                                  />
                                </div>
                                <div className="col-lg-1 col-3 text-right">
                                  <div className="row">{d.timeDept}</div>
                                  <div
                                    className="row"
                                    style={{
                                      fontSize: "x-small",
                                    }}
                                  >
                                    {d.name}
                                  </div>
                                </div>
                                <div className="col-lg-1 col-2 ">
                                  <hr
                                    style={{ border: "solid 1px lightgrey" }}
                                  />
                                </div>
                                <div className="col-lg-1 col-3 text-left">
                                  <div className="row">{d.timeArr} </div>
                                </div>
                                <div className="col-1 pt-1 d-none d-lg-block">
                                  <span
                                    style={{
                                      borderLeft: "3px solid lightgrey",
                                      height: "60px",
                                    }}
                                  ></span>
                                </div>
                                <div
                                  className="col-lg-3 col-6 "
                                  style={{
                                    fontSize: "10px",
                                    fontFamily: "Rubik-Regular",
                                  }}
                                >
                                  <div className="row ml-1">
                                    <div>{d.total}</div>
                                  </div>
                                  <div className="row ml-1">
                                    <div>Non Stop</div>
                                  </div>
                                </div>
                                <div
                                  className="col-lg-2 col-3 pl-1 text-left"
                                  style={{
                                    fontSize: "13px",
                                    fontFamily: "Rubik-Regular",
                                  }}
                                >
                                  <strong>₹{d.Price}</strong>
                                </div>
                                <div className="col-1 text-right">
                                  <div className="row">
                                    <div className="col-1 text-right">
                                      {" "}
                                      <input
                                        type="radio"
                                        id="DeptF1"
                                        name="deptValue"
                                        onChange={this.handleChange1}
                                        value={+i}
                                        checked={+rd.deptValue === i}
                                      />
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <hr style={{ padding: "0", margin: "0" }} />
                              {!d.show ? (
                                <div className="row pb-1">
                                  <div className="col-7 d-none d-lg-block">
                                    <a
                                      style={{
                                        fontSize: "12px",
                                        color: "blue",
                                        cursor: "pointer",
                                      }}
                                      onClick={() => this.handleShow(0, i)}
                                    >
                                      Flight Details{" "}
                                      <i className="fa fa-angle-down" />
                                    </a>
                                  </div>
                                  <div className="col-5 text-right d-none d-lg-block">
                                    <span
                                      className="border "
                                      style={{ borderRadius: "5px" }}
                                    >
                                      eCash
                                    </span>
                                    <span
                                      className="bg-warning border"
                                      style={{ borderRadius: "5px" }}
                                    >
                                      <i className="fa fa-inr" /> {"  "}250
                                    </span>
                                  </div>
                                </div>
                              ) : (
                                <div>
                                  <div className="row pb-1">
                                    <div className="col-7 d-none d-lg-block">
                                      <a
                                        style={{
                                          fontSize: "12px",
                                          color: "blue",
                                          cursor: "pointer",
                                        }}
                                        onClick={() => this.handleShow(0, i)}
                                      >
                                        Flight Details{" "}
                                        <i className="fa fa-angle-up" />
                                      </a>
                                    </div>
                                    <div className="col-5 text-right d-none d-lg-block">
                                      <span
                                        className="border "
                                        style={{ borderRadius: "5px" }}
                                      >
                                        eCash
                                      </span>
                                      <span
                                        className="bg-warning border"
                                        style={{ borderRadius: "5px" }}
                                      >
                                        <i className="fa fa-inr" /> {"  "}250
                                      </span>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-7">
                                      <div
                                        className="row"
                                        style={{
                                          backgroundColor: "#301b41",
                                          fontSize: "20px",
                                          paddingTop: "6px",
                                          paddingBottom: "6px",
                                          paddingLeft: "8px",
                                          color: "white",
                                          fontWeight: "500",
                                        }}
                                      >
                                        Flight Details
                                      </div>
                                      <div className="row">
                                        <div className="col-2">
                                          <img
                                            style={{ width: "40px" }}
                                            src={d.logo}
                                          />
                                        </div>
                                        <div className="col-7 text-left">
                                          <div
                                            className="row"
                                            style={{ fontSize: "12px" }}
                                          >
                                            {d.name}
                                          </div>
                                          <div
                                            className="row"
                                            style={{
                                              fontSize: "10px",
                                              color: "grey",
                                            }}
                                          >
                                            {d.code}
                                          </div>
                                        </div>
                                        <div className="col-3 text-right">
                                          <img
                                            style={{ width: "45px" }}
                                            src={
                                              "https://i.ibb.co/31BTG9K/icons8-food-100.png"
                                            }
                                          />
                                        </div>
                                      </div>
                                      <hr />
                                      <div className="row">
                                        <div
                                          className="col-3 ml-1"
                                          style={{ fontSize: "14px" }}
                                        >
                                          <div className="row ml-1">
                                            {d.desDept}
                                          </div>
                                          <div className="row ml-1">
                                            <strong>{d.timeDept}</strong>
                                          </div>
                                          <div className="row ml-1">{d.T1}</div>
                                        </div>
                                        <div className="col-5">
                                          <div
                                            className="row"
                                            style={{ fontSize: "12px" }}
                                          >
                                            <div className="col text-center">
                                              Time Taken
                                            </div>
                                          </div>
                                          <div className="row">
                                            <div className="col-9">
                                              <hr />
                                            </div>
                                            <div className="col-2 pt-2">
                                              <i className="fa fa-plane" />
                                            </div>
                                          </div>
                                        </div>
                                        <div
                                          className="col-3"
                                          style={{ fontSize: "14px" }}
                                        >
                                          <div className="row">
                                            <strong>{d.desDept}</strong>
                                          </div>
                                          <div className="row">
                                            <strong> {d.timeArr}</strong>
                                          </div>
                                          <div
                                            className="row "
                                            style={{ fontSize: "12px" }}
                                          >
                                            {d.T2}
                                          </div>
                                        </div>
                                      </div>
                                      <br />
                                      <div
                                        className="row ml-2 mr-2"
                                        style={{
                                          fontSize: "12px",
                                          borderRadius: "5px",
                                          backgroundColor: "#F9F9F9",
                                        }}
                                      >
                                        <div className="col text-center">
                                          Checkin Baggage{"  "}
                                          <i className="fa fa-briefcase" />{" "}
                                          {"  "}
                                          {d.checkin}Kg
                                        </div>
                                      </div>
                                    </div>
                                    <div
                                      className="col-5"
                                      style={{
                                        backgroundColor: "#5a426d",
                                        color: "white",
                                      }}
                                    >
                                      <div className="row pl-1 pt-3 pb-3">
                                        <div className="col-6 border-bottom">
                                          Fare Summary
                                        </div>
                                        <div className="col-6">Fare Rules</div>
                                      </div>
                                      <div>
                                        <div
                                          className="row"
                                          style={{ fontSize: "14px" }}
                                        >
                                          <div className="col-4">
                                            Fare Summary
                                          </div>
                                          <div className="col-4">
                                            Base and Fare
                                          </div>
                                          <div className="col-4">
                                            Fees and Taxes
                                          </div>
                                        </div>
                                        <div
                                          className="row"
                                          style={{
                                            fontSize: "12px",
                                            color: "#fff",
                                          }}
                                        >
                                          <div className="col-4">Adult X 1</div>
                                          <div className="col-4">
                                            ₹ {d.Price}
                                          </div>
                                          <div className="col-4">₹ 1000</div>
                                        </div>
                                        <hr />
                                        <div
                                          className="row"
                                          style={{ fontSize: "16px" }}
                                        >
                                          <div className="col-6">You Pay:</div>
                                          <div className="col-6 text-right">
                                            ₹ {d.Price}
                                          </div>
                                        </div>
                                        <div
                                          className="row"
                                          style={{ fontSize: ".85714rem" }}
                                        >
                                          <div className="col-12">
                                            Note: Total fare displayed above has
                                            been rounded off and may show a
                                            slight difference from actual fare.{" "}
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              )}
                            </div>
                          </div>
                          <br />
                        </div>
                      ))}
                    </div>
                    <div className="col-6">
                      <div
                        className="row pl-4 "
                        style={{
                          backgroundColor: "#E5E7EB",
                          borderRadius: "3px",
                          marginLeft: "7px",
                          fontSize: ".78571rem",
                        }}
                      >
                        <div className="col-12">
                          {td.going}
                          {"  "}
                          <i className="fa fa-arrow-right" />
                          {"  "}
                          {td.dept}
                        </div>
                      </div>
                      <br />
                      {rf.map((d, i) => (
                        <div key={i}>
                          <div
                            className="row border mt-2 ml-3 pt-2"
                            style={{
                              borderRadius: "5px",
                              boxShadow: "2px 2px 4px lightgrey",
                            }}
                          >
                            <div className="col p-2">
                              <div className="row">
                                <div className="col-lg-2 col-3">
                                  <img
                                    src={d.logo}
                                    style={{
                                      width: "28px",
                                      height: "28px",
                                    }}
                                  />
                                </div>
                                <div className="col-lg-1 col-3 text-right">
                                  <div className="row">{d.timeDept}</div>
                                  <div
                                    className="row"
                                    style={{
                                      fontSize: "x-small",
                                    }}
                                  >
                                    {d.name}
                                  </div>
                                </div>
                                <div className="col-lg-1 col-2 ">
                                  <hr
                                    style={{ border: "solid 1px lightgrey" }}
                                  />
                                </div>
                                <div className="col-lg-1 col-3 text-left">
                                  <div className="row">{d.timeArr} </div>
                                </div>
                                <div className="col-1 pt-1 d-none d-lg-block">
                                  <span
                                    style={{
                                      borderLeft: "3px solid lightgrey",
                                      height: "60px",
                                    }}
                                  ></span>
                                </div>
                                <div
                                  className="col-lg-3 col-6"
                                  style={{
                                    fontSize: "10px",
                                    fontFamily: "Rubik-Regular",
                                  }}
                                >
                                  <div className="row ml-1">
                                    <div>{d.total}</div>
                                  </div>
                                  <div className="row ml-1">
                                    <div>Non Stop</div>
                                  </div>
                                </div>
                                <div
                                  className="col-lg-2 col-3 pl-1 text-right"
                                  style={{
                                    fontSize: "13px",
                                    fontFamily: "Rubik-Regular",
                                  }}
                                >
                                  <strong>₹{d.Price}</strong>
                                </div>
                                <div className="col-1 text-right">
                                  <div className="row">
                                    <div className="col-1 text-right">
                                      {" "}
                                      <input
                                        type="radio"
                                        id="rrF1"
                                        name="rValue"
                                        onChange={this.handleChange1}
                                        value={+i}
                                        checked={+rd.rValue === i}
                                      />
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <hr style={{ padding: "0", margin: "0" }} />
                              {!d.show ? (
                                <div className="row pb-1">
                                  <div className="col-7 d-none d-lg-block">
                                    <a
                                      style={{
                                        fontSize: "12px",
                                        color: "blue",
                                        cursor: "pointer",
                                      }}
                                      onClick={() => this.handleShow(0, i)}
                                    >
                                      Flight Details{" "}
                                      <i className="fa fa-angle-down" />
                                    </a>
                                  </div>
                                  <div className="col-5 text-right d-none d-lg-block">
                                    <span
                                      className="border "
                                      style={{ borderRadius: "5px" }}
                                    >
                                      eCash
                                    </span>
                                    <span
                                      className="bg-warning border"
                                      style={{ borderRadius: "5px" }}
                                    >
                                      <i className="fa fa-inr" /> {"  "}250
                                    </span>
                                  </div>
                                </div>
                              ) : (
                                <div>
                                  <div className="row pb-1">
                                    <div className="col-7 d-none d-lg-block">
                                      <a
                                        style={{
                                          fontSize: "12px",
                                          color: "blue",
                                          cursor: "pointer",
                                        }}
                                        onClick={() => this.handleShow(0, i)}
                                      >
                                        Flight Details{" "}
                                        <i className="fa fa-angle-up" />
                                      </a>
                                    </div>
                                    <div className="col-5 text-right d-none d-lg-block">
                                      <span
                                        className="border "
                                        style={{ borderRadius: "5px" }}
                                      >
                                        eCash
                                      </span>
                                      <span
                                        className="bg-warning border"
                                        style={{ borderRadius: "5px" }}
                                      >
                                        <i className="fa fa-inr" /> {"  "}250
                                      </span>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-7">
                                      <div
                                        className="row"
                                        style={{
                                          backgroundColor: "#301b41",
                                          fontSize: "20px",
                                          paddingTop: "6px",
                                          paddingBottom: "6px",
                                          paddingLeft: "8px",
                                          color: "white",
                                          fontWeight: "500",
                                        }}
                                      >
                                        Flight Details
                                      </div>
                                      <div className="row">
                                        <div className="col-2">
                                          <img
                                            style={{ width: "40px" }}
                                            src={d.logo}
                                          />
                                        </div>
                                        <div className="col-7 text-left">
                                          <div
                                            className="row"
                                            style={{ fontSize: "12px" }}
                                          >
                                            {d.name}
                                          </div>
                                          <div
                                            className="row"
                                            style={{
                                              fontSize: "10px",
                                              color: "grey",
                                            }}
                                          >
                                            {d.code}
                                          </div>
                                        </div>
                                        <div className="col-3 text-right">
                                          <img
                                            style={{ width: "45px" }}
                                            src={
                                              "https://i.ibb.co/31BTG9K/icons8-food-100.png"
                                            }
                                          />
                                        </div>
                                      </div>
                                      <hr />
                                      <div className="row">
                                        <div
                                          className="col-3 ml-1"
                                          style={{ fontSize: "14px" }}
                                        >
                                          <div className="row ml-1">
                                            {d.desDept}
                                          </div>
                                          <div className="row ml-1">
                                            <strong>{d.timeDept}</strong>
                                          </div>
                                          <div className="row ml-1">{d.T1}</div>
                                        </div>
                                        <div className="col-5">
                                          <div
                                            className="row"
                                            style={{ fontSize: "12px" }}
                                          >
                                            <div className="col text-center">
                                              Time Taken
                                            </div>
                                          </div>
                                          <div className="row">
                                            <div className="col-9">
                                              <hr />
                                            </div>
                                            <div className="col-2 pt-2">
                                              <i className="fa fa-plane" />
                                            </div>
                                          </div>
                                        </div>
                                        <div
                                          className="col-3"
                                          style={{ fontSize: "14px" }}
                                        >
                                          <div className="row">
                                            <strong>{d.desDept}</strong>
                                          </div>
                                          <div className="row">
                                            <strong> {d.timeArr}</strong>
                                          </div>
                                          <div
                                            className="row "
                                            style={{ fontSize: "12px" }}
                                          >
                                            {d.T2}
                                          </div>
                                        </div>
                                      </div>
                                      <br />
                                      <div
                                        className="row ml-2 mr-2"
                                        style={{
                                          fontSize: "12px",
                                          borderRadius: "5px",
                                          backgroundColor: "#F9F9F9",
                                        }}
                                      >
                                        <div className="col text-center">
                                          Checkin Baggage{"  "}
                                          <i className="fa fa-briefcase" />{" "}
                                          {"  "}
                                          {d.checkin}Kg
                                        </div>
                                      </div>
                                    </div>
                                    <div
                                      className="col-5"
                                      style={{
                                        backgroundColor: "#5a426d",
                                        color: "white",
                                      }}
                                    >
                                      <div className="row pl-1 pt-3 pb-3">
                                        <div className="col-6 border-bottom">
                                          Fare Summary
                                        </div>
                                        <div className="col-6">Fare Rules</div>
                                      </div>
                                      <div>
                                        <div
                                          className="row"
                                          style={{ fontSize: "14px" }}
                                        >
                                          <div className="col-4">
                                            Fare Summary
                                          </div>
                                          <div className="col-4">
                                            Base and Fare
                                          </div>
                                          <div className="col-4">
                                            Fees and Taxes
                                          </div>
                                        </div>
                                        <div
                                          className="row"
                                          style={{
                                            fontSize: "12px",
                                            color: "#fff",
                                          }}
                                        >
                                          <div className="col-4">Adult X 1</div>
                                          <div className="col-4">
                                            ₹ {d.Price}
                                          </div>
                                          <div className="col-4">₹ 1000</div>
                                        </div>
                                        <hr />
                                        <div
                                          className="row"
                                          style={{ fontSize: "16px" }}
                                        >
                                          <div className="col-6">You Pay:</div>
                                          <div className="col-6 text-right">
                                            ₹ {d.Price}
                                          </div>
                                        </div>
                                        <div
                                          className="row"
                                          style={{ fontSize: ".85714rem" }}
                                        >
                                          <div className="col-12">
                                            Note: Total fare displayed above has
                                            been rounded off and may show a
                                            slight difference from actual fare.{" "}
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              )}
                            </div>
                          </div>
                          <br />
                        </div>
                      ))}
                    </div>
                  </div>
                )}
              </div>
              <div className="col-2 ml-1 d-none d-lg-block">
                <img
                  src={
                    "https://i.ibb.co/18cngjz/banner-1575268481164-ICICI-Dom-Flight-New-Homepage-SRP-182-X300.png"
                  }
                  style={{ width: "auto" }}
                />
              </div>
              <div
                className="row fixed-bottom pt-4 pb-4"
                style={{ backgroundColor: "#43264E", color: "white" }}
              >
                <div className="col-4 d-none d-lg-block">
                  <div className="row">
                    <div className="col-3 text-center">
                      <img
                        style={{ width: "40px", borderRadius: "6px" }}
                        src={rd.deptData.logo}
                      />
                    </div>
                    <div
                      className="col-2 d-none d-lg-block"
                      style={{ fontSize: ".85714rem" }}
                    >
                      {rd.deptData.name}
                    </div>
                    <div className="col-2 d-none d-lg-block">
                      {rd.deptData.timeDept}
                    </div>
                    <div className="col-3 d-none d-lg-block">
                      <hr style={{ borderColor: "white" }} />
                    </div>
                    <div className="col-2 d-none d-lg-block">
                      {rd.deptData.timeArr}
                    </div>
                  </div>
                </div>
                <div className="col-4 d-none d-lg-block">
                  <div className="row">
                    <div className="col-3 text-center">
                      <img
                        style={{ width: "40px", borderRadius: "6px" }}
                        src={rd.rData.logo}
                      />
                    </div>
                    <div
                      className="col-2 d-none d-lg-block"
                      style={{ fontSize: ".85714rem" }}
                    >
                      {rd.rData.name}
                    </div>
                    <div className="col-2 d-none d-lg-block">
                      {rd.rData.timeDept}
                    </div>
                    <div className="col-3 d-none d-lg-block">
                      <hr style={{ borderColor: "white" }} />
                    </div>
                    <div className="col-2 d-none d-lg-block">
                      {rd.rData.timeArr}
                    </div>
                  </div>
                </div>
                <div className="col-lg-4 col-12">
                  <div className="row">
                    <div className="col-6 ml-lg-0 ml-1">
                      <div className="row" style={{ fontSize: ".85714rem" }}>
                        <div className="col">Total Fare</div>
                      </div>
                      <div className="row" style={{ fontSize: "1.57143rem" }}>
                        <div className="col">
                          <strong>
                            ₹ {rd.deptData.Price + rd.rData.Price}
                          </strong>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-6 col-4 mt-1">
                      <button
                        className="btn btn-danger text-white"
                        onClick={() =>
                          this.handleTicketBook(rd.deptData, rd.rData)
                        }
                      >
                        Book
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
          <br />
          <br />
          <br />
          <br />
        </div>
      </div>
    );
  }
}

export default FlightDetail;
