import React, { Component } from "react";
import DatePicker from "react-datepicker";

let today = new Date();
let todayDate = today.getDate();
let tM = today.getMonth();
let ty = today.getFullYear();

class Flights extends Component {
  state = {
    cities: [
      "New Delhi (DEL)",
      "Mumbai (BOM)",
      "Banglore (BLR)",
      "Kolkata (CCU)",
    ],
    tripWay: "oneway",
    ticketDetail: {
      dept: "",
      going: "",
      deptdate: "",
      returndate: "",
      tclass: "Economy",
      totalpasanger: "",
      returnstatus: false,
      adult: 1,
      child: 0,
      infants: 0,
    },
    deptdate: new Date(),
    returndate: new Date(ty, tM, todayDate + 1),
    dayData: [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ],

    travellerClass: true,
    adult: 1,
    child: 0,
    infants: 0,
    returnstatus: false,
  };
  handleTrip = (s, t) => {
    this.setState({ tripWay: s, returnstatus: t });
  };

  handleChange = (e) => {
    const { currentTarget: input } = e;
    const ticketDetail = { ...this.state.ticketDetail };
    ticketDetail[input.name] = input.value;
    this.setState({ ticketDetail: ticketDetail });
  };

  checkCity = () => {
    const ticketDetail = { ...this.state.ticketDetail };
    if (ticketDetail.dept) {
      if (ticketDetail.dept === ticketDetail.going) {
        alert("Destination and Arrival have to be different");
      }
    }
  };
  handledropDown = (key) => {
    if (key === "travellerClass") {
      this.setState({ travellerClass: !this.state.travellerClass });
    }
  };

  handleAdult = (i) => {
    this.setState({ adult: this.state.adult + i });
  };
  handleChild = (i) => {
    this.setState({ child: this.state.child + i });
  };
  handleInfants = (i) => {
    this.setState({ infants: this.state.infants + i });
  };

  handleDeptDate = (date) => {
    this.setState({
      deptdate: date,
    });
  };

  handleReturnDate = (date) => {
    this.setState({
      returndate: date,
    });
  };

  handleTicket = () => {
    this.props.ticketBooking(this.state.ticketDetail);
  };

  validate = () => {
    let errs = {};
    if (!this.state.ticketDetail.dept.trim())
      errs.dept = "Departure City is required";

    if (!this.state.ticketDetail.going.trim())
      errs.going = "Arrival City is required";
    if (+this.state.ticketDetail.totalpasanger <= 0)
      errs.pasanger = "Pasanger is required";
    return errs;
  };

  isFormValid = () => {
    let errs = this.validate();
    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };

  render() {
    this.state.ticketDetail.totalpasanger =
      this.state.adult + this.state.child + this.state.infants;
    this.state.ticketDetail.deptdate = "" + this.state.deptdate;
    this.state.ticketDetail.returndate = "" + this.state.returndate;
    this.state.ticketDetail.returnstatus = this.state.returnstatus;
    this.state.ticketDetail.adult = this.state.adult;
    this.state.ticketDetail.child = this.state.child;
    this.state.ticketDetail.infants = this.state.infants;
    let {
      cities,
      tripWay,
      ticketDetail,
      travellerClass,
      adult,
      child,
      infants,
      deptdate,
      returndate,
      dayData,
    } = this.state;
    this.checkCity();
    //console.log("tickr", ticketDetail);
    return (
      <div>
        <div className="row">
          <div className="col text-center">
            {tripWay === "oneway" ? (
              <button
                className="btn btn-sm btn-primary m-1"
                onClick={() => this.handleTrip("oneway", false)}
              >
                One Way
              </button>
            ) : (
              <button
                className="btn btn-sm btn-outline-primary m-1"
                onClick={() => this.handleTrip("oneway", false)}
              >
                One Way
              </button>
            )}
            {tripWay === "return" ? (
              <button
                className="btn btn-sm btn-primary m-1"
                onClick={() => this.handleTrip("return", true)}
              >
                Return
              </button>
            ) : (
              <button
                className="btn btn-sm btn-outline-primary m-1"
                onClick={() => this.handleTrip("return", true)}
              >
                Return
              </button>
            )}
          </div>
        </div>
        <div className="row">
          <div className="col-lg-5 col-5">
            <div className="row">
              <div className="col">
                <strong>Depart From</strong>
              </div>
            </div>
            <div className="row">
              <div className="col">
                <select
                  className="form-control"
                  id="dept"
                  name="dept"
                  value={ticketDetail.dept}
                  onChange={this.handleChange}
                >
                  <option value="">Select City</option>
                  {cities.map((c, i) => (
                    <option key={i}>{c}</option>
                  ))}
                </select>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-1 mt-4 d-none d-lg-block">
            <i
              className="fa fa-arrow-right"
              style={{ fontSize: "40px", color: "#9fdbac" }}
            />
          </div>
          <div className="col-5">
            <div className="row">
              <div className="col">
                <strong>Going TO</strong>
              </div>
            </div>
            <div className="row">
              <div className="col">
                <select
                  className="form-control"
                  id="going"
                  name="going"
                  value={ticketDetail.going}
                  onChange={this.handleChange}
                >
                  <option value="">Select City</option>
                  {cities.map((c, i) => (
                    <option key={i}>{c}</option>
                  ))}
                </select>
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-md-5 col-12 ">
            <h6>Departure Date</h6>

            <DatePicker
              selected={deptdate}
              onChange={this.handleDeptDate}
              minDate={new Date()}
            />
            <span>
              <p>{dayData[deptdate.getDay()]}</p>
            </span>
          </div>
          <div className="col-md-2 d-none d-md-block text-center"></div>
          {tripWay === "oneway" ? (
            <div className="col-md-5 col-12">
              <h6>Return Date</h6>

              <div className="">
                <p>
                  <a href="#" onClick={() => this.handleTrip("return", true)}>
                    <small>Book round trip to save extra</small>
                  </a>
                </p>
              </div>
            </div>
          ) : (
            <div className="col-md-5 col-12">
              <h6>Return Date</h6>
              <DatePicker
                selected={returndate}
                onChange={this.handleReturnDate}
                minDate={deptdate}
              />
              <span>
                <p>{dayData[returndate.getDay()]}</p>
              </span>
            </div>
          )}
        </div>
        <hr />
        <div className="row">
          <div className="col">
            <p>
              <strong>Travellers, Class</strong>
            </p>
          </div>
        </div>
        {travellerClass ? (
          <div className="row">
            <div className="col-6 ">
              <h5>
                {ticketDetail.totalpasanger}{" "}
                {ticketDetail.totalpasanger <= 1 ? "Traveller" : "Travellers"},
                {ticketDetail.tclass}
              </h5>
            </div>
            <div className="col-6 text-right ">
              <i
                className="fa fa-chevron-down mt-4"
                onClick={() => this.handledropDown("travellerClass")}
                style={{ cursor: "pointer" }}
              />
            </div>
          </div>
        ) : (
          <div>
            <div className="row">
              <div className="col-6 ">
                <h5>
                  {ticketDetail.totalpasanger}{" "}
                  {ticketDetail.totalpasanger <= 1 ? "Traveller" : "Travellers"}
                  ,{ticketDetail.tclass}
                </h5>
              </div>
              <div className="col-6 text-right ">
                <i
                  className="fa fa-chevron-up mt-4"
                  onClick={() => this.handledropDown("travellerClass")}
                  style={{ cursor: "pointer" }}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-4">
                <p>
                  <strong>Adult</strong>
                </p>
              </div>

              <div className="col-4">
                <p>
                  <strong>Child</strong>
                  <span className="text-muted">(2-12 yrs)</span>
                </p>
              </div>
              <div className="col-4">
                <p>
                  <strong>Infants</strong>
                  <span className="text-muted">(Below 2 yrs)</span>
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-4">
                {adult < 1 ? (
                  <span
                    className="pl-3 pr-3 border"
                    style={{ cursor: "pointer" }}
                  >
                    -
                  </span>
                ) : (
                  <span
                    className="pl-3 pr-3 border"
                    style={{ cursor: "pointer" }}
                    onClick={() => this.handleAdult(-1)}
                  >
                    -
                  </span>
                )}
                <span className="pl-3 pr-3 border bg-light">{adult}</span>
                <span
                  className="pl-3 pr-3 border"
                  style={{ cursor: "pointer" }}
                  onClick={() => this.handleAdult(1)}
                >
                  +
                </span>
              </div>
              <div className="col-4">
                {child < 1 ? (
                  <span
                    className="pl-3 pr-3 border"
                    style={{ cursor: "pointer" }}
                  >
                    -
                  </span>
                ) : (
                  <span
                    className="pl-3 pr-3 border"
                    style={{ cursor: "pointer" }}
                    onClick={() => this.handleChild(-1)}
                  >
                    -
                  </span>
                )}
                <span className="pl-3 pr-3 border bg-light">{child}</span>
                <span
                  className="pl-3 pr-3 border"
                  style={{ cursor: "pointer" }}
                  onClick={() => this.handleChild(1)}
                >
                  +
                </span>
              </div>
              <div className="col-4">
                {infants < 1 ? (
                  <span
                    className="pl-3 pr-3 border"
                    style={{ cursor: "pointer" }}
                  >
                    -
                  </span>
                ) : (
                  <span
                    className="pl-3 pr-3 border"
                    style={{ cursor: "pointer" }}
                    onClick={() => this.handleInfants(-1)}
                  >
                    -
                  </span>
                )}
                <span className="pl-3 pr-3 border bg-light">{infants}</span>
                <span
                  className="pl-3 pr-3 border"
                  style={{ cursor: "pointer" }}
                  onClick={() => this.handleInfants(1)}
                >
                  +
                </span>
              </div>
            </div>

            <div className="row mt-2 ml-1">
              <div className="col-md-12 text-dark form-check">
                <input
                  onChange={this.handleChange}
                  className="form-check-input"
                  type="radio"
                  name="tclass"
                  id="tclass"
                  value="Economy"
                  checked={ticketDetail.tclass === "Economy"}
                />
                <label className="form-check-label" htmlFor="Economy">
                  Economy
                </label>
              </div>
              <div className="col-md-12 text-dark form-check">
                <input
                  onChange={this.handleChange}
                  className="form-check-input"
                  type="radio"
                  name="tclass"
                  id="tclass"
                  value="Premium Economy"
                  checked={ticketDetail.tclass === "Premium Economy"}
                />
                <label className="form-check-label" htmlFor="Premium Economy">
                  Premium Economy
                </label>
              </div>
              <div className="col-md-12 text-dark form-check">
                <input
                  onChange={this.handleChange}
                  className="form-check-input"
                  type="radio"
                  name="tclass"
                  id="tclass"
                  value="Business"
                  checked={ticketDetail.tclass === "Business"}
                />
                <label className="form-check-label" htmlFor="Business">
                  Business
                </label>
              </div>
            </div>
          </div>
        )}
        <hr />
        <div className="row">
          <div className="col-md-12 text-right">
            <button
              className="btn btn-danger p-2"
              style={{ cursor: "pointer" }}
              disabled={this.isFormValid()}
              onClick={() => this.handleTicket()}
            >
              Search Flights{"  "}
              <i className="fa fa-arrow-right"></i>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Flights;
