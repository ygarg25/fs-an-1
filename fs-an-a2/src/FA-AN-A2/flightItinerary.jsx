import React, { Component } from "react";
import axios from "axios";
import jwtDecode from "jwt-decode";
import { Link } from "react-router-dom";

let url = "http://localhost:2410";
// let url = "https://us-central1-yat12-77281.cloudfunctions.net/app";

const jwt = localStorage.getItem("token");
let userid;
if (jwt) {
  let token = jwt.split(" ")[1];

  userid = jwtDecode(token).id;
  console.log("user booking", token, userid);
}

class FlightItinerary extends Component {
  state = {};

  async componentWillMount() {
    const jwt = localStorage.getItem("token");
    let id = this.props.id;
    console.log("get current flightbook", jwt);
    let page = this.state.currentPage;
    try {
      const { data: response } = await axios.get(
        url + "/specificflightbook/" + id + "/" + userid
      );
      console.log("get current flightbook", response);
      this.setState({ flightbook: response });
    } catch (ex) {
      console.log("catch");
      return null;
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    const jwt = localStorage.getItem("token");
    let id = this.props.id;
    console.log("get current flightbook", jwt);
    let page = this.state.currentPage;
    if (prevProps !== currProps) {
      try {
        const { data: response } = await axios.get(
          url + "/specificFlightbook/" + id + "/" + userid
        );
        console.log("get current flightbook", response);
        this.setState({ flightbook: response });
      } catch (ex) {
        console.log("catch");
        return null;
      }
    }
  }

  render() {
    let id = this.props.id;
    const { flightbook: fb } = this.state;

    console.log("flight,book", fb);
    console.log("flight itinerary", id);
    return (
      <div className="col-12 pt-2">
        <div className="row">
          <div className="col-12">
            {!this.state.flightbook && (
              <div className="text-center pt-5 pb-5">
                <br />
                <h6>No Record To Show of this Flight</h6>
              </div>
            )}
            {this.state.flightbook && (
              <div>
                <div
                  className="row"
                  className="mt-5"
                  style={{
                    border: "1px solid lightgrey",
                    borderRadius: "10px",
                  }}
                >
                  <div
                    className="col-12 pb-1"
                    style={{
                      backgroundColor: "lightgrey",
                      borderRadius: "8px 8px 0 0",
                    }}
                  >
                    <div className="row">
                      <div className="col-lg-7 col-12 text-left">
                        <span style={{ color: "grey", fontSize: "18px" }}>
                          <i className="fa fa-plane" />
                        </span>
                        <span>
                          <span
                            style={{
                              color: "black",
                              fontSize: "13px",
                              marginLeft: "8px",
                            }}
                          >
                            {fb[0].GDepartCity}
                            &nbsp; &nbsp;
                            <i className="fa fa-exchange" />
                            &nbsp;&nbsp;
                            {fb[0].GArrivalCity}
                          </span>
                          {fb[0].returnBook === "true" && <br />}
                          {fb[0].returnBook === "true" && (
                            <span
                              style={{
                                color: "black",
                                fontSize: "13px",
                                marginLeft: "27px",
                              }}
                            >
                              {fb[0].RDepartCity}
                              &nbsp; &nbsp;
                              <i className="fa fa-exchange" />
                              &nbsp;&nbsp;
                              {fb[0].RArrivalCity}
                            </span>
                          )}
                          <br />
                          <span
                            style={{
                              color: "grey",
                              fontSize: "10px",
                              marginLeft: "30px",
                            }}
                          >
                            {fb[0].TTravellers} TRAVELLER(S)
                          </span>
                        </span>
                      </div>
                      <div className="col-lg-5 col-12 text-lg-right text-left">
                        <span style={{ color: "grey", fontSize: "11px" }}>
                          Booked On: {fb[0].GDate} | Booking Ref No. 200456{" "}
                          {isNaN(fb[0].UserId) ? (
                            <span>
                              {fb[0].UserId.substring(0, 4)}
                              {fb[0].Id.substring(0, 4)}
                            </span>
                          ) : (
                            <span>
                              {fb[0].UserId}
                              {fb[0].Id}
                            </span>
                          )}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="row mt-3">
                      <div className="col-lg-3 col-6">
                        <div>
                          <span
                            style={{
                              fontSize: "17px",
                              fontWeight: "450",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            Depart Date
                          </span>
                          <br />
                          <span
                            style={{
                              color: "#888",
                              fontSize: "12px",
                              fontWeight: "",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            {" "}
                            {fb[0].GDate}
                          </span>
                        </div>
                        {fb[0].returnBook === "true" && (
                          <div>
                            <hr />
                          </div>
                        )}
                        {fb[0].returnBook === "true" && (
                          <div>
                            <span
                              style={{
                                fontSize: "17px",
                                fontWeight: "450",
                                fontFamily: "Rubik-Regular,Arial",
                              }}
                            >
                              Return Date
                            </span>
                            <br />
                            <span
                              style={{
                                color: "#888",
                                fontSize: "12px",
                                fontWeight: "",
                                fontFamily: "Rubik-Regular,Arial",
                              }}
                            >
                              {fb[0].RDate}
                            </span>
                          </div>
                        )}
                      </div>
                      <div className="col-lg-3 col-6 ">
                        <div>
                          <span
                            style={{
                              fontSize: "17px",
                              fontWeight: "450",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            Depart Time
                          </span>
                          <br />
                          <span
                            style={{
                              color: "#888",
                              fontSize: "12px",
                              fontWeight: "",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            {" "}
                            {fb[0].GDepartTime}
                          </span>
                        </div>
                        {fb[0].returnBook === "true" && (
                          <div>
                            <hr />
                          </div>
                        )}
                        {fb[0].returnBook === "true" && (
                          <div>
                            <span
                              style={{
                                fontSize: "17px",
                                fontWeight: "450",
                                fontFamily: "Rubik-Regular,Arial",
                              }}
                            >
                              Return Time
                            </span>
                            <br />
                            <span
                              style={{
                                color: "#888",
                                fontSize: "12px",
                                fontWeight: "",
                                fontFamily: "Rubik-Regular,Arial",
                              }}
                            >
                              {fb[0].RDepartTime}
                            </span>
                          </div>
                        )}
                      </div>
                      <div
                        className="col-lg-1 d-none d-lg-block "
                        style={{ borderRight: "1px solid lightgrey" }}
                      ></div>
                      <div className="col-12 d-lg-none d-md-block  ">
                        <hr />
                      </div>
                      <div className="col-lg-2 col-6">
                        {" "}
                        <span
                          style={{
                            fontSize: "17px",
                            fontWeight: "450",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          Traveller(s)
                        </span>
                        <br />
                        <span
                          style={{
                            color: "#888",
                            fontSize: "12px",
                            fontWeight: "",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          {fb[0].TTravellers}
                        </span>
                      </div>
                      <div className="col-lg-3 col-6 ">
                        {" "}
                        <span
                          style={{
                            fontSize: "17px",
                            fontWeight: "450",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          Status
                        </span>
                        <br />
                        <span
                          style={{
                            color: "#888",
                            fontSize: "12px",
                            fontWeight: "",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          {fb[0].bookingStatus}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-12">
                    <hr />
                  </div>
                  <div className="col-12">
                    <div className="row pt-3">
                      <div style={{ borderRight: "1px solid lightgrey" }}></div>
                      <div className="col-lg-2 col-5  text-center mt-3 ">
                        <span
                          style={{
                            cursor: "pointer",
                            border: "1px solid red",
                            borderRadius: "100%",
                            padding: "8px",
                          }}
                        >
                          <i className="fa fa-print" style={{ color: "red" }} />
                        </span>
                        <br />
                        <br />
                        <span
                          style={{
                            fontSize: "13px",
                            fontWeight: "600",
                            cursor: "pointer",
                          }}
                        >
                          Print Invoice
                        </span>
                      </div>
                      <div style={{ borderRight: "1px solid lightgrey" }}></div>
                      <div className="col-lg-3 col-5  text-center mt-3">
                        <span
                          style={{
                            cursor: "pointer",
                            border: "1px solid red",
                            borderRadius: "100%",
                            padding: "8px",
                          }}
                        >
                          <i
                            className="fa fa-location-arrow"
                            style={{ color: "red" }}
                          />
                        </span>
                        <br />
                        <br />
                        <span
                          style={{
                            fontSize: "13px",
                            fontWeight: "600",
                            cursor: "pointer",
                          }}
                        >
                          Get Directions
                        </span>
                      </div>
                      <div style={{ borderRight: "1px solid lightgrey" }}></div>
                      <div className="col-lg-2 col-5 text-center mt-3 ">
                        <span
                          style={{
                            cursor: "pointer",
                            border: "1px solid red",
                            borderRadius: "100%",
                            padding: "8px",
                          }}
                        >
                          <i
                            className="fa fa-refresh"
                            style={{ color: "red" }}
                          />
                        </span>
                        <br />
                        <br />
                        <span
                          style={{
                            fontSize: "13px",
                            fontWeight: "600",
                            cursor: "pointer",
                          }}
                        >
                          Re-Book
                        </span>
                      </div>

                      <div style={{ borderRight: "1px solid lightgrey" }}></div>
                      <div className="col-lg-2 col-5 text-center mt-3 ">
                        <span
                          style={{
                            cursor: "pointer",
                            border: "1px solid red",
                            borderRadius: "100%",
                            padding: "8px",
                          }}
                        >
                          <i
                            className="fa fa-pencil"
                            style={{ color: "red" }}
                          />
                        </span>
                        <br />
                        <br />
                        <span
                          style={{
                            fontSize: "13px",
                            fontWeight: "600",
                            cursor: "pointer",
                          }}
                        >
                          Write to Us
                        </span>
                      </div>

                      <div style={{ borderRight: "1px solid lightgrey" }}></div>
                    </div>
                  </div>
                  <div>
                    <br />
                  </div>
                </div>
                <div
                  className="row"
                  className="mt-5"
                  style={{
                    border: "1px solid lightgrey",
                    borderRadius: "10px",
                  }}
                >
                  <div className="col-12 p-3">
                    <h6>Your Booking Details</h6>
                  </div>
                  <div className="p-2">
                    <hr />
                  </div>
                  <div className="col-12">
                    <div className="row ">
                      <div className="col-lg-3 col-12 ">
                        {" "}
                        <span
                          style={{
                            fontSize: "13px",
                            fontWeight: "450",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          <span style={{ color: "grey", fontSize: "18px" }}>
                            <i className="fa fa-plane" />
                          </span>
                          &nbsp;
                          {fb[0].GFlightName}, {fb[0].GFlightCode}
                        </span>
                        <br />
                        <span
                          style={{
                            color: "#888",
                            fontSize: "12px",
                            fontWeight: "",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          {fb[0].GDepartCity}
                          &nbsp; <i className="fa fa-exchange" />
                          &nbsp;
                          {fb[0].GArrivalCity}
                        </span>
                      </div>
                      <div className="col-12 d-lg-none d-md-block  ">
                        <hr />
                      </div>
                      <div className="col-lg-2 col-3 ">
                        {" "}
                        <span
                          style={{
                            fontSize: "17px",
                            fontWeight: "450",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          Date
                        </span>
                        <br />
                        <span
                          style={{
                            color: "#888",
                            fontSize: "12px",
                            fontWeight: "",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          {fb[0].GDate}
                        </span>
                      </div>
                      <div className="col-lg-2 col-3">
                        <span
                          style={{
                            fontSize: "17px",
                            fontWeight: "450",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          Depart
                        </span>
                        <br />
                        <span
                          style={{
                            color: "#888",
                            fontSize: "12px",
                            fontWeight: "",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          {" "}
                          {fb[0].GDepartTime}
                        </span>
                      </div>
                      <div className="col-lg-2 col-3">
                        <span
                          style={{
                            fontSize: "17px",
                            fontWeight: "450",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          Duration
                        </span>
                        <br />
                        <span
                          style={{
                            color: "#888",
                            fontSize: "12px",
                            fontWeight: "",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          {" "}
                          {fb[0].GDuration}
                        </span>
                      </div>
                      <div className="col-lg-3 col-3 ">
                        <span
                          style={{
                            fontSize: "17px",
                            fontWeight: "450",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          Arrival
                        </span>
                        <br />
                        <span
                          style={{
                            color: "#888",
                            fontSize: "12px",
                            fontWeight: "",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          {" "}
                          {fb[0].GArrivalTime}
                        </span>
                      </div>
                    </div>
                    {fb[0].returnBook === "true" && (
                      <div
                        className="m-2"
                        style={{
                          backgroundColor: "red",
                          padding: "2px 10px 4px",
                          borderRadius: "80px",
                        }}
                      ></div>
                    )}
                    {fb[0].returnBook === "true" && (
                      <div className="row ">
                        <div className="col-lg-3 col-12 ">
                          {" "}
                          <span
                            style={{
                              fontSize: "13px",
                              fontWeight: "450",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            <span style={{ color: "grey", fontSize: "18px" }}>
                              &nbsp; <i className="fa fa-plane fa-rotate-180" />
                              &nbsp;
                            </span>
                            &nbsp;
                            {fb[0].RFlightName}, {fb[0].RFlightCode}
                          </span>
                          <br />
                          <span
                            style={{
                              color: "#888",
                              fontSize: "12px",
                              fontWeight: "",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            {fb[0].RDepartCity}
                            &nbsp;
                            <i className="fa fa-exchange" />
                            &nbsp;
                            {fb[0].RArrivalCity}
                          </span>
                        </div>
                        <div className="col-12 d-lg-none d-md-block  ">
                          <hr />
                        </div>
                        <div className="col-lg-2 col-3 ">
                          {" "}
                          <span
                            style={{
                              fontSize: "17px",
                              fontWeight: "450",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            Date
                          </span>
                          <br />
                          <span
                            style={{
                              color: "#888",
                              fontSize: "12px",
                              fontWeight: "",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            {fb[0].RDate}
                          </span>
                        </div>

                        <div className="col-lg-2 col-3">
                          <span
                            style={{
                              fontSize: "17px",
                              fontWeight: "450",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            Depart
                          </span>
                          <br />
                          <span
                            style={{
                              color: "#888",
                              fontSize: "12px",
                              fontWeight: "",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            {" "}
                            {fb[0].RDepartTime}
                          </span>
                        </div>
                        <div className="col-lg-2 col-3">
                          <span
                            style={{
                              fontSize: "17px",
                              fontWeight: "450",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            Duration
                          </span>
                          <br />
                          <span
                            style={{
                              color: "#888",
                              fontSize: "12px",
                              fontWeight: "",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            {" "}
                            {fb[0].RDuration}
                          </span>
                        </div>
                        <div className="col-lg-3 col-3 ">
                          <span
                            style={{
                              fontSize: "17px",
                              fontWeight: "450",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            Arrival
                          </span>
                          <br />
                          <span
                            style={{
                              color: "#888",
                              fontSize: "12px",
                              fontWeight: "",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            {" "}
                            {fb[0].RArrivalTime}
                          </span>
                        </div>
                      </div>
                    )}
                    <div
                      className="m-2"
                      style={{
                        backgroundColor: "red",
                        padding: "2px 10px 4px",
                        borderRadius: "80px",
                      }}
                    ></div>
                    <br />
                    <div className="table-responsive">
                      <table className="table table-bordered">
                        <thead>
                          <tr>
                            <th>Lead Guest</th>
                            <th>Room Type</th>
                            <th>Guest Details</th>
                            <th>Voucher</th>
                            <th>Booking Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>{fb[0].leadGuest}</td>
                            <td>{fb[0].flightClass}</td>
                            <td>{fb[0].TTravellers}</td>
                            <td>A-10091</td>
                            <td>{fb[0].bookingStatus}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default FlightItinerary;
