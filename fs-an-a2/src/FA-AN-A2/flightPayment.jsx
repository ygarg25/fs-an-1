import React, { Component } from "react";
import axios from "axios";
import Navbar from "./navbar";
import jwtDecode from "jwt-decode";

let url = "http://localhost:2410";
// let url = "https://us-central1-yat12-77281.cloudfunctions.net/app";

const jwt = localStorage.getItem("token");
let userid;
if (jwt) {
  let token = jwt.split(" ")[1];

  userid = jwtDecode(token).id;

  console.log("user ecash", token, userid);
}

class FlightPayemnt extends Component {
  state = {
    payemetMethod: ["UPI", "Credit Card", "Debi Card", "Net Banking", "PayPal"],
    selMethod: "UPI",
    dayData: ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"],
    month: [
      "JAN",
      "FEB",
      "MAR",
      "APR",
      "MAY",
      "JUN",
      "JUL",
      "AUG",
      "SEP",
      "OCT",
      "NOV",
      "DEC",
    ],
  };

  async componentWillMount() {
    let apiEndPoint =
      "https://us-central1-yvbty-ab7f2.cloudfunctions.net/app/booking";

    let { data: bookDetail } = await axios.get(apiEndPoint);
    this.setState({ bookDetail: bookDetail });
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    let apiEndPoint =
      "https://us-central1-yvbty-ab7f2.cloudfunctions.net/app/booking";

    if (currProps !== prevProps) {
      let { data: bookDetail } = await axios.get(apiEndPoint);
      this.setState({ bookDetail: bookDetail });
    }
  }
  handlePayMethod = (p) => {
    this.setState({ selMethod: p });
  };

  handlePay = async () => {
    let date = new Date();
    let today =
      date.getDate() +
      " " +
      this.state.month[date.getMonth()] +
      " " +
      date.getFullYear();
    console.log("pay", today);

    const { bookDetail: bd } = this.state;

    let postData = {
      userId: userid,
      gDCity: bd.departure.desDept,
      gACity: bd.departure.desArr,
      gDuration: bd.departure.total,
      gDTime: bd.departure.timeDept,
      gATime: bd.departure.timeArr,
      gFName: bd.departure.name,
      gFCode: bd.departure.code,
      flightClass: bd.departure.type,
      guest: bd.passengers[0].firstName + " " + bd.passengers[0].lastName,
      tGuest: bd.travellers,
      gDate: bd.departure.pickedDate,
    };
    if (Object.keys(bd.arrival).length > 0) {
      postData.returnBook = "true";
      postData.rDCity = bd.arrival.desDept;
      postData.rACity = bd.arrival.desArr;
      postData.rDuration = bd.arrival.total;
      postData.rDTime = bd.arrival.timeDept;
      postData.rATime = bd.arrival.timeArr;
      postData.rFName = bd.arrival.name;
      postData.rFCode = bd.arrival.code;
      postData.rDate = bd.departure.returnDate;
    } else {
      postData.returnBook = "false";
      postData.rDCity = null;
      postData.rACity = null;
      postData.rDuration = null;
      postData.rDTime = null;
      postData.rATime = null;
      postData.rFName = null;
      postData.rFCode = null;
      postData.rDate = null;
    }
    console.log("postData", postData);
    if (localStorage.getItem("token")) {
      console.log("pay");
      const jwt = localStorage.getItem("token");
      const { data: response } = await axios.post(url + "/ecash/" + userid, {
        date: today,
        credit: this.state.bookDetail.travellers * 250,
      });
      const { data: book } = await axios.post(
        url + "/flightbook/" + userid,
        postData
      );
      console.log("post booking current ecash", response, book);
      alert("You book Flight Successfully...");
      // console.log("gecccccccccccccccccccccccc", jwt);
      this.props.history.push("/manage-booking/My Booking/Flights");
    } else {
      this.props.history.push("/login");
    }
  };

  render() {
    const { bookDetail: bd, payemetMethod: pm, selMethod } = this.state;
    console.log("Flight", bd);
    return (
      <div className="container-fluid" style={{ backgroundColor: "lightgrey" }}>
        {/* <Navbar /> */}
        <br />
        <div className="row ml-2">
          <div className="col-lg-9 col-12 pr-5 pl-4">
            <div className="row mt-2 mb-2">
              <div className="col-9 ml-1" style={{ fontSize: "20px" }}>
                <strong>
                  <i className="far fa-credit-card"></i> Payment Method
                </strong>
              </div>
            </div>
            <div className="row bg-white mt-2 mb-2">
              <div className="col-lg-2 col-4 text-center">
                {pm.map((p, i) => (
                  <div
                    key={i}
                    style={{ cursor: "pointer" }}
                    onClick={() => this.handlePayMethod(p)}
                    className={
                      "border " + (selMethod === p ? " bg-white" : " bg-light")
                    }
                  >
                    <div className="mt-2 mb-2">{p}</div>
                  </div>
                ))}
              </div>
              <div className="col-8">
                <br />
                <br />
                <div className="row">
                  <div className="col ml-lg-4 ml-2">
                    <strong>Pay with {selMethod}</strong>
                  </div>
                </div>
                <br />
                <br />
                <div className="row">
                  <div
                    className="col-lg-4 col-12 ml-lg-4 ml-2"
                    style={{ fontSize: "25px" }}
                  >
                    <strong>₹ {bd && bd.total + 350}</strong>
                  </div>
                  <div className="col-lg-6 col-12 ml-lg-0 ml-2">
                    <button
                      className="btn btn-danger text-white btn-md"
                      onClick={() => this.handlePay()}
                    >
                      Pay Now
                    </button>
                  </div>
                </div>
                <div className="row">
                  <div
                    className="col text-secondary text-center"
                    style={{ fontSize: "10px" }}
                  >
                    {" "}
                    By clicking Pay Now, you are agreeing to terms and
                    Conditions and Privacy Policy{" "}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-12">
            <div className="row mt-2 mb-2">
              <div
                className="col-11 ml-1 text-left"
                style={{ fontSize: "16px" }}
              >
                Payment Details
              </div>
            </div>
            <div className="row bg-white ml-1 mr-3" style={{ padding: "20px" }}>
              <div className="col">
                <div className="row">
                  <div className="col-8 text-left" style={{ fontSize: "1rem" }}>
                    Total Flight Price
                  </div>
                  <div
                    className="col-4 text-right"
                    style={{ fontSize: "1rem" }}
                  >
                    {" "}
                    ₹ {bd && bd.total}{" "}
                  </div>
                </div>
                <div className="row">
                  <div className="col-8 text-left" style={{ fontSize: "1rem" }}>
                    Convenience Fees
                  </div>
                  <div
                    className="col-4 text-right"
                    style={{ fontSize: "1rem" }}
                  >
                    {" "}
                    ₹ 350
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div
                    className="col-6 text-left"
                    style={{ fontSize: "1.3rem" }}
                  >
                    <strong>You Pay</strong>
                  </div>
                  <div
                    className="col-6 text-right"
                    style={{ fontSize: "1.3rem" }}
                  >
                    <strong>₹ {bd && bd.total + 350}</strong>
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-6 text-left">Earn eCash</div>
                  <div
                    className="col-6 text-right"
                    style={{ fontSize: "1rem" }}
                  >
                    ₹ {bd && bd.travellers * 250}
                  </div>
                </div>
              </div>
            </div>
            <div className="row mt-1 mb-1">
              <div className="col ml-1"> Booking summary </div>
            </div>
            <div className="row bg-white ml-1 mr-3" style={{ padding: "20px" }}>
              <div className="col">
                <div className="row">
                  <div className="col-6">
                    <img
                      style={{
                        width: "28px",
                        height: "28px",
                        borderRadius: "6px",
                      }}
                      src={bd && bd.departure.logo}
                    />
                  </div>
                  <div className="col-5 mr-1 text-right">
                    {bd && bd.departure.code}
                  </div>
                </div>
                <div className="row">
                  <div className="col-5 pl-1">{bd && bd.departure.desDept}</div>
                  <div className="col-2 text-left">
                    <i
                      className="fas fa-fighter-jet"
                      style={{ color: "lightgray" }}
                    ></i>
                  </div>
                  <div className="col-5">{bd && bd.departure.desArr}</div>
                </div>
                {bd && Object.keys(bd.arrival).length > 0 && (
                  <div>
                    <div className="row mt-3">
                      <div className="col-6">
                        <img
                          style={{
                            width: "28px",
                            height: "28px",
                            borderRadius: "6px",
                          }}
                          src={bd.arrival.logo}
                        />
                      </div>
                      <div className="col-5 mr-1 text-right mt-1">
                        {bd.arrival.code}
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-5 pl-1  mt-1">
                        {bd.arrival.desDept}
                      </div>
                      <div className="col-2 text-left">
                        <i
                          className="fas fa-fighter-jet"
                          style={{ color: "lightgray" }}
                        ></i>
                      </div>
                      <div className="col-5" id="fs12">
                        {bd.arrival.desArr}
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
            <div className="row ml-1 mr-1 mt-1 mb-1">
              <div className="col"> Contact Details </div>
            </div>
            {bd && (
              <div
                className="row bg-white ml-1 mr-3"
                style={{ padding: "20px" }}
              >
                <div className="col">
                  {bd.passengers.map((b, i) => (
                    <div
                      className="row"
                      key={i}
                      style={{ fontSize: ".797rem" }}
                    >
                      <div className="col">
                        {" "}
                        {i + 1}. {b.firstName} {b.lastName}{" "}
                      </div>
                    </div>
                  ))}
                  <hr />
                  <div className="row" style={{ fontSize: ".797rem" }}>
                    <div className="col-4 text-secondary">Email</div>
                    <div className="col-8 text-secondary">{bd.email}</div>
                  </div>
                  <div className="row" style={{ fontSize: ".797rem" }}>
                    <div className="col-4 text-secondary">Phone</div>
                    <div className="col-8 text-secondary">{bd.mobile}</div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default FlightPayemnt;
