import React, { Component } from "react";
import axios from "axios";
import Navbar from "./navbar";

class FlightBook extends Component {
  state = {
    data: this.props.flightBook,
    // data: {
    //   departure: {
    //     id: "F19",
    //     logo: "https://i.ibb.co/0FZw9Ps/images.jpg",
    //     name: "Air India",
    //     code: "AI-865",
    //     checkin: 25,
    //     meal: "Free",
    //     desDept: "Mumbai (BOM)",
    //     desArr: "Banglore (BLR)",
    //     Price: 4139,
    //     airBus: "Airbus A320 Neo",
    //     total: "3h 40 min",
    //     timeDept: "22:00",
    //     timeArr: "1:40",
    //     T1: "Indira Gandhi,T-2",
    //     T2: "Chatrapati Shivaji,T-1",
    //     dept: "Mumbai (BOM)",
    //     arr: "Banglore (BLR)",
    //     pickedDate: "06 Apr 2020",
    //     returnDate: "06 Apr 2020",
    //     count: 7,
    //     type: "Economy",
    //     travellers: [
    //       { name: "Adult", count: 3 },
    //       { name: "Child", count: 2 },
    //       { name: "infants", count: 2 },
    //     ],
    //   },
    //   arrival: {
    //     id: "F50",
    //     logo: "https://i.ibb.co/Q91K71m/images-2.png",
    //     name: "Indigo",
    //     code: "6E-191",
    //     checkin: 15,
    //     meal: "Paid",
    //     desDept: "Banglore (BLR)",
    //     desArr: "Mumbai (BOM)",
    //     Price: 4139,
    //     airBus: "Airbus A320-100",
    //     total: "2h 40 min",
    //     timeDept: "3:30",
    //     timeArr: "6:10",
    //     T1: "Indira Gandhi,T-2",
    //     T2: "Chatrapati Shivaji,T-1",
    //   },
    // },
    otherCharge: { cancel: false, insurance: false, coupon: 0 },
    contact: { email: "", mob: "" },
    errors: {},
    tAmt: 0,
  };
  componentDidMount() {
    let travelArray = [];
    let t = { ...this.state.data };
    if (Object.keys(t).length > 0) {
      for (let i = 0; i < t.departure.travellers[0].count; i++) {
        let c = { label: "Adult " + (i + 1) + " :", fname: "", lname: "" };
        travelArray.push(c);
      }
      for (let i = 0; i < t.departure.travellers[1].count; i++) {
        let c = { label: "Child " + (i + 1) + " :", fname: "", lname: "" };
        travelArray.push(c);
      }
      for (let i = 0; i < t.departure.travellers[2].count; i++) {
        let c = { label: "Infant " + (i + 1) + " :", fname: "", lname: "" };
        travelArray.push(c);
      }
    }
    this.setState({ travelArray: travelArray });
  }

  handleChange = (e) => {
    const { currentTarget: input } = e;
    const otherCharge = { ...this.state.otherCharge };
    otherCharge[input.name] =
      input.type === "checkbox" ? input.checked : input.value;
    this.setState({ otherCharge: otherCharge });
  };

  validate = () => {
    let errs = {};
    if (!this.state.contact.mob.trim()) errs.mob = "Mobile Number is required";
    else if (this.state.contact.mob.length !== 10) {
      errs.mob = "Enter Valid Number";
    } else if (isNaN(this.state.contact.mob) === true) {
      errs.mob = "Enter Valid Number";
    }
    if (!this.state.contact.email.trim()) {
      errs.email = "Email is required";
    } else if (
      !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
        this.state.contact.email
      )
    )
      errs.email = "Enter Valid Email";
    if (this.state.travelArray) {
      let travelArray = [...this.state.travelArray];
      for (let i = 0; i < travelArray.length; i++) {
        if (!travelArray[i].fname.trim()) errs.fname = "Name is required";
        if (!travelArray[i].lname.trim()) errs.lname = "Last Name is required";
      }
    }
    return errs;
  };

  isFormValid = () => {
    let errs = this.validate();
    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };

  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "mob":
        if (!e.currentTarget.value.trim()) return "Mobile number is required";
        if (e.currentTarget.value.length !== 10) return "Enter Valid Number";
        if (isNaN(e.currentTarget.value) === true) return "Enter Valid Number";
        break;
      case "email":
        if (!e.currentTarget.value.trim()) return "Email is required";
        if (
          !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
            e.currentTarget.value
          )
        )
          return "Enter Valid Email";
        break;
      default:
        break;
    }
    return "";
  };

  handleChangeContact = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const { currentTarget: input } = e;
    const contact = { ...this.state.contact };
    contact[input.name] = input.value;
    this.setState({ contact: contact, errors: errors });
  };

  handleChangeTravell = (e) => {
    const { currentTarget: input } = e;
    let travelArray = [...this.state.travelArray];
    travelArray[+input.id][input.name] = input.value;
    this.setState({ travelArray: travelArray });
  };

  handlePayment = async () => {
    let postData = {};
    postData.email = this.state.contact.email;
    postData.mobile = this.state.contact.mob;
    postData.departure = this.state.data.departure;
    postData.arrival = this.state.data.arrival;
    postData.total = this.state.tAmt;
    postData.travellers = this.state.travelArray.length;
    let passengers = this.state.travelArray.map((c) => ({
      firstName: c.fname,
      lastName: c.lname,
    }));
    postData.passengers = passengers;
    // if (localStorage.getItem("token")) {
    const { data: response } = await axios.post(
      "https://us-central1-yvbty-ab7f2.cloudfunctions.net/app/booking",
      postData
    );
    this.props.history.push({ pathname: "/payment", reload: { reload: true } });
    // } else {
    //   this.props.history.push("/login");
    // }
  };

  render() {
    const {
      data: sd,
      otherCharge: oc,
      travelArray: ta,
      contact: c,
      errors,
    } = this.state;
    let count = 0;
    if (Object.keys(this.state.data).length) {
      {
        count = 0;
        {
        }
        this.state.tAmt =
          sd.departure.count * sd.departure.Price + 622 - +oc.coupon;
      }
      if (oc.cancel === true) {
        count += 1;
        this.state.tAmt += 747;
      }
      if (oc.insurance === true) {
        count += 1;
        this.state.tAmt += 269 * sd.departure.count;
      }
    }

    let tAmt = this.state.tAmt;
    console.log("ssd", sd, oc, tAmt, ta, c);
    return (
      <div className="container-fluid" style={{ backgroundColor: "lightgrey" }}>
        {/* <Navbar /> */}
        <br />
        <div className="row m-2">
          <div className="col-lg-9 col-12">
            <div className="row">
              <div className="col-1 text-left" style={{ fontSize: "25px" }}>
                <i className="fa fa-search" />
              </div>
              <div
                className="col-lg-8 col-10 text-left"
                style={{
                  color: "#43264e",
                  fontWeight: "500",
                  fontSize: "1.429rem",
                }}
              >
                Review your Bookings
              </div>
            </div>
            <div
              className="row bg-white"
              style={{
                paddingLeft: "50px",
                paddingRight: "50px",
                paddingBottom: "25px",
                paddingTop: "25px",
              }}
            >
              <div className="col-12">
                <div className="row bg-white" style={{ paddingTop: "12px" }}>
                  <div className="col-lg-2 col-12 text-center d-none d-sm-block d-md-none ">
                    <div className="row pl-1">
                      <div className="col-4 text-center">
                        <img
                          style={{ width: "40px" }}
                          src={sd.departure.logo}
                        />
                      </div>
                      <div className="col-4 pt-2"> {sd.departure.name} </div>
                      <div className="col-4 pt-2" id="rs1">
                        {" "}
                        {sd.departure.airBus}{" "}
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-2 col-12 text-center d-none d-lg-block">
                    <div className="row pl-1">
                      <div className="col-lg-12 col-12">
                        <img
                          style={{ width: "40px" }}
                          src={sd.departure.logo}
                        />
                      </div>
                    </div>
                    <div className="row pl-1">
                      <div className="col-lg-12 col-12">
                        {" "}
                        {sd.departure.name}{" "}
                      </div>
                    </div>
                    <div className="row pl-1 text-secondary">
                      <div className="col-lg-12 col-12">
                        {" "}
                        {sd.departure.airBus}{" "}
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-2 col-4">
                    <div className="row"> {sd.departure.desDept} </div>
                    <div className="row"> {sd.departure.timeDept} </div>
                    <div
                      className="row text-secondary"
                      style={{
                        fontSize: "13.7136px",
                        fontFamily: "Rubik-Regular,Arial",
                      }}
                    >
                      {" "}
                      {sd.departure.T1}{" "}
                    </div>
                  </div>
                  <div className="col-lg-6 col-3">
                    <div className="row">
                      <div
                        className="col-lg-4 col-12 text-right"
                        style={{
                          fontSize: "",
                          fontFamily: "",
                        }}
                      >
                        {sd.departure.total}{" "}
                      </div>
                      <div className="col-4 d-none d-lg-block ">
                        <span className="text-secondary ">|</span>
                        <span className="text-secondary">
                          {" "}
                          {sd.departure.meal} Meal{" "}
                        </span>
                        <span className="text-secondary ">|</span>
                      </div>
                      <div className="col-4 d-none d-lg-block text-left">
                        <span className="text-secondary">
                          {sd.departure.type}
                        </span>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-5 d-none d-lg-block">
                        <hr />
                      </div>
                      <div className="col-lg-2 col-12 mt-1 text-center">
                        <i className="fa fa-fighter-jet" />
                      </div>
                      <div className="col-5 d-none d-lg-block">
                        <hr />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col text-center d-none d-lg-block">
                        {sd.departure.checkin} kgs |{" "}
                        <span className="text-success">
                          Partially Refundable
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-2 col-5">
                    <div className="row"> {sd.departure.desArr} </div>
                    <div className="row"> {sd.departure.timeArr} </div>
                    <div
                      className="row text-secondary"
                      style={{
                        fontSize: "13.7136px",
                        fontFamily: "Rubik-Regular,Arial",
                      }}
                    >
                      {" "}
                      {sd.departure.T2}{" "}
                    </div>
                  </div>
                </div>
                <div className="row bg-white">
                  <div className="col-2 d-none d-lg-block"></div>
                  <div
                    className="col-lg-10 col-12"
                    style={{
                      backgroundColor: " #fffcc7",
                      padding: "8px 15px 7px 15px",
                      borderRadius: "80px",
                    }}
                  ></div>
                </div>
                {Object.keys(sd.arrival).length > 0 && (
                  <div className="row bg-white" style={{ paddingTop: "12px" }}>
                    <div className="col-lg-2 col-12 text-center d-none d-sm-block d-md-none">
                      <div className="row pl-1">
                        <div className="col-4">
                          <img
                            style={{ width: "40px" }}
                            src={sd.arrival.logo}
                          />
                        </div>
                        <div className="col-4"> {sd.arrival.name} </div>
                        <div className="col-4"> {sd.arrival.airBus} </div>
                      </div>
                    </div>
                    <div className="col-lg-2 col-12 text-center d-none d-lg-block">
                      <div className="row pl-1">
                        <div className="col-lg-12 col-12">
                          <img
                            style={{ width: "40px" }}
                            src={sd.arrival.logo}
                          />
                        </div>
                      </div>
                      <div className="row pl-1">
                        <div className="col-lg-12 col-12">
                          {" "}
                          {sd.arrival.name}{" "}
                        </div>
                      </div>
                      <div className="row pl-1 text-secondary">
                        <div className="col-lg-12 col-12">
                          {" "}
                          {sd.arrival.airBus}{" "}
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-2 col-4">
                      <div className="row"> {sd.arrival.desDept} </div>
                      <div className="row"> {sd.arrival.timeDept} </div>
                      <div
                        className="row text-secondary"
                        style={{
                          fontSize: "13.7136px",
                          fontFamily: "Rubik-Regular,Arial",
                        }}
                      >
                        {" "}
                        {sd.arrival.T1}{" "}
                      </div>
                    </div>
                    <div className="col-lg-6 col-4">
                      <div className="row">
                        <div className="col-lg-4 col-12 text-center">
                          {sd.arrival.total}{" "}
                        </div>
                        <div className="col-4 d-none d-lg-block">
                          <span className="text-secondary ">|</span>
                          <span className="text-secondary">
                            {sd.arrival.meal} Meal{" "}
                          </span>
                          <span className="text-secondary ">|</span>
                        </div>
                        <div className="col-4 d-none d-lg-block">
                          <span className="text-secondary">
                            {sd.departure.type}
                          </span>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-5 d-none d-lg-block">
                          <hr />
                        </div>
                        <div className="col-lg-2 col-12 mt-1 text-center">
                          <i className="fa fa-fighter-jet" />
                        </div>
                        <div className="col-5 d-none d-lg-block">
                          <hr />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col text-center d-none d-lg-block">
                          {sd.arrival.checkin} kgs |{" "}
                          <span className="text-success">
                            Partially Refundable
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-2 col-4">
                      <div className="row"> {sd.arrival.desArr} </div>
                      <div className="row"> {sd.arrival.timeArr} </div>
                      <div
                        className="row text-secondary"
                        style={{
                          fontSize: "13.7136px",
                          fontFamily: "Rubik-Regular,Arial",
                        }}
                      >
                        {" "}
                        {sd.arrival.T2}{" "}
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
            <br />
            <div
              className="row bg-white"
              style={{
                paddingLeft: "50px",
                paddingRight: "50px",
                paddingBottom: "25px",
                paddingTop: "25px",
              }}
            >
              <div className="col-12">
                <div className="row bg-white">
                  <div className="col-1 text-right"></div>
                </div>
                <div className="row bg-white">
                  <div className="col-lg-1 col-2 text-right">
                    <input
                      className="form-check-input"
                      id="cancel"
                      name="cancel"
                      type="checkbox"
                      value={oc.cancel}
                      onChange={oc.cancel}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="col-1">
                    <i
                      className="fas fa-plane-departure"
                      style={{ color: "green" }}
                    />
                  </div>
                </div>
                <div className="row text-success">
                  <div className="col ml-1" style={{ fontSize: "20px" }}>
                    <strong>Cancellation Policy</strong>
                  </div>
                </div>
                <div className="row">
                  <div className="col ml-1">
                    Zero cancellation fee for your tickets when you cancel. Pay
                    additional Rs.747
                  </div>
                </div>
                <div className="row bg-white">
                  <div
                    className="col-12 text-center d-none d-lg-block"
                    style={{ backgroundColor: "#FFFCC7" }}
                  >
                    {" "}
                    Travel Smart: Get additional refund of Rs.3,051 in case of
                    cancellation. Terms &amp; Conditions{" "}
                  </div>
                </div>
              </div>
            </div>
            <div className="row mt-2">
              <div
                className="col-1 text-right"
                style={{ fontSize: "25px", color: "red" }}
              >
                <i className="fa fa-user-edit"></i>
              </div>
              <div className="col-10 ml-3 pl-4 mt-1">
                <div className="row">
                  Enter Traveller Details
                  <span
                    className="d-none d-lg-block"
                    style={{
                      fontFamily: "",
                      fontSize: "",
                    }}
                  >
                    &nbsp;| Sign in to book faster and use eCash
                  </span>
                </div>
              </div>
            </div>
            <div className="row mt-2">
              <div className="col-12">
                <div
                  className="row bg-white "
                  style={{
                    paddingLeft: "50px",
                    paddingRight: "50px",
                    paddingBottom: "25px",
                    paddingTop: "25px",
                  }}
                >
                  <div className="col-lg-2 col-12">
                    <b>Contact :</b>
                  </div>
                  <form>
                    <div className="row">
                      <div className="col-6 form-group">
                        <input
                          className="form-control"
                          value={c.email}
                          name="email"
                          onChange={this.handleChangeContact}
                          id="email"
                          placeholder="Enter email"
                          type="email"
                        />
                        {errors.email ? (
                          <div className="text-danger">{errors.email}</div>
                        ) : (
                          ""
                        )}
                      </div>
                      <div className="col-6 form-group">
                        <input
                          className="form-control"
                          id="mob"
                          name="mob"
                          onChange={this.handleChangeContact}
                          value={c.mob}
                          placeholder="Mobile Number"
                          type="text"
                        />
                        {errors.mob ? (
                          <div className="text-danger">{errors.mob}</div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  </form>
                </div>
                <div
                  className="row bg-white"
                  style={{ color: "#666", fontSize: "1rem" }}
                >
                  <div className="col-2 d-none d-lg-block"></div>
                  <div
                    className="col-lg-10 col-12"
                    style={{ color: "#666", fontSize: "1rem" }}
                  >
                    Your booking details will be sent to this email address and
                    mobile number.
                  </div>
                </div>
                <div className="row bg-white">
                  <div className="col-2 d-none d-lg-block"></div>
                  <div
                    className="col-lg-10 col-12"
                    style={{ color: "#666", fontSize: "1rem" }}
                  >
                    Also send my booking details on WhatsApp{" "}
                    <i
                      className="fab fa-whatsapp-square"
                      style={{ color: "green", fontSize: "18px" }}
                    />
                  </div>
                </div>
                <div className="row bg-white">
                  <div className="col-2 d-none d-lg-block"></div>
                  <div className="col-lg-10 col-12">
                    <hr />
                  </div>
                </div>
                <div className="row bg-white">
                  <div className="col-2 d-none d-lg-block"></div>
                  <div className="col-10">
                    <b>Traveller Information</b>
                  </div>
                </div>
                <div className="row bg-white" style={{ fontSize: "1rem" }}>
                  <div className="col-2 d-none d-lg-block"></div>
                  <div className="col-lg-10 col-12">
                    <b>Important Note:</b> Please ensure that the names of the
                    passengers on the travel documents is the same as on their
                    government issued identity proof.{" "}
                  </div>
                  <br />
                </div>
                {this.state.travelArray && (
                  <form className="">
                    {" "}
                    {ta.map((t, i) => (
                      <div className="row bg-white " key={i}>
                        <div
                          className="col-12"
                          style={{ paddingLeft: "50px", paddingRight: "50px" }}
                        >
                          <div className="row">
                            <div className="col-lg-2 col-12">
                              <label>{t.label}</label>
                            </div>
                            <div className="col-5">
                              <input
                                value={t.fname}
                                type="text"
                                id={i}
                                name="fname"
                                onChange={this.handleChangeTravell}
                                className="form-control"
                                placeholder="First Name"
                              />
                            </div>
                            <div className="col-5">
                              <input
                                value={t.lname}
                                type="text"
                                id={i}
                                name="lname"
                                onChange={this.handleChangeTravell}
                                className="form-control"
                                placeholder="Last Name"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    ))}
                  </form>
                )}
              </div>
            </div>
            <div
              className="row bg-white border-top"
              style={{
                paddingLeft: "50px",
                paddingRight: "50px",
                paddingBottom: "25px",
                paddingTop: "25px",
              }}
            >
              <div className="col-12 d-none d-lg-block">
                <div
                  className="row bg-white"
                  style={{ color: "#666", fontSize: "1rem" }}
                >
                  <div className="col-1 text-center mt-1">
                    <i
                      className="fas fa-university"
                      style={{ fontSize: "35px" }}
                    />
                  </div>
                  <div className="col-10 text-left">
                    <div className="row">
                      <b>Add your GST Details (Optional)</b>
                    </div>
                    <div className="row">
                      Claim credit of GST charges. Your taxes may get updated
                      post submitting your GST details.{" "}
                    </div>
                  </div>
                </div>
                <div
                  className="row bg-white"
                  style={{ color: "#666", fontSize: "1rem" }}
                >
                  <div className="col-1 text-center">
                    <i
                      className="fas fa-suitcase-rolling"
                      style={{ fontSize: "35px" }}
                    ></i>
                  </div>
                  <div className="col-10 text-left">
                    <div className="row">
                      <b>Travelling for work?</b>
                    </div>
                    <div className="row">
                      Join Yatra for Business. View Benefits
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row mt-2">
              <div
                className="col-1 text-right"
                style={{ fontSize: "25px", color: "red" }}
              >
                <i className="fas fa-umbrella" />
              </div>
              <div className="col-10 mt-1 ml-3 pl-4">
                {" "}
                Travel Protection <span>(Recommended)</span>
              </div>
            </div>
            <div
              className="row bg-white"
              style={{
                paddingLeft: "50px",
                paddingRight: "50px",
                paddingBottom: "25px",
                paddingTop: "25px",
              }}
            >
              <div className="col-12">
                <div className="row bg-white">
                  <div className="col-2 text-center">
                    <input
                      className="form-check-input"
                      id="insurance"
                      name="insurance"
                      type="checkbox"
                      value={oc.insurance}
                      onChange={oc.insurance}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="col-10 mt-1">
                    Yes, Add Travel Protection to protect my trip (Rs.269 per
                    traveller)
                  </div>
                </div>
                <div
                  className="row bg-white"
                  style={{ color: "#DB9A00", fontSize: "1.143rem" }}
                >
                  <div className="col-1 d-none d-lg-block"></div>
                  <div className="col-11 d-none d-lg-block text-dark">
                    6000+ travellers on Yatra protect their trip daily.
                    <span className="text-primary">Learn More</span>
                  </div>
                </div>
                <div className="row bg-white">
                  <div className="col-12 text-left text-muted d-none d-lg-block">
                    Cover Includes:
                  </div>
                </div>
                <div className="row bg-white">
                  <div className="col-4 text-center d-none d-lg-block">
                    <div className="row">
                      <div className="col text-left">
                        <span className="fa-stack fa-2x">
                          <i className="fas fa-circle fa-stack-2x"></i>
                          <i className="fas fa-plane-departure fa-stack-1x fa-inverse"></i>
                        </span>
                      </div>
                    </div>
                    <div className="row" style={{ fontSize: "0.87rem" }}>
                      Trip Cancellation
                    </div>
                    <div className="row" style={{ fontSize: "0.87rem" }}>
                      Claim upto Rs.25,000
                    </div>
                  </div>
                  <div
                    className="col-4 text-center d-none d-lg-block"
                    style={{ fontSize: "1rem" }}
                  >
                    <div className="row mb-1">
                      <div className="col text-left">
                        <span className="fa-stack fa-2x">
                          <i className="fas fa-circle fa-stack-2x"></i>
                          <i className="fas fa-suitcase-rolling fa-stack-1x fa-inverse"></i>
                        </span>
                      </div>
                    </div>
                    <div className="row" style={{ fontSize: "0.87rem" }}>
                      Loss of Baggage
                    </div>
                    <div className="row" style={{ fontSize: "0.87rem" }}>
                      Claim upto Rs.25,000
                    </div>
                  </div>
                  <div
                    className="col-4 text-center d-none d-lg-block"
                    style={{
                      fontSize: "0.87rem",
                      fontFamily: "Rubik-Regular,Arial, Helvetica, sans-serif",
                    }}
                  >
                    <div className="row">
                      <div className="col text-left">
                        <span className="fa-stack fa-2x">
                          <i className="fas fa-circle fa-stack-2x"></i>
                          <i className="fas fa-ambulance fa-stack-1x fa-inverse"></i>
                        </span>
                      </div>
                    </div>
                    <div className="row" style={{ fontSize: "0.87rem" }}>
                      Medical Emergency
                    </div>
                    <div className="row" style={{ fontSize: "0.87rem" }}>
                      Claim upto Rs.25,000
                    </div>
                  </div>
                </div>
                <div className="row bg-white">
                  <div
                    className="col-12 text-center d-none d-lg-block"
                    style={{ fontSize: "0.87rem" }}
                  >
                    Note: Travel Protection is applicable only for Indian
                    citizens below the age of 70 years.
                    <span className="text-primary">Terms &amp; Conditions</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-12">
            <div className="row">
              <div
                className="col"
                style={{ color: "black", fontWeight: "500" }}
              >
                Fare Details
              </div>
            </div>
            <div
              clas="row bg-white"
              style={{ padding: "10px", paddingBottom: "0px" }}
            >
              <div className="col-12">
                <div className="row bg-white" style={{ padding: "10px" }}>
                  <div className="col-8 text-left" id="fs11">
                    {" "}
                    Base Fare(1 Traveller){" "}
                  </div>
                  <div className="col-4 text-right" id="fs11">
                    {" "}
                    ₹ {sd.departure.Price}{" "}
                  </div>
                </div>
                <div className="row bg-white" style={{ padding: "10px" }}>
                  <div className="col-8 text-left"> Fees &amp; Surcharge </div>
                  <div className="col-4 text-right"> ₹622 </div>
                </div>
                <div className="row bg-white">
                  <div className="col-12 text-center">
                    <hr />
                  </div>
                </div>
                <div className="row bg-white" style={{ padding: "10px" }}>
                  <div className="col-6 text-left"> Total Fare </div>
                  <div className="col-6 text-right"> ₹{tAmt} </div>
                </div>
                <div className="row bg-white" style={{ padding: "10px" }}>
                  {count > 0 && (
                    <div className="col-7 text-left"> Add Ons({count}) </div>
                  )}
                </div>
              </div>
            </div>
            <div
              className="row bg-light ml-2 mr-2"
              style={{ padding: "10px", fontSize: "20px" }}
            >
              <div className="col-7 text-left" id="fs13">
                <strong>You Pay</strong>
              </div>
              <div className="col-5 text-right" id="fs13">
                <strong>₹{tAmt}</strong>
              </div>
            </div>
            <br />
            <div className="row">
              <div className="col">Promo</div>
            </div>
            <div className="row bg-white mr-1 ml-1" style={{ padding: "15px" }}>
              <div className="col-12">
                <div className="row">
                  <div className="col-12"> Select A Promo Code </div>
                </div>
                <div className="row">
                  <div className="col-12">
                    <div className="form-check ml-3">
                      <input
                        checked={+oc.coupon === 1400}
                        className="form-check-input "
                        name="coupon"
                        type="radio"
                        id="coupon"
                        onChange={this.handleChange}
                        value={1400}
                      />
                      <label
                        className="form-check-label "
                        htmlFor="1400"
                        id="promobox"
                      >
                        <span
                          style={{
                            fontSize: "12px",
                            color: "#02CB66",
                            border: "dotted 1px",
                          }}
                        >
                          {" "}
                          &nbsp;NEWPAY
                        </span>
                      </label>
                      <div className="row">
                        <div
                          className="col-10"
                          style={{ fontSize: "12px", color: "#999" }}
                        >
                          Pay with PayPal to save upto Rs.1400 on Domestic
                          flights (Max. discount Rs. 600 + 50% cashback up to
                          Rs. 800).
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12">
                    <div className="form-check ml-3">
                      <input
                        checked={+oc.coupon === 2000}
                        className="form-check-input "
                        name="coupon"
                        type="radio"
                        id="coupon"
                        onChange={this.handleChange}
                        value={2000}
                      />
                      <label
                        className="form-check-label"
                        htmlFor="exampleRadios1"
                        id="promobox"
                      >
                        <span
                          style={{
                            fontSize: "12px",
                            color: "#02CB66",
                            border: "dotted 1px",
                          }}
                        >
                          {" "}
                          &nbsp;YTAMZ19
                        </span>
                      </label>
                      <div className="row">
                        <div
                          className="col-10"
                          style={{ fontSize: "12px", color: "#999" }}
                        >
                          Save up to Rs.2,000 (Flat 6% (max Rs. 1,000) instant
                          OFF + Flat 5% (max Rs. 1,000) cashback).
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 text-center">
            <button
              className="btn btn-danger text-white"
              style={{ borderRadius: "2px" }}
              disabled={this.isFormValid()}
              onClick={this.handlePayment}
            >
              Proceed to Payment
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default FlightBook;
