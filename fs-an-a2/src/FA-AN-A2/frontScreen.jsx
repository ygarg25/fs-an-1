import React, { Component } from "react";
import DatePicker from "react-datepicker";
import Hotel from "./hotel";
import Flights from "./flight";
import Navbar from "./navbar";

class FrontScreen extends Component {
  state = {
    domesticFlight: [
      {
        origin: "New Delhi",
        dest: "Bengaluru",
        date: "Wed, 3 Oct",
        amount: 3590,
      },
      {
        origin: "New Delhi",
        dest: "Mumbai",
        date: "Sun, 13 Oct",
        amount: 2890,
      },
      {
        origin: "Hyderabad",
        dest: "Bengaluru",
        date: "Mon,30 Sep",
        amount: 2150,
      },
      { origin: "Mumbai", dest: "Pune", date: "Sun,6 Oct", amount: 1850 },
    ],
    holiday: [
      {
        img: "https://i.ibb.co/SQ7NSZT/hol1.png",
        place: "Australia",
        price: "177,990",
        days: "9 Nights / 10 Days",
      },
      {
        img: "https://i.ibb.co/Wxj50q1/hol2.png",
        place: "Europe",
        price: "119,990",
        days: "6 Nights / 7 Days",
      },
      {
        img: "https://i.ibb.co/VY3XNZr/hol3.png",
        place: "New Zealand",
        price: "199,990",
        days: "6 Nights / 7 Days",
      },
      {
        img: "https://i.ibb.co/j4NNc35/hol4.jpg",
        place: "Sri Lanka",
        price: "18,999",
        days: "4 Nights / 5 Days",
      },
      {
        img: "https://i.ibb.co/ct6076f/hol5.jpg",
        place: "Kerala",
        price: "12,999",
        days: "4 Nights / 5 Days",
      },
      {
        img: "https://i.ibb.co/vB0CpYK/hol6.jpg",
        place: "Char Dham",
        price: "22,999",
        days: "4 Nights / 5 Days",
      },
    ],
    selectedService: "Flights",
  };

  handleSelecteionService = (s) => {
    this.setState({ selectedService: s });
  };

  handleTicketDetail = (t) => {
    this.props.ticketBooking(t);
    this.props.history.push("/yatra/airSearch");
  };

  handleHotel = (h) => {
    this.props.handleHotel(h);
    this.props.history.push("/hotel-search");
  };

  render() {
    const { domesticFlight, holiday, selectedService } = this.state;
    return (
      <div className="container-fluid">
        {/* <Navbar /> */}
        <br />
        <div className="row pl-1">
          <div className="col-lg-5 col-12">
            <div className="row">
              <div className="col-3">
                <span
                  className={
                    "fa-stack fa-2x" +
                    (selectedService === "Flights" ? " text-danger" : "")
                  }
                  onClick={() => this.handleSelecteionService("Flights")}
                  style={{ cursor: "pointer" }}
                >
                  <i className="fa fa-circle fa-stack-2x"></i>
                  <i className="fa fa-plane fa-stack-1x fa-lg fa-inverse "></i>
                </span>
              </div>
              <div className="col-3">
                <span
                  className={
                    "fa-stack fa-2x" +
                    (selectedService === "Hotel" ? " text-danger" : "")
                  }
                  onClick={() => this.handleSelecteionService("Hotel")}
                  style={{ cursor: "pointer" }}
                >
                  <i className="fa fa-circle fa-stack-2x"></i>
                  <i className="fa fa-bed fa-stack-1x fa-md fa-inverse "></i>
                </span>
              </div>
              <div className="col-3">
                <span
                  className={
                    "fa-stack fa-2x" +
                    (selectedService === "Buses" ? " text-danger" : "")
                  }
                  onClick={() => this.handleSelecteionService("Buses")}
                  style={{ cursor: "pointer" }}
                >
                  <i className="fa fa-circle fa-stack-2x"></i>
                  <i className="fa fa-bus fa-stack-1x fa-md fa-inverse "></i>
                </span>
              </div>
              <div className="col-3">
                <span
                  className={
                    "fa-stack fa-2x" +
                    (selectedService === "Taxis" ? " text-danger" : "")
                  }
                  onClick={() => this.handleSelecteionService("Taxis")}
                  style={{ cursor: "pointer" }}
                >
                  <i className="fa fa-circle fa-stack-2x"></i>
                  <i className="fa fa-taxi fa-stack-1x fa-md fa-inverse "></i>
                </span>
              </div>
            </div>
            <div className="row">
              <div className="col text-center text dark">
                <h4>{selectedService}</h4>
              </div>
            </div>
            {selectedService === "Flights" && (
              <Flights ticketBooking={this.handleTicketDetail} />
            )}
            {selectedService === "Hotel" && (
              <Hotel handleHotel={this.handleHotel} />
            )}
          </div>
          <div className="col-lg-7 col-12 bg-light ">
            <div className="row">
              <div className="col text-muted">
                <strong>Flight Discounts for you</strong>
              </div>
            </div>
            <div className="row">
              <div className="col-4">
                <img
                  className="img-fluid"
                  src={"https://i.ibb.co/qdc2z7Z/ad01.png"}
                />
              </div>
              <div className="col-4">
                <img
                  className="img-fluid"
                  src={"https://i.ibb.co/yp0bbgz/ad02.png"}
                />
              </div>
              <div className="col-4">
                <img
                  className="img-fluid"
                  src={"https://i.ibb.co/DkrVrkY/ad03.png"}
                />
              </div>
            </div>
            <div className="row">
              <div className="col">
                <img
                  className="img-fluid"
                  src={"https://i.ibb.co/Rc9qLyT/banner1.jpg"}
                />
              </div>
            </div>
            <div className="row">
              <div className="col text-muted">
                <h4>Popular Domestic Flight Routes</h4>
              </div>
            </div>
            <div className="row">
              {domesticFlight.map((d, i) => (
                <div className="col-2 bg-white m-1" key={i}>
                  <div className="row">
                    <div className="col text-dark text-center">
                      <strong>{d.dest}</strong>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col text-muted text-center">{d.date}</div>
                  </div>
                  <br />
                  <div className="row">
                    <div className="col text-dark text-center">
                      <strong>{d.origin}</strong>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col text-muted text-center">
                      Starting From
                    </div>
                  </div>
                  <div className="row">
                    <div className="col  text-center">
                      <button className="btn btn-warning">
                        Rs. {d.amount}
                      </button>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <div className="row">
              <div className="col text-muted">
                <h4>Popular Holiday Destinations</h4>
              </div>
            </div>
            <div className="row">
              {holiday.map((h, i) => (
                <div className="col-5 bg-white m-1" key={i}>
                  <div className="row">
                    <div className="col-2 mt-3">
                      <img className="img-fluid" src={h.img} />
                    </div>
                    <div className="col-8">
                      <div className="row">
                        <div className="col text-center text-muted">
                          {h.place}
                        </div>
                      </div>
                      <div className="row">
                        <div className="col text-center">
                          <span className="text-danger">
                            <strong>Rs. {h.price}</strong>
                          </span>
                          <span className="text-dark">{"  "}per person</span>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col text-center text-muted">
                          {h.days}
                        </div>
                      </div>
                    </div>
                    <div className="col-1 mt-3">
                      <i className="fa fa-arrow-right" />
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FrontScreen;
