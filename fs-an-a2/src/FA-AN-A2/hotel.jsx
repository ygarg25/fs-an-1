import React, { Component } from "react";
import DatePicker from "react-datepicker";

let today = new Date();
let todayDate = today.getDate();
let tM = today.getMonth();
let ty = today.getFullYear();

class Hotel extends Component {
  state = {
    checkIn: new Date(),
    checkOut: new Date(ty, tM, todayDate + 2),
    hotelData: [
      { display: "NewDelhi,Delhi,India(7hotels)", value: "New Delhi" },
      { display: "Bengaluru,Karnataka,India(7hotels)", value: "Bengaluru" },
      { display: "Mumbai,Maharashtra,India(7hotels)", value: "Mumbai" },
    ],
    dayData: [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ],
    hotelRoom: [{ roomNo: 0, adult: 1, child: 0 }],
    travellerClass: true,
    goingTo: "",
  };
  handleDate = (date) => {
    this.setState({
      checkIn: date,
    });
  };

  handleSearch = () => {
    let hotel = {};
    hotel.city = this.state.goingTo;
    hotel.in = this.state.checkIn;
    hotel.out = this.state.checkOut;
    hotel.room = this.state.hotelRoom;
    this.props.handleHotel(hotel);
  };

  validate = () => {
    let errs = {};
    if (!this.state.goingTo.trim()) errs.dept = "Hotel City is required";

    let totalGuest = this.state.hotelRoom.reduce(
      (acc, curr) => acc + curr.adult + curr.child,
      0
    );
    if (totalGuest <= 0) errs.pasanger = "Guest is required";
    return errs;
  };

  isFormValid = () => {
    let errs = this.validate();
    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };

  handlecheckOut = (date) => {
    this.setState({
      checkOut: date,
    });
  };

  handleAdult = (i, c) => {
    const hotelRoom = [...this.state.hotelRoom];
    hotelRoom[i].adult += c;
    this.setState({ hotelRoom: hotelRoom });
  };

  handleChild = (i, c) => {
    const hotelRoom = [...this.state.hotelRoom];
    hotelRoom[i].child += c;
    this.setState({ hotelRoom: hotelRoom });
  };
  handledropDown = (key) => {
    if (key === "travellerClass") {
      this.setState({ travellerClass: !this.state.travellerClass });
    }
  };

  handleAddRoom = () => {
    let hotelRoom = [...this.state.hotelRoom];
    hotelRoom.push({ roomNo: this.state.hotelRoom.length, adult: 1, child: 0 });
    this.setState({ hotelRoom: hotelRoom });
  };
  handleRemove = () => {
    let hotelRoom = [...this.state.hotelRoom];
    hotelRoom.pop();
    this.setState({ hotelRoom: hotelRoom });
  };

  handleChange = (e) => {
    this.setState({ goingTo: e.currentTarget.value });
  };
  render() {
    const { hotelData, hotelRoom, travellerClass } = this.state;
    let totalGuest = hotelRoom.reduce(
      (acc, curr) => acc + curr.adult + curr.child,
      0
    );
    let totalRoom = hotelRoom.length;
    return (
      <div className="container">
        <div className="row">
          <br />
          <div className="col-12">
            <div className="form-group">
              <label htmlFor="goingTo">
                <h6>Select City, Location, Hotel Name</h6>
              </label>
              <select
                className="form-control"
                value={this.state.goingTo}
                onChange={this.handleChange}
              >
                <option value="">Select City</option>
                {hotelData.map((h, i) => (
                  <option key={i} value={h.value}>
                    {h.display}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
        <hr className="myhr4" />
        <div className="row">
          <div className="col-md-5 col-12 ">
            <h6>Check-In</h6>
            <DatePicker
              selected={this.state.checkIn}
              onChange={this.handleDate}
              minDate={new Date()}
            />
            <br />
            <span>{this.state.dayData[this.state.checkIn.getDay()]}</span>
          </div>
          <div className="col-md-2 d-none d-md-block text-center"></div>
          <div className="col-md-5 col-12">
            <h6>Check-Out</h6>
            <DatePicker
              selected={this.state.checkOut}
              minDate={this.state.checkIn}
              onChange={this.handlecheckOut}
            />
            <br />
            <span>{this.state.dayData[this.state.checkOut.getDay()]}</span>
          </div>
        </div>
        <hr className="myhr4" />
        {travellerClass ? (
          <div className="row">
            <div className="col-6 ">
              <h5>Traveller and Hotel</h5>
              <h6>
                {totalGuest}
                {"  "}
                {totalGuest <= 1 ? "Traveller" : "Travellers"},
                {hotelRoom.length} {"  "} {totalRoom <= 1 ? "Room" : "Rooms"}
              </h6>
            </div>
            <div className="col-6 text-right ">
              <br />
              <i
                className="fa fa-chevron-down"
                onClick={() => this.handledropDown("travellerClass")}
                style={{ cursor: "pointer" }}
              />
            </div>
          </div>
        ) : (
          <div>
            <div className="row">
              <div className="col-6 ">
                <h5>Traveller and Hotel</h5>
                <h6>
                  {totalGuest} {totalGuest <= 1 ? "Traveller" : "Travellers"},{" "}
                  {hotelRoom.length}
                  {"  "} {totalRoom <= 1 ? "Room" : "Rooms"}
                </h6>
              </div>
              <div className="col-6 text-right ">
                <br />
                <i
                  className="fa fa-chevron-up"
                  onClick={() => this.handledropDown("travellerClass")}
                  style={{ cursor: "pointer" }}
                />
              </div>
            </div>

            <hr className="myhr4" />
            {hotelRoom.map((h, i) => (
              <div className="" key={i}>
                <h6>Room {h.roomNo + 1}:</h6>
                <div className="row">
                  <div
                    className="col-lg-4 col-2"
                    style={{ paddingTop: "20px" }}
                  >
                    <i
                      className="fa fa-user fa-lg Align-self-end"
                      aria-hidden="true"
                    ></i>
                  </div>
                  <div className="col-lg-4 col-5">
                    <span className="font-weight-bold">Adult</span>
                    <br />
                    {h.adult < 1 ? (
                      <span
                        className="pl-3 pr-3 border"
                        style={{ cursor: "pointer" }}
                      >
                        -
                      </span>
                    ) : (
                      <span
                        className="pl-3 pr-3 border"
                        style={{ cursor: "pointer" }}
                        onClick={() => this.handleAdult(i, -1)}
                      >
                        -
                      </span>
                    )}
                    <span className="pl-3 pr-3 border bg-light">{h.adult}</span>
                    {h.adult >= 3 ? (
                      <span
                        className="pl-3 pr-3 border"
                        style={{ cursor: "pointer" }}
                      >
                        +
                      </span>
                    ) : (
                      <span
                        className="pl-3 pr-3 border"
                        style={{ cursor: "pointer" }}
                        onClick={() => this.handleAdult(i, 1)}
                      >
                        +
                      </span>
                    )}
                  </div>
                  <div className="col-lg-4 col-5">
                    <span>
                      <span className="font-weight-bold">Child</span>
                      <span
                        className="text-muted"
                        style={{ fontSize: ".678rem" }}
                      >
                        (Below 12yrs)
                      </span>
                    </span>
                    <br />
                    {h.child < 1 ? (
                      <span
                        className="pl-3 pr-3 border"
                        style={{ cursor: "pointer" }}
                      >
                        -
                      </span>
                    ) : (
                      <span
                        className="pl-3 pr-3 border"
                        style={{ cursor: "pointer" }}
                        onClick={() => this.handleChild(i, -1)}
                      >
                        -
                      </span>
                    )}
                    <span className="pl-3 pr-3 border bg-light">{h.child}</span>
                    {h.child >= 2 ? (
                      <span
                        className="pl-3 pr-3 border"
                        style={{ cursor: "pointer" }}
                      >
                        +
                      </span>
                    ) : (
                      <span
                        className="pl-3 pr-3 border"
                        style={{ cursor: "pointer" }}
                        onClick={() => this.handleChild(i, 1)}
                      >
                        +
                      </span>
                    )}
                  </div>
                </div>
              </div>
            ))}
            <br />
            {totalRoom <= 2 && (
              <button
                className="btn btn-outline-danger"
                onClick={this.handleAddRoom}
              >
                Add Room
              </button>
            )}
            {totalRoom > 1 && (
              <button
                className="btn btn-outline-danger ml-2"
                onClick={this.handleRemove}
              >
                Remove Room
              </button>
            )}
          </div>
        )}
        <hr className="myhr4" />
        <div className="text-right">
          <button
            className="btn btn-danger pl-3 pr-3 m-3"
            onClick={this.handleSearch}
            disabled={this.isFormValid()}
          >
            Search Room
            <i className="fa fa-arrow-right " aria-hidden="true" />
          </button>
        </div>
      </div>
    );
  }
}

export default Hotel;
