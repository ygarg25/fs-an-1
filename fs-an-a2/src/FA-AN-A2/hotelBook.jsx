import React, { Component } from "react";
import axios from "axios";
import Navbar from "./navbar";
import "./hotellist.css";

let today = new Date();
let todayDate = today.getDate();
let tM = today.getMonth();
let ty = today.getFullYear();

class HotelBook extends Component {
  state = {
    data: this.props.hotel,
    // data: {
    //   guest: {
    //     city: "New Delhi",
    //     in: new Date(),
    //     out: new Date(ty, tM, todayDate + 20),
    //     room: [
    //       { roomNo: 0, adult: 3, child: 3 },
    //       { roomNo: 1, adult: 1, child: 1 },
    //     ],
    //   },
    //   hotel: {
    //     id: "H02",
    //     name: "Sheraton New Delhi",
    //     city: "New Delhi",
    //     location: "Saket, New Delhi",
    //     rating: "4",
    //     cancelation: true,
    //     wifi: true,
    //     breakfast: true,
    //     priceR1: 89999,
    //     prevPriceR1: 109900,
    //     priceR2: 111000,
    //     prevPriceR2: 118000,
    //     img: "https://i.ibb.co/qNHdp7j/7.jpg",
    //     hotelImg: [
    //       "https://i.ibb.co/K6wtzt0/8.jpg",
    //       "https://i.ibb.co/Lx4vTCv/9.jpg",
    //       "https://i.ibb.co/FgKjfNh/10.jpg",
    //       "https://i.ibb.co/R3LQkSL/11.jpg",
    //     ],
    //     visitor: 100,
    //     grade: "Excellent",
    //     roomImg: "https://i.ibb.co/Lx4vTCv/9.jpg",
    //   },
    //   selPrice: 89999,
    // },
    otherCharge: { coupon: 0 },
    contact: { email: "", mob: "" },
    errors: {},
    tAmt: 0,
    dayData: ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"],
    month: [
      "JAN",
      "FEB",
      "MAR",
      "APR",
      "MAY",
      "JUN",
      "JUL",
      "AUG",
      "SEP",
      "OCT",
      "NOV",
      "DEC",
    ],
  };
  componentDidMount() {
    let travelArray = [];
    let t = { ...this.state.data };
    if (Object.keys(t).length > 0) {
      for (let i = 0; i < t.guest.room.length; i++) {
        let c = { label: "Room " + (i + 1) + " :", fname: "", lname: "" };
        travelArray.push(c);
      }
    }
    this.setState({ travelArray: travelArray });
  }

  handleChange = (e) => {
    const { currentTarget: input } = e;
    const otherCharge = { ...this.state.otherCharge };
    otherCharge[input.name] =
      input.type === "checkbox" ? input.checked : input.value;
    this.setState({ otherCharge: otherCharge });
  };

  validate = () => {
    let errs = {};
    if (!this.state.contact.mob.trim()) errs.mob = "Mobile Number is required";
    else if (this.state.contact.mob.length != 10)
      errs.mob = "Enter  valid Number";
    else if (isNaN(this.state.contact.mob) === true) {
      errs.mob = "Enter Valid Number";
    }
    if (!this.state.contact.email.trim()) {
      errs.email = "Email is required";
    } else if (
      !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
        this.state.contact.email
      )
    )
      errs.email = "Enter Valid Email";
    if (this.state.travelArray) {
      let travelArray = [...this.state.travelArray];
      for (let i = 0; i < travelArray.length; i++) {
        if (!travelArray[i].fname.trim()) errs.fname = "Name is required";
        if (!travelArray[i].lname.trim()) errs.lname = "Last Name is required";
      }
    }
    return errs;
  };

  handlestar = (s) => {
    if (+s <= 1) {
      return (
        <span className="checked">
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star " style={{ color: "lightgrey" }} />
          <i className="fa fa-star " style={{ color: "lightgrey" }} />
          <i className="fa fa-star " style={{ color: "lightgrey" }} />
          <i className="fa fa-star " style={{ color: "lightgrey" }} />
        </span>
      );
    }

    if (+s === 2) {
      return (
        <span className="checked">
          <i className="fa fa-star text-danger " />
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star " style={{ color: "lightgrey" }} />
          <i className="fa fa-star " style={{ color: "lightgrey" }} />
          <i className="fa fa-star " style={{ color: "lightgrey" }} />
        </span>
      );
    }

    if (+s === 3) {
      return (
        <span className="checked">
          <i className="fa fa-star text-danger " />
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star " style={{ color: "lightgrey" }} />
        </span>
      );
    }

    if (+s === 4) {
      return (
        <span className="checked">
          <i className="fa fa-star text-danger " />
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star " style={{ color: "lightgrey" }} />
        </span>
      );
    }

    if (+s === 5) {
      return (
        <span className="checked">
          <i className="fa fa-star  " />
          <i className="fa fa-star  " />
          <i className="fa fa-star  " />
          <i className="fa fa-star  " />
          <i className="fa fa-star  " />
        </span>
      );
    }
  };

  isFormValid = () => {
    let errs = this.validate();
    let errCount = Object.keys(errs).length;
    return errCount > 0;
  };

  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "mob":
        if (!e.currentTarget.value.trim()) return "Mobile number is required";
        if (e.currentTarget.value.length != 10) return "Enter Valid Number";
        if (isNaN(e.currentTarget.value) === true) return "Enter Valid Number";
        break;
      case "email":
        if (!e.currentTarget.value.trim()) return "Email is required";
        if (
          !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
            e.currentTarget.value
          )
        )
          return "Enter Valid Email";
        break;
      default:
        break;
    }
    return "";
  };

  handleChangeContact = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const { currentTarget: input } = e;
    const contact = { ...this.state.contact };
    contact[input.name] = input.value;
    this.setState({ contact: contact, errors: errors });
  };

  handleChangeTravell = (e) => {
    const { currentTarget: input } = e;
    let travelArray = [...this.state.travelArray];
    travelArray[+input.id][input.name] = input.value;
    this.setState({ travelArray: travelArray });
  };

  handlePayment = async () => {
    let postData = {};
    postData.email = this.state.contact.email;
    postData.mobile = this.state.contact.mob;
    postData.guest = this.state.data.guest;
    postData.hotel = this.state.data.hotel;
    postData.total = this.state.tAmt;
    postData.totalDay = Math.ceil(
      (this.state.data.guest.out - this.state.data.guest.in) /
        (1000 * 3600 * 24)
    );
    postData.travellers = this.state.travelArray.length;
    let passengers = this.state.travelArray.map((c) => ({
      firstName: c.fname,
      lastName: c.lname,
    }));
    postData.passengers = passengers;
    // if (localStorage.getItem("token")) {
    const { data: response } = await axios.post(
      "https://us-central1-yat12-77281.cloudfunctions.net/app/booking",
      postData
    );
    console.log("pasd", response);
    this.props.history.push("/hotel-payment");
    // } else {
    //   this.props.history.push("/login");
    // }
  };

  handleChangeRoom = () => {
    this.props.history.push("/hotel-search/details");
  };

  render() {
    const {
      data: sd,
      otherCharge: oc,
      travelArray: ta,
      contact: c,
      errors,
      dayData,
      month,
    } = this.state;
    if (Object.keys(this.state.data).length > 0) {
      {
        this.state.tAmt = sd.selPrice + 622 - +oc.coupon;
      }
    }

    let checkin = { date: "", month: "", time: "" };
    let checkout = { date: "", month: "", time: "" };
    let day = "";
    let troom = 0;
    let tadult = 0;
    let tchild = 0;
    if (Object.keys(sd).length > 0) {
      checkin.date = sd.guest.in.getDate();
      checkin.month = sd.guest.in.getMonth();
      checkout.date = sd.guest.out.getDate();
      checkout.month = sd.guest.out.getMonth();
      day = Math.ceil((sd.guest.out - sd.guest.in) / (1000 * 3600 * 24));
    }

    let tAmt = this.state.tAmt;
    console.log("ssd", sd, ta, day);
    return (
      <div style={{ backgroundColor: "lightgrey" }}>
        {/* <Navbar /> */}
        <br />
        <div className="row m-2">
          <div className="col-lg-9 col-12">
            <div className="row">
              <div className="col-1 text-left" style={{ fontSize: "25px" }}>
                <i className="fa fa-search" />
              </div>
              <div
                className="col-lg-8 col-10 text-left"
                style={{
                  color: "#43264e",
                  fontWeight: "500",
                  fontSize: "1.429rem",
                }}
              >
                Review your Bookings
              </div>
            </div>
            <div
              className="row bg-white"
              style={{
                paddingLeft: "50px",
                paddingRight: "50px",
                paddingBottom: "25px",
                paddingTop: "25px",
              }}
            >
              <div className="col-lg-3 col-12  p-0 m-0">
                {Object.keys(sd).length > 0 && (
                  <img
                    src={sd.hotel.roomImg}
                    style={{ width: "100%", height: "auto" }}
                  />
                )}
              </div>
              <div className="col-lg-9 col-12">
                <div className="row">
                  {Object.keys(sd).length > 0 && (
                    <div className="col-12">
                      <div>
                        <span
                          style={{
                            fontWeight: "400",
                            fontFamily: "Rubik-Medium ,Arial",
                            fontSize: "1.286rem",
                          }}
                        >
                          {sd.hotel.name}
                        </span>
                        &nbsp;&nbsp;&nbsp;
                        <span style={{ fontSize: ".586rem" }}>
                          {this.handlestar(sd.hotel.rating)}
                        </span>
                        &nbsp;&nbsp;&nbsp;
                        <span>
                          <img
                            src={"https://i.ibb.co/vms0HKZ/yts-sprite-v3.png"}
                            style={{ width: "80px", height: "auto" }}
                          />
                        </span>
                      </div>
                    </div>
                  )}
                  {Object.keys(sd).length > 0 && (
                    <div
                      className="col-12 text-muted"
                      style={{ fontSize: ".8rem" }}
                    >
                      {sd.hotel.location}
                    </div>
                  )}
                  <br />
                  {Object.keys(sd).length > 0 && (
                    <div className="col-12">
                      <div className="row">
                        <div className="col-lg-2 col-md-4 col-6">
                          <div
                            className="text-center pt-3 pb-3"
                            style={{ backgroundColor: "lightgrey" }}
                          >
                            <span
                              className="text-muted"
                              style={{ fontSize: ".786rem" }}
                            >
                              Check-In
                            </span>
                            <div>
                              <strong>{checkin.date}</strong>
                            </div>
                          </div>
                          <div
                            className=" text-center "
                            style={{
                              border: "1px solid lightgrey",
                            }}
                          >
                            <span
                              className="text-muted"
                              style={{ fontSize: ".686rem" }}
                            >
                              {month[checkin.month]} | 2:00 PM
                            </span>
                          </div>
                        </div>
                        <div className="col-lg-2 col-md-4 col-6">
                          <div
                            className="text-center pt-3 pb-3"
                            style={{ backgroundColor: "lightgrey" }}
                          >
                            <span
                              className="text-muted"
                              style={{ fontSize: ".786rem" }}
                            >
                              Check-Out
                            </span>
                            <div>
                              <strong>{checkout.date}</strong>
                            </div>
                          </div>
                          <div
                            className=" text-center "
                            style={{
                              border: "1px solid lightgrey",
                            }}
                          >
                            <span
                              className="text-muted"
                              style={{ fontSize: ".686rem" }}
                            >
                              {month[checkout.month]} | 12:00 PM
                            </span>
                          </div>
                        </div>
                        <div className="col-lg-8 col-md-12 mt-2">
                          <div className="row">
                            <strong className="col">
                              {" "}
                              {day} Days {"&"} {day - 1} Nights
                            </strong>
                            <span className="text-primary col text-right texthover">
                              <span
                                style={{ cursor: "pointer" }}
                                onClick={() => this.handleChangeRoom()}
                              >
                                CHANGE ROOM
                              </span>
                            </span>
                          </div>
                          <hr />
                          <div>
                            {sd.guest.room.map((r, i) => (
                              <div
                                className="row"
                                key={i}
                                style={{ fontSize: ".89rem" }}
                              >
                                <div className="col-5">
                                  Room {r.roomNo + 1}:
                                </div>
                                <div className="col">
                                  {r.adult > 0 && <span>{r.adult} Adult</span>}
                                  {r.adult > 0 && r.child > 0 && " & "}
                                  {r.child > 0 && <span>{r.child} Child</span>}
                                </div>
                              </div>
                            ))}
                          </div>
                          <hr />
                        </div>
                      </div>
                    </div>
                  )}
                  <br />
                  {Object.keys(sd).length > 0 && (
                    <div className="col-12">
                      <div className="row" style={{ fontSize: ".799rem" }}>
                        <div className="col-3">Inclusion:</div>
                        <div className="col">
                          {sd.hotel.wifi === true && (
                            <div>
                              <i className="fa fa-check text-success" />
                              &nbsp; Complimentary Wi-Fi Internet
                            </div>
                          )}
                          {sd.hotel.breakfast === true && (
                            <div>
                              <i className="fa fa-check text-success" />
                              &nbsp; Breakfast
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div
              className="row bg-light"
              style={{
                paddingLeft: "50px",
                paddingRight: "50px",
                paddingBottom: "25px",
                paddingTop: "25px",
              }}
            >
              <div className="col-12">
                <div style={{ fontSize: "15px" }}>
                  Cancellation Policy: No refund if you cancel this
                  booking.&nbsp;
                  <span className="text-primary" style={{ cursor: "pointer" }}>
                    view more
                  </span>
                </div>
              </div>
            </div>
            <div className="row mt-2">
              <div
                className="col-1 text-right"
                style={{ fontSize: "25px", color: "red" }}
              >
                <i className="fa fa-user-edit"></i>
              </div>
              <div className="col-10 ml-3 mt-1 pl-4">
                <div className="row">
                  Enter Traveller Details
                  <span
                    className="d-none d-lg-block"
                    style={{
                      fontFamily: "Rubik-Regular,Arial, Helvetica, sans-serif",
                      fontSize: "1rem",
                    }}
                  >
                    &nbsp;| Sign in to book faster and use eCash
                  </span>
                </div>
              </div>
            </div>
            <div className="row mt-2">
              <div className="col-12">
                <div
                  className="row bg-white "
                  style={{
                    paddingLeft: "50px",
                    paddingRight: "50px",
                    paddingBottom: "25px",
                    paddingTop: "25px",
                  }}
                >
                  <div className="col-lg-2 col-5">
                    <b>Contact :</b>
                  </div>
                  <form>
                    <div className="row">
                      <div className="col-6 form-group">
                        <input
                          className="form-control"
                          value={c.email}
                          name="email"
                          onChange={this.handleChangeContact}
                          id="email"
                          placeholder="Enter email"
                          type="email"
                        />
                        {errors.email ? (
                          <div className="text-danger">{errors.email}</div>
                        ) : (
                          ""
                        )}
                      </div>
                      <div className="col-6 form-group">
                        <input
                          className="form-control"
                          id="mob"
                          name="mob"
                          onChange={this.handleChangeContact}
                          value={c.mob}
                          placeholder="Mobile Number"
                          type="text"
                        />
                        {errors.mob ? (
                          <div className="text-danger">{errors.mob}</div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  </form>
                </div>
                <div
                  className="row bg-white"
                  style={{ color: "#666", fontSize: "1rem" }}
                >
                  <div className="col-2 d-none d-lg-block"></div>
                  <div
                    className="col-lg-10 col-12"
                    style={{ color: "#666", fontSize: "1rem" }}
                  >
                    Your booking details will be sent to this email address and
                    mobile number.
                  </div>
                </div>
                <div className="row bg-white">
                  <div className="col-2 d-none d-lg-block"></div>
                  <div
                    className="col-lg-10 col-12"
                    style={{ color: "#666", fontSize: "1rem" }}
                  >
                    Also send my booking details on WhatsApp{" "}
                    <i
                      className="fab fa-whatsapp-square"
                      style={{ color: "green", fontSize: "18px" }}
                    />
                  </div>
                </div>
                <div className="row bg-white">
                  <div className="col-2 d-none d-lg-block"></div>
                  <div className="col-lg-10 col-12 ">
                    <hr />
                  </div>
                </div>

                {this.state.travelArray && (
                  <form>
                    {ta.map((t, i) => (
                      <div className="row bg-white " key={i}>
                        <div
                          className="col-12"
                          style={{ paddingLeft: "50px", paddingRight: "50px" }}
                        >
                          <div className="row">
                            <div className="col-lg-2 col-12">
                              <label>
                                <strong>{t.label}</strong>
                              </label>
                            </div>
                            <div className="col-5">
                              <input
                                value={t.fname}
                                type="text"
                                id={i}
                                name="fname"
                                onChange={this.handleChangeTravell}
                                className="form-control"
                                placeholder="First Name"
                              />
                            </div>
                            <div className="col-5">
                              <input
                                value={t.lname}
                                type="text"
                                id={i}
                                name="lname"
                                onChange={this.handleChangeTravell}
                                className="form-control"
                                placeholder="Last Name"
                              />
                            </div>
                          </div>
                          <br />
                        </div>
                      </div>
                    ))}
                  </form>
                )}
              </div>
            </div>
            <br />
            <div
              className="row bg-white border-top"
              style={{
                paddingLeft: "50px",
                paddingRight: "50px",
                paddingBottom: "25px",
                paddingTop: "25px",
              }}
            >
              <div className="col-12">
                <div
                  className="row bg-white"
                  style={{ color: "#666", fontSize: "1rem" }}
                >
                  <div className="col-lg-1 col-12 text-center mt-1">
                    <i
                      className="fas fa-university"
                      style={{ fontSize: "35px" }}
                    />
                  </div>
                  <div className="col-lg-9 col-12 text-left ">
                    <div className="row">
                      <b>Add your GST Details (Optional)</b>
                    </div>
                    <div className="row">
                      Claim credit of GST charges. Your taxes may get updated
                      post submitting your GST details.{" "}
                    </div>
                  </div>
                  <div className="col text-right">
                    {" "}
                    <button className="btn btn-outline-danger">Add</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-12 mt-2">
            <div className="row">
              <div className="" style={{ color: "black", fontWeight: "500" }}>
                Tarrif Details
              </div>
            </div>
            <div
              clas="row bg-white"
              style={{ padding: "10px", paddingBottom: "0px" }}
            >
              <div className="col-12">
                <div className="row bg-white" style={{ padding: "10px" }}>
                  <div className="col-8 text-left" id="fs11">
                    {" "}
                    Hotel Charges{" "}
                  </div>
                  <div className="col-4 text-right" id="fs11">
                    {" "}
                    ₹{sd.selPrice}{" "}
                  </div>
                </div>

                <div className="row bg-white" style={{ padding: "10px" }}>
                  <div className="col-8 text-left"> Hotel &amp; GST </div>
                  <div className="col-4 text-right"> ₹622 </div>
                </div>
                <div className="row bg-white">
                  <div className="col-12 text-center">
                    <hr />
                  </div>
                </div>
                <div className="row bg-white" style={{ padding: "10px" }}>
                  <div className="col-6 text-left"> Total Fare </div>
                  <div className="col-6 text-right"> ₹{tAmt} </div>
                </div>
                <div className="row bg-white" style={{ padding: "10px" }}></div>
              </div>
            </div>
            <div
              className="row bg-light ml-2 mr-2"
              style={{ padding: "10px", fontSize: "20px" }}
            >
              <div className="col-7 text-left" id="fs13">
                <strong>You Pay</strong>
              </div>
              <div className="col-5 text-right" id="fs13">
                <strong>₹{tAmt}</strong>
              </div>
            </div>
            <br />
            <div className="row">
              <div className="col">Promo</div>
            </div>
            <div className="row bg-white mr-1 ml-1" style={{ padding: "15px" }}>
              <div className="col-12">
                <div className="row">
                  <div className="col-12"> Select A Promo Code </div>
                </div>
                <div className="row">
                  <div className="col-12">
                    <div className="form-check ml-3">
                      <input
                        checked={+oc.coupon === 500}
                        className="form-check-input "
                        name="coupon"
                        type="radio"
                        id="coupon"
                        onChange={this.handleChange}
                        value={500}
                      />
                      <label
                        className="form-check-label "
                        htmlFor="1400"
                        id="promobox"
                      >
                        <span
                          style={{
                            fontSize: "12px",
                            color: "#02CB66",
                            border: "dotted 1px",
                          }}
                        >
                          {" "}
                          &nbsp;NEWPAY
                        </span>
                      </label>
                      <div className="row">
                        <div
                          className="col-10"
                          style={{ fontSize: "12px", color: "#999" }}
                        >
                          Pay with PayPal to save upto Rs.500 on Domestic
                          flights (Max. discount Rs. 600 + 50% cashback up to
                          Rs. 800).
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12">
                    <div className="form-check ml-3">
                      <input
                        checked={+oc.coupon === 800}
                        className="form-check-input "
                        name="coupon"
                        type="radio"
                        id="coupon"
                        onChange={this.handleChange}
                        value={800}
                      />
                      <label
                        className="form-check-label"
                        htmlFor="exampleRadios1"
                        id="promobox"
                      >
                        <span
                          style={{
                            fontSize: "12px",
                            color: "#02CB66",
                            border: "dotted 1px",
                          }}
                        >
                          {" "}
                          &nbsp;YTAMZ19
                        </span>
                      </label>
                      <div className="row">
                        <div
                          className="col-10"
                          style={{ fontSize: "12px", color: "#999" }}
                        >
                          Save up to Rs.800 (Flat 6% (max Rs. 1,000) instant OFF
                          + Flat 5% (max Rs. 1,000) cashback).
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 text-center">
            <button
              className="btn btn-danger text-white"
              style={{ borderRadius: "2px" }}
              disabled={this.isFormValid()}
              onClick={this.handlePayment}
            >
              Proceed to Payment
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default HotelBook;
