import React, { Component } from "react";
import axios from "axios";
import jwtDecode from "jwt-decode";
import { Link } from "react-router-dom";

let url = "http://localhost:2410";
// let url = "https://us-central1-yat12-77281.cloudfunctions.net/app";

const jwt = localStorage.getItem("token");
let userid;
if (jwt) {
  let token = jwt.split(" ")[1];

  userid = jwtDecode(token).id;
  console.log("user booking", token, userid);
}

class HotelItinerary extends Component {
  state = {};

  async componentWillMount() {
    const jwt = localStorage.getItem("token");
    let id = this.props.id;
    console.log("get current hotelbook", jwt);
    let page = this.state.currentPage;
    try {
      const { data: response } = await axios.get(
        url + "/specifichotelbook/" + id + "/" + userid
      );
      console.log("get current hotelbook", response);
      this.setState({ hotelbook: response });
    } catch (ex) {
      console.log("catch");
      return null;
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    const jwt = localStorage.getItem("token");
    let id = this.props.id;
    console.log("get current hotelbook", jwt);
    let page = this.state.currentPage;
    if (prevProps !== currProps) {
      try {
        const { data: response } = await axios.get(
          url + "/specifichotelbook/" + id + "/" + userid
        );
        console.log("get current hotelbook", response);
        this.setState({ hotelbook: response });
      } catch (ex) {
        console.log("catch");
        return null;
      }
    }
  }

  render() {
    let id = this.props.id;
    const { hotelbook: hb } = this.state;
    console.log("hotel,book", hb);
    return (
      <div className="col-12 pt-2">
        <div className="row">
          <div className="col-12">
            {!this.state.hotelbook && (
              <div className="text-center pt-5 pb-5">
                <br />
                <h6>No Record To Show of this Hotel</h6>
              </div>
            )}
            {this.state.hotelbook && (
              <div>
                <div
                  className="row"
                  className="mt-5"
                  style={{
                    border: "1px solid lightgrey",
                    borderRadius: "10px",
                  }}
                >
                  <div
                    className="col-12 pb-1"
                    style={{
                      backgroundColor: "lightgrey",
                      borderRadius: "8px 8px 0 0",
                    }}
                  >
                    <div className="row">
                      <div className="col-lg-7 col-12 text-left">
                        <span style={{ color: "grey", fontSize: "18px" }}>
                          <i className="fas fa-bed" />
                        </span>
                        <span>
                          <span
                            style={{
                              color: "black",
                              fontSize: "13px",
                              marginLeft: "8px",
                            }}
                          >
                            {hb[0].HName},{hb[0].City}
                          </span>
                          <br />
                        </span>
                      </div>
                      <div className="col-lg-5 col-12 text-lg-right text-left">
                        <span style={{ color: "grey", fontSize: "11px" }}>
                          Booked On: {hb[0].CheckIn} | Booking Ref No. 200456
                          {isNaN(hb[0].UserId) ? (
                            <span>
                              {hb[0].UserId.substring(0, 4)}
                              {hb[0].Id.substring(0, 4)}
                            </span>
                          ) : (
                            <span>
                              {hb[0].UserId}
                              {hb[0].Id}
                            </span>
                          )}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="row mt-3">
                      <div className="col-lg-3 col-6">
                        <span
                          style={{
                            fontSize: "17px",
                            fontWeight: "450",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          Check In
                        </span>
                        <br />
                        <span
                          style={{
                            color: "#888",
                            fontSize: "12px",
                            fontWeight: "",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          {"12:00 PM"} {hb[0].CheckIn}
                        </span>
                      </div>
                      <div className="col-lg-3 col-6 ">
                        <span
                          style={{
                            fontSize: "17px",
                            fontWeight: "450",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          Check Out
                        </span>
                        <br />
                        <span
                          style={{
                            color: "#888",
                            fontSize: "12px",
                            fontWeight: "",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          {"12:00 PM"} {hb[0].CheckOut}
                        </span>
                      </div>
                      <div className="col-12 d-lg-none d-md-block  ">
                        <hr />
                      </div>
                      <div className="col-lg-3 col-6 ">
                        {" "}
                        <span
                          style={{
                            fontSize: "17px",
                            fontWeight: "450",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          {hb[0].TRoom} Room
                        </span>
                        <br />
                        <span
                          style={{
                            color: "#888",
                            fontSize: "12px",
                            fontWeight: "",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          {hb[0].TDay - 1} Night(s) / {hb[0].TDay} Day(s)
                        </span>
                      </div>
                      <div className="col-lg-3 col-6 ">
                        {" "}
                        <span
                          style={{
                            fontSize: "17px",
                            fontWeight: "450",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          Status
                        </span>
                        <br />
                        <span
                          style={{
                            color: "#888",
                            fontSize: "12px",
                            fontWeight: "",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          {hb[0].BookingStatus}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-12">
                    <hr />
                  </div>
                  <div className="col-12">
                    <div className="row pt-3">
                      <div style={{ borderRight: "1px solid lightgrey" }}></div>
                      <div className="col-lg-2 col-5  text-center mt-3 ">
                        <span
                          style={{
                            cursor: "pointer",
                            border: "1px solid red",
                            borderRadius: "100%",
                            padding: "8px",
                          }}
                        >
                          <i className="fa fa-print" style={{ color: "red" }} />
                        </span>
                        <br />
                        <br />
                        <span
                          style={{
                            fontSize: "13px",
                            fontWeight: "600",
                            cursor: "pointer",
                          }}
                        >
                          Print Invoice
                        </span>
                      </div>
                      <div style={{ borderRight: "1px solid lightgrey" }}></div>
                      <div className="col-lg-3 col-5  text-center mt-3">
                        <span
                          style={{
                            cursor: "pointer",
                            border: "1px solid red",
                            borderRadius: "100%",
                            padding: "8px",
                          }}
                        >
                          <i
                            className="fa fa-location-arrow"
                            style={{ color: "red" }}
                          />
                        </span>
                        <br />
                        <br />
                        <span
                          style={{
                            fontSize: "13px",
                            fontWeight: "600",
                            cursor: "pointer",
                          }}
                        >
                          Get Directions
                        </span>
                      </div>
                      <div style={{ borderRight: "1px solid lightgrey" }}></div>
                      <div className="col-lg-2 col-5 text-center mt-3 ">
                        <span
                          style={{
                            cursor: "pointer",
                            border: "1px solid red",
                            borderRadius: "100%",
                            padding: "8px",
                          }}
                        >
                          <i
                            className="fa fa-refresh"
                            style={{ color: "red" }}
                          />
                        </span>
                        <br />
                        <br />
                        <span
                          style={{
                            fontSize: "13px",
                            fontWeight: "600",
                            cursor: "pointer",
                          }}
                        >
                          Re-Book
                        </span>
                      </div>

                      <div style={{ borderRight: "1px solid lightgrey" }}></div>
                      <div className="col-lg-2 col-5 text-center mt-3 ">
                        <span
                          style={{
                            cursor: "pointer",
                            border: "1px solid red",
                            borderRadius: "100%",
                            padding: "8px",
                          }}
                        >
                          <i
                            className="fa fa-pencil"
                            style={{ color: "red" }}
                          />
                        </span>
                        <br />
                        <br />
                        <span
                          style={{
                            fontSize: "13px",
                            fontWeight: "600",
                            cursor: "pointer",
                          }}
                        >
                          Write to Us
                        </span>
                      </div>

                      <div style={{ borderRight: "1px solid lightgrey" }}></div>
                    </div>
                  </div>
                  <div>
                    <br />
                  </div>
                </div>
                <div
                  className="row"
                  className="mt-5"
                  style={{
                    border: "1px solid lightgrey",
                    borderRadius: "10px",
                  }}
                >
                  <div className="col-12 p-3">
                    <h6>Your Booking Details</h6>
                  </div>
                  <div className="p-2">
                    <hr />
                  </div>
                  <div className="col-12">
                    <div className="row ">
                      <div className="col-lg-3 col-12 ">
                        {" "}
                        <span
                          style={{
                            fontSize: "14px",
                            fontWeight: "450",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          <span style={{ color: "grey", fontSize: "18px" }}>
                            <i className="fas fa-bed" />
                          </span>
                          &nbsp;
                          {hb[0].HName}
                        </span>
                        <br />
                        <span
                          style={{
                            color: "#888",
                            fontSize: "12px",
                            fontWeight: "",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          {hb[0].City}
                        </span>
                      </div>
                      <div className="col-12 d-lg-none d-md-block  ">
                        <hr />
                      </div>
                      <div className="col-lg-3 col-6 ">
                        {" "}
                        <span
                          style={{
                            fontSize: "17px",
                            fontWeight: "450",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          Duration
                        </span>
                        <br />
                        <span
                          style={{
                            color: "#888",
                            fontSize: "12px",
                            fontWeight: "",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          {hb[0].TDay - 1} Night(s) / {hb[0].TDay} Day(s)
                        </span>
                      </div>
                      <div className="col-12 d-lg-none d-md-block  ">
                        <hr />
                      </div>
                      <div className="col-lg-3 col-6">
                        <span
                          style={{
                            fontSize: "17px",
                            fontWeight: "450",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          Check In
                        </span>
                        <br />
                        <span
                          style={{
                            color: "#888",
                            fontSize: "12px",
                            fontWeight: "",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          {"12:00 PM"} {hb[0].CheckIn}
                        </span>
                      </div>
                      <div className="col-lg-3 col-6 ">
                        <span
                          style={{
                            fontSize: "17px",
                            fontWeight: "450",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          Check Out
                        </span>
                        <br />
                        <span
                          style={{
                            color: "#888",
                            fontSize: "12px",
                            fontWeight: "",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          {"12:00 PM"} {hb[0].CheckOut}
                        </span>
                      </div>
                    </div>
                    <div
                      className="col-12 m-2"
                      style={{
                        backgroundColor: "lightgrey",
                        borderRadius: "10px",
                        border: "1px solid lightgrey",
                      }}
                    >
                      <i className="fa fa-map-marker" />
                      &nbsp;&nbsp;
                      {hb[0].Address}
                    </div>
                    <div
                      className="col-12 m-2 p-2 mt-3"
                      style={{
                        borderRadius: "10px",
                        border: "1px solid lightgrey",
                      }}
                    >
                      <h6 className="p-2 pb-0">Inclusion</h6>
                      <span style={{ color: "#777", fontSize: "12px" }}>
                        <i className="fa fa-check" />
                        &nbsp; Breakfast, Complimentary WiFi
                      </span>
                    </div>

                    <div className="table-responsive">
                      <table className="table table-bordered">
                        <thead>
                          <tr>
                            <th>Lead Guest</th>
                            <th>Room Type</th>
                            <th>Guest Details</th>
                            <th>Voucher</th>
                            <th>Booking Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>{hb[0].leadguest}</td>
                            <td>{hb[0].RoomType}</td>
                            <td>{hb[0].TGuest}</td>
                            <td>A-10091</td>
                            <td>{hb[0].BookingStatus}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default HotelItinerary;
