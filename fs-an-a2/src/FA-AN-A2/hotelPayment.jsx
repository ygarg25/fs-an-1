import React, { Component } from "react";
import axios from "axios";
import Navbar from "./navbar";
import jwtDecode from "jwt-decode";

let url = "http://localhost:2410";
// let url = "https://us-central1-yat12-77281.cloudfunctions.net/app";

const jwt = localStorage.getItem("token");
let userid;
if (jwt) {
  let token = jwt.split(" ")[1];

  userid = jwtDecode(token).id;

  console.log("user ecash", token, userid);
}

class HotelPayment extends Component {
  state = {
    payemetMethod: ["UPI", "Credit Card", "Debi Card", "Net Banking", "PayPal"],
    selMethod: "UPI",
    dayData: ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"],
    month: [
      "JAN",
      "FEB",
      "MAR",
      "APR",
      "MAY",
      "JUN",
      "JUL",
      "AUG",
      "SEP",
      "OCT",
      "NOV",
      "DEC",
    ],
  };

  async componentWillMount() {
    let apiEndPoint =
      "https://us-central1-yat12-77281.cloudfunctions.net/app/booking";

    let { data: bookDetail } = await axios.get(apiEndPoint);
    console.log("cwm book detail", bookDetail);
    this.setState({ bookDetail: bookDetail });
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    let apiEndPoint =
      "https://us-central1-yat12-77281.cloudfunctions.net/app/booking";

    if (currProps !== prevProps) {
      let { data: bookDetail } = await axios.get(apiEndPoint);
      console.log("cdu book detail", bookDetail);
      this.setState({ bookDetail: bookDetail });
    }
  }
  handlePayMethod = (p) => {
    this.setState({ selMethod: p });
  };

  handlePay = async () => {
    let date = new Date();
    let today =
      date.getDate() +
      " " +
      this.state.month[date.getMonth()] +
      " " +
      date.getFullYear();
    console.log("pay", today);

    const { bookDetail: bd } = this.state;

    let d1 = bd.guest.in.split("-");
    let d2 = bd.guest.out.split("-");
    let checkin =
      d1[2].substring(0, 2) + " " + this.state.month[+d1[1]] + " " + d1[0];
    let checkout =
      d2[2].substring(0, 2) + " " + this.state.month[+d2[1]] + " " + d2[0];

    let postData = {
      userId: userid,
      hName: bd.hotel.name,
      city: bd.hotel.city,
      tRoom: bd.travellers,
      tDay: bd.totalDay,
      checkIn: checkin,
      checkOut: checkout,
      address: bd.hotel.location,
      roomType: "Standard Room",
      guest: bd.passengers[0].firstName + " " + bd.passengers[0].lastName,
      tGuest: bd.guest.room.reduce((a, b) => a + b.adult + b.child, 0),
    };
    console.log("postData", postData);
    if (localStorage.getItem("token")) {
      console.log("pay");
      const jwt = localStorage.getItem("token");
      const { data: response } = await axios.post(url + "/ecash/" + userid, {
        date: today,
        credit: this.state.bookDetail.travellers * 500,
      });
      const { data: book } = await axios.post(
        url + "/hotelbook/" + userid,
        postData
      );
      console.log("post booking current ecash", response, book);
      alert("You book hotel Successfully...");
      // console.log("get", jwt);
      this.props.history.push("/manage-booking/My Booking/Hotel");
    } else {
      this.props.history.push("/login");
    }
  };

  render() {
    const {
      bookDetail: bd,
      payemetMethod: pm,
      selMethod,
      dayData,
      month,
    } = this.state;
    let checkin = "";
    let checkout = "";
    if (this.state.bookDetail && Object.keys(bd).length > 0) {
      let d1 = bd.guest.in.split("-");
      let d2 = bd.guest.out.split("-");
      checkin = d1[2].substring(0, 2) + " " + month[+d1[1]] + " " + d1[0];
      console.log("ddffff", d1);
      checkout = d2[2].substring(0, 2) + " " + month[+d2[1]] + " " + d2[0];
    }
    console.log("book detail", bd);
    return (
      <div style={{ backgroundColor: "lightgrey" }}>
        {/* <Navbar /> */}
        <br />
        <div className="row ml-2">
          <div className="col-lg-9 col-12 pr-5 pl-4">
            <div className="row mt-2 mb-2 p-0">
              <div className="col-9 ml-1" style={{ fontSize: "20px" }}>
                <strong>
                  <i className="far fa-credit-card"></i> Payment Method
                </strong>
              </div>
            </div>
            <div className="row bg-white mt-2 mb-2">
              <div className="col-lg-2 col-4 text-center">
                {pm.map((p, i) => (
                  <div
                    key={i}
                    style={{ cursor: "pointer" }}
                    onClick={() => this.handlePayMethod(p)}
                    className={
                      "border " + (selMethod === p ? " bg-white" : " bg-light")
                    }
                  >
                    <div className="mt-2 mb-2">{p}</div>
                  </div>
                ))}
              </div>
              <div className="col-8">
                <br />
                <br />
                <div className="row">
                  <div className="col ml-lg-4 ml-2">
                    <strong>Pay with {selMethod}</strong>
                  </div>
                </div>
                <br />
                <br />
                <div className="row">
                  <div
                    className="col-lg-4 col-12 ml-lg-4 ml-2"
                    style={{ fontSize: "25px" }}
                  >
                    <strong>₹ {bd && bd.total + 350}</strong>
                  </div>
                  <div className="col-lg-6 col-12 ml-lg-0 ml-4">
                    <button
                      className="btn btn-danger text-white btn-md"
                      onClick={() => this.handlePay()}
                    >
                      Pay Now
                    </button>
                  </div>
                </div>
                <div className="row">
                  <div
                    className="col text-secondary text-center"
                    style={{ fontSize: "10px" }}
                  >
                    {" "}
                    By clicking Pay Now, you are agreeing to terms and
                    Conditions and Privacy Policy{" "}
                  </div>
                </div>
              </div>
            </div>
          </div>
          {bd && Object.keys(bd).length > 0 && (
            <div className="col-lg-3 col-12">
              <div className="row mt-2 mb-2">
                <div
                  className="col-11 ml-1 text-left"
                  style={{ fontSize: "16px" }}
                >
                  Payment Details
                </div>
              </div>
              <div
                className="row bg-white ml-1 mr-3"
                style={{ padding: "20px" }}
              >
                <div className="col">
                  <div className="row">
                    <div
                      className="col-8 text-left"
                      style={{ fontSize: "1rem" }}
                    >
                      Total Hotel Price
                    </div>
                    <div
                      className="col-4 text-right"
                      style={{ fontSize: "1rem" }}
                    >
                      {" "}
                      ₹ {bd && bd.total}{" "}
                    </div>
                  </div>
                  <div className="row">
                    <div
                      className="col-8 text-left"
                      style={{ fontSize: "1rem" }}
                    >
                      Convenience Fees
                    </div>
                    <div
                      className="col-4 text-right"
                      style={{ fontSize: "1rem" }}
                    >
                      {" "}
                      ₹ 350
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div
                      className="col-6 text-left"
                      style={{ fontSize: "1.3rem" }}
                    >
                      <strong>You Pay</strong>
                    </div>
                    <div
                      className="col-6 text-right"
                      style={{ fontSize: "1.3rem" }}
                    >
                      <strong>₹ {bd && bd.total + 350}</strong>
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-6 text-left">Earn eCash</div>
                    <div
                      className="col-6 text-right"
                      style={{ fontSize: "1rem" }}
                    >
                      ₹ {bd && bd.travellers * 500}
                    </div>
                  </div>
                </div>
              </div>
              <div className="row mt-1 mb-1">
                <div className="col ml-1"> Booking summary </div>
              </div>
              <div
                className="row bg-white ml-1 mr-3"
                style={{ padding: "20px" }}
              >
                <div className="col">
                  <div className="row">
                    <div className="col-3">
                      <img
                        style={{
                          width: "28px",
                          height: "28px",
                          borderRadius: "6px",
                        }}
                        src={bd && bd.hotel.roomImg}
                      />
                    </div>
                    <div className="col mr-1 text-right">
                      {bd && bd.hotel.name}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6 ">
                      <strong>Check-In</strong>
                      <br />
                      {checkin}
                    </div>
                    <div className="col-6 text-right">
                      <strong>Check-Out</strong>
                      <br />
                      {checkout}
                    </div>
                  </div>
                </div>
              </div>
              <div className="row ml-1 mr-1 mt-1 mb-1">
                <div className="col"> Contact Details </div>
              </div>
              {bd && (
                <div
                  className="row bg-white ml-1 mr-3"
                  style={{ padding: "20px" }}
                >
                  <div className="col">
                    {bd.passengers.map((b, i) => (
                      <div
                        className="row"
                        key={i}
                        style={{ fontSize: ".797rem" }}
                      >
                        <div className="col">
                          {" "}
                          {i + 1}. {b.firstName} {b.lastName}{" "}
                        </div>
                      </div>
                    ))}
                    <hr />
                    <div className="row" style={{ fontSize: ".797rem" }}>
                      <div className="col-4 text-secondary">Email</div>
                      <div className="col-8 text-secondary">{bd.email}</div>
                    </div>
                    <div className="row" style={{ fontSize: ".797rem" }}>
                      <div className="col-4 text-secondary">Phone</div>
                      <div className="col-8 text-secondary">{bd.mobile}</div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default HotelPayment;
