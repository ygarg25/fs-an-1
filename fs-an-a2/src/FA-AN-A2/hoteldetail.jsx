import React, { Component } from "react";
import Navbar from "./navbar";
import "./hotellist.css";
import DatePicker from "react-datepicker";
import ImageSlider from "./imageSlider";

let today = new Date();
let todayDate = today.getDate();
let tM = today.getMonth();
let ty = today.getFullYear();

class HotelDetail extends Component {
  state = {
    hotel: this.props.hotel,
    // hotel: {
    //   guest: {
    //     city: "New Delhi",
    //     in: new Date(),
    //     out: new Date(ty, tM, todayDate + 2),
    //     room: [
    //       { roomNo: 0, adult: 2, child: 0 },
    //       { roomNo: 1, adult: 1, child: 1 },
    //     ],
    //   },
    //   hotel: {
    //     id: "H02",
    //     name: "Sheraton New Delhi",
    //     city: "New Delhi",
    //     location: "Saket, New Delhi",
    //     rating: "4",
    //     cancelation: true,
    //     wifi: true,
    //     breakfast: true,
    //     priceR1: 89999,
    //     prevPriceR1: 109900,
    //     priceR2: 111000,
    //     prevPriceR2: 118000,
    //     img: "https://i.ibb.co/qNHdp7j/7.jpg",
    //     hotelImg: [
    //       "https://i.ibb.co/K6wtzt0/8.jpg",
    //       "https://i.ibb.co/Lx4vTCv/9.jpg",
    //       "https://i.ibb.co/FgKjfNh/10.jpg",
    //       "https://i.ibb.co/R3LQkSL/11.jpg",
    //     ],
    //     visitor: 100,
    //     grade: "Excellent",
    //     roomImg: "https://i.ibb.co/Lx4vTCv/9.jpg",
    //   },
    // },
  };

  handleBack = () => {
    this.props.history.push("/hotel-search");
  };

  handlestar = (s) => {
    if (+s <= 1) {
      return (
        <span className="checked">
          <i className="fa fa-star  text-danger" />
        </span>
      );
    }

    if (+s === 2) {
      return (
        <span className="checked">
          <i className="fa fa-star  " />
          <i className="fa fa-star  " />
        </span>
      );
    }

    if (+s === 3) {
      return (
        <span className="checked">
          <i className="fa fa-star  " />
          <i className="fa fa-star  " />
          <i className="fa fa-star  " />
        </span>
      );
    }

    if (+s === 4) {
      return (
        <span className="checked">
          <i className="fa fa-star  " />
          <i className="fa fa-star  " />
          <i className="fa fa-star  " />
          <i className="fa fa-star  " />
        </span>
      );
    }

    if (+s === 5) {
      return (
        <span className="checked">
          <i className="fa fa-star  " />
          <i className="fa fa-star  " />
          <i className="fa fa-star  " />
          <i className="fa fa-star  " />
          <i className="fa fa-star  " />
        </span>
      );
    }
  };

  handleCircle = (s) => {
    if (+s <= 1) {
      return (
        <span className="checked">
          <i className="fa fa-circle  ml-1" />
          <i className="fa fa-circle-thin ml-1" />
          <i className="fa fa-circle-thin ml-1" />
          <i className="fa fa-circle-thin ml-1" />
          <i className="fa fa-circle-thin ml-1" />
        </span>
      );
    }

    if (+s === 2) {
      return (
        <span className="checked">
          <i className="fa fa-circle  ml-1" />
          <i className="fa fa-circle ml-1 " />
          <i className="fa fa-circle-thin ml-1" />
          <i className="fa fa-circle-thin ml-1" />
          <i className="fa fa-circle-thin ml-1" />
        </span>
      );
    }

    if (+s === 3) {
      return (
        <span className="checked">
          <i className="fa fa-circle  ml-1" />
          <i className="fa fa-circle ml-1 " />
          <i className="fa fa-circle ml-1 " />
          <i className="fa fa-circle-thin ml-1" />
          <i className="fa fa-circle-thin ml-1" />
        </span>
      );
    }

    if (+s === 4) {
      return (
        <span className="checked">
          <i className="fa fa-circle ml-1 " />
          <i className="fa fa-circle ml-1 " />
          <i className="fa fa-circle ml-1 " />
          <i className="fa fa-circle ml-1 " />
          <i className="fa fa-circle-thin ml-1" />
        </span>
      );
    }

    if (+s === 5) {
      return (
        <span className="checked">
          <i className="fa fa-circle  ml-1" />
          <i className="fa fa-circle  ml-1" />
          <i className="fa fa-circle  ml-1" />
          <i className="fa fa-circle  ml-1" />
          <i className="fa fa-circle  ml-1" />
        </span>
      );
    }
  };

  handleDate = (date) => {
    let hotel = { ...this.state.hotel };
    hotel.guest.in = date;
    this.setState({
      hotel: hotel,
    });
  };

  handlecheckOut = (date) => {
    let hotel = { ...this.state.hotel };
    hotel.guest.out = date;
    this.setState({
      hotel: hotel,
    });
  };

  handleBook = (p) => {
    let hotel = { ...this.state.hotel };
    hotel.selPrice = p;
    this.props.hotelbook(hotel);
    console.log("Dsdxsxdsdsa", hotel);
    this.props.history.push("/hotel-book");
  };

  handleAdult = (i, c) => {
    const hotel = { ...this.state.hotel };
    hotel.guest.room[i].adult += c;
    this.setState({ hotel: hotel });
  };

  handleChild = (i, c) => {
    const hotel = { ...this.state.hotel };
    hotel.guest.room[i].child += c;
    this.setState({ hotel: hotel });
  };

  handleAddRoom = () => {
    const hotel = { ...this.state.hotel };
    hotel.guest.room.push({
      roomNo: this.state.hotel.guest.room.length,
      adult: 1,
      child: 0,
    });
    this.setState({ hotel: hotel });
  };
  handleRemove = () => {
    const hotel = { ...this.state.hotel };
    hotel.guest.room.pop();
    this.setState({ hotel: hotel });
  };

  render() {
    let { hotel } = this.state;
    console.log("hotel detaii", hotel);
    return (
      <div style={{ backgroundColor: "lightgrey" }}>
        {/* <Navbar /> */}
        <div className="row ml-lg-5 mr-lg-4 pt-lg-3">
          <div
            className="col-12 d-lg-none d-md-block"
            style={{ backgroundColor: "red" }}
          >
            <div className="row p-4" style={{ margin: "auto" }}>
              <div className="col-1 pt-4">
                {" "}
                <i
                  className="fas fa-chevron-left text-white"
                  style={{ cursor: "pointer", fontSize: "15px" }}
                  onClick={this.handleBack}
                />
              </div>
              <div className="col-9 text-white">
                {Object.keys(hotel).length > 0 && (
                  <div>
                    <span
                      style={{
                        fontSize: "20px",
                        fontFamily: "Rubik-Regular ,Arial",
                      }}
                    >
                      {hotel.hotel.name}
                    </span>
                    <div style={{ fontSize: "10px", color: "white" }}>
                      {this.handlestar(+hotel.hotel.rating)}
                    </div>
                    <div style={{ fontSize: "14px" }}>
                      {hotel.hotel.location}
                    </div>
                  </div>
                )}
              </div>
              <div className="col-1 text-right text-white pt-4">
                {" "}
                <i
                  className="fas fa-search text-white"
                  style={{ cursor: "pointer", fontSize: "15px" }}
                />
              </div>
            </div>
          </div>
          <div className="col-12 d-none d-lg-block ">
            <div className="row">
              <div className="col-9 texthover">
                <span
                  style={{ cursor: "pointer", fontSize: "12px" }}
                  onClick={this.handleBack}
                >
                  <i className="fas fa-chevron-left text-primary mr-3" />
                  <span>Back to Search Results</span>
                </span>
              </div>
              <div className="col text-right">
                <span className="modifyBtn">
                  <span>
                    <i className="fa fa-search" aria-hidden="true" /> Modify
                    Search
                  </span>
                </span>
              </div>
            </div>
          </div>
          <div className="col-12 d-none d-lg-block">
            <hr />
          </div>
          <div className="col-12 d-none d-lg-block">
            <div className="row">
              <div className="col-lg-6">
                {Object.keys(hotel).length > 0 && (
                  <div>
                    <span
                      style={{
                        fontSize: "20px",
                        color: "#333",
                        fontFamily: "Rubik-Regular ,Arial",
                      }}
                    >
                      {hotel.hotel.name}
                    </span>
                    <span
                      style={{
                        fontSize: "10px",
                        marginLeft: "10px",
                        color: "red",
                      }}
                    >
                      {this.handlestar(+hotel.hotel.rating)}
                    </span>
                    <div style={{ fontSize: "14px", color: "#777" }}>
                      {hotel.hotel.location}
                    </div>
                  </div>
                )}
              </div>
              <div className="col-lg-6 ">
                {Object.keys(hotel).length > 0 && (
                  <div className="row">
                    <div className="col-9 text-right">
                      <span
                        style={{
                          fontSize: "20px",
                          color: "#333",
                          fontFamily: "Rubik-Regular ,Arial",
                        }}
                      >
                        ₹{hotel.hotel.priceR1}
                        <div style={{ fontSize: "14px" }}>
                          Price for 2 nights
                        </div>
                      </span>
                    </div>
                    <div className="col text-right">
                      <a href="#chooseRoom">
                        {" "}
                        <button
                          className="btn btn-danger btn-sm "
                          style={{ fontSize: "10px" }}
                        >
                          Choose Room
                        </button>
                      </a>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
          <br />
          <br />
          <div className="col-12 d-none d-lg-block bg-white mt-2">
            <div className="row">
              <div className="ml-4 optio">
                <a
                  style={{
                    padding: "10px",
                    textDecoration: "none",
                    color: "#333",
                  }}
                  href="#photo"
                >
                  PHOTOS
                </a>
              </div>
              <div className="optio">
                <a
                  style={{
                    padding: "10px",
                    textDecoration: "none",
                    color: "#333",
                  }}
                  href="#chooseRoom"
                >
                  ROOMS
                </a>
              </div>
              <div className="optio">
                <a
                  style={{
                    padding: "10px",
                    textDecoration: "none",
                    color: "#333",
                  }}
                  href="#desc"
                >
                  DESCRIPTION
                </a>
              </div>
            </div>
          </div>
          <br />
          <br />
          <div className="mt-3 m-4 mt-lg-4 m-lg-0 col-12 mr-4 pr-4 mr-lg-0 bg-white">
            <div className="row p-2">
              <div id="photo" className="col-lg-9 col-12 p-2">
                {Object.keys(hotel).length > 0 && (
                  <ImageSlider hotelImg={hotel.hotel.hotelImg} />
                )}
              </div>
              <div className="col-lg-3 col-12 p-1">
                <div className="row">
                  <div className="col-12">
                    {Object.keys(hotel).length > 0 && (
                      <div className="row">
                        <div
                          className="col-12"
                          style={{
                            color: "#00a680",
                            fontSize: "35px",
                            fontFamily: "Arial",
                          }}
                        >
                          {hotel.hotel.grade}
                        </div>
                        <div
                          className="col-lg-8 col-6"
                          style={{
                            color: "#00a680",
                            fontSize: "22px",
                            fontFamily: "Arial",
                          }}
                        >
                          {hotel.hotel.rating}/5 &nbsp;
                          <i className="fa fa-tripadvisor text-dark" />
                          &nbsp;
                          <span style={{ fontSize: "10px" }}>
                            {this.handleCircle(hotel.hotel.rating)}
                          </span>
                        </div>
                        <div
                          className="col texthover"
                          style={{
                            color: "#777",
                            fontSize: "12px",
                            cursor: "pointer",
                            textDecoration: "underline",
                          }}
                        >
                          <span>{hotel.hotel.visitor} REVIEWS</span>
                        </div>
                        <div className="col-12">
                          Based on Overall Traveller Rating
                        </div>
                      </div>
                    )}
                  </div>
                </div>
                <div className="col-12 d-none d-lg-block">
                  <hr />
                  <div>
                    {" "}
                    <i className="fa fa-check" style={{ color: "#777" }} />
                    &nbsp; Wonderful Location{" "}
                    <span style={{ color: "#00a680" }}>4.5/5</span>
                  </div>
                  <div>
                    {" "}
                    <i className="fa fa-check" style={{ color: "#777" }} />
                    &nbsp; Impressive Service{" "}
                    <span style={{ color: "#00a680" }}>3.9/5</span>
                  </div>
                  <hr />
                </div>
                <div className="col-12 d-none d-lg-block">
                  <strong>CHECKIN :</strong> 2:00 PM
                  <br />
                  <br />
                  <strong>CHECKOUT :</strong> 12:00 PM
                </div>
              </div>
            </div>
            <div id="desc">
              <div className="">
                <h6>HIGHLIGHTS</h6>
                <div className="row ml-1">
                  <span className="text-secondary  n1">
                    <i
                      className="fa fa-parking fa-lg"
                      style={{ fontSize: "30px" }}
                    />
                    <span className="n2">Parking</span>
                  </span>
                  <span className="text-secondary  n1">
                    <i
                      className="fas fa-swimming-pool fa-lg"
                      style={{ fontSize: "30px" }}
                    />
                    <span className="n2">Swimming Pool</span>
                  </span>
                  <span className="text-secondary  n1">
                    <i
                      className="fas fa-concierge-bell fa-lg"
                      style={{ fontSize: "30px" }}
                    />
                    <span className="n2">Travel Counter</span>
                  </span>
                  <span className="text-secondary  n1">
                    <i
                      className="fas fa-tshirt fa-lg"
                      style={{ fontSize: "30px" }}
                    />
                    <span className="n2">Laundry Service</span>
                  </span>
                </div>
              </div>
              <br />
              <div>
                <h6>OVERVIEW</h6>
                <p className="text-justify" style={{ fontSize: "14px" }}>
                  Located approximately 9 km away from Kempegowda Bus Station
                  and 7 km from Tipu Sultan's Summer Palace, Royal Comfort Park
                  is a value for money accommodation in Bangalore, offering
                  internet access at a surcharge to its patrons.Guests are
                  offered accommodation in 11 well-kept and spacious rooms,
                  spread across 3 floors. Some of the in-room amenities offered
                  are AC, television, desk, wardrobe, wake-up call service, and
                  attached bathroom with hot/cold running water facility.
                  Laundry, medical aid and room service are a few of the
                  conveniences offered at Royal Comfort Park. Facilities such as
                  travel counter and front desk are also available within its
                  premises. Places of interest include Ragigudda Sri Prasanna
                  Anjaneyaswamy Temple (4 km), Lalbagh Botanical Garden (5 km),
                  Bellandur Lake (8 km) and Bangalore Fort (9 km). This
                  Bangalore hotel is 11 km away from Bengaluru City Junction
                  Railway Station and 40 km from Kempegowda International
                  Airport.
                </p>
              </div>
            </div>
          </div>
          <br />
          <div id="chooseRoom" className="col-12 ml-3 pt-3 pb-3 pl-0">
            <span style={{ fontSize: "23px", fontWeight: "450" }}>
              <b>CHOOSE ROOM</b>
            </span>
          </div>
          <div className="col-12 bg-white p-4 m-4 m-lg-0">
            <div className="row">
              <div className="col-lg-2 col-12 mt-1">
                <h6>CHECK-IN</h6>
                {Object.keys(hotel).length > 0 && (
                  <DatePicker
                    selected={hotel.guest.in}
                    onChange={this.handleDate}
                    minDate={new Date()}
                  />
                )}
              </div>
              <div className="col-lg-1 d-none d-lg-block"></div>
              <div className="col-lg-2 col-12 mt-1">
                <h6>CHECK-OUT</h6>
                {Object.keys(hotel).length > 0 && (
                  <DatePicker
                    selected={hotel.guest.out}
                    onChange={this.handlecheckOut}
                    minDate={hotel.guest.in}
                  />
                )}
              </div>
              <div className="col-lg-1 d-none d-lg-block"></div>
              <div className="col-lg-3 col-md-3 col-7 mt-2 mt-lg-1">
                {Object.keys(hotel).length > 0 && (
                  <div className="calparent">
                    <h6>GUESTS-ROOMS</h6>
                    <div>
                      <div className="border border-dark p-1 pr-0 ">
                        {hotel.guest.room.reduce(
                          (a, b) => a + b.adult + b.child,
                          0
                        )}
                        &nbsp;{" "}
                        {hotel.guest.room.reduce(
                          (a, b) => a + b.adult + b.child,
                          0
                        ) > 1
                          ? "Guests"
                          : "Guest"}{" "}
                        - {hotel.guest.room.length}&nbsp;{" "}
                        {hotel.guest.room.length > 1 ? "Rooms" : "Room"}
                        &nbsp;&nbsp;&nbsp;
                        <i
                          className="fa fa-angle-down text-right"
                          style={{ float: "right" }}
                        />
                      </div>
                    </div>
                    <div className="calhidden">
                      {hotel.guest.room.map((h, i) => (
                        <div className="row" key={i}>
                          <div className=" inn">Room {h.roomNo + 1}:</div>
                          <div className=" inn">
                            <span className="">Adult</span>
                            <br />
                            {h.adult < 1 ? (
                              <span
                                className="pl-1 pr-1 border"
                                style={{ cursor: "pointer" }}
                              >
                                -
                              </span>
                            ) : (
                              <span
                                className="pl-1 pr-1 border"
                                style={{ cursor: "pointer" }}
                                onClick={() => this.handleAdult(i, -1)}
                              >
                                -
                              </span>
                            )}
                            <span className="pl-1 pr-1 border bg-light">
                              {h.adult}
                            </span>
                            {h.adult >= 3 ? (
                              <span
                                className="pl-1 pr-1 border"
                                style={{ cursor: "pointer" }}
                              >
                                +
                              </span>
                            ) : (
                              <span
                                className="pl-1 pr-1 border"
                                style={{ cursor: "pointer" }}
                                onClick={() => this.handleAdult(i, 1)}
                              >
                                +
                              </span>
                            )}
                          </div>
                          <div className=" inn">
                            <span>
                              <span className=" ">Child</span>
                              <span
                                className="text-muted"
                                style={{ fontSize: "8px" }}
                              >
                                (Below 12yrs)
                              </span>
                            </span>
                            <br />
                            {h.child < 1 ? (
                              <span
                                className="pl-1 pr-1 border"
                                style={{ cursor: "pointer" }}
                              >
                                -
                              </span>
                            ) : (
                              <span
                                className="pl-1 pr-1 border"
                                style={{ cursor: "pointer" }}
                                onClick={() => this.handleChild(i, -1)}
                              >
                                -
                              </span>
                            )}
                            <span className="pl-1 pr-1 border bg-light">
                              {h.child}
                            </span>
                            {h.child >= 2 ? (
                              <span
                                className="pl-1 pr-1 border"
                                style={{ cursor: "pointer" }}
                              >
                                +
                              </span>
                            ) : (
                              <span
                                className="pl-1 pr-1 border"
                                style={{ cursor: "pointer" }}
                                onClick={() => this.handleChild(i, 1)}
                              >
                                +
                              </span>
                            )}
                          </div>
                        </div>
                      ))}
                      <div className="row ">
                        <div className="col-2 text-center"></div>
                        {hotel.guest.room.length <= 2 && (
                          <div className=" col-4 text-right texthover">
                            <span
                              onClick={this.handleAddRoom}
                              style={{ fontSize: "13px", cursor: "pointer" }}
                            >
                              Add Room |
                            </span>
                          </div>
                        )}
                        {hotel.guest.room.length > 1 && (
                          <div className=" col-6 text-left texthover">
                            <span
                              onClick={this.handleRemove}
                              style={{ fontSize: "12px", cursor: "pointer" }}
                            >
                              Remove This Room
                            </span>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                )}
              </div>
              <div className="col-lg-2 col-12 mt-4">
                <button
                  className="btn btn-danger btn-sm"
                  style={{ fontSize: "10px" }}
                >
                  Check Availability
                </button>
              </div>
            </div>
            <br />
            <div
              className="row  m-1 p-3"
              style={{ border: "2px solid lightgrey" }}
            >
              <div className="col-12" style={{ fontSize: "1.123rem" }}>
                <b>DELUXE AC ROOM</b>
              </div>
              <div className="row m-2 p-0">
                <div
                  className="col-lg-3 col-12 p-0"
                  style={{ backgroundColor: "lightgrey" }}
                >
                  {Object.keys(hotel).length > 0 && (
                    <img
                      src={hotel.hotel.roomImg}
                      style={{ width: "100%", height: "auto" }}
                    />
                  )}
                </div>
                <div className="col-lg-9 col-12 pl-3">
                  <div
                    className="pt-4 "
                    style={{
                      fontSize: "16px",
                      fontWeight: "550",
                      fontFamily: "Rubik-Regular,Arial",
                    }}
                  >
                    Deluxe AC Room Only
                  </div>
                  <div className="row ">
                    <div className="col-lg-2 col-4 mt-3">
                      <div
                        style={{
                          fontSize: "15px",
                          fontWeight: "500",
                          color: "#222",
                        }}
                      >
                        Max Guests
                      </div>
                      <div
                        style={{
                          fontSize: "25px",
                        }}
                      >
                        <i className="fa fa-male ml-1" />
                        <i className="fa fa-male ml-1" />
                        <i className="fa fa-male ml-1" />
                        <i
                          className="fa fa-male ml-1"
                          style={{
                            fontSize: "18px",
                          }}
                        />
                        <i
                          className="fa fa-male ml-1"
                          style={{
                            fontSize: "18px",
                          }}
                        />
                      </div>
                    </div>
                    <div className="col-lg-2 col-4 mt-3">
                      <div
                        style={{
                          fontSize: "15px",
                          fontWeight: "500",
                          color: "#222",
                        }}
                      >
                        Inclusions
                      </div>
                      <div
                        style={{
                          fontSize: "10px",
                          color: "#777",
                        }}
                      >
                        {Object.keys(hotel).length > 0 &&
                          hotel.hotel.wifi === true && (
                            <div>
                              <i className="fa fa-check" /> Complimentary Wi-Fi
                              Internet
                            </div>
                          )}
                      </div>
                    </div>
                    <div className="col-lg-2 col-4 mt-3">
                      <div
                        style={{
                          fontSize: "15px",
                          fontWeight: "500",
                          color: "#222",
                        }}
                      >
                        Highlights
                      </div>
                      <div
                        style={{
                          fontSize: "10px",
                          color: "#777",
                        }}
                      >
                        {Object.keys(hotel).length > 0 &&
                        hotel.hotel.wifi === true ? (
                          <div>
                            <i className="fa fa-check" /> Refundable{" "}
                            <i className="fa fa-exclamation-circle" />
                          </div>
                        ) : (
                          <div>
                            <i className="fa fa-times" />
                            Non Refundable{" "}
                            <i className="fa fa-exclamation-circle" />
                          </div>
                        )}
                        {Object.keys(hotel).length > 0 &&
                          hotel.hotel.wifi === true && (
                            <div>
                              <i className="fa fa-wifi" /> Free Wi-Fi
                            </div>
                          )}
                      </div>
                    </div>
                    <div className="col-lg-3 col-6 text-left mt-3">
                      <div
                        style={{
                          fontSize: "15px",
                          fontWeight: "500",
                          color: "#222",
                        }}
                      >
                        Price For Stay
                      </div>
                      <div>
                        {Object.keys(hotel).length > 0 && (
                          <div>
                            <span
                              style={{
                                fontSize: "13px",
                                color: "#777",
                                textDecoration: "line-through",
                              }}
                            >
                              ₹ {hotel.hotel.prevPriceR1}
                            </span>
                            &nbsp;
                            <span
                              style={{
                                fontSize: "24px",
                                color: "#333",
                                fontWeight: "600",
                              }}
                            >
                              &nbsp;&nbsp;&nbsp; ₹{hotel.hotel.priceR1}
                            </span>
                            <div
                              style={{
                                fontSize: "14px",
                                color: "444",
                              }}
                            >
                              For 2 nights
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                    <div className="col-lg-3 col-6 text-left mt-3 text-lg-right">
                      <button
                        className="btn btn-danger btn-sm"
                        onClick={() => this.handleBook(hotel.hotel.priceR1)}
                      >
                        BOOK NOW
                      </button>
                      <div className="ml-1 mt-1">
                        <span
                          style={{
                            border: "1px solid lightgrey",
                            borderRadius: "20%",
                            fontSize: "10px",
                          }}
                        >
                          Get eCash<span className="bg-warning">₹500</span>
                        </span>
                      </div>
                    </div>
                    <div className="col-12">
                      <br />
                      {Object.keys(hotel).length > 0 && (
                        <div className="row ml-2 " style={{ fontSize: "12px" }}>
                          <span>
                            You Save&nbsp;
                            <span style={{ color: "#00a680" }}>
                              ₹{+hotel.hotel.prevPriceR1 - +hotel.hotel.priceR1}{" "}
                            </span>
                          </span>
                          &nbsp;| &nbsp;
                          <span
                            className="text-primary"
                            style={{ cursor: "pointer" }}
                          >
                            &nbsp;Login
                          </span>
                          &nbsp; {"&"} save more using&nbsp;
                          <span className="text-warning">eCash</span>
                        </div>
                      )}
                    </div>
                    <br />
                  </div>
                  <br />
                  <div style={{ border: ".4px dashed lightgrey" }}></div>
                  <div
                    className="pt-4 "
                    style={{
                      fontSize: "16px",
                      fontWeight: "550",
                      fontFamily: "Rubik-Regular,Arial",
                    }}
                  >
                    Deluxe AC Room With Breakfast
                  </div>
                  <div className="row ">
                    <div className="col-lg-2 col-4 mt-3">
                      <div
                        style={{
                          fontSize: "15px",
                          fontWeight: "500",
                          color: "#222",
                        }}
                      >
                        Max Guests
                      </div>
                      <div
                        style={{
                          fontSize: "25px",
                        }}
                      >
                        <i className="fa fa-male ml-1" />
                        <i className="fa fa-male ml-1" />
                        <i className="fa fa-male ml-1" />
                        <i
                          className="fa fa-male ml-1"
                          style={{
                            fontSize: "18px",
                          }}
                        />
                        <i
                          className="fa fa-male ml-1"
                          style={{
                            fontSize: "18px",
                          }}
                        />
                      </div>
                    </div>
                    <div className="col-lg-2 col-4 mt-3">
                      <div
                        style={{
                          fontSize: "15px",
                          fontWeight: "500",
                          color: "#222",
                        }}
                      >
                        Inclusions
                      </div>
                      <div
                        style={{
                          fontSize: "10px",
                          color: "#777",
                        }}
                      >
                        {Object.keys(hotel).length > 0 &&
                          hotel.hotel.wifi === true && (
                            <div>
                              <i className="fa fa-check" /> Complimentary Wi-Fi
                              Internet
                            </div>
                          )}
                        {Object.keys(hotel).length > 0 &&
                          hotel.hotel.breakfast === true && (
                            <div>
                              <i className="fa fa-check" />
                              &nbsp; Breakfast
                            </div>
                          )}
                      </div>
                    </div>
                    <div className="col-lg-2 col-4 mt-3">
                      <div
                        style={{
                          fontSize: "15px",
                          fontWeight: "500",
                          color: "#222",
                        }}
                      >
                        Highlights
                      </div>
                      <div
                        style={{
                          fontSize: "10px",
                          color: "#777",
                        }}
                      >
                        {Object.keys(hotel).length > 0 &&
                        hotel.hotel.wifi === true ? (
                          <div>
                            <i className="fa fa-check" /> Refundable{" "}
                            <i className="fa fa-exclamation-circle" />
                          </div>
                        ) : (
                          <div>
                            <i className="fa fa-times" />
                            Non Refundable{" "}
                            <i className="fa fa-exclamation-circle" />
                          </div>
                        )}
                        {Object.keys(hotel).length > 0 &&
                          hotel.hotel.wifi === true && (
                            <div>
                              <i className="fa fa-wifi" /> Free Wi-Fi
                            </div>
                          )}
                      </div>
                    </div>
                    <div className="col-lg-3 col-6 text-left mt-3">
                      <div
                        style={{
                          fontSize: "15px",
                          fontWeight: "500",
                          color: "#222",
                        }}
                      >
                        Price For Stay
                      </div>
                      <div>
                        {Object.keys(hotel).length > 0 && (
                          <div>
                            <span
                              style={{
                                fontSize: "13px",
                                color: "#777",
                                textDecoration: "line-through",
                              }}
                            >
                              ₹ {hotel.hotel.prevPriceR1 + 500}
                            </span>
                            &nbsp;
                            <span
                              style={{
                                fontSize: "24px",
                                color: "#333",
                                fontWeight: "600",
                              }}
                            >
                              &nbsp;&nbsp;&nbsp; ₹{hotel.hotel.priceR1 + 500}
                            </span>
                            <div
                              style={{
                                fontSize: "14px",
                                color: "444",
                              }}
                            >
                              For 2 nights
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                    <div className="col-lg-3 col-6 text-left mt-3 text-lg-right">
                      <button
                        className="btn btn-danger btn-sm"
                        onClick={() =>
                          this.handleBook(hotel.hotel.priceR1 + 500)
                        }
                      >
                        BOOK NOW
                      </button>
                      <div className="ml-1 mt-1">
                        <span
                          style={{
                            border: "1px solid lightgrey",
                            borderRadius: "20%",
                            fontSize: "11px",
                          }}
                        >
                          Get eCash<span className="bg-warning">₹500</span>
                        </span>
                      </div>
                    </div>
                    <div className="col-12">
                      <br />
                      {Object.keys(hotel).length > 0 && (
                        <div className="row ml-2 " style={{ fontSize: "12px" }}>
                          <span>
                            You Save&nbsp;
                            <span style={{ color: "#00a680" }}>
                              ₹{+hotel.hotel.prevPriceR1 - +hotel.hotel.priceR1}{" "}
                            </span>
                          </span>
                          &nbsp;| &nbsp;
                          <span
                            className="text-primary"
                            style={{ cursor: "pointer" }}
                          >
                            &nbsp;Login
                          </span>
                          &nbsp; {"&"} save more using&nbsp;
                          <span className="text-warning">eCash</span>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br />
            <div
              className="row  m-1 p-3"
              style={{ border: "2px solid lightgrey" }}
            >
              <div className="col-12" style={{ fontSize: "20px" }}>
                <b>SUPER DELUXE AC ROOM WITH BATHTUB</b>
              </div>
              <div className="row m-2 p-0">
                <div
                  className="col-lg-3 col-12 p-0"
                  style={{ backgroundColor: "lightgrey" }}
                >
                  {Object.keys(hotel).length > 0 && (
                    <img
                      src={hotel.hotel.roomImg}
                      style={{ width: "100%", height: "auto" }}
                    />
                  )}
                </div>
                <div className="col-lg-9 col-12 pl-3">
                  <div
                    className="pt-4 "
                    style={{
                      fontSize: "16px",
                      fontWeight: "550",
                      fontFamily: "Rubik-Regular,Arial",
                    }}
                  >
                    Super Deluxe AC Room With Bathtub Only
                  </div>
                  <div className="row ">
                    <div className="col-lg-2 col-4 mt-3">
                      <div
                        style={{
                          fontSize: "15px",
                          fontWeight: "500",
                          color: "#222",
                        }}
                      >
                        Max Guests
                      </div>
                      <div
                        style={{
                          fontSize: "25px",
                        }}
                      >
                        <i className="fa fa-male ml-1" />
                        <i className="fa fa-male ml-1" />
                        <i className="fa fa-male ml-1" />
                        <i
                          className="fa fa-male ml-1"
                          style={{
                            fontSize: "18px",
                          }}
                        />
                        <i
                          className="fa fa-male ml-1"
                          style={{
                            fontSize: "18px",
                          }}
                        />
                      </div>
                    </div>
                    <div className="col-lg-2 col-4 mt-3">
                      <div
                        style={{
                          fontSize: "15px",
                          fontWeight: "500",
                          color: "#222",
                        }}
                      >
                        Inclusions
                      </div>
                      <div
                        style={{
                          fontSize: "10px",
                          color: "#777",
                        }}
                      >
                        {Object.keys(hotel).length > 0 &&
                          hotel.hotel.wifi === true && (
                            <div>
                              <i className="fa fa-check" /> Complimentary Wi-Fi
                              Internet
                            </div>
                          )}
                      </div>
                    </div>
                    <div className="col-lg-2 col-4 mt-3">
                      <div
                        style={{
                          fontSize: "15px",
                          fontWeight: "500",
                          color: "#222",
                        }}
                      >
                        Highlights
                      </div>
                      <div
                        style={{
                          fontSize: "10px",
                          color: "#777",
                        }}
                      >
                        {Object.keys(hotel).length > 0 &&
                        hotel.hotel.wifi === true ? (
                          <div>
                            <i className="fa fa-check" /> Refundable{" "}
                            <i className="fa fa-exclamation-circle" />
                          </div>
                        ) : (
                          <div>
                            <i className="fa fa-times" />
                            Non Refundable{" "}
                            <i className="fa fa-exclamation-circle" />
                          </div>
                        )}
                        {Object.keys(hotel).length > 0 &&
                          hotel.hotel.wifi === true && (
                            <div>
                              <i className="fa fa-wifi" /> Free Wi-Fi
                            </div>
                          )}
                      </div>
                    </div>
                    <div className="col-lg-3 col-6 text-left mt-3">
                      <div
                        style={{
                          fontSize: "15px",
                          fontWeight: "500",
                          color: "#222",
                        }}
                      >
                        Price For Stay
                      </div>
                      <div>
                        {Object.keys(hotel).length > 0 && (
                          <div>
                            <span
                              style={{
                                fontSize: "13px",
                                color: "#777",
                                textDecoration: "line-through",
                              }}
                            >
                              ₹ {hotel.hotel.prevPriceR2}
                            </span>
                            &nbsp;
                            <span
                              style={{
                                fontSize: "24px",
                                color: "#333",
                                fontWeight: "600",
                              }}
                            >
                              &nbsp;&nbsp;&nbsp; ₹{hotel.hotel.priceR2}
                            </span>
                            <div
                              style={{
                                fontSize: "14px",
                                color: "444",
                              }}
                            >
                              For 2 nights
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                    <div className="col-lg-3 col-6 text-left mt-3 text-lg-right">
                      <button
                        className="btn btn-danger btn-sm"
                        onClick={() => this.handleBook(hotel.hotel.priceR2)}
                      >
                        BOOK NOW
                      </button>
                      <div className="ml-1 mt-1">
                        <span
                          style={{
                            border: "1px solid lightgrey",
                            borderRadius: "20%",
                            fontSize: "11px",
                          }}
                        >
                          Get eCash<span className="bg-warning">₹500</span>
                        </span>
                      </div>
                    </div>
                    <div className="col-12">
                      <br />
                      {Object.keys(hotel).length > 0 && (
                        <div className="row ml-2 " style={{ fontSize: "12px" }}>
                          <br />
                          <span>
                            You Save&nbsp;
                            <span style={{ color: "#00a680" }}>
                              ₹{+hotel.hotel.prevPriceR2 - +hotel.hotel.priceR2}{" "}
                            </span>
                          </span>
                          &nbsp;| &nbsp;
                          <span
                            className="text-primary"
                            style={{ cursor: "pointer" }}
                          >
                            &nbsp;Login
                          </span>
                          &nbsp; {"&"} save more using&nbsp;
                          <span className="text-warning">eCash</span>
                        </div>
                      )}
                    </div>
                    <br />
                  </div>
                  <br />
                  <div style={{ border: ".4px dashed lightgrey" }}></div>
                  <div
                    className="pt-4 "
                    style={{
                      fontSize: "16px",
                      fontWeight: "550",
                      fontFamily: "Rubik-Regular,Arial",
                    }}
                  >
                    Super Deluxe AC Room With Breakfast With Bathtub{" "}
                  </div>
                  <div className="row ">
                    <div className="col-lg-2 col-4 mt-3">
                      <div
                        style={{
                          fontSize: "15px",
                          fontWeight: "500",
                          color: "#222",
                        }}
                      >
                        Max Guests
                      </div>
                      <div
                        style={{
                          fontSize: "25px",
                        }}
                      >
                        <i className="fa fa-male ml-1" />
                        <i className="fa fa-male ml-1" />
                        <i className="fa fa-male ml-1" />
                        <i
                          className="fa fa-male ml-1"
                          style={{
                            fontSize: "18px",
                          }}
                        />
                        <i
                          className="fa fa-male ml-1"
                          style={{
                            fontSize: "18px",
                          }}
                        />
                      </div>
                    </div>
                    <div className="col-lg-2 col-4 mt-3">
                      <div
                        style={{
                          fontSize: "15px",
                          fontWeight: "500",
                          color: "#222",
                        }}
                      >
                        Inclusions
                      </div>
                      <div
                        style={{
                          fontSize: "10px",
                          color: "#777",
                        }}
                      >
                        {Object.keys(hotel).length > 0 &&
                          hotel.hotel.wifi === true && (
                            <div>
                              <i className="fa fa-check" /> Complimentary Wi-Fi
                              Internet
                            </div>
                          )}
                        {Object.keys(hotel).length > 0 &&
                          hotel.hotel.breakfast === true && (
                            <div>
                              <i className="fa fa-check" />
                              &nbsp; Breakfast
                            </div>
                          )}
                      </div>
                    </div>
                    <div className="col-lg-2 col-4 mt-3">
                      <div
                        style={{
                          fontSize: "15px",
                          fontWeight: "500",
                          color: "#222",
                        }}
                      >
                        Highlights
                      </div>
                      <div
                        style={{
                          fontSize: "10px",
                          color: "#777",
                        }}
                      >
                        {Object.keys(hotel).length > 0 &&
                        hotel.hotel.wifi === true ? (
                          <div>
                            <i className="fa fa-check" /> Refundable{" "}
                            <i className="fa fa-exclamation-circle" />
                          </div>
                        ) : (
                          <div>
                            <i className="fa fa-times" />
                            Non Refundable{" "}
                            <i className="fa fa-exclamation-circle" />
                          </div>
                        )}
                        {Object.keys(hotel).length > 0 &&
                          hotel.hotel.wifi === true && (
                            <div>
                              <i className="fa fa-wifi" /> Free Wi-Fi
                            </div>
                          )}
                      </div>
                    </div>
                    <div className="col-lg-3 col-6 text-left mt-3">
                      <div
                        style={{
                          fontSize: "15px",
                          fontWeight: "500",
                          color: "#222",
                        }}
                      >
                        Price For Stay
                      </div>
                      <div>
                        {Object.keys(hotel).length > 0 && (
                          <div>
                            <span
                              style={{
                                fontSize: "13px",
                                color: "#777",
                                textDecoration: "line-through",
                              }}
                            >
                              ₹ {hotel.hotel.prevPriceR2 + 500}
                            </span>
                            &nbsp;
                            <span
                              style={{
                                fontSize: "24px",
                                color: "#333",
                                fontWeight: "600",
                              }}
                            >
                              &nbsp;&nbsp;&nbsp; ₹{hotel.hotel.priceR2 + 500}
                            </span>
                            <div
                              style={{
                                fontSize: "14px",
                                color: "444",
                              }}
                            >
                              For 2 nights
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                    <div className="col-lg-3 col-6 text-left mt-3 text-lg-right">
                      <button
                        className="btn btn-danger btn-sm "
                        onClick={() =>
                          this.handleBook(hotel.hotel.priceR2 + 500)
                        }
                      >
                        BOOK NOW
                      </button>
                      <div className="ml-1 mt-1">
                        <span
                          style={{
                            border: "1px solid lightgrey",
                            borderRadius: "20%",
                            fontSize: "11px",
                          }}
                        >
                          Get eCash<span className="bg-warning">₹500</span>
                        </span>
                      </div>
                    </div>
                    <div className="col-12">
                      <br />
                      {Object.keys(hotel).length > 0 && (
                        <div className="row ml-2 " style={{ fontSize: "12px" }}>
                          <span>
                            You Save&nbsp;
                            <span style={{ color: "#00a680" }}>
                              ₹{+hotel.hotel.prevPriceR2 - +hotel.hotel.priceR2}{" "}
                            </span>
                          </span>
                          &nbsp;| &nbsp;
                          <span
                            className="text-primary"
                            style={{ cursor: "pointer" }}
                          >
                            &nbsp;Login
                          </span>
                          &nbsp; {"&"} save more using&nbsp;
                          <span className="text-warning">eCash</span>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default HotelDetail;
