import React, { Component } from "react";
import axios from "axios";
import queryString from "query-string";
import Navbar from "./navbar";
import "./hotellist.css";

let ratingRadio = [];
let priceRadio = [];

let today = new Date();
let todayDate = today.getDate();
let tM = today.getMonth();
let ty = today.getFullYear();
class HotelList extends Component {
  state = {
    priceData: [
      { name: "Less than Rs. 2,000 ", value: "<=2000" },
      { name: "Rs. 2,001 to Rs. 7,000 ", value: "2001-7000" },
      { name: "Rs. 7,001 to Rs. 10,000", value: "7001-10000" },
      { name: "Greater than Rs. 10,001 ", value: ">=10001" },
    ],
    ratingData: [
      { name: "0, 1", value: "1" },
      { name: "2", value: "2" },
      { name: "3", value: "3" },
      { name: "4", value: "4" },
      { name: "5", value: "5" },
    ],
    ratingTrue: false,
    priceTrue: false,
    dayData: ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"],
    month: [
      "JAN",
      "FEB",
      "MAR",
      "APR",
      "MAY",
      "JUN",
      "JUL",
      "AUG",
      "SEP",
      "OCT",
      "NOV",
      "DEC",
    ],
    hotel: this.props.hotel,
    // hotel: {
    //   city: "New Delhi",
    //   in: new Date(),
    //   out: new Date(ty, tM, todayDate + 32),
    //   room: [
    //     { roomNo: 0, adult: 2, child: 0 },
    //     { roomNo: 1, adult: 1, child: 1 },
    //   ],
    // },
  };

  async componentWillMount() {
    let { sort, rating, price } = queryString.parse(this.props.location.search);

    const { hotel: h } = this.state;
    let params = "";
    params = this.addToParams(params, "sort", sort);
    params = this.addToParams(params, "rating", rating);
    params = this.addToParams(params, "price", price);
    let apiForHotel =
      "https://us-central1-yat12-77281.cloudfunctions.net/app/hotel" +
      "/" +
      h.city;
    if (params) apiForHotel += params;
    let { data: hotelData } = await axios.get(apiForHotel);
    this.setState({ hotelData: hotelData });
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    let { sort, rating, price } = queryString.parse(this.props.location.search);

    const { hotel: h } = this.state;
    let params = "";
    params = this.addToParams(params, "sort", sort);
    params = this.addToParams(params, "rating", rating);
    params = this.addToParams(params, "price", price);
    let apiForHotel =
      "https://us-central1-yat12-77281.cloudfunctions.net/app/hotel" +
      "/" +
      h.city;
    if (params) apiForHotel += params;
    if (currProps !== prevProps) {
      let { data: hotelData } = await axios.get(apiForHotel);
      this.setState({ hotelData: hotelData });
    }
  }

  makeRadioStructure(data, sel) {
    let radio = {
      sectionData: data,
      selectedData: sel,
    };
    return radio;
  }

  handledropDown = (key) => {
    if (key === "ratingTrue") {
      this.setState({ ratingTrue: !this.state.ratingTrue });
    }
    if (key === "priceTrue") {
      this.setState({ priceTrue: !this.state.priceTrue });
    }
  };

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  callUrl = (params, sort, rating, price) => {
    let path = "/hotel-search";
    params = this.addToParams(params, "sort", sort);
    params = this.addToParams(params, "rating", rating);
    params = this.addToParams(params, "price", price);
    this.props.history.push({ pathname: path, search: params });
  };

  handleChange = (e) => {
    let { sort, rating, price } = queryString.parse(this.props.location.search);

    const { currentTarget: input } = e;

    if (input.name === "price") {
      priceRadio.selectedData = input.value;
      price = input.value;
    }
    if (input.name === "rating") {
      ratingRadio.selectedData = input.value;
      rating = input.value;
    }
    this.callUrl("", sort, rating, price);
  };

  handleRate = (v) => {
    let { sort, rating, price } = queryString.parse(this.props.location.search);
    rating = v;
    this.callUrl("", sort, rating, price);
  };

  clearFilter = (id) => {
    let { sort, rating, price } = queryString.parse(this.props.location.search);
    if (id === "price") price = "";
    if (id === "rating") rating = "";
    this.callUrl("", sort, rating, price);
  };

  handleSort = (s) => {
    let { sort, rating, price } = queryString.parse(this.props.location.search);
    sort = s;
    console.log("dd", sort, s);
    this.callUrl("", sort, rating, price);
  };

  handlestar = (s) => {
    if (+s <= 1) {
      return (
        <span className="checked">
          <i className="fa fa-star  text-danger" />
        </span>
      );
    }

    if (+s === 2) {
      return (
        <span className="checked">
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star  text-danger" />
        </span>
      );
    }

    if (+s === 3) {
      return (
        <span className="checked">
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star  text-danger" />
        </span>
      );
    }

    if (+s === 4) {
      return (
        <span className="checked">
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star  text-danger" />
        </span>
      );
    }

    if (+s === 5) {
      return (
        <span className="checked">
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star  text-danger" />
          <i className="fa fa-star  text-danger" />
        </span>
      );
    }
  };

  handleChoose = (h) => {
    let hotelChoose = {};
    hotelChoose.hotel = h;
    hotelChoose.guest = this.state.hotel;
    console.log("dd", hotelChoose);
    this.props.onChoose(hotelChoose);
    this.props.history.push("/hotel-search/details");
  };

  render() {
    let {
      ratingData,
      priceData,
      ratingTrue,
      priceTrue,
      hotel,
      hotelData: hd,
      dayData,
      month,
    } = this.state;
    let { sort, rating, price } = queryString.parse(this.props.location.search);
    sort = sort ? sort : "";
    rating = rating ? rating : "";
    price = price ? price : "";

    let checkin = "";
    let checkout = "";
    let day = "";
    let troom = 0;
    let tadult = 0;
    let tchild = 0;
    if (Object.keys(hotel).length > 0) {
      checkin =
        "" +
        dayData[hotel.in.getDay()] +
        "," +
        hotel.in.getDate() +
        " " +
        month[hotel.in.getMonth()];
      checkout =
        "" +
        dayData[hotel.out.getDay()] +
        "," +
        hotel.out.getDate() +
        " " +
        month[hotel.out.getMonth()];
      day = Math.ceil((hotel.out - hotel.in) / (1000 * 3600 * 24)) - 1;
      troom = hotel.room.length;
      tadult = hotel.room.reduce((a, b) => a + b.adult, 0);
      tchild = hotel.room.reduce((a, b) => a + b.child, 0);
    }
    priceRadio = this.makeRadioStructure(priceData, price);
    ratingRadio = this.makeRadioStructure(ratingData, rating);

    //console.log("Ddd", hotel, checkout, checkin, day);
    console.log("ff", hd, sort, day);

    return (
      <div className="" style={{ backgroundColor: "lightgrey" }}>
        {/* <Navbar /> */}
        <div className="row pl-3  pr-3  border-top">
          {/* <div className="col-lg-1 d-none d-lg-block"></div> */}
          <div className="col-lg-3 d-none d-lg-block bg-white pl-3 pr-3 pt-3 ml-5">
            <div className="border-bottom pb-3">
              {priceTrue === true ? (
                <div className="row">
                  <div
                    className="col-7 text-left ml-1"
                    style={{ fontWeight: "600", fontSize: "13px" }}
                  >
                    PRICE FOR STAY
                  </div>
                  <div
                    className="col-4 text-right text-muted"
                    style={{ fontSize: "10px", paddingTop: "4px" }}
                  >
                    {price && (
                      <span
                        style={{ cursor: "pointer" }}
                        onClick={() => this.clearFilter("price")}
                      >
                        CLEAR
                      </span>
                    )}
                    <span
                      className="ml-2"
                      style={{ cursor: "pointer" }}
                      onClick={() => this.handledropDown("priceTrue")}
                    >
                      SHOW
                    </span>
                  </div>
                </div>
              ) : (
                <div className="row">
                  <div
                    className="col-7 text-left ml-1"
                    style={{ fontWeight: "600", fontSize: "13px" }}
                  >
                    PRICE FOR STAY
                  </div>
                  <div
                    className="col-4 text-right text-muted"
                    style={{ fontSize: "10px", paddingTop: "4px" }}
                  >
                    {price && (
                      <span
                        style={{ cursor: "pointer" }}
                        onClick={() => this.clearFilter("price")}
                      >
                        CLEAR
                      </span>
                    )}
                    <span
                      className="ml-2"
                      style={{ cursor: "pointer" }}
                      onClick={() => this.handledropDown("priceTrue")}
                    >
                      HIDE
                    </span>
                  </div>
                  {priceRadio.sectionData.map((item, index) => (
                    <div
                      className="form-check col-12 ml-3 mt-1"
                      key={index}
                      style={{ fontSize: "14px" }}
                    >
                      <input
                        value={item.value}
                        onChange={this.handleChange}
                        id={item.name}
                        type="Radio"
                        name="price"
                        checked={item.value === priceRadio.selectedData}
                        className="form-check-input"
                      />
                      <label className="form-check-label" htmlFor={item}>
                        {item.name}
                      </label>
                    </div>
                  ))}
                </div>
              )}
            </div>
            <div className="border-bottom pb-3">
              {ratingTrue === true ? (
                <div className="row">
                  <div
                    className="col-7 text-left ml-1"
                    style={{ fontWeight: "600", fontSize: "13px" }}
                  >
                    STAR RATING
                  </div>
                  <div
                    className="col-4 text-right text-muted"
                    style={{ fontSize: "10px", paddingTop: "4px" }}
                  >
                    {rating && (
                      <span
                        style={{ cursor: "pointer" }}
                        onClick={() => this.clearFilter("rating")}
                      >
                        CLEAR
                      </span>
                    )}
                    <span
                      className="ml-2"
                      style={{ cursor: "pointer" }}
                      onClick={() => this.handledropDown("ratingTrue")}
                    >
                      SHOW
                    </span>
                  </div>
                </div>
              ) : (
                <div>
                  <div className="row">
                    <div
                      className="col-7 text-left ml-1"
                      style={{ fontWeight: "600", fontSize: "13px" }}
                    >
                      STAR RATING
                    </div>
                    <div
                      className="col-4 text-right text-muted"
                      style={{ fontSize: "10px", paddingTop: "4px" }}
                    >
                      {rating && (
                        <span
                          style={{ cursor: "pointer" }}
                          onClick={() => this.clearFilter("rating")}
                        >
                          CLEAR
                        </span>
                      )}
                      <span
                        className="ml-2"
                        style={{ cursor: "pointer" }}
                        onClick={() => this.handledropDown("ratingTrue")}
                      >
                        HIDE
                      </span>
                    </div>
                  </div>
                  <div className="row text-center ml-1 mt-1">
                    {ratingRadio.sectionData.map((item, index) => (
                      <div
                        className={
                          "col-2 text-center " +
                          (rating === item.value ? " border-bottom" : " border")
                        }
                        key={index}
                        style={{
                          borderTop: "3px solid red",
                          cursor: "pointer",
                        }}
                        onClick={() => this.handleRate(item.value)}
                      >
                        <span
                          className=""
                          style={{
                            fontSize: "13px",
                            fontWeight: "600",
                            borderTop: "2px",
                          }}
                        >
                          {item.name}
                        </span>
                        <br />
                        <i className="fas fa-star text-danger" />
                        <br />
                        <hr />
                      </div>
                    ))}
                  </div>
                </div>
              )}
            </div>
          </div>
          <div className="col-lg-8 col-12">
            <div className="row mt-2">
              <div className="col-12 text-center">
                <span style={{ fontWeight: "450", fontSize: "16px" }}>
                  {hotel.city}
                </span>
                <br />
                <small className="text-muted">
                  {checkin} - {checkout} ({day} {day > 1 ? " NIGHTS" : " NIGHT"}
                  ) | {troom} {troom > 1 ? " ROOMS" : " ROOM"} | {tadult}{" "}
                  {tadult > 1 ? " ADULTS" : " ADULT"} | {tchild}{" "}
                  {tchild > 1 ? " CHILDS" : " CHILD"}
                </small>
              </div>
            </div>
            <div className="col-12  bg-white mt-2">
              <div className="row">
                <div className="ml-4 heading">
                  <a style={{ padding: "10px" }}>
                    ALL HOTELS ({this.state.hotelData && hd.length})
                  </a>
                  <br />
                  <a style={{ padding: "10px" }}>
                    From ₹(
                    {this.state.hotelData && hd.length > 0 ? (
                      <span>
                        {hd.reduce((a, b) => Math.min(a, b.priceR1), 1000000)}
                      </span>
                    ) : (
                      0
                    )}
                    )
                  </a>
                </div>
              </div>
            </div>
            <br />
            <div className="row" style={{ fontSize: ".7889rem" }}>
              <div className="col-3 text-muted">SORT BY</div>
              {sort === "rating" ? (
                <div className="col-5 sorthover">
                  <span
                    style={{ cursor: "pointer", fontWeight: "500" }}
                    onClick={() => this.handleSort("")}
                  >
                    STAR RATING <i className="fa fa-arrow-down text-danger" />
                  </span>
                </div>
              ) : (
                <div className="col-5 sorthover">
                  <span
                    style={{ cursor: "pointer", fontWeight: "300" }}
                    onClick={() => this.handleSort("rating")}
                  >
                    STAR RATING
                  </span>
                </div>
              )}
              {sort === "price" ? (
                <div className="col-4 sorthover">
                  <span
                    style={{ cursor: "pointer", fontWeight: "500" }}
                    onClick={() => this.handleSort("")}
                  >
                    PRICE <i className="fa fa-arrow-down text-danger" />
                  </span>
                </div>
              ) : (
                <div className="col-4 sorthover">
                  <span
                    style={{ cursor: "pointer", fontWeight: "300" }}
                    onClick={() => this.handleSort("price")}
                  >
                    PRICE
                  </span>
                </div>
              )}
            </div>
            <br />
            {this.state.hotelData && (
              <div>
                {hd.map((h, i) => (
                  <div key={i} className="col-12">
                    <div className="row bg-white">
                      <div className="col-lg-3 col-12    p-0">
                        <img
                          src={h.img}
                          style={{ width: "100%", height: "100%" }}
                        />
                      </div>
                      <div className="col-lg-6 col-12  pl-4">
                        <div className="mt-2 pl-0 sorthover">
                          <span
                            className="p-0"
                            style={{
                              fontWeight: "450",
                              fontSize: "18px",
                              cursor: "pointer",
                            }}
                            onClick={() => this.handleChoose(h)}
                          >
                            {h.name.length > 40
                              ? h.name.substring(0, 40) + "..."
                              : h.name}
                          </span>
                        </div>
                        <div className="row text-primary ">
                          <span>{this.handlestar(+h.rating)}</span>
                          <span className="sorthover">
                            <span
                              style={{
                                fontSize: "13px",
                                fontWeight: "500",
                                cursor: "pointer",
                              }}
                            >
                              <i
                                className="fa fa-map-marker text-secondary"
                                aria-hidden="true"
                              />
                              {"  "}
                              {h.location}
                            </span>
                          </span>
                        </div>
                        <div className="row  mt-3" style={{ fontSize: "13px" }}>
                          <span>
                            {h.cancelation === true && (
                              <span className="mr-2">
                                <i className="fa fa-check-circle-o" /> {"  "}
                                Free Cancellation
                              </span>
                            )}
                            {h.wifi === true && (
                              <span className="mr-2">
                                <i className="fa fa-wifi" /> {"  "}
                                Free WiFi
                              </span>
                            )}
                            {h.cancelation === true && (
                              <span className="mr-2">
                                <i className="fa fa-coffee" /> {"  "}
                                Free Breakfast
                              </span>
                            )}
                          </span>
                        </div>
                        <div className="row  mt-3">
                          <img
                            src={"https://i.ibb.co/vms0HKZ/yts-sprite-v3.png"}
                            style={{
                              width: "100px",
                              height: "25px",
                              cursor: "pointer",
                            }}
                          />
                        </div>
                        <br />
                        <div
                          className="row pb-2 d-lg-block d-none"
                          style={{ fontSize: "12px", whiteSpace: "nowrap" }}
                        >
                          <span>
                            You Save&nbsp;
                            <span style={{ color: "#00a680" }}>
                              ₹{+h.prevPriceR1 - +h.priceR1}{" "}
                            </span>
                          </span>
                          &nbsp;| &nbsp;
                          <span
                            className="text-primary"
                            style={{ cursor: "pointer" }}
                          >
                            &nbsp;Login
                          </span>
                          &nbsp; {"&"} get additional Rs.250 off using&nbsp;
                          <span className="text-warning">eCash</span>
                        </div>
                      </div>
                      <div className=" col-lg-3 col-12 ">
                        <div className="row mt-3 pr-3 ">
                          <div className="col-lg-12 col-12 text-left text-lg-right">
                            <span className="">
                              <i
                                className="fa fa-tripadvisor"
                                style={{ fontSize: "13px" }}
                              />{" "}
                              <span
                                style={{
                                  color: "#00a680",
                                  fontWeight: "500",
                                  fontSize: "13px",
                                }}
                              >
                                {h.rating}/5
                              </span>
                              <span className="text-muted sorthover">
                                <span
                                  style={{
                                    fontSize: "13px",
                                    cursor: "pointer",
                                  }}
                                >
                                  &nbsp; ({h.visitor})
                                </span>
                              </span>
                            </span>
                          </div>
                          <div className="col-lg-12 col-12 pr-3 pt-2 text-lg-right text-left">
                            <div
                              className=" "
                              style={{
                                style: "Rubik-Medium",
                                cursor: "pointer",
                              }}
                            >
                              <span
                                style={{
                                  fontSize: "14px",
                                  color: "#999",
                                  textDecoration: "line-through",
                                  paddingRight: "10px",
                                }}
                              >
                                ₹{h.prevPriceR1}
                              </span>
                              <span
                                style={{
                                  fontSize: "21.994px",
                                  color: "#333",
                                  fontWeight: "400",
                                }}
                              >
                                ₹{h.priceR1}
                              </span>
                            </div>
                          </div>
                          <div className="col-lg-12 text-lg-right text-left pr-3">
                            <div className="">
                              <span style={{ fontSize: "12px" }}>
                                For 2 nights
                              </span>
                            </div>
                          </div>
                          <div className="col-lg-12 col-4 pr-3 pb-3 pt-2 mt-3 text-lg-right text-left">
                            <div className="">
                              <button
                                className="btn btn-danger btn-sm"
                                style={{ whiteSpace: "nowrap" }}
                                onClick={() => this.handleChoose(h)}
                              >
                                Choose Room
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div
                        className="row  p-3 ml-1  d-lg-none d-md-block"
                        style={{ fontSize: "12px", whiteSpace: "nowrap" }}
                      >
                        <span>
                          You Save&nbsp;
                          <span style={{ color: "#00a680" }}>
                            ₹{+h.prevPriceR1 - +h.priceR1}{" "}
                          </span>
                        </span>
                        &nbsp;| &nbsp;
                        <span
                          className="text-primary"
                          style={{ cursor: "pointer" }}
                        >
                          &nbsp;Login
                        </span>
                        &nbsp; {"&"} get additional Rs.250 off using&nbsp;
                        <span className="text-warning">eCash</span>
                      </div>
                    </div>
                    <br />
                  </div>
                ))}
              </div>
            )}
          </div>
          <div className="col d-none d-lg-block"></div>
        </div>
      </div>
    );
  }
}

export default HotelList;
