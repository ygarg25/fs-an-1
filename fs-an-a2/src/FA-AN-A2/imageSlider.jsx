import React, { Component } from "react";
import "./imageslider.css";

class ImageSlider extends Component {
  state = {
    hotelImg: this.props.hotelImg,
    i: 0,
  };
  handleslide = (s) => {
    this.setState({ i: this.state.i + s });
  };
  handleShow = (s) => {
    this.setState({ i: s });
  };
  render() {
    let { hotelImg: hi, i } = this.state;
    return (
      <div>
        <div className="outer">
          <div className="img">
            <ul className="imgGallery">
              <li className="">
                <img src={hi[i]} />
              </li>
            </ul>
            <div className="action">
              {i > 0 ? (
                <a className="previous" onClick={() => this.handleslide(-1)}>
                  <i class="fa fa-angle-left"></i>
                </a>
              ) : (
                <a className="previous">
                  <i class="fa fa-angle-left"></i>
                </a>
              )}
              {i < hi.length - 1 ? (
                <a className="next" onClick={() => this.handleslide(+1)}>
                  <i class="fa fa-angle-right"></i>
                </a>
              ) : (
                <a className="next">
                  <i class="fa fa-angle-right"></i>
                </a>
              )}
            </div>
          </div>
          <div className="imgslider">
            <ul className="mt-1" style={{ listStyle: "none", padding: 0 }}>
              {hi.map((h, v) => (
                <li key={v} onClick={() => this.handleShow(v)}>
                  <a style={{ cursor: "pointer" }}>
                    <img
                      className={i === v ? " imgclicked" : " imglist"}
                      src={hi[v]}
                    ></img>
                  </a>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default ImageSlider;
