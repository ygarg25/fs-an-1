import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./leftpanel.jsx";

class LeftPanel extends Component {
  state = {};
  render() {
    const opt = this.props.opt;
    return (
      <div className="p-0 text-white">
        <ul className="list-group">
          <Link to="#" style={{ cursor: "pointer", textDecoration: "none" }}>
            <li className={"list-group-item bg-dark text-white"}>
              <i className="fa fa-home" />
              &nbsp; DASHBOARD{" "}
            </li>
          </Link>
          <Link
            to="/manage-booking/My Booking/Hotel"
            style={{ cursor: "pointer", textDecoration: "none" }}
          >
            <li
              className={
                "list-group-item text-white " +
                (opt === "My Booking" ? " bg-danger" : " bg-dark")
              }
              style={{ cursor: "pointer", textDecoration: "none" }}
            >
              <i className="fa fa-plane " />
              &nbsp; ALL BOOKING
            </li>
          </Link>
          <Link
            to="/manage-booking/My eCash"
            style={{ cursor: "pointer", textDecoration: "none" }}
          >
            <li
              className={
                "list-group-item text-white " +
                (opt === "My eCash" ? " bg-danger" : " bg-dark")
              }
              style={{ cursor: "pointer", textDecoration: "none" }}
            >
              <i className="fa fa-money" aria-hidden="true" />
              &nbsp; MY eCASH
            </li>
          </Link>
          <Link
            to="/manage-booking/My Profile"
            style={{ cursor: "pointer", textDecoration: "none" }}
          >
            <li
              className={
                "list-group-item  text-white" +
                (opt === "My Profile" ? " bg-danger" : " bg-dark")
              }
              style={{ cursor: "pointer", textDecoration: "none" }}
            >
              <i className="fa fa-user" aria-hidden="true" />
              &nbsp; YOUR PROFILE
            </li>
          </Link>
        </ul>
      </div>
    );
  }
}

export default LeftPanel;
