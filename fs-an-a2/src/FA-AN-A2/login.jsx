import React, { Component } from "react";
import axios from "axios";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import Navbar from "./navbar";
import "./login.css";
import { Redirect, Link } from "react-router-dom";

let url = "http://localhost:2410";
// let url = "https://us-central1-yat12-77281.cloudfunctions.net/app";

class Login extends Component {
  state = {
    userData: {
      username: "",
      password: "",
    },
  };
  handleSubmit = async (fields) => {
    console.log("submitted : ", fields);
    let apiEndPoint = url + "/login";
    let { state } = this.props.location;
    console.log("ssfff", state);
    try {
      const { data: response } = await axios.post(apiEndPoint, fields);
      console.log("response", response.token);
      localStorage.setItem("token", response.token);
      //window.location = "/";
      window.location = state ? state.from.pathname : "/";
    } catch (err) {
      if (err.response) {
        alert("Invalid Username and Password");
      }
    }
  };
  render() {
    if (localStorage.getItem("token")) return <Redirect to="/" />;
    const { userData } = this.state;
    console.log("Ss", this.state.errors);
    return (
      <div>
        <Formik
          initialValues={{
            username: "",
            password: "",
          }}
          validationSchema={Yup.object().shape({
            username: Yup.string()
              .email("Email is invalid")
              .required("Email is required"),
            password: Yup.string()
              .min(6, "Password must be at least 6 characters")
              .required("Password is required"),
          })}
          onSubmit={(fields) => {
            this.handleSubmit(fields);
          }}
          render={({ errors, status, touched, values }) => {
            return (
              <div style={{ backgroundColor: "#F4F6F7" }}>
                {/* <Navbar /> */}
                <div
                  className="row m-0 p-4 mt-0"
                  style={{ backgroundColor: "#F4F6F7" }}
                >
                  <div className="col-12 text-center">
                    <h4>Welcome to Yatra!</h4>
                  </div>
                  <div
                    className="col-12 text-center mt-2"
                    style={{ fontWeight: "500", fontSize: "12px" }}
                  >
                    <p>
                      Please Login/Register using your Email/Mobile to continue
                    </p>
                  </div>
                  <div className="col-3 d-none d-lg-block"></div>

                  <div
                    className="col-lg-6 col-12 bg-white p-4 m-2"
                    style={{ border: "3px", borderRadius: "7px" }}
                  >
                    <div className="row">
                      <div className="col-lg-6 col-12 text-center">
                        <div className="row">
                          <div className="col-12">
                            <i
                              className="far fa-user-circle text-muted"
                              style={{ fontSize: "100px" }}
                            />
                          </div>
                        </div>
                        <div className="row mt-2">
                          <div className="col-12">
                            <span
                              className="text-muted"
                              style={{ fontWeight: "450", fontSize: "18px" }}
                            >
                              EMAIL ID / MOBILE NUMBER
                            </span>
                          </div>
                        </div>
                        <div className="row  mt-3">
                          <div className="col-12 text-center">
                            <Form>
                              <div className="form-group">
                                <Field
                                  type="text"
                                  name="username"
                                  placeholder="EMAIL ID / MOBILE NUMBER"
                                  className={
                                    "form-control" +
                                    (errors.username && touched.username
                                      ? " is-invalid"
                                      : "")
                                  }
                                />
                                <ErrorMessage
                                  name="username"
                                  component="div"
                                  className="invalid-feedback"
                                />
                              </div>
                              <div className="form-group">
                                <Field
                                  type="password"
                                  name="password"
                                  placeholder="PASSWORD"
                                  className={
                                    "form-control" +
                                    (errors.password && touched.password
                                      ? " is-invalid"
                                      : "")
                                  }
                                />
                                <ErrorMessage
                                  name="password"
                                  component="div"
                                  className="invalid-feedback"
                                />
                              </div>
                              <div className="row">
                                <div className="col-12 text-center">
                                  <button
                                    type="submit"
                                    className="btn btn-danger col-12"
                                  >
                                    LogIn
                                  </button>
                                </div>
                              </div>
                            </Form>
                          </div>
                          <hr />
                          <div className="col-12 text-left">
                            <Link to="/signup">
                              <small>
                                For Registering New User Click Here...
                              </small>
                            </Link>
                          </div>
                          <div className="col-12">
                            <hr />
                          </div>
                          <div className="col-12 text-left">
                            <small>
                              By proceeding, you agree with our{" "}
                              <span
                                className="text-primary"
                                style={{ cursor: "pointer" }}
                              >
                                Terms of Service
                              </span>
                              ,{" "}
                              <span
                                className="text-primary"
                                style={{ cursor: "pointer" }}
                              >
                                Privacy Policy
                              </span>{" "}
                              & &nbsp;
                              <span
                                className="text-primary"
                                style={{ cursor: "pointer" }}
                              >
                                Master User Agreement.
                              </span>
                            </small>
                          </div>
                        </div>
                        <hr />
                        <div className="row">
                          <div className="col-2"></div>
                          <div className="col-8">
                            <button
                              className="btn btn-outline-primary btn-sm"
                              style={{
                                fontSize: "13px",
                                fontWeight: "450",
                              }}
                            >
                              <i className="fab fa-facebook-square" />
                              &nbsp;&nbsp; <b>Sign In With Facebook</b>
                            </button>
                            <br />
                            <br />
                            <button
                              className="btn btn-primary btn-sm"
                              style={{
                                fontSize: "13px",
                                fontWeight: "450",
                              }}
                            >
                              <i className="fab fa-google-plus-square" />
                              &nbsp;&nbsp; <b>Sign In With Google</b>
                            </button>
                            <br />
                          </div>
                          <div className="col-2"></div>
                        </div>
                        <br />
                        <br />
                      </div>
                      <div
                        className="col-lg-6 col-12"
                        style={{
                          border: "1px",
                          borderRadius: "50px",
                        }}
                      >
                        <div className="row">
                          <div
                            className="col-12 text-center p-2"
                            style={{
                              backgroundColor: "#feeab8",
                              borderRadius: "10px 10px 0 0",
                              fontSize: "14px",
                            }}
                          >
                            <span>
                              <b>Logged In/Registered users get MORE!</b>
                            </span>
                          </div>
                          <div
                            className="col-12 p-3"
                            style={{
                              backgroundColor: "#fef2d8",
                              fontSize: "14px",
                            }}
                          >
                            <div>
                              <ul
                                style={{
                                  listStyle: "none",
                                  padding: "10px",
                                  paddingRight: "0px",
                                }}
                              >
                                <li>
                                  <i className="icon-cal"></i>
                                  <p>
                                    <strong>View</strong>/
                                    <strong>Cancel</strong>/
                                    <strong>Reschedule</strong> bookings
                                  </p>
                                </li>
                                <li>
                                  <i className="icon-air-tick"></i>
                                  <p>
                                    Check booking <strong>history</strong>,
                                    manage <strong>cancellations </strong> &amp;
                                    print <strong>eTickets</strong>
                                  </p>
                                </li>
                                <li>
                                  <i className="ico-note"></i>
                                  <p>
                                    Book faster with{" "}
                                    <strong>Pre-Filled Forms</strong>, saved{" "}
                                    <strong>Travellers</strong> &amp;{" "}
                                    <strong>Saved Cards</strong>
                                  </p>
                                </li>
                                <li>
                                  <i className="icon-e-cash"></i>
                                  <p>
                                    Use Yatra <strong>eCash </strong> to get{" "}
                                    <strong>discounts</strong>
                                  </p>
                                </li>
                                <li>
                                  <i className="icon-e-cash-transport"></i>
                                  <p>
                                    <strong>Transfer eCash</strong> to your{" "}
                                    <strong>Family</strong>/
                                    <strong>Friends</strong>
                                  </p>
                                </li>
                                <li>
                                  <i className="icon-e-cash-shop"></i>
                                  <p>
                                    <strong>Convert eCash</strong> to{" "}
                                    <strong>Shopping Coupons</strong> from{" "}
                                    <strong>Amazon</strong>,
                                    <strong>BookMyShow</strong>, etc.
                                  </p>
                                </li>
                                <li>
                                  <i className="icon-sme"></i>
                                  <p className="mb-0">
                                    Do you have GST number?
                                  </p>
                                  <p style={{ paddingLeft: "30px" }}>
                                    Additional Benefits of{" "}
                                    <strong>
                                      Free Meals, Low Cancellation Fee, Free
                                      Rescheduling
                                    </strong>{" "}
                                    for SME business customers
                                  </p>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-3 d-none d-lg-block"></div>
                  <br />
                </div>
              </div>
            );
          }}
        />
      </div>
    );
  }
}

export default Login;
