import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Navbar from "./navbar";
import FrontScreen from "./frontScreen";
import FlightDetail from "./fightdetail";
import FlightBook from "./flightbook";
import FlightPayemnt from "./flightPayment";
import HotelList from "./hotellist";
import HotelDetail from "./hoteldetail";
import HotelBook from "./hotelBook";
import HotelPayment from "./hotelPayment";
import Login from "./login";
import Regitser from "./register";
import LogOut from "./signout";
import Dashboard from "./dashboard";
import ProtectedRoute from "./protectedRoute";

class Main extends Component {
  state = {
    ticketDetail: {
      dept: "",
      going: "",
      deptdate: "",
      returndate: "",
      tclass: "",
      totalpasanger: "",
      returnstatus: "",
      adult: 1,
      child: 0,
      infants: 0,
    },
    flightBook: {
      departure: {
        id: "",
        logo: "",
        name: "",
        code: "",
        checkin: 0,
        meal: "",
        desDept: "",
        desArr: "",
        Price: 0,
        airBus: "",
        total: "",
        timeDept: "",
        timeArr: "",
        T1: "",
        T2: "",
        dept: "",
        arr: "",
        pickedDate: "",
        returnDate: "",
        count: 0,
        type: "",
        travellers: [
          { name: "Adult", count: 0 },
          { name: "Child", count: 0 },
          { name: "Infant", count: 0 },
        ],
      },
      arrival: {},
    },
    //hotel: { city: "", in: "", out: "", room: "" },
    hotel: {},
    hotelChoose: {},
    hotelBook: {},
  };

  handleTicketDetail = (t) => {
    this.setState({ ticketDetail: t });
  };

  handleFlightBook = (t) => {
    this.setState({ flightBook: t });
  };

  handleHotel = (h) => {
    this.setState({ hotel: h });
  };

  handleHotelChoose = (h) => {
    this.setState({ hotelChoose: h });
  };
  handleHotelbook = (h) => {
    this.setState({ hotelBook: h });
  };

  render() {
    //console.log("ticket detail", this.state.ticketDetail);
    //console.log("FLight detail", this.state.flightBook);
    //console.log("Hotel", this.state.hotel);
    //console.log("Choose", this.state.hotelChoose);
    return (
      <div className=" m-0">
        <Navbar />
        <div>
          <Switch>
            <Route
              path="/manage-booking/:opt/:detail?/:id?"
              component={Dashboard}
            />
            <Route path="/signout" component={LogOut} />
            <Route path="/signup" component={Regitser} />
            <Route path="/login" component={Login} />
            <Route
              path="/yatra/airSearch"
              render={(props) => (
                <FlightDetail
                  ticketDetail={this.state.ticketDetail}
                  onBook={this.handleFlightBook}
                  {...props}
                />
              )}
            />
            <Route
              path="/booking"
              render={(props) => (
                <FlightBook flightBook={this.state.flightBook} {...props} />
              )}
            />
            <ProtectedRoute path="/payment" component={FlightPayemnt} />
            <Route
              path="/hotel-book"
              render={(props) => (
                <HotelBook hotel={this.state.hotelBook} {...props} />
              )}
            />
            <ProtectedRoute path="/hotel-payment" component={HotelPayment} />
            <Route
              path="/hotel-search/details"
              render={(props) => (
                <HotelDetail
                  hotel={this.state.hotelChoose}
                  hotelbook={this.handleHotelbook}
                  {...props}
                />
              )}
            />
            <Route
              path="/hotel-search"
              render={(props) => (
                <HotelList
                  hotel={this.state.hotel}
                  onChoose={this.handleHotelChoose}
                  {...props}
                />
              )}
            />
            <Route
              path="/"
              render={(props) => (
                <FrontScreen
                  ticketBooking={this.handleTicketDetail}
                  handleHotel={this.handleHotel}
                  {...props}
                />
              )}
            />
            <Redirect to="/" />
          </Switch>
        </div>
      </div>
    );
  }
}

export default Main;
