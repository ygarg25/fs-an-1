import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import "./navbar.css";

let url = "http://localhost:2410";
// let url = "https://us-central1-yat12-77281.cloudfunctions.net/app";

class Navbar extends Component {
  state = {};

  async componentWillMount() {
    const jwt = localStorage.getItem("token");
    // console.log("get current user", jwt);
    try {
      const { data: response } = await axios.get(url + "/user", {
        headers: {
          Authorization: jwt,
        },
      });
      // console.log("get current user", response);
      this.setState({ user: response });
    } catch (ex) {
      console.log("catch");
      return null;
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    const jwt = localStorage.getItem("token");
    // console.log("get current user", jwt);
    if (prevProps !== currProps) {
      try {
        const { data: response } = await axios.get(url + "/user", {
          headers: {
            Authorization: jwt,
          },
        });
        // console.log("get current user", response);
        this.setState({ user: response });
      } catch (ex) {
        console.log("catch");
        return null;
      }
    }
  }

  render() {
    // console.log("user navbar", this.state.user);
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light text-white">
        <Link className="navbar-brand" to="/">
          <img
            style={{ height: "40px" }}
            src={
              "https://www.yatra.com/content/fresco/beetle/images/newIcons/yatra_logo.svg"
            }
          />
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ml-auto">
            {!this.state.user && (
              <li className="nav-item dropdown">
                <Link
                  className="nav-link dropdown-toggle"
                  to="#"
                  id="navbarDropdown"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  My Account
                </Link>
                <div
                  className="dropdown-menu dr"
                  aria-labelledby="navbarDropdown"
                >
                  <div className="row p-2">
                    <div className="col-12 text-center mb-3 mt-3">
                      <i
                        className="far fa-user-circle text-muted"
                        style={{ fontSize: "50px" }}
                      />
                    </div>
                    <div className="col-6 btn1 text-center">
                      <Link className="" to="/login">
                        <button className="btn btn-danger">LogIn</button>
                      </Link>
                    </div>
                    <div className="col-6 btn1 text-center">
                      <Link className="" to="/signup">
                        <button className="btn btn-outline-danger ">
                          SignUp
                        </button>
                      </Link>
                    </div>
                  </div>
                  <div className="dropdown-divider"></div>
                  <small className="text-muted">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Switch to:
                  </small>
                  <br />
                  <Link className="dropdown-item" to="#">
                    Yatra for Business
                  </Link>
                  <Link className="dropdown-item" to="#">
                    Yatra for Travel Agents
                  </Link>
                </div>
              </li>
            )}
            {this.state.user && (
              <li className="nav-item dropdown">
                <Link
                  className="nav-link dropdown-toggle"
                  to="#"
                  id="navbarDropdown"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Hi {this.state.user.Fname}
                </Link>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <Link
                    className="dropdown-item"
                    to="/manage-booking/My Booking/Hotel"
                  >
                    <i
                      className="fas fa-caret-right"
                      style={{ color: "lightgrey" }}
                    />
                    &nbsp;&nbsp; My Booking
                  </Link>
                  <Link className="dropdown-item" to="/manage-booking/My eCash">
                    <i
                      className="fas fa-caret-right"
                      style={{ color: "lightgrey" }}
                    />
                    &nbsp;&nbsp; My eCash
                  </Link>
                  <Link
                    className="dropdown-item"
                    to="/manage-booking/My Profile"
                  >
                    <i
                      className="fas fa-caret-right"
                      style={{ color: "lightgrey" }}
                    />
                    &nbsp;&nbsp; My Profile
                  </Link>
                  <Link className="dropdown-item" to="/signout">
                    &nbsp;&nbsp;&nbsp;&nbsp;Logout
                  </Link>
                  <div className="dropdown-divider"></div>
                  <small className="text-muted">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Switch to:
                  </small>
                  <br />
                  <Link className="dropdown-item" to="#">
                    Yatra for Business
                  </Link>
                  <Link className="dropdown-item" to="#">
                    Yatra for Travel Agents
                  </Link>
                </div>
              </li>
            )}
            <li className="nav-item dropdown">
              <Link
                className="nav-link dropdown-toggle"
                to="#"
                id="navbarDropdown"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Support
              </Link>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <Link className="dropdown-item" to="#">
                  <i
                    className="fas fa-caret-right"
                    style={{ color: "lightgrey" }}
                  />
                  &nbsp;&nbsp; Talk To Us
                </Link>
                <Link className="dropdown-item" to="#">
                  <i
                    className="fas fa-caret-right"
                    style={{ color: "lightgrey" }}
                  />
                  &nbsp;&nbsp; Contact Us
                </Link>
                <Link className="dropdown-item" to="#">
                  <i
                    className="fas fa-caret-right"
                    style={{ color: "lightgrey" }}
                  />
                  &nbsp;&nbsp; Complete Booking
                </Link>
                <Link className="dropdown-item" to="#">
                  <i
                    className="fas fa-caret-right"
                    style={{ color: "lightgrey" }}
                  />
                  &nbsp;&nbsp; Make a Payment
                </Link>
                <Link className="dropdown-item" to="#">
                  <i
                    className="fas fa-caret-right"
                    style={{ color: "lightgrey" }}
                  />
                  &nbsp;&nbsp; Flight Cancellation Charges
                </Link>
              </div>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="#">
                Offers
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="#">
                Join Yatra for Business
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default Navbar;
