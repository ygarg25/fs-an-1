import React, { Component } from "react";
import axios from "axios";

let url = "http://localhost:2410";
// let url = "https://us-central1-yat12-77281.cloudfunctions.net/app";

class Profile extends Component {
  state = {};
  async componentWillMount() {
    const jwt = localStorage.getItem("token");
    console.log("get current user", jwt);
    try {
      const { data: response } = await axios.get(url + "/user", {
        headers: {
          Authorization: jwt,
        },
      });
      console.log("get current user", response);
      this.setState({ user: response });
    } catch (ex) {
      console.log("catch");
      return null;
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    const jwt = localStorage.getItem("token");
    console.log("get current user", jwt);
    if (prevProps !== currProps) {
      try {
        const { data: response } = await axios.get(url + "/user", {
          headers: {
            Authorization: jwt,
          },
        });
        console.log("get current user", response);
        this.setState({ user: response });
      } catch (ex) {
        console.log("catch");
        return null;
      }
    }
  }
  render() {
    const { user } = this.state;
    console.log("user", user);
    return (
      <div>
        <div className="row">
          <div
            className="col-12 p-3 pl-5"
            style={{ backgroundColor: "#F4F6F7" }}
          >
            <span
              style={{
                fontWeight: "450",
                fontFamily: "Arial",
                color: "#666",
                fontSize: "14px",
              }}
            >
              MY ACCOUNT
            </span>
          </div>
          <div className="col-12 p-2 pl-3">
            <span style={{ color: "#333", fontWeight: "500" }}>MY PROFILE</span>
          </div>
          <div className="col-11  m-3 mr-5 border  mt-0 ">
            {this.state.user && (
              <div className="row ">
                <div
                  className="col-2 pl-2 pt-2 mt-1"
                  style={{ fontSize: "13px" }}
                >
                  <span
                    className="fa-stack fa-2x text-danger"
                    style={{ cursor: "pointer" }}
                  >
                    <i className="fa fa-circle fa-stack-2x"></i>
                    <i className="far fa-user  fa-stack-1x fa-lg fa-inverse "></i>
                  </span>
                </div>
                <div className="col-7 pt-2 pl-4 pr-0 pb-1 pl-lg-0">
                  <div>
                    <h5>
                      {user.Title}&nbsp;{user.Fname}&nbsp;{user.Lname}
                    </h5>
                  </div>
                  <div className="text-muted">
                    <small>
                      <i className="fa fa-envelope" />
                      &nbsp;&nbsp;
                      {user.Email}
                    </small>
                    <br />
                    <small>
                      <i className="fa fa-phone" />
                      &nbsp;&nbsp;
                      {user.Mob}
                    </small>
                  </div>
                </div>
                <div className="col text-right p-3">
                  <i
                    className="fa fa-pencil-square text-danger"
                    style={{ cursor: "pointer", fontSize: "30px" }}
                  />
                </div>
              </div>
            )}
          </div>
          <div className="col-12 p-2 pl-3">
            <span style={{ color: "#333", fontWeight: "500" }}>
              GST DETAILS
            </span>
          </div>
          <div className="col-11  m-3 mr-5 border  mt-0 ">
            <div className="row ">
              <div
                className="col-2 pl-2 pt-2 mt-1"
                style={{ fontSize: "13px" }}
              >
                <span
                  className="fa-stack fa-2x text-danger"
                  style={{ cursor: "pointer" }}
                >
                  <i className="fa fa-circle fa-stack-2x"></i>
                  <i className="fa fa-university  fa-stack-1x fa-lg fa-inverse "></i>
                </span>
              </div>
              <div className="col-7 pt-2 pl-4 pr-0 pb-1 pl-lg-0">
                <div>
                  <h5>GST Number</h5>
                </div>
                <div className="text-muted">
                  <small>Add GST Details</small>
                </div>
              </div>
              <div className="col text-right p-3">
                <i
                  className="fa fa-plus-square text-danger"
                  style={{ cursor: "pointer", fontSize: "30px" }}
                />
              </div>
            </div>
          </div>
          <div className="col-12 p-2 pl-3">
            <span style={{ color: "#333", fontWeight: "500" }}>
              SAVED TRAVELLERS
            </span>
          </div>
          <div className="col-11  m-3 mr-5 border  mt-0 ">
            <div className="row ">
              <div
                className="col-2 pl-2 pt-2 mt-1"
                style={{ fontSize: "13px" }}
              >
                <span
                  className="fa-stack fa-2x text-danger"
                  style={{ cursor: "pointer" }}
                >
                  <i className="fa fa-circle fa-stack-2x"></i>
                  <i className="fa fa-users  fa-stack-1x fa-lg fa-inverse "></i>
                </span>
              </div>
              <div className="col-7 pt-2 pl-4 pr-0 pb-1 pl-lg-0">
                <div>
                  <h5>No Travellers added</h5>
                </div>
                <div className="text-muted">
                  <small>Add Traveller for a faster booking experience</small>
                </div>
              </div>
              <div className="col text-right p-3">
                <i
                  className="fa fa-plus-square text-danger"
                  style={{ cursor: "pointer", fontSize: "30px" }}
                />
              </div>
            </div>
          </div>
        </div>
        <br />
      </div>
    );
  }
}

export default Profile;
