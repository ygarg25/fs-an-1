import React, { Component } from "react";
import axios from "axios";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import Navbar from "./navbar";
import { Link, Redirect } from "react-router-dom";

let url = "http://localhost:2410";
// let url = "https://us-central1-yat12-77281.cloudfunctions.net/app";

class Regitser extends Component {
  state = {};

  handleSubmit = async (fields) => {
    console.log("submitted : ", fields);
    try {
      let apiEndPoint = url + "/register";
      const { data: response } = await axios.post(apiEndPoint, fields);
      if (response.error == 1062) {
        alert("Username already exist, try adding another Username");
      } else {
        alert("Successfully Added... ");
        window.location = "/login";
      }
    } catch (err) {
      console.log(err);
      alert("Registeration Unsuccessfull! Please try again....");
    }
  };
  render() {
    if (localStorage.getItem("token")) return <Redirect to="/" />;
    return (
      <div>
        <Formik
          initialValues={{
            title: "",
            firstName: "",
            lastName: "",
            email: "",
            password: "",
            mob: "",
          }}
          validationSchema={Yup.object().shape({
            title: Yup.string().required("Title is required"),
            firstName: Yup.string().required("First Name is required"),
            lastName: Yup.string().required("Last Name is required"),
            email: Yup.string()
              .email("Email is invalid")
              .required("Email is required"),
            password: Yup.string()
              .min(6, "Password must be at least 6 characters")
              .required("Password is required"),
            mob: Yup.number()
              .integer("Enter Valid Number")
              .required("Mobile Number is Required"),
          })}
          onSubmit={(fields) => {
            this.handleSubmit(fields);
          }}
        >
          {({ errors, status, touched }) => (
            <div style={{ backgroundColor: "#F4F6F7" }}>
              {/* <Navbar /> */}
              <div
                className="row m-0 pt-4 mt-0 mr-3"
                style={{ backgroundColor: "#F4F6F7" }}
              >
                <div className="col-12 text-center">
                  <h4>Welcome to Yatra!</h4>
                </div>
                <div
                  className="col-12 text-center mt-2"
                  style={{ fontWeight: "500", fontSize: "12px" }}
                >
                  <p>
                    We just need a few more details to create your Yatra account
                  </p>
                </div>
                <div className="col-4 d-none d-lg-block"></div>

                <div
                  className="col-lg-4 col-12 bg-white p-4 m-2"
                  style={{ border: "3px", borderRadius: "7px" }}
                >
                  <div className="row">
                    <div className="col-lg-12 col-12 text-center">
                      <div className="row">
                        <div className="col-12 text-left">
                          <Link to="/login">
                            <i
                              className="fas fa-arrow-left text-dark"
                              style={{ cursor: "pointer" }}
                            />
                          </Link>
                        </div>
                      </div>
                      <div className="row  mt-3">
                        <div className="col-12 text-center">
                          <Form>
                            <div className="form-group text-left">
                              <label htmlFor="email">EMAIL ID</label>
                              <Field
                                name="email"
                                type="text"
                                placeholder="Enter Email"
                                className={
                                  "form-control" +
                                  (errors.email && touched.email
                                    ? " is-invalid"
                                    : "")
                                }
                              />
                              <ErrorMessage
                                name="email"
                                component="div"
                                className="invalid-feedback"
                              />
                            </div>
                            <div className="form-group text-left">
                              <label htmlFor="mob">MOBILE NUMBER</label>
                              <Field
                                type="number"
                                name="mob"
                                placeholder="Enter Mobile Number"
                                className={
                                  "form-control" +
                                  (errors.mob && touched.mob
                                    ? " is-invalid"
                                    : "")
                                }
                              />
                              <ErrorMessage
                                name="mob"
                                component="div"
                                className="invalid-feedback"
                              />
                            </div>
                            <div className="form-row">
                              <div className="form-group col text-left">
                                <label htmlFor="password">
                                  CREATE PASSWORD
                                </label>
                                <Field
                                  name="password"
                                  type="password"
                                  placeholder="Enter your Password"
                                  className={
                                    "form-control" +
                                    (errors.password && touched.password
                                      ? " is-invalid"
                                      : "")
                                  }
                                />
                                <ErrorMessage
                                  name="password"
                                  component="div"
                                  className="invalid-feedback"
                                />
                              </div>
                            </div>
                            <div className="form-row">
                              <div className="form-group col-12 text-left">
                                <label>FULL NAME</label>
                              </div>
                              <div className="form-group col text-left">
                                <Field
                                  name="title"
                                  as="select"
                                  className={
                                    "form-control" +
                                    (errors.title && touched.title
                                      ? " is-invalid"
                                      : "")
                                  }
                                >
                                  <option value="">Title</option>
                                  <option value="Mr">Mr</option>
                                  <option value="Mrs">Mrs</option>
                                  <option value="Dr.">Dr.</option>
                                  <option value="Ms">Ms</option>
                                </Field>
                                <ErrorMessage
                                  name="title"
                                  component="div"
                                  className="invalid-feedback"
                                />
                              </div>
                              <div className="form-group col-5">
                                <Field
                                  name="firstName"
                                  placeholder="First Name"
                                  type="text"
                                  className={
                                    "form-control" +
                                    (errors.firstName && touched.firstName
                                      ? " is-invalid"
                                      : "")
                                  }
                                />
                                <ErrorMessage
                                  name="firstName"
                                  component="div"
                                  className="invalid-feedback"
                                />
                              </div>
                              <div className="form-group col-4">
                                <Field
                                  name="lastName"
                                  placeholder="Last Name"
                                  type="text"
                                  className={
                                    "form-control" +
                                    (errors.lastName && touched.lastName
                                      ? " is-invalid"
                                      : "")
                                  }
                                />
                                <ErrorMessage
                                  name="lastName"
                                  component="div"
                                  className="invalid-feedback"
                                />
                              </div>
                            </div>
                            <div className="form-group form-check text-left">
                              <Field
                                type="checkbox"
                                name="acceptTerm"
                                className={"form-check-input "}
                              />
                              <label
                                htmlFor="acceptTerms"
                                className="form-check-label"
                              >
                                I would like to be kept informed of Special
                                Promotions and offers by Yatra.
                              </label>
                            </div>
                            <div className="form-group form-check text-left">
                              <Field
                                type="checkbox"
                                name="acceptTerms"
                                className={"form-check-input "}
                              />
                              <label
                                htmlFor="acceptTerms"
                                className="form-check-label"
                              >
                                I would like to get What'sApp notifications.
                              </label>
                            </div>
                            <div className="form-group col-12">
                              <button
                                type="submit"
                                className="btn btn-danger mr-2 col-12"
                              >
                                Register
                              </button>
                            </div>
                          </Form>
                        </div>
                        <div className="col-12 text-left">
                          <small>
                            By proceeding, you agree with our{" "}
                            <span
                              className="text-primary"
                              style={{ cursor: "pointer" }}
                            >
                              Terms of Service
                            </span>
                            ,{" "}
                            <span
                              className="text-primary"
                              style={{ cursor: "pointer" }}
                            >
                              Privacy Policy
                            </span>{" "}
                            & &nbsp;
                            <span
                              className="text-primary"
                              style={{ cursor: "pointer" }}
                            >
                              Master User Agreement.
                            </span>
                          </small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-4 d-none d-lg-block"></div>
                <br />
              </div>
            </div>
          )}
        </Formik>
      </div>
    );
  }
}

export default Regitser;
