import React, { Component } from "react";
import axios from "axios";

let url = "http://localhost:2410";
// let url = "https://us-central1-yat12-77281.cloudfunctions.net/app";

class LogOut extends Component {
  state = {};

  async componentDidMount() {
    let token = localStorage.getItem("token");
    const { data: response } = await axios.delete(url + "/logout", {
      headers: {
        Authorization: token,
      },
    });
    localStorage.removeItem("token");
    window.location = "/";
  }
  render() {
    return null;
  }
}

export default LogOut;
