import React, { Component } from "react";
import axios from "axios";
import jwtDecode from "jwt-decode";
import { Link } from "react-router-dom";
import queryString from "query-string";

let url = "http://localhost:2410";
// let url = "https://us-central1-yat12-77281.cloudfunctions.net/app";

const jwt = localStorage.getItem("token");
let userid;
if (jwt) {
  let token = jwt.split(" ")[1];

  userid = jwtDecode(token).id;
  console.log("user booking", token, userid);
}

class UserFlight extends Component {
  state = { currentPage: 1 };

  async componentWillMount() {
    const jwt = localStorage.getItem("token");
    // console.log("get current flightbook", jwt);
    let { page } = queryString.parse(this.props.location.search);
    let params = "";
    page = page ? page : 1;
    params = this.addToParams(params, "page", page);
    //let page = this.state.currentPage;
    try {
      const { data: response } = await axios.get(
        url + "/flightbook/" + userid + params
      );
      console.log("get current flightbook", response);
      this.setState({ flightbook: response });
    } catch (ex) {
      console.log("catch");
      return null;
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    const jwt = localStorage.getItem("token");
    // console.log("get current flightbook", jwt);
    let { page } = queryString.parse(this.props.location.search);
    let params = "";
    page = page ? page : 1;
    params = this.addToParams(params, "page", page);
    //let page = this.state.currentPage;
    if (prevProps !== currProps) {
      try {
        const { data: response } = await axios.get(
          url + "/flightbook/" + userid + params
        );
        //console.log("get current flightbook", response);
        this.setState({ flightbook: response });
      } catch (ex) {
        console.log("catch");
        return null;
      }
    }
  }

  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }

  callUrl = (params, page) => {
    let path = "/manage-booking/My Booking/Flights";
    params = this.addToParams(params, "page", page);
    console.log("params", params);
    this.props.history.push({ pathname: path, search: params });
  };

  gotoPage = (x) => {
    let { page } = queryString.parse(this.props.location.search);
    let currentPage = page ? +page : 1;
    currentPage = currentPage + x;
    let params = "";
    this.callUrl(params, currentPage);
  };

  // gotoPage = (p) => {
  //   this.setState({ currentPage: this.state.currentPage + p });
  // };
  render() {
    const { flightbook: fb, currentPage } = this.state;
    let { page } = queryString.parse(this.props.location.search);
    page = page ? page : 1;
    // let page;
    if (this.state.flightbook) {
      page = fb.pageInfo.currPage;
    }
    page = page ? page : 1;
    console.log("flight,book", fb);
    console.log("user Flight", this.props, page);
    return (
      <div className="col-12 pt-2">
        <div className="row">
          <div className="col-12">
            {!this.state.flightbook && (
              <div className="text-center pt-5 pb-5">
                <br />
                <h6>No Record To Show any Flight Booking</h6>
              </div>
            )}
            {this.state.flightbook && (
              <div>
                <div
                  className="row"
                  className="mt-5"
                  style={{
                    border: "1px solid lightgrey",
                    borderRadius: "10px",
                  }}
                >
                  <div
                    className="col-12 pb-1"
                    style={{
                      backgroundColor: "lightgrey",
                      borderRadius: "8px 8px 0 0",
                    }}
                  >
                    <div className="row">
                      <div className="col-lg-7 col-12 text-left">
                        <span style={{ color: "grey", fontSize: "18px" }}>
                          <i className="fa fa-plane" />
                        </span>
                        <span>
                          <span
                            style={{
                              color: "black",
                              fontSize: "13px",
                              marginLeft: "8px",
                            }}
                          >
                            {fb.flight[0].GDepartCity}
                            &nbsp; &nbsp;
                            <i className="fa fa-exchange" />
                            &nbsp;&nbsp;
                            {fb.flight[0].GArrivalCity}
                          </span>
                          {fb.flight[0].returnBook === "true" && <br />}
                          {fb.flight[0].returnBook === "true" && (
                            <span
                              style={{
                                color: "black",
                                fontSize: "13px",
                                marginLeft: "27px",
                              }}
                            >
                              {fb.flight[0].RDepartCity}
                              &nbsp; &nbsp;
                              <i className="fa fa-exchange" />
                              &nbsp;&nbsp;
                              {fb.flight[0].RArrivalCity}
                            </span>
                          )}
                          <br />
                          <span
                            style={{
                              color: "grey",
                              fontSize: "10px",
                              marginLeft: "30px",
                            }}
                          >
                            {fb.flight[0].TTravellers} TRAVELLER(S)
                          </span>
                        </span>
                      </div>
                      <div className="col-lg-5 col-12 text-lg-right text-left">
                        <span style={{ color: "grey", fontSize: "11px" }}>
                          Booked On: {fb.flight[0].GDate} | Booking Ref No.
                          200456
                          {isNaN(fb.flight[0].UserId) ? (
                            <span>
                              {fb.flight[0].UserId.substring(0, 4)}
                              {fb.flight[0].Id.substring(0, 4)}
                            </span>
                          ) : (
                            <span>
                              {fb.flight[0].UserId}
                              {fb.flight[0].Id}
                            </span>
                          )}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="row mt-3">
                      <div className="col-lg-3 col-6 ">
                        <div>
                          <span
                            style={{
                              fontSize: "18px",
                              fontWeight: "600",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            Depart Date
                          </span>
                          <br />
                          <span
                            style={{
                              color: "#888",
                              fontSize: "12px",
                              fontWeight: "",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            {fb.flight[0].GDate}
                          </span>
                        </div>
                        {fb.flight[0].returnBook === "true" && (
                          <div>
                            <hr />
                          </div>
                        )}
                        {fb.flight[0].returnBook === "true" && (
                          <div>
                            <span
                              style={{
                                fontSize: "18px",
                                fontWeight: "600",
                                fontFamily: "Rubik-Regular,Arial",
                              }}
                            >
                              Return Date
                            </span>
                            <br />
                            <span
                              style={{
                                color: "#888",
                                fontSize: "12px",
                                fontWeight: "",
                                fontFamily: "Rubik-Regular,Arial",
                              }}
                            >
                              {fb.flight[0].RDate}
                            </span>
                          </div>
                        )}
                      </div>
                      <div className="col-lg-3 col-6 ">
                        <div>
                          <span
                            style={{
                              fontSize: "18px",
                              fontWeight: "600",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            Depart Time
                          </span>
                          <br />
                          <span
                            style={{
                              color: "#888",
                              fontSize: "12px",
                              fontWeight: "",
                              fontFamily: "Rubik-Regular,Arial",
                            }}
                          >
                            {fb.flight[0].GDepartTime}
                          </span>
                        </div>
                        {fb.flight[0].returnBook === "true" && (
                          <div>
                            <hr />
                          </div>
                        )}
                        {fb.flight[0].returnBook === "true" && (
                          <div>
                            <span
                              style={{
                                fontSize: "18px",
                                fontWeight: "600",
                                fontFamily: "Rubik-Regular,Arial",
                              }}
                            >
                              Return Time
                            </span>
                            <br />
                            <span
                              style={{
                                color: "#888",
                                fontSize: "12px",
                                fontWeight: "",
                                fontFamily: "Rubik-Regular,Arial",
                              }}
                            >
                              {fb.flight[0].RDepartTime}
                            </span>
                          </div>
                        )}
                      </div>
                      <div
                        className="col-lg-1 d-none d-lg-block "
                        style={{ borderRight: "1px solid lightgrey" }}
                      ></div>
                      <div className="col-12 d-lg-none d-md-block  ">
                        <hr />
                      </div>
                      <div className="col-lg-5 col-12 text-center mt-2">
                        <Link
                          to={`/manage-booking/My Booking/Flights/${fb.flight[0].Id}`}
                        >
                          <button className="btn btn-danger btn sm mr-2">
                            Fare Details
                          </button>
                        </Link>
                        <Link
                          to={`/manage-booking/My Booking/Flights/${fb.flight[0].Id}`}
                        >
                          <button className="btn btn-danger btn sm ml-2">
                            Itinerary
                          </button>
                        </Link>
                      </div>
                    </div>
                  </div>
                  <div className="col-12">
                    <hr />
                  </div>
                  <div className="col-12">
                    <div className="row">
                      <div style={{ borderRight: "1px solid lightgrey" }}></div>
                      <div className="col-3 text-center ">
                        <span
                          style={{
                            cursor: "pointer",
                            border: "1px solid lightgrey",
                            borderRadius: "100%",
                            padding: "8px",
                          }}
                        >
                          <i
                            className="fa fa-print"
                            style={{ color: "lightgrey" }}
                          />
                        </span>
                        <br />
                        <br />
                        <span
                          style={{
                            fontSize: "13px",
                            fontWeight: "600",
                            cursor: "pointer",
                          }}
                        >
                          Print Invoice
                        </span>
                      </div>
                      <div style={{ borderRight: "1px solid lightgrey" }}></div>
                      <div className="col-3 text-center ">
                        <span
                          style={{
                            cursor: "pointer",
                            border: "1px solid lightgrey",
                            borderRadius: "100%",
                            padding: "8px",
                          }}
                        >
                          <i
                            className="fa fa-location-arrow"
                            style={{ color: "lightgrey" }}
                          />
                        </span>
                        <br />
                        <br />
                        <span
                          style={{
                            fontSize: "13px",
                            fontWeight: "600",
                            cursor: "pointer",
                          }}
                        >
                          Get Directions
                        </span>
                      </div>
                      <div style={{ borderRight: "1px solid lightgrey" }}></div>
                      <div className="col-3 text-center ">
                        <span
                          style={{
                            cursor: "pointer",
                            border: "1px solid lightgrey",
                            borderRadius: "100%",
                            padding: "8px",
                          }}
                        >
                          <i
                            className="fa fa-refresh"
                            style={{ color: "lightgrey" }}
                          />
                        </span>
                        <br />
                        <br />
                        <span
                          style={{
                            fontSize: "13px",
                            fontWeight: "600",
                            cursor: "pointer",
                          }}
                        >
                          Re-Book
                        </span>
                      </div>
                    </div>
                  </div>
                  <div>
                    <br />
                  </div>
                </div>
                <div className="row  mt-3">
                  <div className="col-6 text-left">
                    <div style={{ color: "#888", fontSize: "13px" }}>
                      Showing &nbsp;&nbsp;
                      {fb.pageInfo.startIndex + 1} -{" "}
                      {fb.flight.length === 1 ? (
                        <span>{fb.pageInfo.pageNumber * 1}</span>
                      ) : (
                        <span>{fb.pageInfo.totalItem}</span>
                      )}
                      {"       "}
                      of {fb.pageInfo.totalItem}
                    </div>
                  </div>

                  <div className="col-6 text-right">
                    {+fb.pageInfo.currPage > 1 ? (
                      <button
                        className="btn btn-secondary btn-sm"
                        onClick={() => this.gotoPage(-1)}
                      >
                        {"<"}
                      </button>
                    ) : (
                      <button
                        className="btn btn-secondary btn-sm"
                        onClick={() => this.gotoPage(-1)}
                        disabled={true}
                      >
                        {"<"}
                      </button>
                    )}{" "}
                    <button
                      className="btn btn-outline-dark btn-sm m-1"
                      disabled={true}
                    >
                      {page}
                    </button>
                    {+page < fb.pageInfo.totalPages ? (
                      <button
                        className="btn btn-secondary btn-sm"
                        onClick={() => this.gotoPage(1)}
                      >
                        {">"}
                      </button>
                    ) : (
                      <button
                        className="btn btn-secondary btn-sm"
                        onClick={() => this.gotoPage(1)}
                        disabled={true}
                      >
                        {">"}
                      </button>
                    )}
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default UserFlight;
