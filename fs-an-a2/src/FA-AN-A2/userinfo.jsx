import React, { Component } from "react";
import axios from "axios";

let url = "http://localhost:2410";
// let url = "https://us-central1-yat12-77281.cloudfunctions.net/app";

class UserInfo extends Component {
  state = {};
  async componentWillMount() {
    const jwt = localStorage.getItem("token");
    // console.log("get current user", jwt);
    try {
      const { data: response } = await axios.get(url + "/user", {
        headers: {
          Authorization: jwt,
        },
      });
      // console.log("get current user", response);
      this.setState({ user: response });
    } catch (ex) {
      console.log("catch");
      return null;
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    const jwt = localStorage.getItem("token");
    // console.log("get current user", jwt);
    if (prevProps !== currProps) {
      try {
        const { data: response } = await axios.get(url + "/user", {
          headers: {
            Authorization: jwt,
          },
        });
        // console.log("get current user", response);
        this.setState({ user: response });
      } catch (ex) {
        console.log("catch");
        return null;
      }
    }
  }

  render() {
    const { user } = this.state;
    // console.log("user", user);
    return (
      <div
        className="bg-white m-1 p-2 pl-3"
        style={{ borderRadius: "10px", border: "2px" }}
      >
        {this.state.user && (
          <div className="row">
            <div className="col-12 text-center p-1">
              <span
                className="fa-stack fa-2x text-danger"
                style={{ cursor: "pointer" }}
              >
                <i className="fa fa-circle fa-stack-2x"></i>
                <i className="far fa-user  fa-stack-1x fa-lg fa-inverse "></i>
              </span>
            </div>
            <div className="col-12 text-center p-1">
              <div>
                <h6>
                  {user.Title}&nbsp;{user.Fname}&nbsp;{user.Lname}
                </h6>
              </div>
            </div>
            <div className="col-12 text-center pl-4 pr-4">
              <hr className="my-hr-4" />
            </div>
            <div className="col-12 text-center p-1">
              <span
                style={{ fontSize: "20px", fontFamily: "Rubik-Regular,Arial" }}
              >
                {user.Email}
              </span>
              <br />
              <span
                style={{ fontSize: "14px", fontFamily: "Rubik-Regular,Arial" }}
              >
                Phone: {user.Mob}
              </span>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default UserInfo;
