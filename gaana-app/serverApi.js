var express = require("express");
var app = express();
app.use(express.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin,X-Requested-With,Content-Type,Accept"
  );
  res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
  //res.header("Access-Control-Allow-Credentials", true);
  res.setHeader("Access-Control-Expose-Headers", "X-Auth-Token");
  next();
});

const port = 2410;

app.listen(port, () => console.log("listening on port", port));

//for sql connection

var mysql = require("mysql");
var connection = mysql.createConnection({
  host: "remotemysql.com",
  user: "vrgR1vCXfg",
  password: "KEGJz0CcSa",
  database: "vrgR1vCXfg",
  multipleStatements: true,
});

connection.query("SELECT * FROM GannaUser", function (error, results, fields) {
  if (error) console.log("errore", error);
  console.log("DB connected");
  // results.forEach((result) => {
  //   // console.log(result);
  // })
});

// for login and logout

const jwt = require("jsonwebtoken");
var bodyParser = require("body-parser");
var passport = require("passport");
var passportJWT = require("passport-jwt");
app.use(bodyParser.json());

passport.initialize();

var ExtractJWT = passportJWT.ExtractJwt;
var Strategy = passportJWT.Strategy;

var cfg = require("./config.js");

var params = {
  secretOrKey: cfg.jwtSecret,
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
};

var strategy = new Strategy(params, function (payload, done) {
  console.log("strategy", payload);
  connection.query(`SELECT * FROM GannaUser WHERE Id=${payload.id}`, function (
    error,
    results,
    fields
  ) {
    console.log("in query");
    if (error) {
      return done(new Error("user not found"), null);
    } else {
      console.log("DB in get request of user after login");
      return done(null, { id: results[0].Id });
    }
  });
});

passport.use(strategy);

const jwtExpirySeconds = 300000;

app.post("/login", function (req, res, payload) {
  console.log("start", req.body);
  connection.query(
    `SELECT * FROM GannaUser WHERE Email='${req.body.username}' AND EnterKey='${req.body.password}'`,
    function (error, results, fields) {
      console.log("in query");
      if (error) {
        console.log("error", error);
        res.sendStatus(401).end();
      } else {
        console.log("DB in post request of login", results);
        if (results.length > 0) {
          var payload = { id: results[0].Id };
          var token = jwt.sign(payload, cfg.jwtSecret, {
            algorithm: "HS256",
            expiresIn: jwtExpirySeconds,
          });
          console.log("token", token);
          res.setHeader("X-Auth-Token", token);
          res.json({ success: true, token: "bearer " + token });
        } else {
          res.sendStatus(401).end();
        }
      }
    }
  );
});

app.get("/user", passport.authenticate("jwt", { session: false }), function (
  req,
  res
) {
  console.log("ssdddddddddddddddddddddddddddddddd", req.user);

  connection.query(`SELECT * FROM GannaUser WHERE Id=${req.user.id}`, function (
    error,
    results,
    fields
  ) {
    console.log("in query");
    if (error) {
      console.log("in query error", error);
      res.sendStatus(401);
    } else {
      console.log("DB in get request of user after login to get user");
      res.json(results[0]);
    }
  });
});

var logout = require("express-passport-logout");

app.delete(
  "/logout",
  passport.authenticate("jwt", { session: false }),
  logout()
);

app.post("/register", function (req, res) {
  console.log(
    "in post request of new user",
    req.body,
    JSON.stringify(req.body)
  );
  connection.query(
    `INSERT INTO GannaUser(Fname,Lname,Email,EnterKey) VALUES('${req.body.firstName}','${req.body.lastName}','${req.body.email}','${req.body.password}')`,
    function (error, results, fields) {
      console.log("in query");
      if (error) {
        console.log("error", error.errno);
        let Err = { error: error.errno };
        res.send(Err).end();
      } else {
        console.log("DB in post request of new Registeration", results);
        res.end();
      }
    }
  );
});

// for getting all trending song
app.get("/allTrendingSong", function (req, res) {
  console.log("in get request of all Trending song ");
  connection.query(
    `SELECT * FROM GannaSongs WHERE Rating='Trending'`,
    function (error, results, fields) {
      console.log("in query");
      if (error) {
        console.log("error", error.errno);
        res.sendStatus(401).end();
      } else {
        console.log("DB in get request of all Trending song", results);
        res.send(results);
      }
    }
  );
});

// for getting all top chart song acc to category
app.get("/topChartPlaylist/:cat", function (req, res) {
  let cat = req.params.cat;
  console.log("in get request of top chart playlist acc to category ", cat);
  connection.query(
    `SELECT * FROM GannaSongs WHERE Rating='Top Chart'AND Category='${cat}'`,
    function (error, results, fields) {
      console.log("in query");
      if (error) {
        console.log("error", error.errno);
        res.sendStatus(401).end();
      } else {
        console.log("DB in get request of a top chart playlist", results);
        res.send(results);
      }
    }
  );
});

// for getting  trending song  acc to name
app.get("/trendingSong/:name", function (req, res) {
  let name = req.params.name;
  console.log("in get request of trending song  acc to name ", name);
  connection.query(
    `SELECT * FROM GannaSongs WHERE Rating='Trending'AND SName='${name}'`,
    function (error, results, fields) {
      console.log("in query");
      if (error) {
        console.log("error", error.errno);
        res.sendStatus(401).end();
      } else {
        console.log(
          "DB in get request of a trending song  acc to name",
          results
        );
        res.send(results);
      }
    }
  );
});

// for getting  search song  acc to search parameter
app.get("/search/:name", function (req, res) {
  let name = req.params.name;
  console.log("in get request of trending song  acc to name ", name);
  connection.query(
    `SELECT * FROM GannaSongs WHERE SName Like '%${name}%' OR AlbumName Like '%${name}%' OR Artist Like '%${name}%' OR Category Like '%${name}%'`,
    function (error, results, fields) {
      console.log("in query");
      if (error) {
        console.log("error", error.errno);
        res.sendStatus(401).end();
      } else {
        console.log(
          "DB in get request of a trending song  acc to name",
          results
        );
        res.send(results);
      }
    }
  );
});

// for getting  searching song  acc to name in playlist
app.get("/searchPlaylist/:name", function (req, res) {
  let name = req.params.name;
  console.log("in get request of trending song  acc to name ", name);
  connection.query(`SELECT * FROM GannaSongs WHERE SName='${name}'`, function (
    error,
    results,
    fields
  ) {
    console.log("in query");
    if (error) {
      console.log("error", error.errno);
      res.sendStatus(401).end();
    } else {
      console.log("DB in get request of a trending song  acc to name", results);
      res.send(results);
    }
  });
});

// for posting song in playlist acc to user
app.post("/playlist/:id", function (req, res) {
  const id = req.params.id;
  console.log("in post request of Flight if user", id, req.body);
  connection.query(
    `INSERT INTO GannaSongPlayList (UserId, SId, SName, SImage, SUrl, Artist, AlbumName, Duration, Rating, Category) VALUES(${id},${req.body.Id},"${req.body.SName}","${req.body.SImage}","${req.body.SUrl}","${req.body.Artist}","${req.body.AlbumName}","${req.body.Duration}","${req.body.Rating}","${req.body.Category}")`,
    function (error, results, fields) {
      console.log("in query");
      if (error) {
        console.log("error", error.errno);
        let Err = { error: error.errno };
        res.send(Err).end();
      } else {
        console.log("DB in post request of Flight", results);
        res.send({ status: "Success" });
      }
    }
  );
});

// for getting playlist acc to user
app.get("/playlist/:id", function (req, res) {
  const id = req.params.id;
  console.log("in get request of playlist song ", id);
  connection.query(
    `SELECT * FROM GannaSongPlayList WHERE UserId=${id}`,
    function (error, results, fields) {
      console.log("in query");
      if (error) {
        console.log("error", error.errno);
        res.sendStatus(401).end();
      } else {
        console.log("DB in get request of playlist song", results);
        res.send(results);
      }
    }
  );
});

// for getting all song
app.get("/allSong", function (req, res) {
  console.log("in get request of all song ");
  connection.query(`SELECT * FROM GannaSongs`, function (
    error,
    results,
    fields
  ) {
    console.log("in query");
    if (error) {
      console.log("error", error.errno);
      res.sendStatus(401).end();
    } else {
      console.log("DB in get request of all songs", results);
      res.send(results);
    }
  });
});
