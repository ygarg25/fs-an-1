import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import { Route, Switch, Redirect } from "react-router-dom";
import Navbar from "./Component/navbar";
import ProtectedRoute from "./Component/protectedRoute";
// user authentication
import Login from "./Component/login";
import LogOut from "./Component/signout";
import Regitser from "./Component/register";
//profile
import UserProfile from "./Component/userprofile";
//home
import Home from "./Component/home";
// Media Player
import MediaPlayer from "./Component/mediaPlayer";
// Top Chart
import TopChartAll from "./Component/topchartAll";
// Top Trending
import TopTrendingAll from "./Component/topTrendingAll";
//  Top Chart Playlist
import TopChartPlaylist from "./Component/topChartsPlaylist";
// Top Trending Playlist
import TopTrendingPlaylist from "./Component/topTrendingPlaylist";
// search page
import SearchPage from "./Component/searchPage";
// search Playlist
import SearchPlaylist from "./Component/searchPlaylist";
// my music
import MyMusic from "./Component/mymusic";

class App extends Component {
  state = {
    songPlay: [1],
    modal: 1,
    show: 1,
  };

  handleSongPlay = (e) => {
    console.log("handlesongPlay in app.js ", e);
    let songPlay = [];
    songPlay = e.map((c) => +c);
    this.setState({ songPlay: songPlay });
  };

  handleSignIn = (m, s) => {
    this.setState({ modal: m, show: s });
  };

  render() {
    const { songPlay, modal, show } = this.state;
    return (
      <div>
        <div className="fixed-top">
          <Route
            to="/"
            render={(props) => (
              <Navbar handleSignIn={this.handleSignIn} {...props} />
            )}
          />
        </div>
        <div>
          <Switch>
            {/* <Route path="/login" component={Login} /> */}
            <Route path="/signout" component={LogOut} />
            {/* <Route path="/signup" component={Regitser} /> */}
            <Route path="/myzone" component={UserProfile} />
            <ProtectedRoute
              path="/music"
              handleSignIn={this.handleSignIn}
              render={(props) => (
                <MyMusic
                  {...props}
                  handleSongPlay={this.handleSongPlay}
                  handleSignIn={this.handleSignIn}
                />
              )}
            />
            <Route
              path="/album/:name"
              render={(props) => (
                <SearchPlaylist
                  {...props}
                  handleSongPlay={this.handleSongPlay}
                  handleSignIn={this.handleSignIn}
                />
              )}
            />
            <Route path="/search/:name" component={SearchPage} />
            <Route
              path="/song/:name"
              render={(props) => (
                <TopTrendingPlaylist
                  {...props}
                  handleSongPlay={this.handleSongPlay}
                  handleSignIn={this.handleSignIn}
                />
              )}
            />{" "}
            <Route
              path="/songs"
              render={(props) => (
                <TopTrendingAll
                  {...props}
                  handleSongPlay={this.handleSongPlay}
                  handleSignIn={this.handleSignIn}
                />
              )}
            />
            <Route
              path="/playlist/:category"
              render={(props) => (
                <TopChartPlaylist
                  {...props}
                  handleSongPlay={this.handleSongPlay}
                  handleSignIn={this.handleSignIn}
                />
              )}
            />
            <Route path="/topCharts" component={TopChartAll} />
            <Route
              path="/"
              render={(props) => (
                <Home
                  {...props}
                  handleSongPlay={this.handleSongPlay}
                  handleSignIn={this.handleSignIn}
                  modal={modal}
                  show={show}
                />
              )}
            />
          </Switch>
        </div>
        <div className="fixed-bottom text-center bg-white">
          <MediaPlayer
            songPlay={this.state.songPlay}
            // handlePlayingSong={this.handlePlayingSong}
            handleSignIn={this.handleSignIn}
          />
        </div>
      </div>
    );
  }
}

export default App;
