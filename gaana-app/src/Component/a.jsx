import React, { Component } from "react";
// import ReactWebMediaPlayer from "react-web-media-player";
import AudioPlayer from "react-modular-audio-player";
// import { Media, Player, controls, utils } from "react-media-player";
import "./audioPlayer.css";

// import ReactPlayer from "react-player";
import PlaylistOption from "./playlistOption";

// const {
//   PlayPause,
//   CurrentTime,
//   Progress,
//   SeekBar,
//   Duration,
//   MuteUnmute,
//   Volume,
//   Fullscreen,
// } = controls;
// const { keyboardControls } = utils;

let rearrangedPlayer = [
  {
    className: "top",
    style: { width: "100%" },
    innerComponents: [
      {
        type: "seek",
        style: {
          width: "100%",
          height: "10px",
          float: "left",
          position: "relative",
          marginBottom: "10px",
          marginTop: "10px",
        },
      },
    ],
  },
  {
    className: "bottom",
    style: { marginBottom: "20px" },
    innerComponents: [
      {
        type: "rewind",
        style: {
          width: "12.5%",
          cursor: "pointer",
        },
      },

      {
        type: "play",
        style: { width: "12.5%", cursor: "pointer" },
      },
      {
        type: "forward",
        style: {
          width: "12.5%",
          justifyContent: "flex-end",
          cursor: "pointer",
          float: "left",
        },
      },
      {
        type: "time",
        style: {
          justifyContent: "",
          marginLeft: "5%",
          color: "black",
        },
      },
      {
        type: "volume",
        style: { width: "100%", left: "-50%" },
      },
      {
        type: "loop",
        style: { width: "12.5%" },
      },
    ],
  },
];

class MediaPlayer extends Component {
  state = {
    songsData: [
      {
        Id: "1",
        SName: "Bheegi Bheegi",
        SImage: "https://i.ibb.co/7Ct4805/Bheegi-Bheegi.jpg",
        SUrl:
          "https://funksyou.com/files/data/6663/Bheegi%20Bheegi%20Tony%20Kakkar%20Neha%20Kakkar.mp3",
        Artist: "Tonny Kakkar",
        AlbumName: "Bheegi Bheegi",
        Duration: "03:24",
        Rating: "Trending",
        Category: "Punjabi",
      },
      {
        Id: "2",
        SName: "Ringtone",
        SImage: "https://i.ibb.co/0GSSVDx/Ringtone.jpg",
        SUrl:
          "https://funksyou.com/files/data/6656/Ringtone%20-%20Preetinder.mp3",
        Artist: "Rajat Nagpal",
        AlbumName: "Ringtone",
        Duration: "02:56",
        Rating: "Trending",
        Category: "Punjabi",
      },
      {
        Id: "3",
        SName: "Jannat",
        SImage: "https://i.ibb.co/fpp8RdQ/Jannat.jpg",
        SUrl: "https://funksyou.com/files/data/6530/Jannat%20-%20B%20Praak.mp3",
        Artist: "B Praak",
        AlbumName: "Jannat",
        Duration: "03:40",
        Rating: "Trending",
        Category: "Punjabi",
      },
      {
        Id: "4",
        SName: "Aeroplane",
        SImage: "https://i.ibb.co/2jTBwGH/Aeroplane.png",
        SUrl:
          "https://funksyou.com/files/data/6590/Aeroplane%20-%20Mr%20Faisu.mp3",
        Artist: "Rajat Nagpal",
        AlbumName: "Aeroplane",
        Duration: "02:42",
        Rating: "Trending",
        Category: "Punjabi",
      },
      {
        Id: "5",
        SName: "Masakali 2.0",
        SImage: "https://i.ibb.co/Xjdf0q9/Masakali-2.jpg",
        SUrl:
          "https://funksyou.com/files/data/6625/Masakali%202%20-%20Sachet%20Tandon.mp3",
        Artist: "A R Rehman , Tanishq Bagchi",
        AlbumName: "Masakali 2.0",
        Duration: "02:52",
        Rating: "Trending",
        Category: "Hindi",
      },
      {
        Id: "6",
        SName: "Meri Aashiqui ",
        SImage:
          "https://i.ibb.co/GH9WX5T/14659-Meri-Aashiqui-Jubin-Nautiyal.jpg",
        SUrl:
          "https://funksyou.com/files/data/6706/Meri%20Aashiqui%20-%20Jubin%20Nautiyal.mp3",
        Artist: "Jubin Nautiyal,Rochak Kohli",
        AlbumName: "Meri Aashiqui ",
        Duration: "04:40",
        Rating: "Trending",
        Category: "Hindi",
      },
      {
        Id: "7",
        SName: "Hawa Banke",
        SImage: "https://i.ibb.co/xCfC463/10345-Hawa-Banke-Darshan-Raval.jpg",
        SUrl:
          "https://funksyou.com/files/data/6077/Hawa%20Banke%20-%20Darshan%20Raval.mp3",
        Artist: "Darshan Raval, Nirmaan, Goldboy",
        AlbumName: "Hawa Banke",
        Duration: "02:52",
        Rating: "Trending",
        Category: "Hindi",
      },
      {
        Id: "8",
        SName: "Udaarian",
        SImage: "https://i.ibb.co/FBPtbfK/udaarian.jpg",
        SUrl: "http://api.pendusaab.com/download/128k-dtyjp/Udaarian.mp3",
        Artist: "Satinder Sartaj ",
        AlbumName: "Season Of Sartaj",
        Duration: "05:50",
        Rating: "Trending",
        Category: "Punjabi",
      },
      {
        Id: "9",
        SName: "Jinke Liye",
        SImage: "https://i.ibb.co/8MnT9YF/Jinke-Liye.jpg",
        SUrl:
          "https://dl.freemp3downloads.online/file/youtube0NhiNqI0SFs128.mp3?fn=Jinke%20Liye%20(Official%20Video)%20_%20Neha%20Kakkar%20Feat.%20Jaani%20_%20B%20Praak%20_%20Arvindr%20Khaira%20_%20Bhushan%20Kumar.mp3",
        Artist: "Neha Kakkar , B Praak",
        AlbumName: "Jaani Ve",
        Duration: "04:42",
        Rating: "Trending",
        Category: "Hindi",
      },
      {
        Id: "10",
        SName: "Sauda Khara Khara",
        SImage: "https://i.ibb.co/ng2kCJ8/sauda-kkhra-khra.jpg",
        SUrl:
          "https://funksyou.com/files/data/6379/Sauda%20Khara%20Khara%20(Good%20Newwz).mp3",
        Artist: "Diljit Dosanjh",
        AlbumName: "Created by Ganna",
        Duration: "03:31",
        Rating: "Top Chart",
        Category: "Hindi",
      },
      {
        Id: "11",
        SName: "Malang",
        SImage: "https://i.ibb.co/7nJLggc/malang.jpg",
        SUrl:
          "https://funksyou.com/files/data/6463/Malang%20-%20Title%20Track.mp3",
        Artist: "Ved Sharma",
        AlbumName: "Malang",
        Duration: "04:47",
        Rating: "Top Chart",
        Category: "Hindi",
      },
      {
        Id: "12",
        SName: "8 Parche",
        SImage: "https://i.ibb.co/sW38Fdt/8-parche.jpg",
        SUrl: "http://api.pendusaab.com/download/128k-dtldl/8-Parche.mp3",
        Artist: "Baani Sandhu , Gur Sidhu",
        AlbumName: "8 Parche",
        Duration: "03:28",
        Rating: "Top Chart",
        Category: "Punjabi",
      },
      {
        Id: "13",
        SName: "Relation",
        SImage: "https://i.ibb.co/DRQGzkK/Relation.jpg",
        SUrl: "https://funksyou.com/files/data/6172/Relation%20-%20Nikk.mp3",
        Artist: "Nikk",
        AlbumName: "Relation",
        Duration: "02:45",
        Rating: "Top Chart",
        Category: "Punjabi",
      },
      {
        Id: "14",
        SName: "Ek Diamond Da Haar",
        SImage: "https://i.ibb.co/19WKJSn/Ek-Diamond-Da-Haar.jpg",
        SUrl:
          "https://funksyou.com/files/data/6473/Ek%20Diamond%20Da%20Haar%20Lede%20Yaar%20-%20Jyotica%20Tangri.mp3",
        Artist: "Meet Bros. ,Jyotica Tangri",
        AlbumName: "Ek Diamond Da Haar",
        Duration: "03:25",
        Rating: "Top Chart",
        Category: "Ganna Original",
      },
      {
        Id: "15",
        SName: "Superstar",
        SImage: "https://i.ibb.co/DQ1p54y/Superstar.jpg",
        SUrl:
          "https://funksyou.com/files/data/6453/Superstar%20-%20Neha%20Kakkar.mp3",
        Artist: "Neha Kakkar , Vibhor Paras",
        AlbumName: "Superstar",
        Duration: "03:18",
        Rating: "Top Chart",
        Category: "Ganna Original",
      },
      {
        Id: "16",
        SName: "O Jaanwale",
        SImage: "https://i.ibb.co/jDq6h61/o-Jaanwale.jpg",
        SUrl:
          "https://funksyou.com/files/data/6655/O%20Jaan%20Waale%20-%20Akhil%20Sachdeva.mp3",
        Artist: " Akhi;l Sachdeva",
        AlbumName: "O Jaanwale",
        Duration: "04:19",
        Rating: "Top Chart",
        Category: "Romance Hindi",
      },
      {
        Id: "17",
        SName: "Humraah",
        SImage: "https://i.ibb.co/0sPg1vQ/Humraah.jpg",
        SUrl: "https://funksyou.com/files/data/6463/Humraah%20-%20Malang.mp3",
        Artist: " Sachet Tondon",
        AlbumName: "Humraah",
        Duration: "04:59",
        Rating: "Top Chart",
        Category: "Romance Hindi",
      },
      {
        Id: "18",
        SName: "Teri Yaari",
        SImage: "https://i.ibb.co/CQSzW0p/Teri-Yaari.jpg",
        SUrl:
          "https://pagalsongs.site/files/sfd3/1038/Teri%20Yaari(PaglaSongs).mp3",
        Artist: " Milind Gaba",
        AlbumName: "Teri Yaari",
        Duration: "04:12",
        Rating: "Top Chart",
        Category: "Latest Dance",
      },
      {
        Id: "19",
        SName: "I am Disco Dancer 2.0",
        SImage: "https://i.ibb.co/hVDB3Jh/Disco-Dancer-2-0.jpg",
        SUrl:
          "https://funksyou.com/files/data/6572/I%20Am%20A%20Disco%20Dancer.mp3",
        Artist: " Benny Dayal",
        AlbumName: "I am Disco Dancer 2.0",
        Duration: "03:07",
        Rating: "Top Chart",
        Category: "Latest Dance",
      },
      {
        Id: "20",
        SName: "Channa Ve",
        SImage: "https://i.ibb.co/JdhJ9sG/Channa-Ve.jpg",
        SUrl:
          "https://funksyou.com/files/data/6531/Channa%20Ve%20-%20Bhoot%20Part%20One.mp3",
        Artist: " Akhil Sachdeva",
        AlbumName: "Channa Ve",
        Duration: "03:32",
        Rating: "Top Chart",
        Category: "Latest Love",
      },
      {
        Id: "21",
        SName: "Nai Chaida",
        SImage: "https://i.ibb.co/FmgHymD/Nai-Chaida.jpg",
        SUrl:
          "https://funksyou.com/files/data/6639/Nai%20Chaida%20-%20Lisa%20Mishra.mp3",
        Artist: " Lisa Mishra",
        AlbumName: "Nai Chaida",
        Duration: "02:55",
        Rating: "Top Chart",
        Category: "Latest Love",
      },
      {
        Id: "22",
        SName: "Genda Phool",
        SImage: "https://i.ibb.co/6B8jDJJ/Genda-Phool.jpg",
        SUrl:
          "https://funksyou.com/files/data/6582/Genda%20Phool%20Badshah.mp3",
        Artist: " Badshah , Payal Dev",
        AlbumName: "Genda Phool",
        Duration: "02:50",
        Rating: "Top Chart",
        Category: "Indiepop ",
      },
      {
        Id: "23",
        SName: "Goa Beach",
        SImage: "https://i.ibb.co/GHT8JdS/Goa-Beach.jpg",
        SUrl:
          "https://funksyou.com/files/data/6504/Goa%20Beach%20-%20Neha%20Kakkar.mp3",
        Artist: " Tony Kakkar , Neha Kakkar",
        AlbumName: "Goa Beach",
        Duration: "03:38",
        Rating: "Top Chart",
        Category: "Indiepop ",
      },
      {
        Id: "24",
        SName: "Tere Karke",
        SImage: "https://i.ibb.co/8PS5X0g/tere-karke.jpg",
        SUrl:
          "https://funksyou.com/files/convert/34085/128/Tere%20Karke%20Guri%20(SongsMp3.Com).mp3",
        Artist: " Guri",
        AlbumName: "Tere Karke",
        Duration: "02:46",
        Rating: "Top Chart",
        Category: "Today ",
      },
      {
        Id: "25",
        SName: "Maahi Ve",
        SImage: "https://i.ibb.co/X7w21Dn/Mahi-Ve.jpg",
        SUrl:
          "https://funksyou.com/files/data/3527/2%20-%20Maahi%20Ve%20-%20Highway%20-%20DJ%20Xylo%20(Dubai)%20Remix%20[SongsMp3.Com].mp3",
        Artist: " A. R. Rehman",
        AlbumName: "Maahi Ve",
        Duration: "04:24",
        Rating: "Top Chart",
        Category: "Today ",
      },
    ],
    playId: this.props.songPlay,
    indexPlay: 0,
    audioFiles: undefined,
    show: 1,
  };

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    console.log(currProps !== prevState);
    if (prevProps !== currProps) {
      console.log("cdm");
      this.setState({ playId: this.props.songPlay });
    }
  }

  handleShow = (e) => {
    this.setState({ show: e });
  };

  handleClickNext = () => {
    console.log("next");
    if (this.state.indexPlay < this.state.songsData.length) {
      this.setState({ indexPlay: this.state.indexPlay + 1 });
    } else {
      this.setState({ indexPlay: 0 });
    }
  };

  handleClickPrevious = () => {
    console.log("previous");
    if (this.state.indexPlay > 0) {
      this.setState({ indexPlay: this.state.indexPlay - 1 });
    } else {
      this.setState({ indexPlay: this.state.songsData.length - 1 });
    }
  };

  render() {
    let { playId, songsData, indexPlay, audioFiles, show } = this.state;
    // const { Player } = this.props;
    audioFiles = [];
    let index = songsData.findIndex((s) => +s.Id === +playId[0]);
    songsData.map((s, i) => {
      if (i >= index) audioFiles.push({ src: s.SUrl });
    });
    songsData.map((s, i) => {
      if (i < index) audioFiles.push({ src: s.SUrl });
    });
    let playData = songsData.filter((c) => +c.Id === +playId[indexPlay]);

    console.log(
      "Media player",
      playId,
      playData,
      this.props.songPlay,
      index,
      audioFiles
    );
    return (
      <div>
        {/* <ReactPlayer url={playData[0].SUrl} playing={true} /> */}
        {/* <ReactWebMediaPlayer
          title="My own video player"
          video={playData[0].SUrl}
        /> */}
        {/* 
        <figure>
          <figcaption>Listen to the T-Rex:</figcaption>
          <audio
            controls
            src="https://funksyou.com/files/data/6706/Meri%20Aashiqui%20-%20Jubin%20Nautiyal.mp3"
          >
            Your browser does not support the
            <code>audio</code> element.
          </audio>
        </figure> */}
        <div className="row p-1">
          <div className="col-md-4 col-12 text-left">
            {show === 1 && (
              <div className="row">
                <div className="col-3 p-0 pl-4">
                  <img
                    src={playData.SImage}
                    style={{
                      borderRadius: "5px",
                      width: "50px",
                      height: "auto",
                    }}
                  />
                </div>
                <div
                  className="col-5 pl-1 mt-2"
                  style={{ fontSize: "10px", color: "#999" }}
                >
                  <span style={{ fontSize: "10px", color: "#333" }}>
                    {playData.SName}
                    <br />
                    <span style={{ fontSize: "10px", color: "#999" }}>
                      {playData.AlbumName}
                    </span>
                  </span>
                </div>
                <div className="col-lg-2 col text-right mt-3 pr-5 pr-lg-0">
                  <span
                    className=""
                    style={{ cursor: "pointer", color: "#999" }}
                  >
                    <i className="far fa-heart" />
                  </span>
                </div>
                <div className="col-lg-1 d-none d-lg-block mt-3">
                  <span
                    className=""
                    onClick={() => this.handleShow(2)}
                    style={{ cursor: "pointer", color: "#999" }}
                  >
                    <i className="fas fa-ellipsis-h" />
                  </span>
                </div>
              </div>
            )}
            {show === 2 && (
              <div className="row" style={{ position: "" }}>
                <div className="col-3 p-0 pl-4">
                  <img
                    src={playData.SImage}
                    style={{
                      borderRadius: "5px",
                      width: "50px",
                      height: "auto",
                    }}
                  />
                </div>
                <div
                  className="col-5 pl-1 mt-2"
                  style={{ fontSize: "10px", color: "#999" }}
                >
                  <span style={{ fontSize: "10px", color: "#333" }}>
                    {playData.SName}
                    <br />
                    <span style={{ fontSize: "10px", color: "#999" }}>
                      {playData.AlbumName}
                    </span>
                  </span>
                </div>
                <div className="col-lg-2 col text-right mt-3 pr-5 pr-lg-0">
                  <span
                    className=""
                    style={{ cursor: "pointer", color: "#999" }}
                  >
                    <i className="far fa-heart" />
                  </span>
                </div>
                <div className="col-lg-1 d-none d-lg-block mt-3">
                  <span
                    className=""
                    onClick={() => this.handleShow(1)}
                    style={{ cursor: "pointer", color: "#999" }}
                  >
                    <i className="fas fa-chevron-down" />
                  </span>
                </div>
                <div className="col-12">
                  <hr />
                </div>
                <div className="col-12 p-0">
                  <PlaylistOption song={playData[0]} {...this.props} />
                </div>
              </div>
            )}
          </div>
          <div
            className="col-md-8 col-12 "
            style={{ borderLeft: "1px solid lightgrey" }}
          >
            <div className="row">
              <div className="col-md-5 col-12 row ml-1">
                <AudioPlayer
                  audioFiles={audioFiles}
                  rearrange={rearrangedPlayer}
                  playerWidth="90%"
                  iconSize="1.5rem"
                  playIcon="https://i.ibb.co/xJ9qQqM/play-button.png"
                  playHoverIcon="https://i.ibb.co/xJ9qQqM/play-button.png"
                  pauseIcon="https://i.ibb.co/q5sJzn3/pause-button.jpg"
                  pauseHoverIcon="https://i.ibb.co/q5sJzn3/pause-button.jpg"
                  rewindIcon="https://i.ibb.co/TrjTNgx/backword-button.png"
                  rewindHoverIcon="https://i.ibb.co/TrjTNgx/backword-button.png"
                  forwardIcon="https://i.ibb.co/z7MTSPv/forward-button.png"
                  forwardHoverIcon="https://i.ibb.co/z7MTSPv/forward-button.png"
                  volumeIcon="https://i.ibb.co/7251c2w/volume-icon.png"
                  volumeEngagedIcon="https://i.ibb.co/7251c2w/volume-icon.png"
                  muteIcon="https://i.ibb.co/mHkY8qg/mute.jpg"
                  muteEngagedIcon="https://i.ibb.co/mHkY8qg/mute.jpg"
                  loopIcon="https://i.ibb.co/d5Jxvd6/loop-icon.png"
                  loopEngagedIcon="https://i.ibb.co/d5Jxvd6/loop-icon.png"
                  fontFamily="serif"
                />
                <i
                  className="fas fa-random"
                  style={{
                    color: "#999",
                    marginTop: "10%",
                    cursor: "pointer ",
                    marginLeft: "2%",
                  }}
                />
              </div>
              <div
                className="col-md-2 d-none d-md-block "
                style={{ borderLeft: "1px solid lightgrey" }}
              >
                <div className="mt-4">
                  <span
                    style={{
                      border: "1px solid lightgrey",
                      borderRadius: "50px",
                      paddingLeft: "10px",
                      paddingRight: "10px",
                      paddingTop: "4px",
                      paddingBottom: "5px",
                      cursor: "pointer",
                      color: "#999",
                    }}
                  >
                    High &nbsp;
                    <i className="fas fa-sort-up" />
                  </span>
                </div>
              </div>
              <div
                className="col-md d-none d-md-block text-left "
                style={{ borderLeft: "1px solid lightgrey" }}
              >
                <div className="mt-4">
                  <span
                    style={{
                      paddingLeft: "10px",
                      paddingRight: "10px",
                      paddingTop: "4px",
                      paddingBottom: "5px",
                      color: "#999",
                    }}
                  >
                    AUTOPLAY
                  </span>
                  <span
                    style={{
                      border: "1px solid lightgrey",
                      borderRadius: "50px",
                      paddingLeft: "10px",
                      paddingRight: "10px",
                      paddingTop: "4px",
                      paddingBottom: "5px",
                      cursor: "pointer",
                      marginLeft: "10px",
                      color: "#999",
                    }}
                  >
                    off
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MediaPlayer;
