import React, { Component } from "react";
import "./footer.css";

class Footer extends Component {
  state = {};

  render() {
    return (
      <div style={{ background: "#EBECED" }}>
        <div className="row  p-1 pb-5 mb-5">
          <div className="col-12">
            <hr />
          </div>
          <div
            className="col-4 mt-2"
            style={{ fontSize: "12px", color: "#999" }}
          >
            <h6>
              <a href="#" style={{ textDecoration: "none", color: "black" }}>
                Albums
              </a>
            </h6>
            <a className="_links" href="https://gaana.com/album/english">
              English
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/album/hindi">
              Hindi
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/album/telugu">
              Telugu
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/album/punjabi">
              Punjabi
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/album/tamil">
              Tamil
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/album/kannada">
              Kannada
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/album/bhojpuri">
              Bhojpuri
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/album/malayalam">
              Malayalam
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/album/marathi">
              Marathi
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/album/bengali">
              Bengali
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/album/gujarati">
              Gujarati Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/album/haryanvi">
              Haryanvi
            </a>
            <span>|</span>
          </div>
          <div
            className="col-4 mt-2"
            style={{ fontSize: "12px", color: "#999" }}
          >
            <h6>
              <a
                className="_links"
                href="#"
                style={{ textDecoration: "none", color: "black" }}
              >
                Genres
              </a>
            </h6>{" "}
            <a className="_links" href="https://gaana.com/genre/bollywood">
              Bollywood Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/genre/romantic">
              Romantic Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/genre/devotional">
              Devotional Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/genre/ghazal-1">
              Ghazals
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/genre/bhajans">
              Bhajan
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/genre/patriotic">
              Patriotic Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/genre/kids">
              Kids Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/genre/rock">
              Rock Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/genre/disco">
              Disco Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/genre/sufi">
              Sufi Songs
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/playlist/gaana-dj-love-songs-upbeat"
            >
              Love Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/genre/patriotic">
              Patriotic Songs
            </a>{" "}
            <span>|</span>
          </div>
          <div
            className="col-4 mt-2"
            style={{ fontSize: "12px", color: "#999" }}
          >
            <h6>
              <a href="#" style={{ textDecoration: "none", color: "black" }}>
                Artists
              </a>
            </h6>{" "}
            <a className="_links" href="https://gaana.com/artist/arijit-singh">
              Arijit Singh
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/artist/neha-kakkar">
              Neha Kakkar
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/artist/yo-yo-honey-singh"
            >
              Honey Singh
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/artist/atif-aslam">
              Atif Aslam
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/artist/ar-rahman">
              A R Rahman
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/artist/lata-mangeshkar"
            >
              Lata Mangeshkar
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/artist/kishore-kumar">
              Kishore Kumar
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/artist/arman-mallik">
              Armaan Malik
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/artist/sunidhi-chauhan"
            >
              Sunidhi Chauhan
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/artist/nusrat-fateh-ali-khan"
            >
              Nusrat Fateh Ali Khan
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/artist/mohammad-rafi">
              Mohammed Rafi
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/artist/guru-randhawa">
              Guru Randhawa
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/artist/justin-bieber">
              Justin Bieber
            </a>{" "}
            <span>|</span>
          </div>
          <div
            className="col-4 mt-4"
            style={{ fontSize: "12px", color: "#999" }}
          >
            <h6>
              <a href="#" style={{ textDecoration: "none", color: "black" }}>
                New Release
              </a>
            </h6>{" "}
            <a className="_links" href="https://gaana.com/newrelease/english">
              English Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/newrelease/hindi">
              Hindi Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/newrelease/punjabi">
              Punjabi Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/newrelease/tamil">
              Tamil Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/newrelease/telugu">
              Telugu Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/newrelease/kannada">
              Kannada Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/newrelease/bhojpuri">
              Bhojpuri Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/newrelease/malayalam">
              Malayalam Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/newrelease/marathi">
              Marathi Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/newrelease/bengali">
              Bengali Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/newrelease/assamese">
              Assamese Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/newrelease/haryanvi">
              Haryanvi Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/newrelease/gujarati">
              Gujarati
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/newrelease/odia">
              Odia Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/newrelease/urdu">
              Urdu Songs
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/newrelease/rajasthani"
            >
              Rajasthani Songs
            </a>{" "}
            <span>|</span>
          </div>
          <div
            className="col-4 mt-4"
            style={{ fontSize: "12px", color: "#999" }}
          >
            <h6>
              <a href="#" style={{ textDecoration: "none", color: "black" }}>
                Trending Songs
              </a>
            </h6>{" "}
            <a className="_links" href="https://gaana.com/song/filhall">
              Filhall
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/song/dhummu-dholi">
              Dhummu Dholi
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/song/shree-hanuman-chalisa-22"
            >
              Hanuman Chalisa
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/song/genda-phool-6">
              Genda Phool
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/song/shaitan-ka-saala-from-housefull-4"
            >
              Shaitan Ka Saala
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/song/godzilla-191">
              Godzilla
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/album/moscow-suka">
              Moscow Mashuka
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/song/bekhayali-1">
              Bekhayali
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/song/tujhe-kitna-chahne-lage"
            >
              Tujhe Kitna Chahne Lage
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/song/rabb-wangu-1">
              Rabb Wangu
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/song/muqabla-from-street-dancer-3d-2"
            >
              Muqabla
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/song/pachtaoge">
              Pachtaoge
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/song/gajban-1">
              Gajban Pani Ne Chali
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/song/pyaar-karona">
              Pyaar Karona
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/song/tera-ban-jaunga">
              Tera Ban Jaunga
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/song/chumma-kizhi">
              Chumma Kizhi
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/song/tum-hi-aana-from-marjaavaan"
            >
              Tum Hi Aana
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/song/jinke-liye-from-jaani-ve"
            >
              Jinke Liye
            </a>{" "}
            <span>|</span>
          </div>
          <div
            className="col-4  mt-4"
            style={{ fontSize: "12px", color: "#999" }}
          >
            <h6>
              <a
                href="/album"
                style={{ textDecoration: "none", color: "black" }}
              >
                Trending Albums
              </a>
            </h6>{" "}
            <a
              className="_links"
              href="https://gaana.com/album/happy-birthday-english-0"
            >
              Happy Birthday
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/album/bollywood-sad-songs"
            >
              Sad Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/album/pk">
              PK
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/album/taki-taki">
              Taki Taki
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/album/malang-unleash-the-madness"
            >
              Malang
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/album/music-to-be-murdered-by"
            >
              Music To Be Murdered By
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/album/maharshi-telugu-1"
            >
              Maharshi
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/album/street-dancer-3d"
            >
              Street Dancer 3d
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/album/marjaavaan-hindi"
            >
              Marjaavaan
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/album/syeraa-narasimha-reddy-telugu"
            >
              Syeraa Narasimha Reddy
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/album/kabir-singh">
              Kabir Singh
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/playlist/inhouse-dj-eid-mubarak"
            >
              Eid Mubarak Songs
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/album/love-aaj-kal-2020"
            >
              Love Aaj Kal
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/album/chhapaak">
              Chhapaak
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/album/mata-ki-bhetein"
            >
              Mata ki Bhetein
            </a>{" "}
            <span>|</span>
          </div>
          <div
            className="col-4  mt-4"
            style={{ fontSize: "12px", color: "#999" }}
          >
            <h6>
              <a href="#" style={{ textDecoration: "none", color: "black" }}>
                Lyrics
              </a>
            </h6>{" "}
            <a className="_links" href="https://gaana.com/lyrics/tumhiho">
              Tum Hi Ho
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/lyrics/jab-koi-baat-bigad-jaye-3"
            >
              Jab Koi Baat Bigad Jaye
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/lyrics/nazm-nazm">
              Nazm Nazm
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/lyrics/leja-re">
              Leja Re
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/lyrics/duniyaa">
              Duniyaa
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/lyrics/asli-hip-hop">
              Asli Hip Hop
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/lyrics/cheap-thrills-37"
            >
              Cheap Thrills
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/lyrics/gayatri-mantra-82"
            >
              Gayatri Mantra
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/lyrics/kya-hua-tera-vada-2"
            >
              Kya Hua Tera Vada
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/lyrics/machayenge">
              Machayenge
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/lyrics/teri-mitti">
              Teri Mitti
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/lyrics/panga-title-track"
            >
              Panga
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/lyrics/jana-gana-mana-national-anthem-1"
            >
              Gana Gana Mana Lyrics
            </a>{" "}
            <span>|</span>
          </div>
          <div
            className="col-4  mt-4"
            style={{ fontSize: "12px", color: "#999" }}
          >
            <h6>
              <a href="#" style={{ textDecoration: "none", color: "black" }}>
                Old Songs
              </a>
            </h6>{" "}
            <a className="_links" href="https://gaana.com/old-songs/hindi">
              Old Hindi Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/old-songs/english">
              Old English Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/old-songs/punjabi">
              Old Punjabi Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/old-songs/telugu">
              Old Telugu Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/old-songs/tamil">
              Old Tamil Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/old-songs/bhojpuri">
              Old Bhojpuri Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/old-songs/bengali">
              Old Bengali Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/old-songs/malayalam">
              Old Malayalam Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/old-songs/kannada">
              Old Kannada Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/old-songs/marathi">
              Old Marathi Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/old-songs/gujarati">
              Old Gujarati Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/old-songs/haryanvi">
              Old Haryanvi Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/old-songs/urdu">
              Old Urdu Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/old-songs/assamese">
              Old Assamese Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/old-songs/rajasthani">
              Old Rajasthani Songs
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/old-songs/odia">
              Old Odia Songs
            </a>{" "}
            <span>|</span>
          </div>
          <div
            className="col-4  mt-4"
            style={{ fontSize: "12px", color: "#999" }}
          >
            <h6>
              <a href="#" style={{ textDecoration: "none", color: "black" }}>
                Video
              </a>
            </h6>{" "}
            <a className="_links" href="https://gaana.com/video/teri-mitti">
              Teri Mitti
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/video/chal-oye">
              Chal Oye
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/video/machayenge">
              Machayenge
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/video/shuru-karein-kya"
            >
              Shuru Karein Kya
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/video/mujhe-kaise-pata-na-chala"
            >
              Mujhe Kaise Pata Na Chala
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/gaanavideo/prada-1">
              Prada
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/gaanavideo/wakhra-swag-2"
            >
              Wakhra Swag
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/video/fakira-4">
              Fakira
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/gaanavideo/hawa-banke"
            >
              Hawa Banke
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/video/the-hook-up-song"
            >
              The Hook Up Song
            </a>{" "}
            <span>|</span>{" "}
            <a
              className="_links"
              href="https://gaana.com/video/dheeme-dheeme-7"
            >
              Dheeme Dheeme
            </a>{" "}
            <span>|</span>{" "}
            <a className="_links" href="https://gaana.com/gaanavideo/paagal">
              Paagal
            </a>{" "}
            <span>|</span>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
