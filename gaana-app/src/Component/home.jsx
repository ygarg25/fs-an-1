import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Footer from "./footer";
import "./thumbnail.css";
import Login from "./login";
import jwtDecode from "jwt-decode";
import Regitser from "./register";

const jwt = localStorage.getItem("token");
let userid;
if (jwt) {
  let token = jwt.split(" ")[1];

  userid = jwtDecode(token).id;
}

let url = "http://localhost:2410";

class Home extends Component {
  state = {
    currentPagePick: 1,
    pageSizePick: 2,
    currentPageTrending: 1,
    pageSizeTrending: 4,
    currentPageChart: 1,
    pageSizeChart: 4,
    formData: { q: "" },
    topPick: [
      "https://i.ibb.co/dgdYxbc/1.png",
      "https://i.ibb.co/Hht04SY/2.png",
      "https://i.ibb.co/M1v2kLF/3.png",
      "https://i.ibb.co/GFWZn30/4.png",
      "https://i.ibb.co/zQZcXT2/5.png",
      "https://i.ibb.co/SNxzf6d/6.png",
      "https://i.ibb.co/28kvVzG/7.png",
      "https://i.ibb.co/HgWbt7k/8.png",
    ],
    topChart: [
      {
        url: "https://i.ibb.co/PxRkk4d/ganna-Original.jpg",
        value: "Ganna Original",
        desc: "Gaana Originals Top 2 ",
      },
      {
        url: "https://i.ibb.co/Gxj1w30/hindi.jpg",
        value: "Hindi",
        desc: "Hindi Top 2",
      },
      {
        url: "https://i.ibb.co/qCmV4G7/indiePop.jpg",
        value: "Indiepop",
        desc: "IndiePop Top 2",
      },
      {
        url: "https://i.ibb.co/fN9KcmX/latest-Dance.jpg",
        value: "Latest Dance",
        desc: "Latest Dance Top 2",
      },
      {
        url: "https://i.ibb.co/ydMR4Dz/latestlove.jpg",
        value: "Latest Love",
        desc: "Latest Love Top 2",
      },
      {
        url: "https://i.ibb.co/dsT8j7N/punjabi.jpg",
        value: "Punjabi",
        desc: "Punjabi Top 2",
      },
      {
        url: " https://i.ibb.co/Pw6TZ0L/romance-Hindi.jpg",
        value: "Romance Hindi",
        desc: "Romance Hindi Top 2",
      },
      {
        url: "https://i.ibb.co/9WNGkkw/today.jpg",
        value: "Today",
        desc: "Today's Top 2",
      },
    ],
    Search: [
      "Bheegi Bheegi",
      "Tere Karke",
      "Meri Aashiqui",
      "Hawa Banke",
      "I am Disco Dancer 2.0",
    ],
  };

  //WARNING! To be deprecated in React v17. Use componentDidMount instead.
  async componentWillMount() {
    try {
      const { data: topTrending } = await axios.get(url + "/allTrendingSong");
      this.setState({ topTrending: topTrending });
    } catch (ex) {
      console.log("catch");
      return null;
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    if (prevProps !== currProps) {
      try {
        const { data: topTrending } = await axios.get(url + "/allTrendingSong");
        this.setState({ topTrending: topTrending });
      } catch (ex) {
        console.log("catch");
        return null;
      }
    }
  }

  handlePagePick = (p) => {
    this.setState({ currentPagePick: this.state.currentPagePick + p });
  };

  handlePageTreding = (p) => {
    this.setState({ currentPageTrending: this.state.currentPageTrending + p });
  };

  handlePageChart = (p) => {
    this.setState({ currentPageChart: this.state.currentPageChart + p });
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    console.log("search", this.state.formData.q);

    this.props.history.push(`/search/${this.state.formData.q}`);
  };

  handleChange = (e) => {
    const { currentTarget: input } = e;
    let formData = { ...this.state.formData };
    console.log("change", input.value);
    formData[input.name] = input.value;
    this.setState({ formData: formData });
  };

  render() {
    const {
      topChart: tc,
      topPick: tp,
      topTrending: tt,
      currentPagePick,
      pageSizePick,
      currentPageTrending,
      pageSizeTrending,
      currentPageChart,
      pageSizeChart,
      Search,
    } = this.state;
    let pickPage = [];
    let maxPagePick;
    if (this.state.topPick) {
      maxPagePick = Math.ceil(this.state.topPick.length - 1);
      let startIndex = currentPagePick - 1;
      let copyPickData = [...this.state.topPick];
      pickPage = copyPickData.splice(startIndex, pageSizePick);
    }

    let trendingPage = [];
    let maxPageTrending;
    if (this.state.topTrending) {
      maxPageTrending = Math.ceil(this.state.topTrending.length - 4);
      let startIndex = currentPageTrending - 1;
      let copyTrendingData = [...this.state.topTrending];
      trendingPage = copyTrendingData.splice(startIndex, pageSizeTrending);
    }

    let chartPage = [];
    let maxPageChart;
    if (this.state.topChart) {
      maxPageChart = Math.ceil(this.state.topChart.length - 4);
      let startIndex = currentPageChart - 1;
      let copyChartData = [...this.state.topChart];
      chartPage = copyChartData.splice(startIndex, pageSizeChart);
    }
    return (
      <div style={{ backgroundColor: "lightgrey" }}>
        <div className=" row">
          {!jwt && this.props.modal === 1 && (
            <div className="col-12  bg-dark">
              <div className="overl row">
                <div className="col-lg-3 d-none d-lg-block"></div>
                <div
                  className="col-lg-6 col-12 row p-0 mt-lg-5 mb-lg-5 "
                  style={{ opacity: "2", height: "", marginBottom: "" }}
                >
                  <div className="col-12 bg-white text-right">
                    <span
                      style={{
                        cursor: "pointer",
                        fontSize: "30px",
                        paddingRight: "15px",
                      }}
                      onClick={() => this.props.handleSignIn(-1, 1)}
                    >
                      &times;
                    </span>
                  </div>
                  <div className="col-12 p-lg-0 pl-4 pr-0">
                    {this.props.show === 2 && <Regitser {...this.props} />}
                    {this.props.show === 1 && <Login {...this.props} />}
                  </div>
                </div>
                <div className="col-lg-3 d-none d-lg-block"></div>
              </div>
            </div>
          )}
          <div className="col-lg-9 col-12" style={{ background: "#EBECED" }}>
            <div className="row pt-5 ">
              <div className=" col-12 bg-white border mt-4 ">
                <form onSubmit={this.handleSubmit} className="row p-2 pl-4">
                  <div className="form-group col-7">
                    <input
                      value={this.state.formData.q}
                      className="form-control"
                      type="search"
                      id="q"
                      name="q"
                      placeholder="Search for Songs, Artists, Playlists and More"
                      onChange={this.handleChange}
                      style={{ borderRadius: "50px" }}
                    />
                  </div>
                  <div className="col-2 text-right" style={{ left: "-16.5%" }}>
                    <button
                      type="submit"
                      className="btn btn-danger"
                      style={{ borderRadius: "50px" }}
                    >
                      Search
                    </button>
                  </div>
                  <div className="col-2" style={{ left: "-8%" }}>
                    <div className="btn-group dropleft">
                      <button
                        type="button"
                        className="btn btn-danger "
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                        style={{ borderRadius: "50px" }}
                      >
                        Trending
                      </button>
                      <div className="dropdown-menu">
                        {Search.map((s, i) => (
                          <div className="dropdown-item m-1" key={i}>
                            <Link
                              to={`/search/${s}`}
                              style={{ textDecoration: "none" }}
                            >
                              <span
                                className="border  p-1 pl-2 pr-2"
                                style={{
                                  borderRadius: "50px",
                                  backgroundColor: "#EBECED",
                                  color: "red",
                                }}
                              >
                                {s}
                              </span>
                            </Link>
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div className="col-lg-1 d-none d-lg-block"></div>
              <div className="col-lg-11 col-12 ">
                <div className="row pt-5 pr-5 ml-4">
                  <div className="col-12">
                    <h2 style={{ fontSize: "18px", color: "#555" }}>
                      TOP PICKS
                    </h2>
                  </div>
                  <div className="row">
                    {pickPage.map((t, i) => (
                      <div
                        className="col-6 text-center mb-1 pr-1 "
                        style={{ float: "left", width: "" }}
                        key={i}
                      >
                        <img
                          className="imageParent"
                          src={t}
                          style={{
                            width: "100%",
                            height: "200px",
                            cursor: "pointer",
                          }}
                        />
                        <span className="imagePlay">
                          <i className="fas fa-play-circle"></i>
                        </span>
                      </div>
                    ))}

                    {currentPagePick > 1 ? (
                      <span
                        style={{
                          textAlign: "center",
                          fontSize: "20px",
                          padding: "12px",
                          cursor: "pointer",
                          top: "-50%",
                          position: "relative",
                          zIndex: "1",
                          borderRadius: "50%",
                          border: "none",
                          backgroundColor: "white",
                        }}
                        onClick={() => this.handlePagePick(-1)}
                      >
                        <i className="fa fa-angle-left" />
                      </span>
                    ) : (
                      <span
                        style={{
                          textAlign: "center",
                          fontSize: "20px",
                          padding: "12px",
                          cursor: "pointer",
                          top: "-50%",
                          position: "relative",
                          zIndex: "1",
                          borderRadius: "50%",
                          border: "none",
                          backgroundColor: "grey",
                          opacity: "0.5",
                        }}
                      >
                        <i className="fa fa-angle-left" />
                      </span>
                    )}
                    {currentPagePick < maxPagePick ? (
                      <span
                        style={{
                          textAlign: "center",
                          fontSize: "20px",
                          padding: "12px",
                          cursor: "pointer",
                          top: "-50%",
                          left: "93%",
                          position: "relative",
                          zIndex: "1",
                          borderRadius: "50%",
                          border: "none",
                          backgroundColor: "white",
                        }}
                        onClick={() => this.handlePagePick(1)}
                      >
                        <i className="fa fa-angle-right" />
                      </span>
                    ) : (
                      <span
                        style={{
                          textAlign: "center",
                          fontSize: "20px",
                          padding: "12px",
                          cursor: "pointer",
                          top: "-50%",
                          left: "93%",
                          position: "relative",
                          zIndex: "1",
                          borderRadius: "50%",
                          border: "none",
                          backgroundColor: "grey",
                          opacity: "0.5",
                        }}
                      >
                        <i className="fa fa-angle-right" />
                      </span>
                    )}
                  </div>
                  <div className="col-12">
                    <hr />
                  </div>
                  <div className="col-6">
                    <Link to="songs" style={{ textDecoration: "none" }}>
                      <h2
                        style={{
                          fontSize: "18px",
                          color: "#555",
                          cursor: "pointer",
                        }}
                      >
                        TRENDINGS SONGS
                      </h2>
                    </Link>
                  </div>
                  <div className="col-6 text-right">
                    <Link to="/songs" style={{ textDecoration: "none" }}>
                      <span
                        style={{
                          fontSize: "14px",
                          color: "red",
                          cursor: "pointer",
                        }}
                      >
                        View all
                      </span>
                    </Link>
                  </div>
                  <div className="row">
                    {this.state.topTrending &&
                      trendingPage.map((t, i) => (
                        <div className="col-3 text-left " key={i}>
                          <Link
                            to={`/song/${t.SName}`}
                            style={{ textDecoration: "none" }}
                            onClick={() => this.props.handleSongPlay([t.Id])}
                          >
                            <img
                              className="imageParent"
                              src={t.SImage}
                              style={{
                                width: "100%",
                                height: "auto",
                                cursor: "pointer",
                              }}
                            />
                            <span className="imagePlay">
                              <i className="fas fa-play-circle"></i>
                            </span>
                          </Link>
                          <Link
                            to={`/song/${t.SName}`}
                            style={{ textDecoration: "none" }}
                            onClick={() => this.props.handleSongPlay([t.Id])}
                          >
                            <span style={{ fontSize: "70%", color: "#999" }}>
                              {t.SName}
                            </span>
                          </Link>
                        </div>
                      ))}
                    {currentPageTrending > 1 ? (
                      <span
                        style={{
                          textAlign: "center",
                          fontSize: "20px",
                          padding: "12px",
                          cursor: "pointer",
                          top: "-50%",
                          position: "relative",
                          zIndex: "1",
                          borderRadius: "50%",
                          border: "none",
                          backgroundColor: "white",
                        }}
                        onClick={() => this.handlePageTreding(-1)}
                      >
                        <i className="fa fa-angle-left" />
                      </span>
                    ) : (
                      <span
                        style={{
                          textAlign: "center",
                          fontSize: "20px",
                          padding: "12px",
                          cursor: "pointer",
                          top: "-50%",
                          position: "relative",
                          zIndex: "1",
                          borderRadius: "50%",
                          border: "none",
                          backgroundColor: "grey",
                          opacity: "0.5",
                        }}
                      >
                        <i className="fa fa-angle-left" />
                      </span>
                    )}

                    {currentPageTrending < maxPageTrending ? (
                      <span
                        style={{
                          textAlign: "center",
                          fontSize: "20px",
                          padding: "12px",
                          cursor: "pointer",
                          top: "-50%",
                          left: "93%",
                          position: "relative",
                          zIndex: "1",
                          borderRadius: "50%",
                          border: "none",
                          backgroundColor: "white",
                        }}
                        onClick={() => this.handlePageTreding(1)}
                      >
                        <i className="fa fa-angle-right" />
                      </span>
                    ) : (
                      <span
                        style={{
                          textAlign: "center",
                          fontSize: "20px",
                          padding: "12px",
                          cursor: "pointer",
                          top: "-50%",
                          left: "93%",
                          position: "relative",
                          zIndex: "1",
                          borderRadius: "50%",
                          border: "none",
                          backgroundColor: "grey",
                          opacity: "0.5",
                        }}
                      >
                        <i className="fa fa-angle-right" />
                      </span>
                    )}
                  </div>
                  <div className="col-12">
                    <hr />
                  </div>
                  <div className="col-6">
                    <Link to="topCharts" style={{ textDecoration: "none" }}>
                      <h2
                        style={{
                          fontSize: "18px",
                          color: "#555",
                          cursor: "pointer",
                        }}
                      >
                        TOP CHARTS
                      </h2>
                    </Link>
                  </div>
                  <div className="col-6 text-right">
                    <Link to="/topCharts" style={{ textDecoration: "none" }}>
                      <span
                        style={{
                          fontSize: "14px",
                          color: "red",
                          cursor: "pointer",
                        }}
                      >
                        View all
                      </span>
                    </Link>
                  </div>

                  <div className="row">
                    {this.state.topChart &&
                      chartPage.map((t, i) => (
                        <div className="col-3  text-left " key={i}>
                          <Link to={`/playlist/${t.value}`}>
                            <img
                              className="imageParent"
                              src={t.url}
                              style={{
                                width: "100%",
                                height: "auto",
                                cursor: "pointer",
                              }}
                            />
                            <span className="imagePlay">
                              <i className="fas fa-play-circle"></i>
                            </span>
                          </Link>
                          <Link
                            to={`/playlist/${t.value}`}
                            style={{ textDecoration: "none" }}
                          >
                            <span style={{ fontSize: "70%", color: "#999" }}>
                              {t.desc}
                            </span>
                          </Link>
                        </div>
                      ))}
                    {currentPageChart > 1 ? (
                      <span
                        style={{
                          textAlign: "center",
                          fontSize: "20px",
                          padding: "12px",
                          cursor: "pointer",
                          top: "-50%",
                          position: "relative",
                          zIndex: "1",
                          borderRadius: "50%",
                          border: "none",
                          backgroundColor: "white",
                        }}
                        onClick={() => this.handlePageChart(-1)}
                      >
                        <i className="fa fa-angle-left" />
                      </span>
                    ) : (
                      <span
                        style={{
                          textAlign: "center",
                          fontSize: "20px",
                          padding: "12px",
                          cursor: "pointer",
                          top: "-50%",
                          position: "relative",
                          zIndex: "1",
                          borderRadius: "50%",
                          border: "none",
                          backgroundColor: "grey",
                          opacity: "0.5",
                        }}
                      >
                        <i className="fa fa-angle-left" />
                      </span>
                    )}
                    {currentPageChart < maxPageChart ? (
                      <span
                        style={{
                          textAlign: "center",
                          fontSize: "20px",
                          padding: "12px",
                          cursor: "pointer",
                          top: "-50%",
                          left: "93%",
                          position: "relative",
                          zIndex: "1",
                          borderRadius: "50%",
                          border: "none",
                          backgroundColor: "white",
                        }}
                        onClick={() => this.handlePageChart(1)}
                      >
                        <i className="fa fa-angle-right" />
                      </span>
                    ) : (
                      <span
                        style={{
                          textAlign: "center",
                          fontSize: "20px",
                          padding: "12px",
                          cursor: "pointer",
                          top: "-50%",
                          left: "93%",
                          position: "relative",
                          zIndex: "1",
                          borderRadius: "50%",
                          border: "none",
                          backgroundColor: "grey",
                          opacity: "0.5",
                        }}
                      >
                        <i className="fa fa-angle-right" />
                      </span>
                    )}
                  </div>
                  <div className="col-12">
                    <hr />
                  </div>
                  <br />
                </div>
                <Footer />
              </div>
            </div>
          </div>
          <div className="col-lg-3 d-none d-lg-block"></div>
        </div>
      </div>
    );
  }
}

export default Home;
