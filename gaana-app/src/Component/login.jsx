import React, { Component } from "react";
import axios from "axios";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import "./login.css";
import { Redirect, Link } from "react-router-dom";

let url = "http://localhost:2410";
// let url = "https://us-central1-yat12-77281.cloudfunctions.net/app";

class Login extends Component {
  state = {
    userData: {
      username: "",
      password: "",
    },
  };
  handleSubmit = async (fields) => {
    console.log("submitted : ", fields);
    let apiEndPoint = url + "/login";
    let { state } = this.props.location;
    console.log("ssfff", state);
    try {
      const { data: response } = await axios.post(apiEndPoint, fields);
      console.log("response", response.token);
      localStorage.setItem("token", response.token);
      //window.location = "/";
      window.location = state ? state.from.pathname : "/";
    } catch (err) {
      if (err.response) {
        alert("Invalid Username and Password");
      }
    }
  };
  render() {
    if (localStorage.getItem("token")) return <Redirect to="/" />;
    const { userData } = this.state;
    console.log("Ss", this.state.errors);
    return (
      <div>
        <Formik
          initialValues={{
            username: "",
            password: "",
          }}
          validationSchema={Yup.object().shape({
            username: Yup.string()
              .email("Email is invalid")
              .required("Email is required"),
            password: Yup.string()
              .min(6, "Password must be at least 6 characters")
              .required("Password is required"),
          })}
          onSubmit={(fields) => {
            this.handleSubmit(fields);
          }}
          render={({ errors, status, touched, values }) => {
            return (
              <div style={{ backgroundColor: "#F4F6F7" }}>
                <div
                  className="row m-0 mt-0 "
                  style={{ backgroundColor: "", margin: "" }}
                >
                  {/* <div className="col-4 d-none d-lg-block"></div> */}
                  <div
                    className="col-lg-12 col-12 bg-white "
                    style={{ border: "3px", borderRadius: "7px" }}
                  >
                    <div className="row">
                      <div className="col-12 text-center">
                        <h5>INDIA'S NO.1 MUSIC APP</h5>
                      </div>
                      <div
                        className="col-12 text-center mt-1 text-muted"
                        style={{ fontWeight: "500", fontSize: "13px" }}
                      >
                        <p>
                          Over 30 million songs to suit every mood and ocassion
                        </p>
                      </div>
                      <div className="col-12  mt-2">
                        <div className="row text-center ">
                          <div className="col-4">
                            <span
                              style={{
                                cursor: "pointer",
                                border: "2px solid red",
                                borderRadius: "100%",
                                padding: "8px",
                                color: "red",
                                fontSize: "18px",
                              }}
                            >
                              <i className="fas fa-headphones-alt"></i>
                            </span>
                            <div
                              className="m-2 text-muted"
                              style={{ fontSize: "10px" }}
                            >
                              Create your
                              <br /> own playlists
                            </div>
                          </div>
                          <div className="col-4">
                            <span
                              style={{
                                cursor: "pointer",
                                border: "2px solid red",
                                borderRadius: "100%",
                                padding: "8px",
                                color: "red",
                                fontSize: "18px",
                              }}
                            >
                              <i className="fas fa-share-alt"></i>
                            </span>
                            <div
                              className="m-2 text-muted"
                              style={{ fontSize: "10px" }}
                            >
                              Share music with
                              <br /> family and friends
                            </div>
                          </div>
                          <div className="col-4">
                            <span
                              style={{
                                cursor: "pointer",
                                border: "2px solid red",
                                borderRadius: "100%",
                                padding: "8px",
                                color: "red",
                                fontSize: "18px",
                              }}
                            >
                              <i className="far fa-heart"></i>
                            </span>
                            <div
                              className="m-2 text-muted"
                              style={{ fontSize: "10px" }}
                            >
                              Save your
                              <br /> favourites{" "}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-12">
                        <hr />
                      </div>
                      <div className="col-12 text-center">
                        <div className="row  mt-1">
                          <div className="col-12 text-center">
                            <Form>
                              <div className="form-group">
                                <Field
                                  type="text"
                                  name="username"
                                  placeholder="EMAIL ID / MOBILE NUMBER"
                                  className={
                                    "form-control" +
                                    (errors.username && touched.username
                                      ? " is-invalid"
                                      : "")
                                  }
                                />
                                <ErrorMessage
                                  name="username"
                                  component="div"
                                  className="invalid-feedback"
                                />
                              </div>
                              <div className="form-group">
                                <Field
                                  type="password"
                                  name="password"
                                  placeholder="PASSWORD"
                                  className={
                                    "form-control" +
                                    (errors.password && touched.password
                                      ? " is-invalid"
                                      : "")
                                  }
                                />
                                <ErrorMessage
                                  name="password"
                                  component="div"
                                  className="invalid-feedback"
                                />
                              </div>
                              <div className="row">
                                <div className="col-12 text-center">
                                  <button
                                    type="submit"
                                    className="btn btn-danger col-12"
                                  >
                                    LogIn
                                  </button>
                                </div>
                              </div>
                            </Form>
                          </div>
                          <hr />
                          <div className="col-12 text-left">
                            <Link
                              to="/"
                              onClick={() => this.props.handleSignIn(1, 2)}
                            >
                              <small>
                                For Registering New User Click Here...
                              </small>
                            </Link>
                          </div>
                          <div
                            className="col-12 text-center"
                            style={{ fontSize: "12px", color: "#777" }}
                          >
                            --------or continue with--------
                          </div>
                        </div>
                        <div className="col-12 pb-3">
                          <div className="row text-center">
                            <div className="col-4 mt-2">
                              <div
                                className="text-primary"
                                style={{ fontSize: "300%", cursor: "pointer" }}
                              >
                                <i className="fab fa-facebook"></i>
                              </div>
                              <div
                                style={{
                                  color: "#777",
                                  fontWeight: "500",
                                  fontSize: "12px",
                                  cursor: "pointer",
                                }}
                              >
                                Facebook
                              </div>
                            </div>
                            <div className="col-4 mt-2">
                              <div
                                className="text-danger"
                                style={{ fontSize: "300%", cursor: "pointer" }}
                              >
                                <i className="fab fa-google"></i>
                              </div>
                              <div
                                style={{
                                  color: "#777",
                                  fontWeight: "500",
                                  fontSize: "12px",
                                  cursor: "pointer",
                                }}
                              >
                                Google
                              </div>
                            </div>
                            <div className="col-4 mt-2">
                              <div
                                className="text-muted"
                                style={{ fontSize: "300%", cursor: "pointer" }}
                              >
                                <i className="fas fa-envelope"></i>
                              </div>
                              <div
                                style={{
                                  color: "#777",
                                  fontWeight: "500",
                                  fontSize: "12px",
                                  cursor: "pointer",
                                }}
                              >
                                Email
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* <div className="col-4 d-none d-lg-block"></div> */}
                </div>
              </div>
            );
          }}
        />
      </div>
    );
  }
}

export default Login;
