import React, { Component } from "react";
import PlaylistOption from "./playlistOption";
import AudioPlayer from "react-h5-audio-player";
import "react-h5-audio-player/lib/styles.css";
import axios from "axios";

let url = "http://localhost:2410";

class MediaPlayer extends Component {
  state = {
    songsData: undefined,
    playId: this.props.songPlay,
    indexPlay: 0,
    audioFiles: undefined,
    show: 1,
    fav: false,
  };

  //WARNING! To be deprecated in React v17. Use componentDidMount instead.
  async componentWillMount() {
    try {
      const { data: songsData } = await axios.get(url + "/allSong");
      this.setState({ songsData: songsData, fav: false });
    } catch (ex) {
      console.log("catch");
      return null;
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    if (prevProps !== currProps) {
      console.log("cdm");
      this.setState({ playId: this.props.songPlay, indexPlay: 0, fav: false });
    }
    if (prevProps !== currProps) {
      try {
        const { data: songsData } = await axios.get(url + "/allSong");
        this.setState({ songsData: songsData, fav: false });
      } catch (ex) {
        console.log("catch");
        return null;
      }
    }
  }

  handleShow = (e) => {
    this.setState({ show: e });
  };

  handleClickNext = () => {
    if (this.state.indexPlay < this.state.songsData.length) {
      this.setState({ indexPlay: this.state.indexPlay + 1, fav: false });
    } else {
      this.setState({ indexPlay: 0, fav: false });
    }
  };

  handleClickPrevious = () => {
    if (this.state.indexPlay > 0) {
      this.setState({ indexPlay: this.state.indexPlay - 1, fav: false });
    } else {
      this.setState({ indexPlay: this.state.songsData.length - 1, fav: false });
    }
  };

  handleFav = () => {
    this.setState({ fav: !this.state.fav });
  };

  render() {
    let { playId, songsData, indexPlay, audioFiles, show, fav } = this.state;
    audioFiles = [];
    let playData;
    let index;
    if (this.state.songsData) {
      index = songsData.findIndex((s) => +s.Id === +playId[0]);
      songsData.map((s, i) => {
        if (i >= index) audioFiles.push(s);
      });
      songsData.map((s, i) => {
        if (i < index) audioFiles.push(s);
      });
      playData = audioFiles[indexPlay];
    }
    console.log(
      "Media player",
      playId,
      playData,
      indexPlay,
      this.props.songPlay,
      index,
      audioFiles
    );
    return (
      <div>
        {this.state.songsData && (
          <div className="row p-1">
            <div className="col-lg-4 col-12 text-left mt-2">
              {show === 1 && (
                <div className="row">
                  <div className="col-3 p-0 pl-4">
                    <img
                      src={playData.SImage}
                      style={{
                        borderRadius: "5px",
                        width: "65px",
                        height: "auto",
                      }}
                    />
                  </div>
                  <div
                    className="col-5 pl-3 mt-2"
                    style={{ fontSize: "10px", color: "#999" }}
                  >
                    <span style={{ fontSize: "10px", color: "#333" }}>
                      {playData.SName}
                      <br />
                      <span style={{ fontSize: "10px", color: "#999" }}>
                        {playData.AlbumName}
                      </span>
                    </span>
                  </div>
                  <div className="col-lg-2 col text-right mt-3 pr-5 pr-lg-0">
                    {fav === true ? (
                      <span
                        className=""
                        onClick={() => this.handleFav()}
                        style={{ cursor: "pointer", color: "#999" }}
                      >
                        <i className="fas fa-heart text-danger" />
                      </span>
                    ) : (
                      <span
                        className=""
                        onClick={() => this.handleFav()}
                        style={{ cursor: "pointer", color: "#999" }}
                      >
                        <i className="far fa-heart" />
                      </span>
                    )}
                  </div>
                  <div className="col-lg-1 d-none d-lg-block mt-3">
                    <span
                      className=""
                      onClick={() => this.handleShow(2)}
                      style={{ cursor: "pointer", color: "#999" }}
                    >
                      <i className="fas fa-ellipsis-h" />
                    </span>
                  </div>
                </div>
              )}
              {show === 2 && (
                <div className="row bg-white">
                  <div
                    className="col-lg-12 d-none d-lg-block bg-white pt-1"
                    style={{ zIndex: "1", position: "absolute", top: "-200%" }}
                  >
                    <div className="row">
                      <div className="col-3 p-0 pl-4">
                        <img
                          src={playData.SImage}
                          style={{
                            borderRadius: "5px",
                            width: "65px",
                            height: "auto",
                          }}
                        />
                      </div>
                      <div
                        className="col-5 pl-md-3 pl-1 mt-2"
                        style={{ fontSize: "10px", color: "#999" }}
                      >
                        <span style={{ fontSize: "10px", color: "#333" }}>
                          {playData.SName}
                          <br />
                          <span style={{ fontSize: "10px", color: "#999" }}>
                            {playData.AlbumName}
                          </span>
                        </span>
                      </div>
                      <div className="col-lg-2 col text-right mt-3 pr-5 pr-lg-0">
                        {fav === true ? (
                          <span
                            className=""
                            onClick={() => this.handleFav()}
                            style={{ cursor: "pointer", color: "#999" }}
                          >
                            <i className="fas fa-heart text-danger" />
                          </span>
                        ) : (
                          <span
                            className=""
                            onClick={() => this.handleFav()}
                            style={{ cursor: "pointer", color: "#999" }}
                          >
                            <i className="far fa-heart" />
                          </span>
                        )}
                      </div>
                      <div className="col-lg-1 d-none d-lg-block mt-3">
                        <span
                          className=""
                          onClick={() => this.handleShow(1)}
                          style={{ cursor: "pointer", color: "#999" }}
                        >
                          <i className="fas fa-chevron-down" />
                        </span>
                      </div>
                      <div className="col-12">
                        <hr />
                      </div>
                      <div className="col-12 p-0">
                        <PlaylistOption song={playData[0]} {...this.props} />
                      </div>
                    </div>
                  </div>
                  <div className="col-md-12 d-lg-none d-block">
                    <div className="row">
                      <div className="col-3 p-0 pl-4">
                        <img
                          src={playData.SImage}
                          style={{
                            borderRadius: "5px",
                            width: "65px",
                            height: "auto",
                          }}
                        />
                      </div>
                      <div
                        className="col-5 pl-3 mt-2"
                        style={{ fontSize: "10px", color: "#999" }}
                      >
                        <span style={{ fontSize: "10px", color: "#333" }}>
                          {playData.SName}
                          <br />
                          <span style={{ fontSize: "10px", color: "#999" }}>
                            {playData.AlbumName}
                          </span>
                        </span>
                      </div>
                      <div className="col-lg-2 col text-right mt-3 pr-5 pr-lg-0">
                        {fav === true ? (
                          <span
                            className=""
                            onClick={() => this.handleFav()}
                            style={{ cursor: "pointer", color: "#999" }}
                          >
                            <i className="fas fa-heart text-danger" />
                          </span>
                        ) : (
                          <span
                            className=""
                            onClick={() => this.handleFav()}
                            style={{ cursor: "pointer", color: "#999" }}
                          >
                            <i className="far fa-heart" />
                          </span>
                        )}
                      </div>
                      <div className="col-lg-1 d-none d-lg-block mt-3">
                        <span
                          className=""
                          onClick={() => this.handleShow(2)}
                          style={{ cursor: "pointer", color: "#999" }}
                        >
                          <i className="fas fa-ellipsis-h" />
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
            <div
              className="col-lg-8 col-12 mt-3 "
              style={{ borderLeft: "1px solid lightgrey" }}
            >
              <div className="row">
                <div className="col-md-7 col-12 row ml-1">
                  <AudioPlayer
                    autoPlayAfterSrcChange={true}
                    autoPlay={true}
                    src={audioFiles[indexPlay].SUrl}
                    onPlay={(e) => console.log("onPlay", e)}
                    showSkipControls={true}
                    // other props here
                    onEnded={this.handleClickNext}
                    onClickPrevious={this.handleClickPrevious}
                    onClickNext={this.handleClickNext}
                  />
                </div>
                <div
                  className="col-md-2 d-none d-md-block "
                  style={{ borderLeft: "1px solid lightgrey" }}
                >
                  <div className="mt-4">
                    <span
                      style={{
                        border: "1px solid lightgrey",
                        borderRadius: "50px",
                        paddingLeft: "10px",
                        paddingRight: "10px",
                        paddingTop: "4px",
                        paddingBottom: "5px",
                        cursor: "pointer",
                        color: "#999",
                      }}
                    >
                      High &nbsp;
                      <i className="fas fa-sort-up" />
                    </span>
                  </div>
                </div>
                <div
                  className="col-md d-none d-md-block text-left "
                  style={{ borderLeft: "1px solid lightgrey" }}
                >
                  <div className="mt-4">
                    <span
                      style={{
                        paddingLeft: "10px",
                        paddingRight: "10px",
                        paddingTop: "4px",
                        paddingBottom: "5px",
                        color: "#999",
                      }}
                    >
                      AUTOPLAY
                    </span>
                    <span
                      style={{
                        border: "1px solid lightgrey",
                        borderRadius: "50px",
                        paddingLeft: "10px",
                        paddingRight: "10px",
                        paddingTop: "4px",
                        paddingBottom: "5px",
                        cursor: "pointer",
                        marginLeft: "10px",
                        color: "#999",
                      }}
                    >
                      on
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default MediaPlayer;
