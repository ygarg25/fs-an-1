import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import "./navbar.css";

let url = "http://localhost:2410";

class Navbar extends Component {
  state = {
    opt: "Home",
  };

  async componentWillMount() {
    const jwt = localStorage.getItem("token");
    try {
      const { data: response } = await axios.get(url + "/user", {
        headers: {
          Authorization: jwt,
        },
      });
      this.setState({ user: response });
    } catch (ex) {
      console.log("catch");
      return null;
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    const jwt = localStorage.getItem("token");
    if (prevProps !== currProps) {
      try {
        const { data: response } = await axios.get(url + "/user", {
          headers: {
            Authorization: jwt,
          },
        });
        this.setState({ user: response });
      } catch (ex) {
        console.log("catch");
        return null;
      }
    }
  }

  handleOpt = (o) => {
    this.setState({ opt: o });
  };

  render() {
    const { opt } = this.state;
    return (
      <div className="sticky-top">
        <nav className="navbar navbar-expand-lg navbar-light bg-white text-white">
          <Link className="navbar-brand text-danger" to="/">
            <svg
              width="83"
              height="33"
              viewBox="0 0 83 33"
              style={{ fill: "#E90909" }}
            >
              <g
                className="fill_path orange"
                fillRule="evenodd"
                transform="translate(-84 -28)"
              >
                <path d="M142.715 47.83c.182-1.009.346-1.955.518-2.901.572-3.215 1.144-6.429 1.698-9.643.163-.929-.418-1.599-1.38-1.643-.618-.027-1.244-.018-1.862-.018-1.289.009-2.142.759-2.36 2.009-.69 3.946-1.399 7.893-2.107 11.83-.027.134-.19.349-.29.349-1.771-.027-3.532-.045-5.294-.161-1.144-.072-2.061-.643-2.706-1.59-.036-.044-.082-.08-.145-.151-.245.259-.472.509-.717.74-.808.795-1.744 1.269-2.915 1.18-.826-.063-1.67 0-2.479-.144-1.444-.258-2.624-.964-3.332-2.366-.11.483-.227.956-.336 1.438-.245 1.152-.236 1.143-1.435 1.09-.853-.036-1.707-.153-2.442-.59-.563-.34-1.044-.821-1.607-1.268-.164.17-.39.42-.627.652-.808.786-1.743 1.232-2.915 1.187-1.116-.044-2.233.027-3.314-.366-1.925-.705-2.969-2.357-2.633-4.357.436-2.634.817-5.286 1.417-7.884.608-2.607 2.197-4.428 5.003-4.99.4-.081.808-.126 1.216-.126 2.316-.009 4.631-.009 6.946-.009.118 0 .227.018.382.036-.227 1.268-.454 2.509-.672 3.75-.518 2.866-1.044 5.723-1.544 8.59-.045.267.064.597.2.839.345.616 1.417 1.044 2.125.928.263-3.045.871-6.027 1.516-8.991.572-2.625 2.197-4.464 5.021-5.027.4-.08.808-.125 1.217-.125 2.315-.009 4.63-.009 6.946-.009.1 0 .209.018.363.027-.136.777-.263 1.536-.4 2.286-.617 3.402-1.243 6.803-1.86 10.205-.091.482.081.884.48 1.179.155.116.328.232.51.277.371.098.79.267 1.143.196.49-.098.309-.634.372-.973.709-3.84 1.39-7.679 2.07-11.527.1-.536.2-1.08.31-1.643.145-.009.272-.027.408-.027 2.252 0 4.513-.009 6.765 0a5.97 5.97 0 0 1 2.542.572c1.353.66 2.143 1.74 2.08 3.232-.046 1.25-.273 2.5-.482 3.74-.545 3.144-1.117 6.287-1.68 9.42-.027.18-.045.358-.1.527-.036.09-.154.206-.236.206-1.126.053-2.224.044-3.378.044zm-27.902-14.17c-.136-.026-.182-.035-.218-.035-1.144 0-2.297-.009-3.441 0-1.144.009-2.007.696-2.216 1.795a382.321 382.321 0 0 0-1.27 7.044c-.2 1.188.399 1.857 1.652 1.911.499.018.998.009 1.498 0 1.37-.009 2.188-.705 2.433-2.036.372-2.071.763-4.143 1.144-6.205a93.62 93.62 0 0 1 .418-2.473zm15.172.01c-.127-.027-.172-.045-.208-.045-1.154 0-2.298-.018-3.45 0-1.163.018-2.026.705-2.234 1.821a637.194 637.194 0 0 0-1.272 7.045c-.181 1.063.291 1.697 1.38 1.83.6.072 1.208.045 1.807.045 1.317-.009 2.152-.705 2.38-1.982l1.062-5.732c.181-.973.354-1.964.535-2.982zM98.605 47.84c-.317 0-.572.008-.826 0-1.253-.027-2.515.026-3.759-.108-1.88-.214-3.187-1.375-3.56-2.973-.208-.875-.09-1.741.064-2.607.418-2.304.817-4.616 1.262-6.92.536-2.786 2.724-4.786 5.62-5.009 1.699-.134 3.415-.08 5.122-.098.908-.009 1.807 0 2.787 0-.118.705-.227 1.375-.345 2.036-.653 3.651-1.316 7.303-1.98 10.955-.417 2.295-.798 4.59-1.27 6.875-.636 3.116-3.051 4.554-5.53 4.902-.6.08-1.208.09-1.807.09-2.088.008-4.177 0-6.265 0-.027 0-.054-.01-.118-.028.01-.053 0-.107.027-.151.581-1.027 1.153-2.054 1.762-3.063.09-.143.363-.241.554-.241 1.834-.018 3.659-.009 5.493-.009 1.398 0 2.116-.562 2.415-1.91.145-.563.227-1.099.354-1.742zm-1.38-3.519v.054c.472 0 .944-.036 1.408.009.463.045.626-.143.699-.571.481-2.804.98-5.608 1.48-8.411.19-1.054-.354-1.741-1.453-1.777-.608-.018-1.208-.009-1.816-.009-1.217.018-2.043.705-2.26 1.893-.237 1.286-.482 2.562-.71 3.848-.199 1.152-.426 2.304-.562 3.464-.1.83.363 1.375 1.226 1.474.653.08 1.325.026 1.988.026zM162.918 30.134c-.318 1.777-.635 3.491-.944 5.214a980.315 980.315 0 0 0-1.29 7.134c-.172.956.237 1.554 1.172 1.857.118.036.254.26.236.375-.163 1.027-.354 2.045-.545 3.125-1.607-.098-3.005-.58-4.004-1.937-.263.277-.49.535-.735.777-.8.767-1.717 1.232-2.879 1.17-1.008-.054-2.016.044-3.005-.26-2.225-.678-3.342-2.402-2.951-4.652.445-2.589.826-5.187 1.416-7.75.563-2.41 2.016-4.17 4.558-4.839a6.972 6.972 0 0 1 1.607-.223c2.361-.027 4.722-.009 7.074-.009.072 0 .154.009.29.018zm-4.176 3.527c-.182-.018-.291-.036-.41-.036-1.089 0-2.178-.009-3.268 0-1.126.018-1.988.679-2.188 1.759-.454 2.42-.88 4.84-1.299 7.268-.154.91.318 1.536 1.263 1.652.608.08 1.234.062 1.852.062 1.416 0 2.234-.714 2.479-2.09.4-2.25.826-4.49 1.235-6.731.118-.616.217-1.232.336-1.884z"></path>{" "}
              </g>
            </svg>
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div
            className="collapse navbar-collapse"
            id="navbarSupportedContent"
            style={{ fontSize: "13px" }}
          >
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link
                  className="nav-link"
                  to="/"
                  onClick={() => this.handleOpt("Home")}
                >
                  {opt === "Home" ? (
                    <span className="text-danger">HOME</span>
                  ) : (
                    <span className="listItem">HOME</span>
                  )}
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link " to="#">
                  <span className="listItem">BROWSE</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="#">
                  <span className="listItem"> DISCOVER</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="#">
                  <span className="listItem"> RADIO</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className="nav-link"
                  to="/music"
                  onClick={() => this.handleOpt("My Music")}
                >
                  {opt === "My Music" ? (
                    <span className="text-danger">My Music</span>
                  ) : (
                    <span className="listItem">My Music</span>
                  )}
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="#">
                  <span className="listItem"> PODCASTS</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="#">
                  <span className="listItem"> INDIA'S MUSIC</span>
                </Link>
              </li>
            </ul>
            <ul className="navbar-nav ml-auto">
              <li className="nav-item pt-2">
                <button
                  className="btn btn-outline-danger btn-sm"
                  style={{ borderRadius: "50px" }}
                >
                  GET GAANA PLUS
                </button>
              </li>
              <li className="nav-item">
                <span className="nav-link">
                  <span style={{ fontSize: "16px", color: "#999" }}>
                    Download App
                  </span>
                  <Link to="#" className="ml-3">
                    <span style={{ fontSize: "21px", color: "#333" }}>
                      <i className="fab fa-apple" />
                    </span>
                  </Link>
                  <Link to="#" className="ml-3">
                    <span style={{ fontSize: "21px", color: "#333" }}>
                      <i className="fab fa-android" />
                    </span>
                  </Link>
                </span>
              </li>
              {!this.state.user && (
                <li className="nav-item text-danger">
                  <Link
                    className="nav-link text-danger"
                    to="/"
                    onClick={() => this.props.handleSignIn(1, 1)}
                  >
                    <span
                      className="listItem text-danger"
                      style={{ fontWeight: "600", fontSize: "16px" }}
                    >
                      SIGN IN
                    </span>
                  </Link>
                </li>
              )}
              {this.state.user && (
                <li className="nav-item dropdown">
                  <Link
                    className="nav-link dropdown-toggle"
                    to="#"
                    id="navbarDropdown"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <span style={{ fontSize: "20px" }}>
                      <span
                        className="fa-stack fa-x"
                        style={{ cursor: "pointer" }}
                      >
                        <i className="fa fa-circle fa-stack-2x"></i>
                        <i className="far fa-user  fa-stack-1x fa-lg fa-inverse "></i>
                      </span>
                    </span>
                  </Link>
                  <div
                    className="dropdown-menu"
                    aria-labelledby="navbarDropdown"
                    style={{
                      left: "-175%",
                    }}
                  >
                    <Link
                      className="dropdown-item"
                      to="/myzone"
                      onClick={() => this.handleOpt("Profile")}
                      style={{ color: "#999" }}
                    >
                      <i className="far fa-user" />
                      &nbsp;&nbsp;&nbsp; Profile
                    </Link>
                    <Link
                      className="dropdown-item"
                      to="#"
                      style={{ color: "#999" }}
                    >
                      <i className="fas fa-language" />
                      &nbsp;&nbsp; Languages
                    </Link>
                    <Link
                      className="dropdown-item"
                      to="#"
                      style={{ color: "#999" }}
                    >
                      <i className="fas fa-cog" />
                      &nbsp;&nbsp;&nbsp; Settings
                    </Link>
                    <Link
                      className="dropdown-item"
                      to="/signout"
                      style={{ color: "#999" }}
                    >
                      <i className="fas fa-power-off" />{" "}
                      &nbsp;&nbsp;&nbsp;Logout
                    </Link>
                    <div className="dropdown-divider"></div>
                    <Link
                      className="dropdown-item"
                      to="#"
                      style={{ color: "#999" }}
                    >
                      <i className="fas fa-moon" />
                      &nbsp;&nbsp;&nbsp;Black Theme
                    </Link>
                  </div>
                </li>
              )}
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default Navbar;
