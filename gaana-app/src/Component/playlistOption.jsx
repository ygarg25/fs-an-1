import React, { Component } from "react";
import "./playlistOption.css";
import jwtDecode from "jwt-decode";
import axios from "axios";
import { Redirect } from "react-router-dom";

let url = "http://localhost:2410";
// let url = "https://us-central1-yat12-77281.cloudfunctions.net/app";

const jwt = localStorage.getItem("token");
let userid;
if (jwt) {
  let token = jwt.split(" ")[1];

  userid = jwtDecode(token).id;
}

class PlaylistOption extends Component {
  state = {};

  handleAddPlayList = async (s) => {
    // const jwt = localStorage.getItem("token");
    // let userid;
    // if (jwt) {
    //   let token = jwt.split(" ")[1];

    //   userid = jwtDecode(token).id;
    // }
    // s.userid = userid;
    console.log("playlist", s, jwt);
    if (jwt) {
      const { data: response } = await axios.post(
        url + "/playlist/" + userid,
        s
      );
      console.log("post playlist song", response);
      if (response.error == 1062) {
        //   alert("Username already exist, try adding another Username");
      } else {
        alert("Song Added in playlist...");
      }
    } else {
      console.log("dd");
      this.props.handleSignIn(1, 1);
      this.props.history.push("/");
    }
  };

  render() {
    return (
      <div className="bg-white" style={{ width: "100%", left: "-275%" }}>
        <ul className="mainList">
          <li className="playlist">
            <i className="fas fa-share-alt" /> &nbsp;Share
          </li>
          <li
            className="playlist"
            onClick={() => this.handleAddPlayList(this.props.song)}
          >
            <i className="fas fa-list" /> &nbsp; Add to Playlist
          </li>
          <li className="playlist">
            {" "}
            <i className="far fa-plus-square" /> &nbsp;Add to Queue
          </li>
          <li className="playlist">
            <i className="fas fa-download" /> &nbsp;Download
          </li>
          <li className="playlist">
            <i className="fas fa-exclamation-circle" /> &nbsp;Get Playlist Info
          </li>
        </ul>
      </div>
    );
  }
}

export default PlaylistOption;
