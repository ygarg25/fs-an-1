import React from "react";
import { Redirect, Route } from "react-router-dom";

const ProtectedRoute = ({
  path,
  component: Component,
  render,
  handleSignIn,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        console.log(props);
        if (!localStorage.getItem("token")) {
          handleSignIn(1, 1);
          return (
            <Redirect to={{ pathname: "/", state: { from: props.location } }} />
          );
        }
        return Component ? <Component {...props} /> : render(props);
      }}
    />
  );
};

export default ProtectedRoute;
