import React, { Component } from "react";
import axios from "axios";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import Navbar from "./navbar";
import { Link, Redirect } from "react-router-dom";

let url = "http://localhost:2410";
// let url = "https://us-central1-yat12-77281.cloudfunctions.net/app";

class Regitser extends Component {
  state = {};

  handleSubmit = async (fields) => {
    console.log("submitted : ", fields);
    try {
      let apiEndPoint = url + "/register";
      const { data: response } = await axios.post(apiEndPoint, fields);
      if (response.error == 1062) {
        alert("Username already exist, try adding another Username");
      } else {
        alert("Successfully Added... ");
        this.props.handleSignIn(1, 1);
        window.location = "/";
      }
    } catch (err) {
      console.log(err);
      alert("Registeration Unsuccessfull! Please try again....");
    }
  };
  render() {
    if (localStorage.getItem("token")) return <Redirect to="/" />;
    return (
      <div>
        <Formik
          initialValues={{
            firstName: "",
            lastName: "",
            email: "",
            password: "",
          }}
          validationSchema={Yup.object().shape({
            firstName: Yup.string().required("First Name is required"),
            lastName: Yup.string().required("Last Name is required"),
            email: Yup.string()
              .email("Email is invalid")
              .required("Email is required"),
            password: Yup.string()
              .min(6, "Password must be at least 6 characters")
              .required("Password is required"),
          })}
          onSubmit={(fields) => {
            this.handleSubmit(fields);
          }}
          render={({ errors, status, touched }) => {
            return (
              <div style={{ backgroundColor: "#F4F6F7" }}>
                <div
                  className="row m-0  "
                  style={{ backgroundColor: "#F4F6F7", margin: "" }}
                >
                  {/* <div className="col-4 d-none d-lg-block"></div> */}
                  <div
                    className="col-lg-12 col-12 bg-white "
                    style={{ border: "3px", borderRadius: "7px" }}
                  >
                    <div className="row pl-3 pr-lg-0 pr-3">
                      {/* <div className="col-lg-12 col-12 text-center"> */}
                      <div className="col-12 text-left">
                        <Link
                          to="/"
                          onClick={() => this.props.handleSignIn(1, 1)}
                        >
                          <i
                            className="fas fa-arrow-left text-dark"
                            style={{ cursor: "pointer" }}
                          />
                        </Link>
                      </div>
                      <div className="col-12 text-center">
                        <h5>INDIA'S NO.1 MUSIC APP</h5>
                      </div>
                      <div
                        className="col-12 text-center mt-1 text-muted"
                        style={{ fontWeight: "500", fontSize: "13px" }}
                      >
                        <p>
                          Over 30 million songs to suit every mood and ocassion
                        </p>
                      </div>
                      <div className="col-12  mt-2">
                        <div className="row text-center ">
                          <div className="col-4">
                            <span
                              style={{
                                cursor: "pointer",
                                border: "2px solid red",
                                borderRadius: "100%",
                                padding: "8px",
                                color: "red",
                                fontSize: "18px",
                              }}
                            >
                              <i className="fas fa-headphones-alt"></i>
                            </span>
                            <div
                              className="m-2 text-muted"
                              style={{ fontSize: "10px" }}
                            >
                              Create your
                              <br /> own playlists
                            </div>
                          </div>
                          <div className="col-4">
                            <span
                              style={{
                                cursor: "pointer",
                                border: "2px solid red",
                                borderRadius: "100%",
                                padding: "8px",
                                color: "red",
                                fontSize: "18px",
                              }}
                            >
                              <i className="fas fa-share-alt"></i>
                            </span>
                            <div
                              className="m-2 text-muted"
                              style={{ fontSize: "10px" }}
                            >
                              Share music with
                              <br /> family and friends
                            </div>
                          </div>
                          <div className="col-4">
                            <span
                              style={{
                                cursor: "pointer",
                                border: "2px solid red",
                                borderRadius: "100%",
                                padding: "8px",
                                color: "red",
                                fontSize: "18px",
                              }}
                            >
                              <i className="far fa-heart"></i>
                            </span>
                            <div
                              className="m-2 text-muted"
                              style={{ fontSize: "10px" }}
                            >
                              Save your
                              <br /> favourites{" "}
                            </div>
                          </div>
                        </div>
                      </div>
                      {/* <div className="row  mt-1"> */}
                      <div className="col-12">
                        <Form>
                          <div className="form-group text-left mb-1">
                            <label htmlFor="email">EMAIL ID</label>
                            <Field
                              name="email"
                              type="text"
                              placeholder="Enter Email"
                              className={
                                "form-control" +
                                (errors.email && touched.email
                                  ? " is-invalid"
                                  : "")
                              }
                            />
                            <ErrorMessage
                              name="email"
                              component="div"
                              className="invalid-feedback"
                            />
                          </div>
                          <div className="form-row mb-0">
                            <div className="form-group col text-left mb-1">
                              <label htmlFor="password">CREATE PASSWORD</label>
                              <Field
                                name="password"
                                type="password"
                                placeholder="Enter your Password"
                                className={
                                  "form-control" +
                                  (errors.password && touched.password
                                    ? " is-invalid"
                                    : "")
                                }
                              />
                              <ErrorMessage
                                name="password"
                                component="div"
                                className="invalid-feedback"
                              />
                            </div>
                          </div>
                          <div className="form-row">
                            <div className="form-group col-12 text-left pb-0 mb-0">
                              <label>FULL NAME</label>
                            </div>
                            <div className="form-group col-6 mt-0">
                              <Field
                                name="firstName"
                                placeholder="First Name"
                                type="text"
                                className={
                                  "form-control" +
                                  (errors.firstName && touched.firstName
                                    ? " is-invalid"
                                    : "")
                                }
                              />
                              <ErrorMessage
                                name="firstName"
                                component="div"
                                className="invalid-feedback"
                              />
                            </div>
                            <div className="form-group col-6 mt-0">
                              <Field
                                name="lastName"
                                placeholder="Last Name"
                                type="text"
                                className={
                                  "form-control" +
                                  (errors.lastName && touched.lastName
                                    ? " is-invalid"
                                    : "")
                                }
                              />
                              <ErrorMessage
                                name="lastName"
                                component="div"
                                className="invalid-feedback"
                              />
                            </div>
                          </div>

                          <div className="form-group col-12">
                            <button
                              type="submit"
                              className="btn btn-danger mr-2 col-12"
                            >
                              Register
                            </button>
                          </div>
                        </Form>
                      </div>
                      {/* </div> */}
                      {/* </div> */}
                    </div>
                  </div>
                  {/* <div className="col-4 d-none d-lg-block"></div> */}
                </div>
              </div>
            );
          }}
        />

        {/* </Formik> */}
      </div>
    );
  }
}

export default Regitser;
