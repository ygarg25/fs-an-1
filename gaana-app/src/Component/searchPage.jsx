import React, { Component } from "react";
import { Link } from "react-router-dom";
import Footer from "./footer";
import axios from "axios";
import "./thumbnail.css";

let url = "http://localhost:2410";

class SearchPage extends Component {
  state = {};

  //WARNING! To be deprecated in React v17. Use componentDidMount instead.
  async componentWillMount() {
    let name = this.props.match.params.name;
    try {
      const { data: searchResult } = await axios.get(
        url + "/search" + "/" + name
      );
      this.setState({ searchResult: searchResult });
    } catch (ex) {
      console.log("catch");
      return null;
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    let name = this.props.match.params.name;
    if (prevProps !== currProps) {
      try {
        const { data: searchResult } = await axios.get(
          url + "/search" + "/" + name
        );
        this.setState({ searchResult: searchResult });
      } catch (ex) {
        console.log("catch");
        return null;
      }
    }
  }

  render() {
    const { searchResult: sr } = this.state;
    console.log("search Result", sr);
    let name = this.props.match.params.name;
    return (
      <div style={{ backgroundColor: "lightgrey" }}>
        <div className=" row">
          <div className="col-lg-9 col-12" style={{ background: "#EBECED" }}>
            <div className="row pl-5 pt-5 pr-5">
              <div className="col-lg-1 d-none d-lg-block"></div>
              <div className="col-lg-11 col-12 ">
                <div className="row ">
                  <div className="col-12">
                    <span>
                      <Link to="/">
                        <svg
                          itemProp="name"
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                        >
                          <g fill="none" fillRule="evenodd">
                            <rect
                              width="24"
                              height="24"
                              className="fill_path"
                              rx="2"
                            ></rect>
                            <path
                              className="fill_path blackbg"
                              fill="#333"
                              d="M12.872 14.976c-.176 0-.317.005-.458 0-.694-.015-1.392.015-2.081-.06-1.04-.12-1.765-.774-1.97-1.674-.116-.493-.05-.98.035-1.468.23-1.297.452-2.6.698-3.896.297-1.569 1.508-2.695 3.112-2.82.94-.076 1.89-.046 2.835-.056.503-.005 1 0 1.544 0-.066.397-.126.774-.191 1.146-.362 2.057-.73 4.113-1.096 6.169-.231 1.292-.443 2.584-.704 3.87-.352 1.755-1.69 2.564-3.062 2.76a7.41 7.41 0 0 1-1 .05c-1.156.006-2.312 0-3.469 0-.015 0-.03-.004-.065-.014.005-.03 0-.06.015-.086.322-.578.639-1.156.975-1.724.05-.08.201-.136.307-.136 1.015-.01 2.026-.005 3.041-.005.775 0 1.172-.317 1.338-1.076.08-.316.125-.618.196-.98zm-.764-1.98v.03c.261 0 .522-.02.779.005.256.025.347-.08.387-.322.266-1.579.543-3.157.82-4.736.105-.593-.197-.98-.805-1-.337-.01-.669-.005-1.005-.005-.674.01-1.132.397-1.252 1.066-.13.724-.267 1.442-.392 2.166-.111.649-.237 1.297-.312 1.95-.055.468.201.775.679.83.362.046.734.015 1.1.015z"
                            ></path>
                          </g>
                        </svg>
                      </Link>
                    </span>
                    <Link to="/">
                      <span
                        style={{
                          textDecoration: "underline",
                          color: "black",
                          fontWeight: "550",
                        }}
                      >
                        Gaana
                      </span>
                    </Link>
                    <span
                      className="ml-1"
                      style={{
                        fontSize: "50px",
                        fontWeight: "550",
                        verticalAlign: "center",
                      }}
                    >
                      .
                    </span>
                    <span
                      className="ml-1"
                      style={{
                        color: "#999",
                        fontWeight: "550",
                        fontSize: "12px",
                      }}
                    >
                      Search/ Trending
                    </span>
                  </div>
                  <div className="col-12">
                    <hr />
                  </div>
                  <div className="col-md-12 d-none d-md-block">
                    <h1
                      className="top-heading desktop"
                      style={{ fontSize: "22px", color: "#999" }}
                    >
                      Search results for{" "}
                      <span className="text-dark">{name}</span>
                    </h1>
                    <br />
                    <h2
                      className="top-heading desktop"
                      style={{ fontSize: "19px" }}
                    >
                      Top Results
                    </h2>
                  </div>
                  {this.state.searchResult &&
                    sr.map((t, i) => (
                      <div key={i} className="col-md-2 col-5  p-0 m-2">
                        <Link to={`/album/${t.SName}`}>
                          <img
                            className="imageParent"
                            src={t.SImage}
                            style={{
                              width: "100%",
                              height: "150px",
                              cursor: "pointer",
                            }}
                          />
                          <span className="imagePlay">
                            <i className="fas fa-play-circle"></i>
                          </span>
                        </Link>
                        <Link
                          to={`/album/${t.SName}`}
                          style={{ textDecoration: "none" }}
                        >
                          <span
                            className="text-left"
                            style={{
                              color: "#333",
                              fontSize: "12px",
                              fontFamily: "Arial",
                            }}
                          >
                            {t.SName}
                          </span>
                        </Link>
                      </div>
                    ))}
                  <br />
                </div>
                <Footer />
              </div>
            </div>
          </div>
          <div className="col-lg-3 d-none d-lg-block"></div>
        </div>
      </div>
    );
  }
}

export default SearchPage;
