import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Footer from "./footer";
import PlaylistOption from "./playlistOption";
import "./list.css";

let url = "http://localhost:2410";

class TopChartPlaylist extends Component {
  state = {
    topChart: [
      {
        url: "https://i.ibb.co/PxRkk4d/ganna-Original.jpg",
        value: "Ganna Original",
        desc: "Gaana Originals Top 2 ",
      },
      {
        url: "https://i.ibb.co/Gxj1w30/hindi.jpg",
        value: "Hindi",
        desc: "Hindi Top 2",
      },
      {
        url: "https://i.ibb.co/qCmV4G7/indiePop.jpg",
        value: "Indiepop",
        desc: "IndiePop Top 2",
      },
      {
        url: "https://i.ibb.co/fN9KcmX/latest-Dance.jpg",
        value: "Latest Dance",
        desc: "Latest Dance Top 2",
      },
      {
        url: "https://i.ibb.co/ydMR4Dz/latestlove.jpg",
        value: "Latest Love",
        desc: "Latest Love Top 2",
      },
      {
        url: "https://i.ibb.co/dsT8j7N/punjabi.jpg",
        value: "Punjabi",
        desc: "Punjabi Top 2",
      },
      {
        url: " https://i.ibb.co/Pw6TZ0L/romance-Hindi.jpg",
        value: "Romance Hindi",
        desc: "Romance Hindi Top 2",
      },
      {
        url: "https://i.ibb.co/9WNGkkw/today.jpg",
        value: "Today",
        desc: "Today's Top 2",
      },
    ],
    show: -1,
    index: -1,
  };
  //WARNING! To be deprecated in React v17. Use componentDidMount instead.
  async componentWillMount() {
    let category = this.props.match.params.category;
    try {
      const { data: topChartPlaylist } = await axios.get(
        url + "/topChartPlaylist" + "/" + category
      );
      this.setState({ topChartPlaylist: topChartPlaylist });
    } catch (ex) {
      console.log("catch");
      return null;
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    let category = this.props.match.params.category;
    if (prevProps !== currProps) {
      try {
        const { data: topChartPlaylist } = await axios.get(
          url + "/topChartPlaylist" + "/" + category
        );
        this.setState({ topChartPlaylist: topChartPlaylist });
      } catch (ex) {
        console.log("catch");
        return null;
      }
    }
  }

  handlePlayOption = (show, index) => {
    this.setState({ show: show, index: index });
  };

  handleMouseOut = (show, index) => {
    this.setState({ show: show, index: index });
  };

  handleFav = (index) => {
    let data = [...this.state.topChartPlaylist];
    data[index].fav = !this.state.topChartPlaylist[index].fav;
    this.setState({ topChartPlaylist: data });
  };

  render() {
    let category = this.props.match.params.category;
    const { topChart: tc, topChartPlaylist: tcp, show, index } = this.state;
    let headerData = tc.filter((c) => c.value === category)[0];
    return (
      <div style={{ backgroundColor: "lightgrey" }}>
        <div className=" row">
          <div className="col-lg-9 col-12" style={{ background: "#EBECED" }}>
            <div className="row pl-3 pt-5 pr-3">
              <div className="col-lg-1 d-none d-lg-block"></div>
              <div className="col-lg-11 col-12 ">
                <div className="row ">
                  <div className="col-12">
                    <span>
                      <Link to="/">
                        <svg
                          itemProp="name"
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                        >
                          <g fill="none" fillRule="evenodd">
                            <rect
                              width="24"
                              height="24"
                              className="fill_path"
                              rx="2"
                            ></rect>
                            <path
                              className="fill_path blackbg"
                              fill="#333"
                              d="M12.872 14.976c-.176 0-.317.005-.458 0-.694-.015-1.392.015-2.081-.06-1.04-.12-1.765-.774-1.97-1.674-.116-.493-.05-.98.035-1.468.23-1.297.452-2.6.698-3.896.297-1.569 1.508-2.695 3.112-2.82.94-.076 1.89-.046 2.835-.056.503-.005 1 0 1.544 0-.066.397-.126.774-.191 1.146-.362 2.057-.73 4.113-1.096 6.169-.231 1.292-.443 2.584-.704 3.87-.352 1.755-1.69 2.564-3.062 2.76a7.41 7.41 0 0 1-1 .05c-1.156.006-2.312 0-3.469 0-.015 0-.03-.004-.065-.014.005-.03 0-.06.015-.086.322-.578.639-1.156.975-1.724.05-.08.201-.136.307-.136 1.015-.01 2.026-.005 3.041-.005.775 0 1.172-.317 1.338-1.076.08-.316.125-.618.196-.98zm-.764-1.98v.03c.261 0 .522-.02.779.005.256.025.347-.08.387-.322.266-1.579.543-3.157.82-4.736.105-.593-.197-.98-.805-1-.337-.01-.669-.005-1.005-.005-.674.01-1.132.397-1.252 1.066-.13.724-.267 1.442-.392 2.166-.111.649-.237 1.297-.312 1.95-.055.468.201.775.679.83.362.046.734.015 1.1.015z"
                            ></path>
                          </g>
                        </svg>
                      </Link>
                    </span>
                    <Link to="/">
                      <span
                        style={{
                          textDecoration: "underline",
                          color: "black",
                          fontWeight: "550",
                        }}
                      >
                        Gaana
                      </span>
                    </Link>
                    <span
                      className="ml-1"
                      style={{
                        fontSize: "50px",
                        fontWeight: "550",
                        verticalAlign: "center",
                      }}
                    >
                      .
                    </span>
                    <Link to="/topCharts">
                      <span
                        style={{
                          textDecoration: "underline",
                          color: "black",
                          fontWeight: "550",
                        }}
                      >
                        Playlists
                      </span>
                    </Link>
                    <span
                      className="ml-1"
                      style={{
                        fontSize: "50px",
                        fontWeight: "550",
                        verticalAlign: "center",
                      }}
                    >
                      .
                    </span>
                    <span
                      className="ml-1"
                      style={{
                        color: "#999",
                        fontWeight: "550",
                        fontSize: "12px",
                      }}
                    >
                      {category} Top 2
                    </span>
                  </div>
                  <div className="col-12">
                    <hr />
                  </div>
                  <div className="col-12">
                    <div className="row">
                      <div className="col-3">
                        <img
                          src={headerData.url}
                          style={{
                            borderRadius: "5%",
                            width: "100%",
                            height: "100%",
                          }}
                        />
                      </div>
                      <div className="col-5 p-0">
                        <h5 style={{ fontSize: "16px" }}>{headerData.desc}</h5>
                        <span
                          className="text-muted mb-lg-5"
                          style={{ fontSize: "10px" }}
                        >
                          Created By Gaana | Tracks 2
                        </span>
                        <br />
                        <br />
                        {this.state.topChartPlaylist && (
                          <button
                            onClick={() =>
                              this.props.handleSongPlay([tcp[0].Id])
                            }
                            className="btn btn-danger pl-4 pr-4 btn-sm mt-lg-5"
                            style={{ borderRadius: "70px" }}
                          >
                            <i className="fas fa-play text-white" />
                            &nbsp; Play All
                          </button>
                        )}
                      </div>
                      <div className="col text-right">
                        <span
                          className="mr-2"
                          style={{ cursor: "pointer", color: "#999" }}
                        >
                          <i className="far fa-heart" />
                        </span>
                        <span
                          className="ml-2"
                          style={{ cursor: "pointer", color: "#999" }}
                        >
                          <i className="fas fa-share-alt" />
                        </span>
                      </div>
                    </div>
                    <br />
                  </div>
                  <div className="col-lg-12 d-none d-lg-block">
                    <div className=" row border-bottom text-secondary p-1">
                      <div className="col-1">#</div>
                      <div className="col-1"></div>
                      <div className="col-4">TITLE</div>
                      <div className="col-3">ARTIST</div>
                      <div className="col-2">
                        <i className="far fa-clock" />
                      </div>
                      <div className="col-1"></div>
                    </div>
                    {this.state.topChartPlaylist &&
                      tcp.map((t, i) => (
                        <div
                          className="row border-bottom text-secondary p-2 child child1 parent"
                          key={i}
                          onMouseLeave={() => this.handleMouseOut(-1, -1)}
                        >
                          <div
                            className="col-1"
                            style={{ cursor: "pointer" }}
                            onClick={() => this.props.handleSongPlay([t.Id])}
                          >
                            <span className="index1 pt-2">{i + 1}</span>
                            <span className="playicon">
                              <i className="far fa-play-circle"></i>
                            </span>
                          </div>
                          <div
                            className="col-1 pt-2"
                            style={{ cursor: "pointer" }}
                          >
                            {t.fav === true ? (
                              <i
                                className="fas fa-heart text-danger"
                                onClick={() => this.handleFav(i)}
                              />
                            ) : (
                              <i
                                className="far fa-heart"
                                onClick={() => this.handleFav(i)}
                              />
                            )}
                          </div>
                          <div
                            className="col-4 "
                            style={{ cursor: "pointer" }}
                            onClick={() => this.props.handleSongPlay([t.Id])}
                          >
                            <span>
                              <img
                                src={t.SImage}
                                style={{ width: "40px", borderRadius: "5px" }}
                              />
                            </span>
                            &nbsp;&nbsp;
                            <span>{t.SName}</span>
                          </div>
                          <div
                            className="col-3 pt-2"
                            style={{ cursor: "pointer" }}
                            onClick={() => this.props.handleSongPlay([t.Id])}
                          >
                            {t.Artist}
                          </div>
                          <div
                            className="col-2 pt-2"
                            style={{ cursor: "pointer" }}
                            onClick={() => this.props.handleSongPlay([t.Id])}
                          >
                            {t.Duration}
                          </div>

                          <div className="col-1  ellipse pt-2">
                            {show === 1 && index === i ? (
                              <div>
                                <i
                                  className="fas fa-ellipsis-h"
                                  onClick={() => this.handlePlayOption(-1, -1)}
                                  style={{ cursor: "pointer" }}
                                />
                                <div
                                  style={{
                                    position: "absolute",
                                    left: "-250%",
                                    zIndex: "1",
                                    top: "-300%",
                                  }}
                                >
                                  {" "}
                                  <PlaylistOption song={t} {...this.props} />
                                </div>
                              </div>
                            ) : (
                              <div>
                                <i
                                  className="fas fa-ellipsis-h"
                                  onClick={() => this.handlePlayOption(1, i)}
                                  style={{ cursor: "pointer" }}
                                />
                              </div>
                            )}
                          </div>
                        </div>
                      ))}
                  </div>
                  <div className="col-12 d-lg-none d-block">
                    {this.state.topChartPlaylist &&
                      tcp.map((t, i) => (
                        <div
                          className="row border-bottom text-secondary p-2 smallParent"
                          key={i}
                          style={{ cursor: "pointer" }}
                        >
                          <div
                            className="col-2"
                            onClick={() => this.props.handleSongPlay([t.Id])}
                          >
                            <span>
                              <img
                                src={t.SImage}
                                style={{ width: "40px", borderRadius: "5px" }}
                              />
                            </span>
                          </div>
                          <div
                            className="col-8"
                            style={{ fontSize: "13px" }}
                            onClick={() => this.props.handleSongPlay([t.Id])}
                          >
                            <div>{t.SName}</div>
                            <div className="text-primary">{t.Artist}</div>
                          </div>
                          <div className="col-1 text-right">
                            {t.fav === true ? (
                              <i
                                className="fas fa-heart text-danger"
                                onClick={() => this.handleFav(i)}
                              />
                            ) : (
                              <i
                                className="far fa-heart"
                                onClick={() => this.handleFav(i)}
                              />
                            )}
                          </div>
                        </div>
                      ))}
                  </div>

                  <br />
                </div>
                <Footer />
              </div>
            </div>
          </div>
          <div className="col-lg-3 d-none d-lg-block"></div>
        </div>
      </div>
    );
  }
}

export default TopChartPlaylist;
