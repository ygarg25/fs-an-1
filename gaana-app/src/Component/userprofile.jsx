import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import Footer from "./footer";

let url = "http://localhost:2410";
// let url = "https://us-central1-yat12-77281.cloudfunctions.net/app";

class UserProfile extends Component {
  state = {};
  async componentWillMount() {
    const jwt = localStorage.getItem("token");
    try {
      const { data: response } = await axios.get(url + "/user", {
        headers: {
          Authorization: jwt,
        },
      });
      this.setState({ user: response });
    } catch (ex) {
      console.log("catch");
      return null;
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    let currProps = this.props;
    const jwt = localStorage.getItem("token");
    if (prevProps !== currProps) {
      try {
        const { data: response } = await axios.get(url + "/user", {
          headers: {
            Authorization: jwt,
          },
        });
        this.setState({ user: response });
      } catch (ex) {
        console.log("catch");
        return null;
      }
    }
  }

  render() {
    if (!localStorage.getItem("token")) return <Redirect to="/login" />;
    const { user } = this.state;
    return (
      <div style={{ backgroundColor: "lightgrey" }}>
        <div className=" row">
          <div className="col-lg-9 col-12" style={{ background: "#EBECED" }}>
            <div className="row pl-5 pt-5 pr-5">
              <div className="col-lg-2 d-none d-lg-block"></div>
              <div className="col-lg-8 col-12 ">
                <div className="row text-center">
                  <div className="col-12">
                    <div style={{ color: "white", fontSize: "130px" }}>
                      <i className="fas fa-user-circle" />
                    </div>
                    <div>
                      {this.state.user && (
                        <React.Fragment>
                          <h3>
                            {user.Fname}&nbsp;{user.Lname}
                          </h3>
                          <span
                            className="text-muted"
                            style={{ fontSize: "13px" }}
                          >
                            {user.Email}
                          </span>
                        </React.Fragment>
                      )}
                    </div>
                  </div>
                  <div className="col-12 mt-2">
                    <br />
                    <div className="row">
                      <div className="col-lg-6 col-12 text-left">
                        <span
                          className=""
                          style={{
                            fontWeight: "500",
                            cursor: "pointer",
                            color: "#777",
                            borderBottom: "3px solid red",
                          }}
                        >
                          SUBSCRIPTION
                        </span>
                      </div>
                      <div className="col-lg-6 col-12 text-lg-right text-left mt-lg-0 mt-3 text-danger">
                        <span
                          style={{
                            fontWeight: "500",
                            cursor: "pointer",
                          }}
                        >
                          VIEW TRANSACTION HISTORY
                        </span>
                      </div>
                    </div>
                  </div>

                  <br />
                  <div
                    className="col-12 bg-white mt-4 p-5 text-left"
                    style={{ borderRadius: "5px" }}
                  >
                    <div>
                      <div>
                        <span
                          className="text-white p-1"
                          style={{
                            backgroundImage:
                              "linear-gradient(to left,#38ef7d -25%,#11998e 166%)",
                            fontWeight: "500",
                          }}
                        >
                          Current Plan
                        </span>
                      </div>
                      <br />
                      <div>
                        <p className="h6" style={{ fontWeight: "700" }}>
                          Ad Supported Free Streaming
                        </p>
                        <p
                          className="text-justify text-muted"
                          style={{
                            fontSize: "14px",
                            fontWeight: "450",
                            fontFamily: "Rubik-Regular,Arial",
                          }}
                        >
                          You Are Currently active on ad supported, low, medium
                          and high quality streaming services
                        </p>
                      </div>
                    </div>
                    <div>
                      <hr />
                    </div>
                    <div className="row pt-4 text-center">
                      <div className="col-12 text-center">
                        <h5>Get more with Ganna Plus</h5>
                      </div>
                      <div
                        className="col-lg-3 m-2 ml-lg-5 col-12 bg-dark p-2 text-white text-center"
                        style={{ borderRadius: "5px" }}
                      >
                        <span style={{ fontSize: "11px" }}>
                          <b>PREMIUM</b>
                        </span>
                        <br />
                        <span
                          className="text-success"
                          style={{ fontSize: "50px" }}
                        >
                          <i className="fas fa-arrow-circle-down" />
                        </span>
                        <br />
                        <span style={{ fontSize: "12px" }}>
                          <b>Unlimited Downloads</b>
                        </span>
                      </div>
                      <div
                        className="col-lg-3 m-2  col-12 bg-dark p-2 text-white text-center"
                        style={{ borderRadius: "5px" }}
                      >
                        <span style={{ fontSize: "11px" }}>
                          <b>PREMIUM</b>
                        </span>
                        <br />
                        <span
                          className="text-success"
                          style={{ fontSize: "50px" }}
                        >
                          <i className="fas fa-ad" />
                        </span>
                        <br />
                        <span style={{ fontSize: "12px" }}>
                          <b>Get Rid of Ads</b>
                        </span>
                      </div>
                      <div
                        className="col-lg-3 m-2 col-12 bg-dark p-2 text-white text-center"
                        style={{ borderRadius: "5px" }}
                      >
                        <span style={{ fontSize: "11px" }}>
                          <b>PREMIUM</b>
                        </span>
                        <br />
                        <span
                          className="text-success"
                          style={{ fontSize: "50px" }}
                        >
                          <i className="fas fa-arrow-circle-down" />
                        </span>
                        <br />
                        <span style={{ fontSize: "12px" }}>
                          <b>Listen in HD Quality</b>
                        </span>
                      </div>
                      <div className="col-12 pt-3">
                        <button
                          className="btn btn-danger btn-sm col-6"
                          style={{ borderRadius: "50px" }}
                        >
                          UPGRADE NOW
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <Footer />
              </div>
              <div className="col-lg-2 d-none d-lg-block"></div>
            </div>
          </div>
          <div className="col-lg-3 d-none d-lg-block"></div>
        </div>
      </div>
    );
  }
}

export default UserProfile;
